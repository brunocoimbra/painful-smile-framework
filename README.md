# Painful Smile Framework

This framework contains several modular tools to be used with [Unity](https://unity.com/) (2018.3 or newer).

There are many ways to setup the framework into your project:

* **Git package**: Add `"br.com.painfulsmile": "https://gitlab.com/painful-smile/painful-smile-framework.git#{tag}"` as a dependency in the Packages manifest.
* **Local folder**: Clone it to the project root and add `"br.com.painfulsmile": "file:../{folder}"` as a dependency in the Packages manifest.
* **Packages folder**: Clone it inside the Packages folder.
* **Assets folder**: Clone it inside the Assets folder.

To check what changed in each version, check the [changelog](CHANGELOG.md).

* [Attributes](Documentation/ATTRIBUTES.md)
  * [`AnimatorParameter`](Documentation/ATTRIBUTES.md#animatorparameter)
  * [`Button`](Documentation/ATTRIBUTES.md#button)
  * [`Callback`](Documentation/ATTRIBUTES.md#callback)
  * [`EnumFlags`](Documentation/ATTRIBUTES.md#enumflags)
  * [`InputSelector`](Documentation/ATTRIBUTES.md#inputselector)
  * [`LayerSelector`](Documentation/ATTRIBUTES.md#layerselector)
  * [`Negative`](Documentation/ATTRIBUTES.md#negative)
  * [`NotGreaterThan`](Documentation/ATTRIBUTES.md#notgreaterthan)
  * [`NotLessThan`](Documentation/ATTRIBUTES.md#notlessthan)
  * [`ObjectPicker`](Documentation/ATTRIBUTES.md#objectpicker)
  * [`Positive`](Documentation/ATTRIBUTES.md#positive)
  * [`RangeSlider`](Documentation/ATTRIBUTES.md#rangeslider)
  * [`ReadOnly`](Documentation/ATTRIBUTES.md#readonly)
  * [`ReadWrite`](Documentation/ATTRIBUTES.md#readwrite)
  * [`Reorderable`](Documentation/ATTRIBUTES.md#reorderable)
  * [`ScenePath`](Documentation/ATTRIBUTES.md#scenepath)
  * [`SceneSelector`](Documentation/ATTRIBUTES.md#sceneselector)
  * [`SortingLayerID`](Documentation/ATTRIBUTES.md#sortinglayerid)
  * [`TagSelector`](Documentation/ATTRIBUTES.md#tagselector)
  * [`UnityEvent`](Documentation/ATTRIBUTES.md#unityevent)
* [Class Pool](Documentation/CLASSPOOL.md)
  * [`ClassPool<T>`](Documentation/CLASSPOOL.md#classpoolt)
  * [`ClassPoolBase<T>`](Documentation/CLASSPOOL.md#classpoolbaset)
  * [`DictionaryPool<TKey, TValue>`](Documentation/CLASSPOOL.md#dictionarypooltkey-tvalue)
  * [`HashSetPool<T>`](Documentation/CLASSPOOL.md#hashsetpoolt)
  * [`ListPool<T>`](Documentation/CLASSPOOL.md#listpoolt)
  * [`QueuePool<T>`](Documentation/CLASSPOOL.md#queuepoolt)
  * [`StackPool<T>`](Documentation/CLASSPOOL.md#stackpoolt)
* [GUI Scopes](Documentation/GUISCOPES.md)
  * [`BackgroundColorScope`](Documentation/GUISCOPES.md#backgroundcolorscope)
  * [`HierarchyModeScope`](Documentation/GUISCOPES.md#hierarchymodescope)
  * [`LabelWidthScope`](Documentation/GUISCOPES.md#labelwidthscope)
  * [`ShowMixedValueScope`](Documentation/GUISCOPES.md#showmixedvaluescope)
* [MainThread Dispatcher](Documentation/MAINTHREADDISPATCHER.md)
  * [`MainThreadDispatcher`](Documentation/MAINTHREADDISPATCHER.md#mainthreaddispatcher)
  * [`MainThreadCommand`](Documentation/MAINTHREADDISPATCHER.md#mainthreadcommand)
  * [`MainThreadCommand<TResult>`](Documentation/MAINTHREADDISPATCHER.md#mainthreadcommandtresult)
  * [`MainThreadAction`](Documentation/MAINTHREADDISPATCHER.md#mainthreadaction)
  * [`MainThreadFunc<TResult>`](Documentation/MAINTHREADDISPATCHER.md#mainthreadfunctresult)
  * [`WaitForMainThreadCommand`](Documentation/MAINTHREADDISPATCHER.md#waitformainthreadcommand)
* [Physics Listeners](Documentation/PHYSICSLISTENERS.md)
  * [`Collision2DListener`](Documentation/PHYSICSLISTENERS.md#collision2dlistener)
  * [`CollisionListener`](Documentation/PHYSICSLISTENERS.md#collisionlistener)
  * [`Trigger2DListener`](Documentation/PHYSICSLISTENERS.md#collision2dlistener)
  * [`TriggerListener`](Documentation/PHYSICSLISTENERS.md#collisionlistener)
* [Pool System](Documentation/POOLSYSTEM.md)
  * [`Pool`](Documentation/POOLSYSTEM.md#pool)
  * [`PoolData`](Documentation/POOLSYSTEM.md#pooldata)
  * [`PoolDataAsset`](Documentation/POOLSYSTEM.md#pooldataasset)
  * [`PoolDataCollection`](Documentation/POOLSYSTEM.md#pooldatacollection)
  * [`PoolSystem`](Documentation/POOLSYSTEM.md#poolsystem)
* [Ranges](Documentation/RANGES.md)
  * [`FloatRange`](Documentation/RANGES.md#floatrange)
  * [`IntRange`](Documentation/RANGES.md#intrange)
* [Singletons](Documentation/SINGLETONS.md)
  * [`Singleton<T>`](Documentation/SINGLETONS.md#singletont)
  * [`DynamicSingleton<T>`](Documentation/SINGLETONS.md#dynamicsingletont)
  * [`SceneSingleton<T>`](Documentation/SINGLETONS.md#scenesingletont)
  * [`DynamicSceneSingleton<T>`](Documentation/SINGLETONS.md#dynamicscenesingletont)
  * [`ScriptableSingleton<T>`](Documentation/SINGLETONS.md#scriptablesingletont)
  * [`EditorScriptableSingleton<T>`](Documentation/SINGLETONS.md#editorscriptablesingletont)
* [Threading](Documentation/THREADING.md)
  * [`ThreadCommand`](Documentation/THREADING.md#threadcommand)
  * [`ThreadCommand<TResult>`](Documentation/THREADING.md#threadcommandtresult)
  * [`ThreadAction`](Documentation/THREADING.md#threadaction)
  * [`ThreadAction<T>`](Documentation/THREADING.md#threadactiont)
  * [`ThreadFunc<TResult>`](Documentation/THREADING.md#threadfunctresult)
  * [`ThreadFunc<T, TResult>`](Documentation/THREADING.md#threadfuncttresult)
  * [`ThreadUtility`](Documentation/THREADING.md#threadutility)
  * [`WaitForThreadCommand`](Documentation/THREADING.md#waitforthreadcommand)
  * [`WaitForTask`](Documentation/THREADING.md#waitfortask)
* [Utilities](Documentation/UTILITIES.md)
  * [`CacheCleaner`](Documentation/UTILITIES.md#cachecleaner)
  * [`ScriptableObjectWizard`](Documentation/UTILITIES.md#scriptableobjectwizard)
  * [`Ease`](Documentation/UTILITIES.md#ease)
  * [`Math`](Documentation/UTILITIES.md#math)
  * [`PSUtility`](Documentation/UTILITIES.md#psutility)
  * [`PSEditorUtility`](Documentation/UTILITIES.md#pseditorutility)
  * [`PSEditorGUIUtility`](Documentation/UTILITIES.md#pseditorguiutility)
  * [`Extensions`](Documentation/UTILITIES.md#extensions)
* [`ConstantsGenerator`](Documentation/CONSTANTSGENERATOR.md)
* [`FakeDisposable<T>`](Documentation/FAKEDISPOSABLE.md)
* [`InterfaceReference<T>`](Documentation/INTERFACEREFERENCE.md)
* [`ParameterSetter`](Documentation/PARAMETERSETTER.md)
* [`PropertyPathInfo`](Documentation/PROPERTYPATHINFO.md)
* [`Reference<T>`](Documentation/REFERENCE.md)
* [`ReorderableList`](Documentation/REORDERABLELIST.md)
* [`SerializableList<T>`](Documentation/SERIALIZABLELIST.md)

## Third Party Notices

This package also incorporates work from the following projects:

* [UtilityKit](https://github.com/prime31/UtilityKit/blob/master/README_CONSTANTS_GENERATOR_KIT.md)
* [Unity-Reorderable-List](https://github.com/cfoulston/Unity-Reorderable-List/blob/master/LICENSE)
* [UnityToolbag](https://github.com/nickgravelyn/UnityToolbag/blob/master/LICENSE)

If you think that I've missed any additional project on the list above, please let me know and I will include it as soon as possible.

## License

[MIT](LICENSE.md)
