# Changelog

## [1.7.3] 2019-MM-DD

- Fixed SingletonBase.SetInstance and SceneSingletonBase not calling Awake 
- Fixed edge-case error on ConstantsGenerator when there is no scene in the build settings
- Updated documentation for PoolSystem and MainThreadDispatcher regarding the auto-initialization

## [1.7.2] 2019-07-26

- Changed Initialize method from PoolSystem and MainThreadDispatcher to be public
- Changed ModulesInitializer to save it's info in Library folder
- Removed temporary fix for PhysicsListener in Unity 2019.3 and newer

## [1.7.1] 2019-06-20

- Fixed README to match the new minimum supported version

## [1.7.0] 2019-06-20

- Back-ported to Unity 2018.3
- Changed Preview to Experimental to match Unity standards

## [1.6.0] 2019-06-08

- Added All and None in PhysicsCallback enum values
- Added OnePerCallback option to ButtonAttribute
- Changed the output of the ConstantsGenerator to be more intuitive
- Finished documentation
- Fixed naming conflict in PSUtility with AssetBundle module
- Reorganized folders and made some cleanup
- Temporarily fixed not being able to add a PhysicsListener through the AddComponent menu due an Unity internal bug

## [1.5.0-preview] 2019-05-28

- Added first documentation version
- Added Compress/Decompress methods in PSUtility
- Added ContainsAll and ContainsAny to LayerMaskExtensions
- Added ForEachReverse to TransformExtensions
- Added IsAnySet to IntExtensions
- Added Preview folder for unfinished and undocumented stuff
- Fixed boxing issue on generated constants
- Moved DialogSystem to newly created Preview folder
- Updated ClassPoolBase to allow to define a preload count and a max capacity
- Updated ClassPoolBase to be thread safe
- Updated Contains to IsSet in IntExtensions
- Removed CoreSystem
- Removed FindChild/ChildrenWithTag from TransformExtensions (use ForEach instead)
- Updated minimum Unity version to 2019.1
- Updated project-related settings to use new Project Settings menu
- Updated README structure

## [1.4.1] 2019-05-24

- Fixed a NullReferenceException in the ConstantsGenerator
- Fixed CoreManager not drawing children properties

## [1.4.0] 2019-05-20

- Added CoreManager and CoreSystem
- Improved InterfaceReference logic
- Improved ReorderableList customization

## [1.3.1] 2019-05-19

- Added FakeDisposable
- Added GetDisposable for collection pools
- Added InterfaceReference
- Changed collection pools to clear on release
- Removed type selector from ObjectPickerAttribute

## [1.3.0] 2019-05-07

- Changed structure to work without any built-in module

## [1.2.1] 2019-04-25

- Fixed PoolDataCollectionEditor and PoolSystemEditor not loading up properly

## [1.2.0] 2019-04-25

- First stable version
