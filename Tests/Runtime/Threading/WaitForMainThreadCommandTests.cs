using NUnit.Framework;
using System.Collections;
using System.Threading;
using UnityEngine;
using UnityEngine.TestTools;

namespace PainfulSmile.Tests
{
    public class WaitForMainThreadCommandTests
    {
        [UnityTest]
        public IEnumerator WaitForMainThreadAction()
        {
            const string name = "MainThreadAction";
            GameObject target = null;
            var command = new MainThreadAction(() => target = new GameObject(name));

            new ThreadAction(() =>
            {
                Thread.Sleep(100);
                command.Schedule();
            }).Enqueue();

            while (command.IsReady)
            {
                yield return null;
            }

            yield return new WaitForMainThreadCommand(command);

            Assert.IsTrue(target.name == name);
        }

        [UnityTest]
        public IEnumerator WaitForMainThreadFunc()
        {
            const string name = "MainThreadFunc";
            var command = new MainThreadFunc<string>(() => new GameObject(name).name);

            new ThreadAction(() =>
            {
                Thread.Sleep(100);
                command.Schedule();
            }).Enqueue();

            while (command.IsReady)
            {
                yield return null;
            }

            yield return new WaitForMainThreadCommand(command);

            Assert.IsTrue(command.Result == name);
        }
    }
}
