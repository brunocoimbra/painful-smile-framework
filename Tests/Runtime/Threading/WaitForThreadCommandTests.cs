using NUnit.Framework;
using System.Collections;
using System.Threading;
using UnityEngine.TestTools;

namespace PainfulSmile.Tests
{
    public class WaitForThreadCommandTests
    {
        [UnityTest]
        public IEnumerator WaitForThreadAction_0()
        {
            bool completed = false;

            var command = new ThreadAction(() =>
            {
                Thread.Sleep(500);
                completed = true;
            });

            command.Enqueue();

            yield return new WaitForThreadCommand(command);

            Assert.IsTrue(completed);
        }

        [UnityTest]
        public IEnumerator WaitForThreadAction_1()
        {
            bool completed = false;

            var command = new ThreadAction<bool>((param) =>
            {
                Thread.Sleep(500);
                completed = param;
            });

            command.Enqueue(true);

            yield return new WaitForThreadCommand(command);

            Assert.IsTrue(completed);
        }

        [UnityTest]
        public IEnumerator WaitForThreadFunc_1()
        {
            var command = new ThreadFunc<bool>(() =>
            {
                Thread.Sleep(500);

                return true;
            });

            command.Enqueue();

            yield return new WaitForThreadCommand(command);

            Assert.IsTrue(command.Result);
        }

        [UnityTest]
        public IEnumerator WaitForThreadFunc_2()
        {
            var command = new ThreadFunc<bool, bool>((param) =>
            {
                Thread.Sleep(500);

                return param;
            });

            command.Enqueue(true);

            yield return new WaitForThreadCommand(command);

            Assert.IsTrue(command.Result);
        }
    }
}
