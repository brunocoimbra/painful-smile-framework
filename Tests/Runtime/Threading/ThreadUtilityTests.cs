using System.Collections;
using System.Threading;
using NUnit.Framework;
using UnityEngine.TestTools;

namespace PainfulSmile.Tests
{
    public class ThreadUtilityTests
    {
        [Test]
        public void IsUnityThread()
        {
            Assert.IsTrue(ThreadUtility.IsUnityThread);
        }

        [UnityTest]
        public IEnumerator IsUnityThreadOnMainThreadAction()
        {
            var command = new ThreadFunc<bool>(() =>
            {
                var nestedCommand = new MainThreadFunc<bool>(() => ThreadUtility.IsUnityThread);
                nestedCommand.Schedule();
                
                while (nestedCommand.IsReady == false)
                {
                    Thread.Sleep(100);
                }
                
                return nestedCommand.Result;
            });
            command.Enqueue();

            yield return new WaitForThreadCommand(command);

            Assert.IsTrue(command.Result);
        }

        [UnityTest]
        public IEnumerator IsNotUnityThreadOnThreadAction()
        {
            var command = new ThreadFunc<bool>(() => !ThreadUtility.IsUnityThread);
            command.Enqueue();

            yield return new WaitForThreadCommand(command);

            Assert.IsTrue(command.Result);
        }
    }
}
