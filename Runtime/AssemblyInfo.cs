﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("PainfulSmileTests")]

#if UNITY_EDITOR
[assembly: InternalsVisibleTo("PainfulSmileEditor")]
#endif
