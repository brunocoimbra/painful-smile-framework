﻿#if UNITY_ANIMATION
using UnityEngine;

namespace PainfulSmile
{
    public sealed class ParameterSetter : StateMachineBehaviour
    {
        [System.Flags]
        public enum Callbacks
        {
            StateEnter = 1 << 0,
            StateExit = 1 << 1,
            StateMachineEnter = 1 << 2,
            StateMachineExit = 1 << 3
        }

        [System.Serializable]
        public struct AnimatorControllerParameter
        {
            public AnimatorControllerParameterType Type;
            public string Name;
            public bool BoolValue;
            public int IntValue;
            public float FloatValue;
        }

        [Tooltip("Callbacks being used.")]
        [SerializeField, EnumFlags] private Callbacks _callbacks = Callbacks.StateEnter | Callbacks.StateExit;
        [Tooltip("Parameters to be set when entering the state.")]
        [SerializeField] private AnimatorControllerParameter[] _parametersOnStateEnter = new AnimatorControllerParameter[0];
        [Tooltip("Parameters to be set when exiting the state.")]
        [SerializeField] private AnimatorControllerParameter[] _parametersOnStateExit = new AnimatorControllerParameter[0];
        [Tooltip("Parameters to be set when entering the state machine.")]
        [SerializeField] private AnimatorControllerParameter[] _parametersOnStateMachineEnter = new AnimatorControllerParameter[0];
        [Tooltip("Parameters to be set when exiting the state machine.")]
        [SerializeField] private AnimatorControllerParameter[] _parametersOnStateMachineExit = new AnimatorControllerParameter[0];

        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            OnStateEnter(animator);
        }

        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            OnStateExit(animator);
        }

        public override void OnStateMachineEnter(Animator animator, int stateMachinePathHash)
        {
            OnStateMachineEnter(animator);
        }

        public override void OnStateMachineExit(Animator animator, int stateMachinePathHash)
        {
            OnStateMachineExit(animator);
        }

        private static void SetParameters(Animator animator, AnimatorControllerParameter[] parameters)
        {
            for (var i = 0; i < parameters.Length; i++)
            {
                switch (parameters[i].Type)
                {
                    case AnimatorControllerParameterType.Float:
                    {
                        animator.SetFloat(parameters[i].Name, parameters[i].FloatValue);

                        break;
                    }

                    case AnimatorControllerParameterType.Int:
                    {
                        animator.SetInteger(parameters[i].Name, parameters[i].IntValue);

                        break;
                    }

                    case AnimatorControllerParameterType.Bool:
                    {
                        animator.SetBool(parameters[i].Name, parameters[i].BoolValue);

                        break;
                    }

                    case AnimatorControllerParameterType.Trigger:
                    {
                        if (parameters[i].BoolValue)
                        {
                            animator.SetTrigger(parameters[i].Name);
                        }
                        else
                        {
                            animator.ResetTrigger(parameters[i].Name);
                        }

                        break;
                    }
                }
            }
        }

        private void OnStateEnter(Animator animator)
        {
            if ((_callbacks & Callbacks.StateEnter) != 0)
            {
                SetParameters(animator, _parametersOnStateEnter);
            }
        }

        private void OnStateExit(Animator animator)
        {
            if ((_callbacks & Callbacks.StateExit) != 0)
            {
                SetParameters(animator, _parametersOnStateExit);
            }
        }

        private void OnStateMachineEnter(Animator animator)
        {
            if ((_callbacks & Callbacks.StateMachineEnter) != 0)
            {
                SetParameters(animator, _parametersOnStateMachineEnter);
            }
        }

        private void OnStateMachineExit(Animator animator)
        {
            if ((_callbacks & Callbacks.StateMachineExit) != 0)
            {
                SetParameters(animator, _parametersOnStateMachineExit);
            }
        }
    }
}
#endif
