﻿using UnityEngine;
using UnityEngine.Events;

namespace PainfulSmile
{
    public abstract class PhysicsListener<TParam, TEvent, TEnterCallback, TExitCallback, TStayCallback> : PhysicsListenerBase
        where TEvent : UnityEvent<GameObject, TParam>, new()
        where TEnterCallback : PhysicsCallback<TParam>
        where TExitCallback : PhysicsCallback<TParam>
        where TStayCallback : PhysicsCallback<TParam>
    {
        [SerializeField, EnumFlags] private PhysicsCallbacks m_Callbacks;
        [SerializeField, UnityEvent] private TEvent m_OnEnter = new TEvent();
        [SerializeField, UnityEvent] private TEvent m_OnExit = new TEvent();
        [SerializeField, UnityEvent] private TEvent m_OnStay = new TEvent();

        private TEnterCallback _enterCallback;
        private TExitCallback _exitCallback;
        private TStayCallback _stayCallback;

        public TEvent OnEnter => m_OnEnter;
        public TEvent OnExit => m_OnExit;
        public TEvent OnStay => m_OnStay;

        public PhysicsCallbacks Callbacks
        {
            get => m_Callbacks;
            protected set => m_Callbacks = value;
        }

        private void Awake()
        {
            TryAddCallback(PhysicsCallbacks.Enter, ref _enterCallback, OnEnter);
            TryAddCallback(PhysicsCallbacks.Exit, ref _exitCallback, OnExit);
            TryAddCallback(PhysicsCallbacks.Stay, ref _stayCallback, OnStay);
        }

        private void OnDestroy()
        {
            DestroyCallback(_enterCallback);
            DestroyCallback(_exitCallback);
            DestroyCallback(_stayCallback);
        }

        private void Reset()
        {
            Callbacks = PhysicsCallbacks.Enter | PhysicsCallbacks.Exit;
        }

        public sealed override void AddCallbacks(PhysicsCallbacks value)
        {
            Callbacks |= value;
            TryAddCallback(PhysicsCallbacks.Enter, ref _enterCallback, OnEnter);
            TryAddCallback(PhysicsCallbacks.Exit, ref _exitCallback, OnExit);
            TryAddCallback(PhysicsCallbacks.Stay, ref _stayCallback, OnStay);
        }

        public sealed override void RemoveCallbacks(PhysicsCallbacks value, bool destroyCallbacks = false)
        {
            Callbacks &= ~value;
            TryRemoveCallback(PhysicsCallbacks.Enter, ref _enterCallback, destroyCallbacks);
            TryRemoveCallback(PhysicsCallbacks.Exit, ref _exitCallback, destroyCallbacks);
            TryRemoveCallback(PhysicsCallbacks.Stay, ref _stayCallback, destroyCallbacks);
        }

        private void TryAddCallback<T>(PhysicsCallbacks flag, ref T callback, TEvent e) where T : PhysicsCallback<TParam>
        {
            if ((Callbacks & flag) == flag)
            {
                if (callback == null)
                {
                    callback = gameObject.GetComponent<T>();

                    if (callback == null)
                    {
                        callback = gameObject.AddComponent<T>();
                    }
                }

                callback.Event = e;
            }
        }

        private void DestroyCallback(PhysicsCallbackBase callback)
        {
            if (callback != null)
            {
                Destroy(callback);
            }
        }

        private void TryRemoveCallback<T>(PhysicsCallbacks flag, ref T callback, bool destroy) where T : PhysicsCallback<TParam>
        {
            if ((Callbacks & flag) == 0 && callback != null)
            {
                if (destroy)
                {
                    Destroy(callback);
                    callback = null;
                }
                else
                {
                    callback.Event = null;
                }
            }
        }
    }
}
