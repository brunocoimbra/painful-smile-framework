﻿namespace PainfulSmile
{
    [System.Flags]
    public enum PhysicsCallbacks
    {
        None = 0,
        Enter = 1 << 0,
        Exit = 1 << 1,
        Stay = 1 << 2,
        All = ~0
    }
}
