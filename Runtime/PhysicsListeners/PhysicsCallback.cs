﻿using UnityEngine;
using UnityEngine.Events;

namespace PainfulSmile
{
    public abstract class PhysicsCallback<T> : PhysicsCallbackBase
    {
        internal UnityEvent<GameObject, T> Event { get; set; }

        protected void Invoke(T parameter)
        {
            Event?.Invoke(gameObject, parameter);
        }
    }
}
