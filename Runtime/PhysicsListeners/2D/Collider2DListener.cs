﻿#if UNITY_PHYSICS2D
using UnityEngine;
using UnityEngine.Events;

namespace PainfulSmile
{
    [RequireComponent(typeof(Collider2D))]
    public abstract class Collider2DListener<TParam, TEvent, TEnterCallback, TExitCallback, TStayCallback> : PhysicsListener<TParam, TEvent, TEnterCallback, TExitCallback, TStayCallback>
        where TEvent : UnityEvent<GameObject, TParam>, new()
        where TEnterCallback : PhysicsCallback<TParam>
        where TExitCallback : PhysicsCallback<TParam>
        where TStayCallback : PhysicsCallback<TParam>
    {
        protected abstract bool IsTrigger { get; }

        protected sealed override void DrawGizmos()
        {
            Matrix4x4 matrix = Gizmos.matrix;
            DrawBoxColliders();
            DrawCircleColliders();
            DrawCapsuleColliders();
            DrawEdgeColliders();
            DrawPolygonColliders();
            Gizmos.matrix = matrix;
        }

        private void DrawBoxColliders()
        {
            BoxCollider2D[] colliders = GetComponents<BoxCollider2D>();

            if (colliders == null)
            {
                return;
            }

            Gizmos.matrix = Matrix4x4.TRS(transform.position, transform.rotation, new Vector3(transform.lossyScale.x, transform.lossyScale.y));

            for (int i = 0; i < colliders.Length; i++)
            {
                if (colliders[i].isTrigger != IsTrigger)
                {
                    return;
                }

                Gizmos.DrawWireCube(colliders[i].offset, colliders[i].size);
            }
        }

        private void DrawCapsuleColliders()
        {
            CapsuleCollider2D[] colliders = GetComponents<CapsuleCollider2D>();

            if (colliders == null)
            {
                return;
            }

            for (int i = 0; i < colliders.Length; i++)
            {
                if (colliders[i].isTrigger != IsTrigger)
                {
                    return;
                }

                float height;
                float radius = 0;
                Vector2 displacement = Vector2.zero;
                Quaternion rotation = Quaternion.identity;
                Gizmos.matrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector2.one);

                switch (colliders[i].direction)
                {
                    case CapsuleDirection2D.Horizontal:
                    {
                        height = colliders[i].size.x * transform.lossyScale.x;
                        radius = colliders[i].size.y / 2 * transform.lossyScale.y;
                        displacement = Vector2.right * Mathf.Max(0, height / 2 - radius);
                        rotation = Quaternion.Euler(transform.eulerAngles.x, 0, 0);

                        Gizmos.DrawRay(Vector2.down * radius + colliders[i].offset - displacement, displacement * 2);
                        Gizmos.DrawRay(Vector2.up * radius + colliders[i].offset - displacement, displacement * 2);

                        break;
                    }

                    case CapsuleDirection2D.Vertical:
                    {
                        height = colliders[i].size.y * transform.lossyScale.y;
                        radius = colliders[i].size.x / 2 * transform.lossyScale.x;
                        displacement = Vector2.up * Mathf.Max(0, height / 2 - radius);
                        rotation = Quaternion.Euler(0, transform.eulerAngles.y, 0);

                        Gizmos.DrawRay(Vector2.left * radius + colliders[i].offset - displacement, displacement * 2);
                        Gizmos.DrawRay(Vector2.right * radius + colliders[i].offset - displacement, displacement * 2);

                        break;
                    }
                }

                Gizmos.matrix = Matrix4x4.TRS(Gizmos.matrix.MultiplyPoint3x4(colliders[i].offset + displacement), rotation, Vector2.one);
                Gizmos.DrawWireSphere(Vector3.zero, radius);

                Gizmos.matrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector2.one);
                Gizmos.matrix = Matrix4x4.TRS(Gizmos.matrix.MultiplyPoint3x4(colliders[i].offset - displacement), rotation, Vector2.one);
                Gizmos.DrawWireSphere(Vector3.zero, radius);
            }
        }

        private void DrawCircleColliders()
        {
            CircleCollider2D[] colliders = GetComponents<CircleCollider2D>();

            if (colliders == null)
            {
                return;
            }

            Gizmos.matrix = Matrix4x4.TRS(transform.position, Quaternion.identity, Vector2.one * Mathf.Max(transform.lossyScale.x, transform.lossyScale.y));

            for (int i = 0; i < colliders.Length; i++)
            {
                if (colliders[i].isTrigger != IsTrigger)
                {
                    return;
                }

                Gizmos.DrawWireSphere(colliders[i].offset, colliders[i].radius);
            }
        }

        private void DrawEdgeColliders()
        {
            EdgeCollider2D[] colliders = GetComponents<EdgeCollider2D>();

            if (colliders == null)
            {
                return;
            }

            Gizmos.matrix = Matrix4x4.TRS(transform.position, transform.rotation, new Vector3(transform.lossyScale.x, transform.lossyScale.y));

            for (int i = 0; i < colliders.Length; i++)
            {
                if (colliders[i].isTrigger != IsTrigger)
                {
                    return;
                }

                DrawPoints(colliders[i].points);
            }
        }

        private void DrawPoints(Vector2[] points)
        {
            for (int i = 0; i < (points.Length - 1); i++)
            {
                Gizmos.DrawLine(points[i], points[i + 1]);
            }

            Gizmos.DrawLine(points[points.Length - 1], points[0]);
        }

        private void DrawPolygonColliders()
        {
            PolygonCollider2D[] colliders = GetComponents<PolygonCollider2D>();

            if (colliders == null)
            {
                return;
            }

            Gizmos.matrix = Matrix4x4.TRS(transform.position, transform.rotation, new Vector3(transform.lossyScale.x, transform.lossyScale.y));

            for (int i = 0; i < colliders.Length; i++)
            {
                if (colliders[i].isTrigger != IsTrigger)
                {
                    return;
                }

                DrawPoints(colliders[i].points);
            }
        }
    }
}
#endif
