﻿#if UNITY_PHYSICS2D
using UnityEngine;

namespace PainfulSmile
{
    [DisallowMultipleComponent, AddComponentMenu(PSUtility.ComponentMenu + "Collision 2D Listener", 200)]
    public sealed class Collision2DListener : Collider2DListener<Collision2D, Collision2DEvent, CollisionEnter2DCallback, CollisionExit2DCallback, CollisionStay2DCallback>
    {
        protected override bool IsTrigger => false;
    }
}
#endif
