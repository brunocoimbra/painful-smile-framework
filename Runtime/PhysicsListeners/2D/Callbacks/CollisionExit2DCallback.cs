﻿#if UNITY_PHYSICS2D
using UnityEngine;

namespace PainfulSmile
{
    [AddComponentMenu("")]
    public sealed class CollisionExit2DCallback : PhysicsCallback<Collision2D>
    {
        private void OnCollisionExit2D(Collision2D collision)
        {
            Invoke(collision);
        }
    }
}
#endif
