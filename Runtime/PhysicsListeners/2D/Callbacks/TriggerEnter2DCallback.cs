﻿#if UNITY_PHYSICS2D
using UnityEngine;

namespace PainfulSmile
{
    [AddComponentMenu("")]
    public sealed class TriggerEnter2DCallback : PhysicsCallback<Collider2D>
    {
        private void OnTriggerEnter2D(Collider2D other)
        {
            Invoke(other);
        }
    }
}
#endif
