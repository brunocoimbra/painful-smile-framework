﻿#if UNITY_PHYSICS2D
using UnityEngine;

namespace PainfulSmile
{
    [AddComponentMenu("")]
    public sealed class TriggerExit2DCallback : PhysicsCallback<Collider2D>
    {
        private void OnTriggerExit2D(Collider2D other)
        {
            Invoke(other);
        }
    }
}
#endif
