﻿#if UNITY_PHYSICS2D
using UnityEngine;

namespace PainfulSmile
{
    [AddComponentMenu("")]
    public sealed class CollisionStay2DCallback : PhysicsCallback<Collision2D>
    {
        private void OnCollisionStay2D(Collision2D collision)
        {
            Invoke(collision);
        }
    }
}
#endif
