﻿#if UNITY_PHYSICS2D
using UnityEngine;

namespace PainfulSmile
{
    [AddComponentMenu("")]
    public sealed class CollisionEnter2DCallback : PhysicsCallback<Collision2D>
    {
        private void OnCollisionEnter2D(Collision2D collision)
        {
            Invoke(collision);
        }
    }
}
#endif
