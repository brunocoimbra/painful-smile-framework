﻿#if UNITY_PHYSICS2D
using UnityEngine;
using UnityEngine.Events;

namespace PainfulSmile
{
    [System.Serializable]
    public sealed class Trigger2DEvent : UnityEvent<GameObject, Collider2D> { }
}
#endif
