﻿#if UNITY_PHYSICS2D
using UnityEngine;
using UnityEngine.Events;

namespace PainfulSmile
{
    [System.Serializable]
    public sealed class Collision2DEvent : UnityEvent<GameObject, Collision2D> { }
}
#endif
