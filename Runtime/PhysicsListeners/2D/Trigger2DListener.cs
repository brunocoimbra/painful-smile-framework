﻿#if UNITY_PHYSICS2D
using UnityEngine;

namespace PainfulSmile
{
    [DisallowMultipleComponent, AddComponentMenu(PSUtility.ComponentMenu + "Trigger 2D Listener", 205)]
    public sealed class Trigger2DListener : Collider2DListener<Collider2D, Trigger2DEvent, TriggerEnter2DCallback, TriggerExit2DCallback, TriggerStay2DCallback>
    {
        protected override bool IsTrigger => true;
    }
}
#endif
