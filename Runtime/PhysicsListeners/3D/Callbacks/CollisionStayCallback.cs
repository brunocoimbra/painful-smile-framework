﻿#if UNITY_PHYSICS
using UnityEngine;

namespace PainfulSmile
{
    [AddComponentMenu("")]
    public sealed class CollisionStayCallback : PhysicsCallback<Collision>
    {
        private void OnCollisionStay(Collision collision)
        {
            Invoke(collision);
        }
    }
}
#endif
