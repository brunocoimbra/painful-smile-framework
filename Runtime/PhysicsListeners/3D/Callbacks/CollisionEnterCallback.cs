﻿#if UNITY_PHYSICS
using UnityEngine;

namespace PainfulSmile
{
    [AddComponentMenu("")]
    public sealed class CollisionEnterCallback : PhysicsCallback<Collision>
    {
        private void OnCollisionEnter(Collision collision)
        {
            Invoke(collision);
        }
    }
}
#endif
