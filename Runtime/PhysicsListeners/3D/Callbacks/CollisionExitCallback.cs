﻿#if UNITY_PHYSICS
using UnityEngine;

namespace PainfulSmile
{
    [AddComponentMenu("")]
    public sealed class CollisionExitCallback : PhysicsCallback<Collision>
    {
        private void OnCollisionExit(Collision collision)
        {
            Invoke(collision);
        }
    }
}
#endif
