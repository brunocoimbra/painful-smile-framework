﻿#if UNITY_PHYSICS
using UnityEngine;

namespace PainfulSmile
{
    [AddComponentMenu("")]
    public sealed class TriggerEnterCallback : PhysicsCallback<Collider>
    {
        private void OnTriggerEnter(Collider other)
        {
            Invoke(other);
        }
    }
}
#endif
