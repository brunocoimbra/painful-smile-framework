﻿#if UNITY_PHYSICS
using UnityEngine;

namespace PainfulSmile
{
    [AddComponentMenu("")]
    public sealed class TriggerExitCallback : PhysicsCallback<Collider>
    {
        private void OnTriggerExit(Collider other)
        {
            Invoke(other);
        }
    }
}
#endif
