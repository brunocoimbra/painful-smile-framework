﻿#if UNITY_PHYSICS
using UnityEngine;

namespace PainfulSmile
{
    [AddComponentMenu("")]
    public sealed class TriggerStayCallback : PhysicsCallback<Collider>
    {
        private void OnTriggerStay(Collider other)
        {
            Invoke(other);
        }
    }
}
#endif
