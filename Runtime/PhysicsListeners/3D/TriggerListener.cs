﻿#if UNITY_PHYSICS
using UnityEngine;

namespace PainfulSmile
{
    [DisallowMultipleComponent, AddComponentMenu(PSUtility.ComponentMenu + "Trigger Listener", 105)]
    public sealed class TriggerListener : ColliderListener<Collider, TriggerEvent, TriggerEnterCallback, TriggerExitCallback, TriggerStayCallback>
    {
        protected override bool IsTrigger => true;
    }
}
#endif
