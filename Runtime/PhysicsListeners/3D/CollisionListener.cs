﻿#if UNITY_PHYSICS
using UnityEngine;

namespace PainfulSmile
{
    [DisallowMultipleComponent, AddComponentMenu(PSUtility.ComponentMenu + "Collision Listener", 100)]
    public sealed class CollisionListener : ColliderListener<Collision, CollisionEvent, CollisionEnterCallback, CollisionExitCallback, CollisionStayCallback>
    {
        protected override bool IsTrigger => false;
    }
}
#endif
