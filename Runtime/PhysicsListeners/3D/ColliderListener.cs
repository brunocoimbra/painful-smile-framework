﻿#if UNITY_PHYSICS
using UnityEngine;
using UnityEngine.Events;

namespace PainfulSmile
{
    [RequireComponent(typeof(Collider))]
    public abstract class ColliderListener<TParam, TEvent, TEnterCallback, TExitCallback, TStayCallback> : PhysicsListener<TParam, TEvent, TEnterCallback, TExitCallback, TStayCallback>
        where TEvent : UnityEvent<GameObject, TParam>, new()
        where TEnterCallback : PhysicsCallback<TParam>
        where TExitCallback : PhysicsCallback<TParam>
        where TStayCallback : PhysicsCallback<TParam>
    {
        protected abstract bool IsTrigger { get; }

        protected sealed override void DrawGizmos()
        {
            Matrix4x4 matrix = Gizmos.matrix;
            DrawBoxColliders();
            DrawSphereColliders();
#if UNITY_VEHICLES
            DrawWheelColliders();
#endif
            DrawCapsuleColliders();
            DrawMeshColliders();
            Gizmos.matrix = matrix;

#if UNITY_TERRAIN && UNITY_TERRAINPHYSICS
            DrawTerrainColliders();
#endif
        }

        private void DrawBoxColliders()
        {
            BoxCollider[] colliders = GetComponents<BoxCollider>();

            if (colliders == null)
            {
                return;
            }

            Gizmos.matrix = transform.localToWorldMatrix;

            for (int i = 0; i < colliders.Length; i++)
            {
                if (colliders[i].isTrigger != IsTrigger)
                {
                    return;
                }

                Gizmos.DrawWireCube(colliders[i].center, colliders[i].size);
            }
        }

        private void DrawCapsuleColliders()
        {
            CapsuleCollider[] colliders = GetComponents<CapsuleCollider>();

            if (colliders == null)
            {
                return;
            }

            Gizmos.matrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one);

            for (int i = 0; i < colliders.Length; i++)
            {
                if (colliders[i].isTrigger != IsTrigger)
                {
                    return;
                }

                float height;
                float radius = 0;
                Vector3 displacement = Vector3.zero;

                switch (colliders[i].direction)
                {
                    case 0:
                    {
                        height = colliders[i].height * transform.lossyScale.x;
                        radius = colliders[i].radius * Mathf.Max(Mathf.Abs(transform.lossyScale.y), Mathf.Abs(transform.lossyScale.z));
                        displacement = Vector3.right * Mathf.Max(0, height / 2 - radius);

                        Gizmos.DrawRay(Vector3.down * radius + colliders[i].center - displacement, displacement * 2);
                        Gizmos.DrawRay(Vector3.up * radius + colliders[i].center - displacement, displacement * 2);
                        Gizmos.DrawRay(Vector3.back * radius + colliders[i].center - displacement, displacement * 2);
                        Gizmos.DrawRay(Vector3.forward * radius + colliders[i].center - displacement, displacement * 2);

                        break;
                    }

                    case 1:
                    {
                        height = colliders[i].height * transform.lossyScale.y;
                        radius = colliders[i].radius * Mathf.Max(Mathf.Abs(transform.lossyScale.x), Mathf.Abs(transform.lossyScale.z));
                        displacement = Vector3.up * Mathf.Max(0, height / 2 - radius);

                        Gizmos.DrawRay(Vector3.left * radius + colliders[i].center - displacement, displacement * 2);
                        Gizmos.DrawRay(Vector3.right * radius + colliders[i].center - displacement, displacement * 2);
                        Gizmos.DrawRay(Vector3.back * radius + colliders[i].center - displacement, displacement * 2);
                        Gizmos.DrawRay(Vector3.forward * radius + colliders[i].center - displacement, displacement * 2);

                        break;
                    }

                    case 2:
                    {
                        height = colliders[i].height * transform.lossyScale.z;
                        radius = colliders[i].radius * Mathf.Max(Mathf.Abs(transform.lossyScale.x), Mathf.Abs(transform.lossyScale.y));
                        displacement = Vector3.forward * Mathf.Max(0, height / 2 - radius);

                        Gizmos.DrawRay(Vector3.down * radius + colliders[i].center - displacement, displacement * 2);
                        Gizmos.DrawRay(Vector3.up * radius + colliders[i].center - displacement, displacement * 2);
                        Gizmos.DrawRay(Vector3.left * radius + colliders[i].center - displacement, displacement * 2);
                        Gizmos.DrawRay(Vector3.right * radius + colliders[i].center - displacement, displacement * 2);

                        break;
                    }
                }

                Gizmos.DrawWireSphere(colliders[i].center + displacement, radius);
                Gizmos.DrawWireSphere(colliders[i].center - displacement, radius);
            }
        }

        private void DrawMeshColliders()
        {
            MeshCollider[] colliders = GetComponents<MeshCollider>();

            if (colliders == null)
            {
                return;
            }

            for (int i = 0; i < colliders.Length; i++)
            {
                if (colliders[i].isTrigger != IsTrigger)
                {
                    return;
                }

                Gizmos.matrix = Matrix4x4.TRS(colliders[i].bounds.center, transform.rotation, transform.lossyScale);
                Gizmos.DrawWireCube(Vector3.zero, colliders[i].sharedMesh.bounds.size);
            }
        }

        private void DrawSphereColliders()
        {
            SphereCollider[] colliders = GetComponents<SphereCollider>();

            if (colliders == null)
            {
                return;
            }

            Gizmos.matrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one * Mathf.Max(transform.lossyScale.x, transform.lossyScale.y, transform.lossyScale.z));

            for (int i = 0; i < colliders.Length; i++)
            {
                if (colliders[i].isTrigger != IsTrigger)
                {
                    return;
                }

                Gizmos.DrawWireSphere(colliders[i].center, colliders[i].radius);
            }
        }

#if UNITY_TERRAIN && UNITY_TERRAINPHYSICS
        private void DrawTerrainColliders()
        {
            TerrainCollider[] colliders = GetComponents<TerrainCollider>();

            if (colliders == null)
            {
                return;
            }

            for (int i = 0; i < colliders.Length; i++)
            {
                if (colliders[i].isTrigger != IsTrigger)
                {
                    return;
                }

                Gizmos.DrawWireCube(colliders[i].bounds.center, colliders[i].bounds.size);
            }
        }
#endif

#if UNITY_VEHICLES
        private void DrawWheelColliders()
        {
            WheelCollider[] colliders = GetComponents<WheelCollider>();

            if (colliders == null)
            {
                return;
            }

            Gizmos.matrix = Matrix4x4.TRS(transform.position, transform.rotation, new Vector3(0, transform.lossyScale.y, transform.lossyScale.y));

            for (int i = 0; i < colliders.Length; i++)
            {
                if (colliders[i].isTrigger != IsTrigger)
                {
                    return;
                }

                Gizmos.DrawWireSphere(colliders[i].center + Vector3.down * Mathf.Lerp(colliders[i].suspensionDistance, 0, colliders[i].suspensionSpring.targetPosition), colliders[i].radius);
            }
        }
#endif
    }
}
#endif
