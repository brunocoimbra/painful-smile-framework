﻿#if UNITY_PHYSICS
using UnityEngine;
using UnityEngine.Events;

namespace PainfulSmile
{
    [System.Serializable]
    public sealed class TriggerEvent : UnityEvent<GameObject, Collider> { }
}
#endif
