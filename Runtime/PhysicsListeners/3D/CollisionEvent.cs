﻿#if UNITY_PHYSICS
using UnityEngine;
using UnityEngine.Events;

namespace PainfulSmile
{
    [System.Serializable]
    public sealed class CollisionEvent : UnityEvent<GameObject, Collision> { }
}
#endif
