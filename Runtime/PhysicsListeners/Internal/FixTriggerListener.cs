﻿#if UNITY_PHYSICS && !UNITY_2019_3_OR_NEWER
using PainfulSmile;
using UnityEngine;

[ExecuteAlways, AddComponentMenu(PSUtility.ComponentMenu + "Trigger Listener", 105)]
internal sealed class FixTriggerListener : MonoBehaviour
{
    private void Reset()
    {
        gameObject.AddComponent<TriggerListener>();
    }

    private void Update()
    {
        DestroyImmediate(this);
    }
}
#endif
