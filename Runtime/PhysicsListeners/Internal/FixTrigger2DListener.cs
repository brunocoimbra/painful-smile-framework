﻿#if UNITY_PHYSICS2D && !UNITY_2019_3_OR_NEWER
using PainfulSmile;
using UnityEngine;

[ExecuteAlways, AddComponentMenu(PSUtility.ComponentMenu + "Trigger 2D Listener", 205)]
internal sealed class FixTrigger2DListener : MonoBehaviour
{
    private void Reset()
    {
        gameObject.AddComponent<Trigger2DListener>();
    }

    private void Update()
    {
        DestroyImmediate(this);
    }
}
#endif
