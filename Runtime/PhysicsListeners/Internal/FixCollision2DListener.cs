﻿#if UNITY_PHYSICS2D && !UNITY_2019_3_OR_NEWER
using PainfulSmile;
using UnityEngine;

[ExecuteAlways, AddComponentMenu(PSUtility.ComponentMenu + "Collision 2D Listener", 200)]
internal sealed class FixCollision2DListener : MonoBehaviour
{
    private void Reset()
    {
        gameObject.AddComponent<Collision2DListener>();
    }

    private void Update()
    {
        DestroyImmediate(this);
    }
}
#endif
