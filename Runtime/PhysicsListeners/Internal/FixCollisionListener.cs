﻿#if UNITY_PHYSICS && !UNITY_2019_3_OR_NEWER
using PainfulSmile;
using UnityEngine;

[ExecuteAlways, AddComponentMenu(PSUtility.ComponentMenu + "Collision Listener", 100)]
internal sealed class FixCollisionListener : MonoBehaviour
{
    private void Reset()
    {
        gameObject.AddComponent<CollisionListener>();
    }

    private void Update()
    {
        DestroyImmediate(this);
    }
}
#endif
