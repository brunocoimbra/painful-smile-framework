﻿using UnityEngine;

namespace PainfulSmile
{
    public abstract class PhysicsListenerBase : MonoBehaviour
    {
        private enum GizmosMode
        {
            Never,
            Selected,
            Always
        }

#if UNITY_EDITOR
        [SerializeField] private GizmosMode _gizmosMode = GizmosMode.Selected;
        [SerializeField] private Color _gizmosColor = new Color(0, 1, 0, 0.5f);
#endif

        public abstract void AddCallbacks(PhysicsCallbacks value);

        public abstract void RemoveCallbacks(PhysicsCallbacks value, bool destroyCallbacks = false);

        protected abstract void DrawGizmos();

#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            if (_gizmosMode != GizmosMode.Always)
            {
                return;
            }

            Color color = Gizmos.color;
            Gizmos.color = _gizmosColor;
            {
                DrawGizmos();
            }

            Gizmos.color = color;
        }

        private void OnDrawGizmosSelected()
        {
            if (_gizmosMode != GizmosMode.Selected)
            {
                return;
            }

            Color color = Gizmos.color;
            Gizmos.color = _gizmosColor;
            {
                DrawGizmos();
            }

            Gizmos.color = color;
        }
#endif        
    }
}
