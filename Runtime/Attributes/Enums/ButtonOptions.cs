namespace PainfulSmile
{
    /// <summary>
    /// Use with <see cref="ButtonAttribute"/> to define its appearance.
    /// </summary>
    [System.Flags]
    public enum ButtonOptions
    {
        Default = Stretch,
        FillLabelsArea = 1 << 0,
        FillFieldsArea = 1 << 1,
        OnePerCallback = (1 << 2) | Stretch,
        Stretch = FillFieldsArea | FillFieldsArea,
    }
}
