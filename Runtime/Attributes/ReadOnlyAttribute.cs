﻿namespace PainfulSmile
{
    /// <summary>
    /// Makes a field to be read-only in the inspector.
    /// </summary>
    public sealed class ReadOnlyAttribute : PropertyAttributeBase
    {
        public ReadOnlyAttribute()
            : base(ReadWriteMode.Read, ReadWriteMode.Read) { }
    }
}
