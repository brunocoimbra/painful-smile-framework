﻿using UnityEngine;
using UnityEngine.Events;

namespace PainfulSmile
{
    /// <summary>
    /// Upgrades the <see cref="UnityEvent"/> to be reorderable and collapsible.
    /// </summary>
    public class UnityEventAttribute : PropertyAttribute
    {
        public readonly UnityEventPermissions EditModePermissions;
        public readonly UnityEventPermissions PlayModePermissions;

        public UnityEventAttribute(UnityEventPermissions permissions = UnityEventPermissions.Everything)
            : this(permissions, permissions) { }

        public UnityEventAttribute(UnityEventPermissions editModePermissions, UnityEventPermissions playModePermissions)
        {
            EditModePermissions = editModePermissions;
            PlayModePermissions = playModePermissions;
        }
    }
}
