﻿namespace PainfulSmile
{
    /// <summary>
    /// Transforms an int field into an sorting layer popup.
    /// </summary>
    public sealed class SortingLayerIDAttribute : PropertyAttributeBase
    {
        public SortingLayerIDAttribute(params string[] callbacks)
            : base(callbacks) { }

        public SortingLayerIDAttribute(ReadWriteMode editMode, ReadWriteMode playMode, params string[] callbacks)
            : base(editMode, playMode, callbacks) { }
    }
}
