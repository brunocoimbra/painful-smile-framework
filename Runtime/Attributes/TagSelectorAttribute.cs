﻿namespace PainfulSmile
{
    /// <summary>
    /// Transforms a string field into a tag popup.
    /// </summary>
    public sealed class TagSelectorAttribute : PropertyAttributeBase
    {
        public TagSelectorAttribute(params string[] callbacks)
            : base(callbacks) { }

        public TagSelectorAttribute(ReadWriteMode editMode, ReadWriteMode playMode, params string[] callbacks)
            : base(editMode, playMode, callbacks) { }
    }
}
