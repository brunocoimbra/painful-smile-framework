﻿namespace PainfulSmile
{
    internal sealed class InactivePoolException : SpawnPoolException
    {
        public InactivePoolException() : base("Pool is inactive!") { }
    }
}
