﻿namespace PainfulSmile
{
    internal enum PoolSearchMode
    {
        FirstMatch,
        AllMatches
    }
}
