using UnityEngine;

namespace PainfulSmile
{
    /// <summary>
    /// Library with various easing functions.
    /// </summary>
    public static class Ease
    {
        public static float Back(float time)
        {
            if (time < 0.5f)
            {
                time *= 2;

                return 0.5f * (time * time * time - time * Mathf.Sin(time * Mathf.PI));
            }

            time = 1 - (2 * time - 1);

            return 0.5f * (1 - (time * time * time - time * Mathf.Sin(time * Mathf.PI))) + 0.5f;
        }

        public static float BackIn(float time)
        {
            return time * time * time - time * Mathf.Sin(time * Mathf.PI);
        }

        public static float BackOut(float time)
        {
            time = 1 - time;

            return 1 - (time * time * time - time * Mathf.Sin(time * Mathf.PI));
        }

        public static float Bounce(float time)
        {
            if (time < 0.5f)
            {
                return 0.5f * BounceIn(time * 2);
            }

            return 0.5f * BounceOut(time * 2 - 1) + 0.5f;
        }

        public static float BounceIn(float time)
        {
            return 1 - BounceOut(1 - time);
        }

        public static float BounceOut(float time)
        {
            if (time < (4 / 11f))
            {
                return 121 * time * time / 16f;
            }

            if (time < (8 / 11f))
            {
                return 363 / 40f * time * time - 99 / 10f * time + 17 / 5f;
            }

            if (time < (9 / 10f))
            {
                return 4356 / 361f * time * time - 35442 / 1805f * time + 16061 / 1805f;
            }

            return 54 / 5f * time * time - 513 / 25f * time + 268 / 25f;
        }

        public static float Circular(float time)
        {
            if (time < 0.5f)
            {
                return 0.5f * (1 - Mathf.Sqrt(1 - 4 * (time * time)));
            }

            return 0.5f * (Mathf.Sqrt(-(2 * time - 3) * (2 * time - 1)) + 1);
        }

        public static float CircularIn(float time)
        {
            return 1 - Mathf.Sqrt(1 - time * time);
        }

        public static float CircularOut(float time)
        {
            return Mathf.Sqrt((2 - time) * time);
        }

        public static float Cubic(float time)
        {
            if (time < 0.5f)
            {
                return 4 * time * time * time;
            }

            time = 2 * time - 2;

            return 0.5f * time * time * time + 1;
        }

        public static float CubicIn(float time)
        {
            return time * time * time;
        }

        public static float CubicOut(float time)
        {
            time--;

            return time * time * time + 1;
        }

        public static float Elastic(float time)
        {
            if (time < 0.5f)
            {
                return 0.5f * Mathf.Sin(13 * (Mathf.PI / 2) * (2 * time)) * Mathf.Pow(2, 10 * (2 * time - 1));
            }

            return 0.5f * (Mathf.Sin(-13 * (Mathf.PI / 2) * (2 * time - 1 + 1)) * Mathf.Pow(2, -10 * (2 * time - 1)) + 2);
        }

        public static float ElasticIn(float time)
        {
            return Mathf.Sin(13 * (Mathf.PI / 2) * time) * Mathf.Pow(2, 10 * (time - 1));
        }

        public static float ElasticOut(float time)
        {
            return Mathf.Sin(-13 * (Mathf.PI / 2) * (time + 1)) * Mathf.Pow(2, -10 * time) + 1;
        }

        public static float Exponential(float time)
        {
            if (Mathf.Abs(time) < Mathf.Epsilon || Mathf.Abs(time - 1) < Mathf.Epsilon)
            {
                return time;
            }

            if (time < 0.5f)
            {
                return 0.5f * Mathf.Pow(2, 20 * time - 10);
            }

            return -0.5f * Mathf.Pow(2, -20 * time + 10) + 1;
        }

        public static float ExponentialIn(float time)
        {
            return Mathf.Abs(time) < Mathf.Epsilon ? time : Mathf.Pow(2, 10 * (time - 1));
        }

        public static float ExponentialOut(float time)
        {
            if (Mathf.Abs(time - 1) < Mathf.Epsilon)
            {
                return time;
            }

            return 1 - Mathf.Pow(2, -10 * time);
        }

        public static float Linear(float time)
        {
            return time;
        }

        public static float Quadratic(float time)
        {
            if (time < 0.5f)
            {
                return 2 * time * time;
            }

            return -2 * time * time + 4 * time - 1;
        }

        public static float QuadraticIn(float time)
        {
            return time * time;
        }

        public static float QuadraticOut(float time)
        {
            return -(time * (time - 2));
        }

        public static float Quartic(float time)
        {
            if (time < 0.5f)
            {
                return 8 * time * time * time * time;
            }

            time--;

            return -8 * time * time * time * time + 1;
        }

        public static float QuarticIn(float time)
        {
            return time * time * time * time;
        }

        public static float QuarticOut(float time)
        {
            time--;

            return time * time * time * (2 - time) + 1;
        }

        public static float Quintic(float time)
        {
            if (time < 0.5f)
            {
                return 16 * time * time * time * time * time;
            }

            time = 2 * time - 2;

            return 0.5f * time * time * time * time * time + 1;
        }

        public static float QuinticIn(float time)
        {
            return time * time * time * time * time;
        }

        public static float QuinticOut(float time)
        {
            time--;

            return time * time * time * time * time + 1;
        }

        public static float Sine(float time)
        {
            return 0.5f * (1 - Mathf.Cos(time * Mathf.PI));
        }

        public static float SineIn(float time)
        {
            return Mathf.Sin((time - 1) * (Mathf.PI / 2)) + 1;
        }

        public static float SineOut(float time)
        {
            return Mathf.Sin(time * (Mathf.PI / 2));
        }
    }
}
