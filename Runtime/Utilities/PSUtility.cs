﻿using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using CompressionLevel = System.IO.Compression.CompressionLevel;

namespace PainfulSmile
{
    public static class PSUtility
    {
        public enum CompressionType
        {
            Fastest = CompressionLevel.Fastest,
            Optimal = CompressionLevel.Optimal
        }

        private static readonly BinaryFormatter Formatter = new BinaryFormatter();
        private static readonly string[] InvalidChars;

        internal const string ComponentMenu = "Painful Smile/";
        internal const string CreateAssetMenuItem = "Assets/Create/";
        internal const string DataPath = "Assets/PainfulSmile/Data/";
        internal const string GeneratedPath = "Assets/PainfulSmile/Generated/";
        internal const string ResourcesPath = "Assets/PainfulSmile/Resources/";
        internal const string ToolsMenuItem = "Tools/Painful Smile/";
        internal const int MappedUtilityMenuOrder = 200;
        internal const int QuickUtilityMenuOrder = 100;

        static PSUtility()
        {
            var invalidChars = new List<string> { "." };

            foreach (char c in Path.GetInvalidFileNameChars())
            {
                string s = c.ToString();

                if (string.IsNullOrEmpty(s) || s == "\\" || s == "/")
                {
                    continue;
                }

                invalidChars.Add(s);
            }

            InvalidChars = invalidChars.ToArray();
        }

        public static bool IsEditModeOrExitingPlayMode
        {
            get
            {
#if UNITY_EDITOR
                return Application.isPlaying == false || UnityEditor.EditorApplication.isPlaying != UnityEditor.EditorApplication.isPlayingOrWillChangePlaymode;
#else
                return false;
#endif
            }
        }

        private static byte[] Compress(byte[] data, CompressionType compressionType = CompressionType.Optimal)
        {
            using (var resultStream = new MemoryStream())
            {
                using (var deflateStream = new DeflateStream(resultStream, (CompressionLevel)compressionType))
                {
                    deflateStream.Write(data, 0, data.Length);
                }

                return resultStream.ToArray();
            }
        }

        private static byte[] Decompress(byte[] data)
        {
            using (var outputStream = new MemoryStream())
            {
                using (var inputStream = new MemoryStream(data))
                using (var deflateStream = new DeflateStream(inputStream, CompressionMode.Decompress))
                {
                    deflateStream.CopyTo(outputStream);
                }

                return outputStream.ToArray();
            }
        }

        public static void Serialize(string key, object obj)
        {
            using (var memoryStream = new MemoryStream())
            {
                Formatter.Serialize(memoryStream, obj);
                PlayerPrefs.SetString(key, System.Convert.ToBase64String(memoryStream.ToArray()));
            }
        }

        public static T Deserialize<T>(string key, T defaultValue = default)
        {
            string value = PlayerPrefs.GetString(key, null);

            if (string.IsNullOrEmpty(value))
            {
                return defaultValue;
            }

            using (var memoryStream = new MemoryStream(System.Convert.FromBase64String(value)))
            {
                return (T)Formatter.Deserialize(memoryStream);
            }
        }

        public static string GetFolder(string sourcePath, string relativePath)
        {
            string result = sourcePath;

            foreach (string folder in relativePath.Split('/'))
            {
                result = folder == ".." ? result.Remove(result.LastIndexOf("/")) : $"{result}/{folder}";
            }

            return result;
        }

        public static string GetValidFolderName(string name, string defaultValue = "")
        {
            for (var i = 0; i < InvalidChars.Length; i++)
            {
                name = name.Replace(InvalidChars[i], string.Empty);

                if (name == "")
                {
                    return defaultValue;
                }
            }

            if (name[0] == '\\' || name[0] == '/')
            {
                name = name.Remove(0, 1);

                if (name == "")
                {
                    return defaultValue;
                }
            }

            char last = name[name.Length - 1];

            if (last == '\\' || last == '/')
            {
                name = name.Remove(name.Length - 1);

                if (name == "")
                {
                    return defaultValue;
                }
            }

            return name;
        }

        public static bool IsSubclassOfRawGeneric(System.Type target, System.Type generic)
        {
            while (target != null && target != typeof(object))
            {
                System.Type current = target.IsGenericType ? target.GetGenericTypeDefinition() : target;

                if (current == generic)
                {
                    return true;
                }

                target = target.BaseType;
            }

            return false;
        }
    }
}
