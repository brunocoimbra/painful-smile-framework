﻿using System.Collections.Generic;
using System.ComponentModel;

namespace PainfulSmile
{
    public static class DescriptionUtility
    {
        /// <summary>
        /// Collect the <see cref="DescriptionAttribute"/> values for all enum values in this enum type.
        /// </summary>
        /// <param name="index"> Index of DescriptionAttribute, if multiples. </param>
        /// <param name="fallback"> If true, it will try to pick another description if the index is not valid. </param>
        public static int GetEnumDescriptions<T>(List<string> appendResults, int index = 0, bool fallback = true) where T : System.Enum
        {
            System.Array values = System.Enum.GetValues(typeof(T));

            appendResults.EnsureCapacity(appendResults.Count + values.Length);

            for (int i = 0; i < values.Length; i++)
            {
                var enumValue = (System.Enum)values.GetValue(i);
                appendResults.Add(enumValue.GetDescription(index, fallback));
            }

            return values.Length;
        }

        /// <summary>
        /// Return the <see cref="DescriptionAttribute"/> values for all enum values in this enum type.
        /// </summary>
        /// <param name="index"> Index of DescriptionAttribute, if multiples. </param>
        /// <param name="fallback"> If true, it will try to pick another description if the index is not valid. </param>
        public static string[] GetEnumDescriptions<T>(int index = 0, bool fallback = true) where T : System.Enum
        {
            System.Array values = System.Enum.GetValues(typeof(T));
            var results = new string[values.Length];

            for (int i = 0; i < values.Length; i++)
            {
                var enumValue = (System.Enum)values.GetValue(i);
                results[i] = enumValue.GetDescription(index, fallback);
            }

            return results;
        }
    }
}
