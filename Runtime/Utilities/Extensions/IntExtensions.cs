﻿namespace PainfulSmile
{
    public static class IntExtensions
    {
        /// <summary>
        /// Check if all the given bits are set.
        /// </summary>
        public static bool IsSet(this int mask, int bits)
        {
            return (mask & bits) == bits;
        }

        /// <summary>
        /// Check if any of the given bits are set.
        /// </summary>
        public static bool IsAnySet(this int mask, int bits)
        {
            return (mask & bits) != 0;
        }
    }
}
