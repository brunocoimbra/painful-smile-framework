using System.Collections.Generic;
using UnityEngine;

namespace PainfulSmile
{
    public static class TransformExtensions
    {
        /// <summary>
        /// Listener will receive the current transform in the hierarchy and the path to reach it. Listener will return if the operation should be stopped;
        /// </summary>
        public delegate bool ForEachHandler(Transform current, IReadOnlyList<int> path);

        /// <summary>
        /// Listener will receive the current transform in the hierarchy and parent iterations to reach it. Listener will return if the operation should be stopped;
        /// </summary>
        public delegate bool ForEachReverseHandler(Transform current, int parentIterations);

        /// <summary>
        /// Executes an action on this transform and its child recursively.
        /// </summary>
        public static void ForEach(this Transform transform, ForEachHandler action, bool ignoreSelf = false)
        {
            if (action == null)
            {
                return;
            }

            Transform current = transform;

            using (var path = ListPool<int>.GetDisposable())
            {
                if (ignoreSelf)
                {
                    if (transform.childCount == 0)
                    {
                        return;
                    }

                    path.Value.Add(0);
                    current = current.GetChild(0);
                }

                do
                {
                    if (action.Invoke(current, path.Value))
                    {
                        break;
                    }

                    if (current.childCount > 0)
                    {
                        path.Value.Add(0);
                        current = current.GetChild(0);
                    }
                    else if (current != transform)
                    {
                        do
                        {
                            path.Value.RemoveLast();

                            int candidate = current.GetSiblingIndex() + 1;

                            if (candidate < current.parent.childCount)
                            {
                                path.Value.Add(candidate);
                                current = current.parent.GetChild(candidate);

                                break;
                            }

                            current = current.parent;

                            if (current == transform)
                            {
                                goto BREAK;
                            }
                        }
                        while (true);
                    }
                    else
                    {
                        break;
                    }
                }
                while (true);

                BREAK: ;
            }
        }

        /// <summary>
        /// Executes an action on this transform and its parent recursively.
        /// </summary>
        public static void ForEachReverse(this Transform transform, ForEachReverseHandler action, bool ignoreSelf = false)
        {
            if (action == null)
            {
                return;
            }

            int parentIterations = 0;
            Transform current = transform;

            if (ignoreSelf)
            {
                if (current.parent == null)
                {
                    return;
                }

                current = current.parent;
                parentIterations++;
            }

            do
            {
                if (action.Invoke(current, parentIterations))
                {
                    break;
                }

                current = current.parent;
                parentIterations++;
            }
            while (current != null);
        }

        /// <summary>
        /// Turn the transform towards the given point.
        /// </summary>
        /// <returns> True if transform is facing the point. </returns>
        public static bool TurnTowardsPoint(this Transform transform, Vector3 point, float maxDegreesDelta, float minAngle = 0.1f, float minMagnitude = 0.001f)
        {
            Vector3 direction = point - transform.position;
            direction.y = 0;

            if (direction.magnitude > minMagnitude)
            {
                direction.Normalize();

                Quaternion targetRotation = Quaternion.LookRotation(direction);
                Quaternion rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, maxDegreesDelta);

                transform.rotation = rotation;

                return Quaternion.Angle(targetRotation, rotation) < minAngle;
            }

            return true;
        }
    }
}
