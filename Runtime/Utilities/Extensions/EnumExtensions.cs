﻿using System.ComponentModel;
using UnityEngine;

namespace PainfulSmile
{
    public static class EnumExtensions
    {
        /// <summary>
        /// Returns the <see cref="DescriptionAttribute"/> value for this enum value. If none, returns the string representation.
        /// </summary>
        /// <param name="index"> Index of DescriptionAttribute, if multiples. </param>
        /// <param name="fallback"> If true, it will try to pick another description if the index is not valid. </param>
        public static string GetDescription(this System.Enum value, int index = 0, bool fallback = true)
        {
            var descriptions = (DescriptionAttribute[])value.GetType().GetField(value.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (descriptions.Length == 0)
            {
                return value.ToString();
            }

            if (fallback)
            {
                return descriptions[Mathf.Min(index, descriptions.Length - 1)].Description;
            }

            return index < descriptions.Length ? descriptions[index].Description : value.ToString();
        }
    }
}
