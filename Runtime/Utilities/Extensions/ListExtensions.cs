using System.Collections.Generic;

namespace PainfulSmile
{
    public static class ListExtensions
    {
        /// <summary>
        /// Ensure the list has at least the given capacity.
        /// </summary>
        public static void EnsureCapacity<T>(this List<T> list, int capacity)
        {
            if (list.Capacity < capacity)
            {
                list.Capacity = capacity;
            }
        }

        /// <summary>
        /// Remove the last element of the list.
        /// </summary>
        public static void RemoveLast<T>(this List<T> list)
        {
            if (list.Count == 0)
            {
                return;
            }

            list.RemoveAt(list.Count - 1);
        }
    }
}
