using UnityEngine;

namespace PainfulSmile
{
    public static class GameObjectExtensions
    {
        /// <summary>
        /// Get a component in the object, or add a new one if none.
        /// </summary>
        public static void GetOrAddComponent<T>(this GameObject gameObject, out T result) where T : Component
        {
            result = gameObject.GetComponent<T>();

            if (result == null)
            {
                result = gameObject.AddComponent<T>();
            }
        }

        /// <summary>
        /// Get a component in the object, or add a new one if none. For a non-alloc version use <see cref="GetOrAddComponent{T}(GameObject, out T)"/>.
        /// </summary>
        public static T GetOrAddComponent<T>(this GameObject gameObject) where T : Component
        {
            var component = gameObject.GetComponent<T>();

            if (component == null)
            {
                component = gameObject.AddComponent<T>();
            }

            return component;
        }

        /// <summary>
        /// Remove the first component of the specified type found in the gameObject.
        /// </summary>
        public static void RemoveComponent<T>(this GameObject gameObject) where T : Component
        {
            var component = gameObject.GetComponent<T>();

            if (component != null)
            {
                Object.Destroy(component);
            }
        }

        /// <summary>
        /// Get the total bounds for this object.
        /// </summary>
#if UNITY_PARTICLESYSTEM
        /// <param name="includeParticles"> Should ParticleSystemRenderers be included? </param>
        public static Bounds? GetBounds(this GameObject gameObject, bool includeParticles = false)
#else
        public static Bounds? GetBounds(this GameObject gameObject)
#endif
        {
            Bounds? bounds = null;
            Renderer[] renderers = gameObject.GetComponentsInChildren<Renderer>();

            for (int i = 0, length = renderers.Length; i < length; i++)
            {
#if UNITY_PARTICLESYSTEM
                if (includeParticles || renderers[i].GetType() != typeof(ParticleSystemRenderer))
#endif
                {
                    if (bounds.HasValue)
                    {
                        bounds.Value.Encapsulate(renderers[i].bounds);
                    }
                    else
                    {
                        bounds = renderers[i].bounds;
                    }
                }
            }

            return bounds;
        }
    }
}
