﻿using UnityEngine;

namespace PainfulSmile
{
    public static class LayerMaskExtensions
    {
        /// <summary>
        /// Check if the <see cref="LayerMask"/> contains a layer.
        /// </summary>
        public static bool Contains(this LayerMask mask, int layer)
        {
            return mask.value.IsSet(1 << layer);
        }

        /// <summary>
        /// Check if the <see cref="LayerMask"/> contains all given layers.
        /// </summary>
        public static bool ContainsAll(this LayerMask mask, params int[] layers)
        {
            foreach (var layer in layers)
            {
                if (mask.value.IsSet(1 << layer) == false)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Check if the <see cref="LayerMask"/> contains any given layer.
        /// </summary>
        public static bool ContainsAny(this LayerMask mask, params int[] layers)
        {
            foreach (var layer in layers)
            {
                if (mask.value.IsSet(1 << layer))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
