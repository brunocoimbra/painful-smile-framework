using UnityEngine;

namespace PainfulSmile
{
    public static class Math
    {
        public static float Lerp(float start, float end, float time, System.Func<float, float> easing = null)
        {
            return LerpUnclamped(start, end, Mathf.Clamp01(time), easing);
        }

        public static float LerpUnclamped(float start, float end, float time, System.Func<float, float> easing = null)
        {
            return start + (end - start) * (easing?.Invoke(time) ?? time);
        }

        public static Bounds BoundsLerp(Bounds from, Bounds to, float time)
        {
            return new Bounds(new Vector3(Lerp(from.center.x, to.center.x, time),
                                          Lerp(from.center.y, to.center.y, time),
                                          Lerp(from.center.z, to.center.z, time)),
                              new Vector3(Lerp(from.size.x, to.size.x, time),
                                          Lerp(from.size.y, to.size.y, time),
                                          Lerp(from.size.z, to.size.z, time)));
        }

        public static Bounds BoundsLerpUnclamped(Bounds from, Bounds to, float time)
        {
            return new Bounds(new Vector3(LerpUnclamped(from.center.x, to.center.x, time),
                                          LerpUnclamped(from.center.y, to.center.y, time),
                                          LerpUnclamped(from.center.z, to.center.z, time)),
                              new Vector3(LerpUnclamped(from.size.x, to.size.x, time),
                                          LerpUnclamped(from.size.y, to.size.y, time),
                                          LerpUnclamped(from.size.z, to.size.z, time)));
        }

        public static Rect RectLerp(Rect from, Rect to, float time)
        {
            return new Rect(Lerp(from.x, to.x, time),
                            Lerp(from.y, to.y, time),
                            Lerp(from.width, to.width, time),
                            Lerp(from.height, to.height, time));
        }

        public static Rect RectLerpUnclamped(Rect from, Rect to, float time)
        {
            return new Rect(LerpUnclamped(from.x, to.x, time),
                            LerpUnclamped(from.y, to.y, time),
                            LerpUnclamped(from.width, to.width, time),
                            LerpUnclamped(from.height, to.height, time));
        }
    }
}
