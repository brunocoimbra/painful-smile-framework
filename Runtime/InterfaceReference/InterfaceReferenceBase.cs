using System.ComponentModel;
using UnityEngine;

namespace PainfulSmile
{
    [System.Serializable, EditorBrowsable(EditorBrowsableState.Never)]
    public abstract class InterfaceReferenceBase
    {
#pragma warning disable 0649
        [SerializeField] private Object m_Object;
#pragma warning restore 0649

        public Object Object => m_Object;
    }
}
