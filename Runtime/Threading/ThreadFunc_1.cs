﻿namespace PainfulSmile
{
    public sealed class ThreadFunc<TResult> : ThreadCommand<TResult>
    {
        private System.Func<TResult> _callback;

        public ThreadFunc(System.Func<TResult> callback)
        {
            _callback = callback ?? throw new System.ArgumentNullException(nameof(callback));
        }

        /// <summary>
        /// Try to set a new callback for the command. It will fail if the command is not ready to be enqueued.
        /// </summary>
        /// <returns>True if the callback was successfully changed.</returns>
        public bool SetCallback(System.Func<TResult> value)
        {
            if (value == null)
            {
                throw new System.ArgumentNullException(nameof(value));
            }

            if (IsReady == false)
            {
                return false;
            }

            _callback = value;

            return true;
        }

        /// <summary>
        /// Try to enqueue this command. Returns false if the command wasn't ready or failed to be enqueued.
        /// </summary>
        /// <returns>True if the command was successfully enqueued.</returns>
        public bool Enqueue()
        {
            return Enqueue(null);
        }

        protected override void OnExecute(object param)
        {
            Result = _callback();
        }
    }
}
