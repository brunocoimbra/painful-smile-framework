﻿namespace PainfulSmile
{
    /// <summary>
    /// Represents the current state of a <see cref="ThreadCommand"/>.
    /// </summary>
    public enum ThreadCommandState
    {
        /// <summary>
        /// The default state when the <see cref="ThreadCommand"/> is created.
        /// </summary>
        Created = 0,
        /// <summary>
        /// The <see cref="ThreadCommand"/> was successfully dequeued.
        /// </summary>
        Dequeued = 1,
        /// <summary>
        /// An <see cref="System.Exception"/> has been thrown by the <see cref="ThreadCommand"/>.
        /// </summary>
        Aborted = 2,
        /// <summary>
        /// The <see cref="ThreadCommand"/> completed successfully.
        /// </summary>
        Completed = 4,
        /// <summary>
        /// The <see cref="ThreadCommand"/> has been enqueued, but haven't started its execution.
        /// </summary>
        Enqueued = 8,
        /// <summary>
        /// The <see cref="ThreadCommand"/> is being dequeued.
        /// </summary>
        Dequeuing = 16,
        /// <summary>
        /// The <see cref="ThreadCommand"/> is being executed.
        /// </summary>
        Executing = 32
    }
}
