﻿using System.Threading;

namespace PainfulSmile
{
    /// <summary>
    /// Base class to create thread commands, override this to create your custom one.
    /// <para></para>
    /// Examples: <see cref="ThreadAction"/> and <see cref="ThreadAction{T}"/>.
    /// </summary>
    public abstract class ThreadCommand : System.IDisposable
    {
        private const int Aborted = (int)ThreadCommandState.Aborted;
        private const int Completed = (int)ThreadCommandState.Completed;
        private const int Created = (int)ThreadCommandState.Created;
        private const int Dequeued = (int)ThreadCommandState.Dequeued;
        private const int Dequeuing = (int)ThreadCommandState.Dequeuing;
        private const int Enqueued = (int)ThreadCommandState.Enqueued;
        private const int Executing = (int)ThreadCommandState.Executing;
        private const int Ready = Aborted | Completed | Created | Dequeued;
        private const int Locked = 1;
        private const int Unlocked = 0;

        private readonly WaitCallback ExecuteCallback;
        private readonly ManualResetEvent EventWaitHandler = new ManualResetEvent(true);

        private int _lock;
        private int _state;

        /// <summary>
        /// Is the command ready to be enqueued?
        /// </summary>
        public bool IsReady => _lock == Unlocked && (_state & Ready) != 0;
        /// <summary>
        /// The current state of the command.
        /// </summary>
        public ThreadCommandState State => (ThreadCommandState)_state;

        /// <summary>
        /// Any exception thrown since the last execution of the command.
        /// </summary>
        public System.Exception Exception { get; private set; }

        protected ThreadCommand()
        {
            _lock = Unlocked;
            _state = Created;
            ExecuteCallback = Execute;
        }

        /// <summary>
        /// Dispose this command> managed state.
        /// </summary>
        public void Dispose()
        {
            if (_lock == Locked)
            {
                throw new System.InvalidOperationException("The command is still being used.");
            }

            EventWaitHandler.Close();
        }

        /// <summary>
        /// Wait for the command execution.
        /// </summary>
        public void Wait()
        {
            EventWaitHandler.WaitOne();
        }

        /// <summary>
        /// Try to set the command to be dequeued.
        /// </summary>
        /// <returns>True if the command was successfully set to be dequeued.</returns>
        public bool Dequeue()
        {
            return _lock == Locked && Interlocked.CompareExchange(ref _state, Dequeuing, Enqueued) == Enqueued;
        }

        /// <summary>
        /// Define what should happen when the command executes.
        /// </summary>
        protected abstract void OnExecute(object param);

        /// <summary>
        /// Try to enqueue this command. Returns false if the command wasn't ready or failed to be enqueued.
        /// </summary>
        /// <returns>True if the command was successfully enqueued.</returns>
        protected bool Enqueue(object param)
        {
            if (Interlocked.CompareExchange(ref _lock, Locked, Unlocked) == Locked)
            {
                return false;
            }

            Interlocked.Exchange(ref _state, Enqueued);
            EventWaitHandler.Reset();
            Exception = null;

            try
            {
                if (ThreadPool.QueueUserWorkItem(ExecuteCallback, param))
                {
                    return true;
                }
            }
            catch (System.Exception e)
            {
                Exception = e;
            }

            Interlocked.Exchange(ref _state, Aborted);
            Interlocked.Exchange(ref _lock, Unlocked);
            EventWaitHandler.Set();

            return false;
        }

        private void Execute(object param)
        {
            if (Interlocked.CompareExchange(ref _state, Executing, Enqueued) == Dequeuing)
            {
                Interlocked.Exchange(ref _lock, Unlocked);
                EventWaitHandler.Set();

                return;
            }

            try
            {
                OnExecute(param);
                Interlocked.Exchange(ref _state, Completed);
            }
            catch (System.Exception e)
            {
                Exception = e;
                Interlocked.Exchange(ref _state, Aborted);
            }

            Interlocked.Exchange(ref _lock, Unlocked);
            EventWaitHandler.Set();
        }
    }
}
