using System.Threading;
using UnityEngine;

namespace PainfulSmile
{
    [AddComponentMenu("")]
    public sealed class ThreadUtility : SingletonBase<ThreadUtility>
    {
        public static bool IsUnityThread => _unityThread == Thread.CurrentThread;

        private static Thread _unityThread;

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        private static void Setup()
        {
            GetInstance(true);
        }

        protected override void OnAwake()
        {
            _unityThread = Thread.CurrentThread;

            Destroy(gameObject);
        }

        protected override void OnDispose(bool isInstance) { }
    }
}
