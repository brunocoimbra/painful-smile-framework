namespace PainfulSmile
{
    /// <summary>
    /// Base class to create thread commands with results, override this to create your custom one.
    /// <para></para>
    /// Examples: <see cref="ThreadFunc{TResult}"/> and <see cref="ThreadFunc{T, TResult}"/>.
    /// </summary>
    public abstract class ThreadCommand<TResult> : ThreadCommand
    {
        private TResult m_Result;

        /// <summary>
        /// The command result. It will only be available if the command completed.
        /// </summary>
        public TResult Result
        {
            get
            {
                if (IsReady && State == ThreadCommandState.Completed)
                {
                    return m_Result;
                }

                throw new System.InvalidOperationException("Result is only available if the command completed.");
            }
            protected set => m_Result = value;
        }

    }
}
