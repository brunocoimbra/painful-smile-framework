﻿using UnityEngine;

namespace PainfulSmile
{
    public sealed class WaitForThreadCommand : CustomYieldInstruction
    {
        public ThreadCommand Command { get; set; }

        public override bool keepWaiting => Command != null && Command.IsReady == false;

        public WaitForThreadCommand() { }

        public WaitForThreadCommand(ThreadCommand command)
        {
            Command = command;
        }
    }
}
