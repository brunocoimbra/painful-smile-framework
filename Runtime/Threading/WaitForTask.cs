﻿using System.Threading.Tasks;
using UnityEngine;

namespace PainfulSmile
{
    public sealed class WaitForTask : CustomYieldInstruction
    {
        public Task Task { get; set; }

        public override bool keepWaiting => Task != null && Task.IsCompleted == false && Task.IsFaulted == false && Task.IsCanceled == false;

        public WaitForTask() { }

        public WaitForTask(Task task)
        {
            Task = task;
        }
    }
}
