using UnityEngine;

namespace PainfulSmile
{
    public sealed class WaitForMainThreadCommand : CustomYieldInstruction
    {
        public MainThreadCommand Command { get; set; }

        public override bool keepWaiting => Command != null && Command.IsReady == false;

        public WaitForMainThreadCommand() { }

        public WaitForMainThreadCommand(MainThreadCommand command)
        {
            Command = command;
        }
    }
}
