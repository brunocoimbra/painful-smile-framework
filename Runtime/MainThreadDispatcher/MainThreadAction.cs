namespace PainfulSmile
{
    public sealed class MainThreadAction : MainThreadCommand
    {
        private System.Action _callback;

        public MainThreadAction(System.Action callback)
        {
            _callback = callback ?? throw new System.ArgumentNullException(nameof(callback));
        }

        /// <summary>
        /// Try to set a new callback for the command. It will fail if the command is not ready to be enqueued.
        /// </summary>
        /// <returns>True if the callback was successfully changed.</returns>
        public bool SetCallback(System.Action value)
        {
            if (value == null)
            {
                throw new System.ArgumentNullException(nameof(value));
            }

            if (IsReady == false)
            {
                return false;
            }

            _callback = value;

            return true;
        }

        protected override void OnExecute()
        {
            _callback();
        }
    }
}
