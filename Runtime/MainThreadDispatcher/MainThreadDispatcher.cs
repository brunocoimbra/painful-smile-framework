﻿using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace PainfulSmile
{
    /// <summary>
    /// Use this to execute commands on the Unity thread, useful for calling the Unity API when in another thread.
    /// </summary>
    [AddComponentMenu("")]
    public sealed class MainThreadDispatcher : Singleton<MainThreadDispatcher>
    {
        private static readonly Stopwatch Stopwatch = new Stopwatch();

        private static bool _hasCommands;
        private static bool _hasInstance;
        private static Queue<MainThreadCommandInfo> _commands;

        private static MainThreadDispatcher m_Instance;

#if UNITY_EDITOR
#pragma warning disable 0414
        [Tooltip("Current amount of scheduled commands.")]
        [SerializeField, ReadOnly]
        private int m_CommandsCount;
#pragma warning restore 0414
#endif

        private static MainThreadDispatcher Instance
        {
            get
            {
#if UNITY_EDITOR
                if (PSUtility.IsEditModeOrExitingPlayMode)
                {
                    throw new System.InvalidOperationException($"{nameof(MainThreadDispatcher)} should not be called in edit mode or when exiting play mode!");
                }
#endif
                Initialize();

                return m_Instance;
            }
        }

        /// <summary>
        /// Max milliseconds per frame that the <see cref="MainThreadDispatcher"/> will expend.
        /// </summary>
        public static float MillisecondsPerFrame { get; set; }

        private void Update()
        {
#if UNITY_EDITOR
            m_CommandsCount = _commands.Count;
#endif
            if (_hasCommands == false)
            {
                return;
            }

            Stopwatch.Reset();
            Stopwatch.Start();

            do
            {
                MainThreadCommandInfo command;

                lock (_commands)
                {
                    command = _commands.Dequeue();
                    _hasCommands = _commands.Count > 0;
                }

                command.Invoke();
            }
            while (_hasCommands && Stopwatch.ElapsedMilliseconds < MillisecondsPerFrame);
        }

        /// <summary>
        /// Initialize the <see cref="MainThreadDispatcher"/>. You only need to call it once.
        /// </summary>
        public static void Initialize()
        {
            if (_hasInstance == false)
            {
                m_Instance = GetInstance(true);
                _hasInstance = true;
            }
        }

        /// <summary>
        /// Schedule a new command to be executed on the <see cref="MainThreadDispatcher"/>.
        /// </summary>
        /// <param name="command">The command to be executed on the <see cref="MainThreadDispatcher"/>.</param>
        /// <param name="isPersistent">Should the command persist across scene changes?</param>
        public static bool Schedule(System.Action command, bool isPersistent)
        {
            if (command == null)
            {
                throw new System.ArgumentNullException(nameof(command), "command should not be null!");
            }

            if (ThreadUtility.IsUnityThread)
            {
                command.Invoke();

                return false;
            }

            lock (_commands)
            {
                _commands.Enqueue(new MainThreadCommandInfo(command, isPersistent));
            }

            _hasCommands = true;

            return true;
        }

        protected override void OnDispose(bool isInstance)
        {
            if (isInstance)
            {
                SceneManager.sceneUnloaded -= HandleSceneUnloaded;
            }
        }

        protected override void OnInitialize()
        {
            DontDestroyOnLoad(gameObject);

            SceneManager.sceneUnloaded += HandleSceneUnloaded;

            _commands = new Queue<MainThreadCommandInfo>();
            MillisecondsPerFrame = MainThreadDispatcherSettings.MillisecondsPerFrame;
        }

        private static void HandleSceneUnloaded(Scene scene)
        {
            lock (_commands)
            {
                var commands = new Queue<MainThreadCommandInfo>(_commands.Count);

                while (_commands.Count > 0)
                {
                    MainThreadCommandInfo command = _commands.Dequeue();

                    if (command.IsPersistent)
                    {
                        commands.Enqueue(command);
                    }
                }

                _commands = commands;
                _hasCommands = _commands.Count > 0;
            }
        }
    }
}
