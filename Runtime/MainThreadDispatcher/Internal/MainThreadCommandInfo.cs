namespace PainfulSmile
{
    [System.Serializable]
    internal struct MainThreadCommandInfo
    {
        public readonly bool IsPersistent;

        private readonly System.Action Command;

        public MainThreadCommandInfo(System.Action command, bool isPersistent)
        {
            IsPersistent = isPersistent;
            Command = command;
        }

        public void Invoke()
        {
            Command.Invoke();
        }
    }
}
