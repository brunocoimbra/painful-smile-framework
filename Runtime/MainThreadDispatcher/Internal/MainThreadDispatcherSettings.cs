﻿using UnityEngine;

namespace PainfulSmile
{
    internal sealed class MainThreadDispatcherSettings : ScriptableSingleton<MainThreadDispatcherSettings>
    {
        private const float DefaultMillisecondsPerFrame = 1;

#pragma warning disable 0414
        [SerializeField, Positive] private float m_MillisecondsPerFrame = DefaultMillisecondsPerFrame;
#pragma warning restore 0414

        public static float MillisecondsPerFrame => Exists ? GetInstance(false).m_MillisecondsPerFrame : DefaultMillisecondsPerFrame;

        protected override string DefaultAssetPath => $"{PSUtility.ResourcesPath}/{GetType().Name}.asset";

        internal static MainThreadDispatcherSettings GetInstance()
        {
            return GetInstance(true);
        }

        protected override void OnInitialize()
        {
            DontDestroyOnLoad(this);
        }
    }
}
