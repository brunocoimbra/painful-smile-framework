﻿using System.Threading;

namespace PainfulSmile
{
    /// <summary>
    /// Allows the usage of Unity API from another thread.
    /// <para></para>
    /// Examples: <see cref="MainThreadAction"/>.
    /// </summary>
    public abstract class MainThreadCommand : System.IDisposable
    {
        private const int Aborted = (int)MainThreadCommandState.Aborted;
        private const int Completed = (int)MainThreadCommandState.Completed;
        private const int Created = (int)MainThreadCommandState.Created;
        private const int Executing = (int)MainThreadCommandState.Executing;
        private const int Scheduled = (int)MainThreadCommandState.Scheduled;
        private const int Ready = Aborted | Completed | Created;
        private const int Locked = 1;
        private const int Unlocked = 0;

        private readonly ManualResetEvent EventWaitHandler = new ManualResetEvent(true);

        private int _lock;
        private int _state;
        private System.Action _executeCallback;

        /// <summary>
        /// Is the command ready to be scheduled?
        /// </summary>
        public bool IsReady => _lock == Unlocked && (_state & Ready) != 0;
        /// <summary>
        /// The current state of the command.
        /// </summary>
        public MainThreadCommandState State => (MainThreadCommandState)_state;

        /// <summary>
        /// Any exception thrown since the last execution of the command.
        /// </summary>
        public System.Exception Exception { get; private set; }

        protected MainThreadCommand()
        {
            _lock = Unlocked;
            _state = Created;
        }

        /// <summary>
        /// Dispose this command> managed state.
        /// </summary>
        public void Dispose()
        {
            if (_lock == Locked)
            {
                throw new System.InvalidOperationException("The command is still being used.");
            }

            EventWaitHandler.Close();
        }

        /// <summary>
        /// Wait for the command execution.
        /// </summary>
        public void Wait()
        {
            EventWaitHandler.WaitOne();
        }

        /// <summary>
        /// Define what should happen when the command executes.
        /// </summary>
        protected abstract void OnExecute();

        /// <summary>
        /// Try to schedule this command. Returns false if the command wasn't ready or failed to be scheduled.
        /// </summary>
        /// <param name="isPersistent">Should the command persist across scene changes?</param>
        /// <returns>True if the command was successfully scheduled.</returns>
        public bool Schedule(bool isPersistent = false)
        {
            if (Interlocked.CompareExchange(ref _lock, Locked, Unlocked) == Locked)
            {
                return false;
            }

            Interlocked.Exchange(ref _state, Scheduled);
            EventWaitHandler.Reset();

            Exception = null;
            MainThreadDispatcher.Schedule(_executeCallback ?? (_executeCallback = Execute), isPersistent);

            return true;
        }

        private void Execute()
        {
            Interlocked.Exchange(ref _state, Executing);

            try
            {
                OnExecute();
                Interlocked.Exchange(ref _state, Completed);
            }
            catch (System.Exception e)
            {
                Exception = e;
                Interlocked.Exchange(ref _state, Aborted);
            }

            Interlocked.Exchange(ref _lock, Unlocked);
            EventWaitHandler.Set();
        }
    }
}
