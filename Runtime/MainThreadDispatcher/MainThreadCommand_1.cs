namespace PainfulSmile
{
    /// <summary>
    /// Allows the usage of Unity API from another thread while also being able to get a result.
    /// <para></para>
    /// Examples: <see cref="MainThreadFunc{TResult}"/>.
    /// </summary>
    public abstract class MainThreadCommand<TResult> : MainThreadCommand
    {
        private TResult m_Result;

        /// <summary>
        /// The command result. It will only be available if the command completed.
        /// </summary>
        public TResult Result
        {
            get
            {
                if (IsReady && State == MainThreadCommandState.Completed)
                {
                    return m_Result;
                }

                throw new System.InvalidOperationException("Result is only available if the command completed.");
            }
            protected set => m_Result = value;
        }
    }
}
