﻿namespace PainfulSmile
{
    /// <summary>
    /// Represents the current state of a <see cref="MainThreadCommand"/>.
    /// </summary>
    public enum MainThreadCommandState
    {
        /// <summary>
        /// The default state when the <see cref="MainThreadCommand"/> is created.
        /// </summary>
        Created = 0,
        /// <summary>
        /// An <see cref="System.Exception"/> has been thrown by the <see cref="MainThreadCommand"/>.
        /// </summary>
        Aborted = 2,
        /// <summary>
        /// The <see cref="MainThreadCommand"/> completed successfully.
        /// </summary>
        Completed = 4,
        /// <summary>
        /// The <see cref="MainThreadCommand"/> has been scheduled, but haven't started its execution.
        /// </summary>
        Scheduled = 8,
        /// <summary>
        /// The <see cref="MainThreadCommand"/> is being executed.
        /// </summary>
        Executing = 32
    }
}
