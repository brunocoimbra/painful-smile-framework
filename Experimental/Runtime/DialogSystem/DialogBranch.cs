﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace PainfulSmile.Experimental
{
    public class DialogBranch<T> : DialogBranch where T : DialogActorBase
    {
        private enum UnityTag
        {
            Bold,
            Italic,
            Size,
            Color
        }

        private static readonly Dictionary<UnityTag, string> DictUnityTagsPrefix = new Dictionary<UnityTag, string>()
        {
            { UnityTag.Bold, "<b" },
            { UnityTag.Italic, "<i" },
            { UnityTag.Size, "<size" },
            { UnityTag.Color, "<color" }
        };
        private static readonly Dictionary<UnityTag, string> DictUnityTagsSufix = new Dictionary<UnityTag, string>()
        {
            { UnityTag.Bold, "</b>" },
            { UnityTag.Italic, "</i>" },
            { UnityTag.Size, "</size>" },
            { UnityTag.Color, "</color>" }
        };

        [SerializeField, HideInInspector] private T[] m_Actors = new T[0];

        private bool _isGrouped;
        private bool _isPaused;
        private bool _isSkipping;
        private bool _groupStateChanged;
        private bool _speedChanged;
        private int _dialogIndex;
        private Coroutine _pauseCoroutine;
        private Coroutine _textCoroutine;
        private DialogSystem _system;
        private StringBuilder _prefixBuilder = new StringBuilder();
        private StringBuilder _sufixBuilder = new StringBuilder();
        private StringBuilder _textBuilder = new StringBuilder();
        private List<string> _dialog = new List<string>();
        private Dictionary<int, string> _dictCustomTagValue = new Dictionary<int, string>();
        private int m_ActorIndex = -1;

        /// <summary>
        /// The system that owns that branch.
        /// </summary>
        public DialogSystem System => _system;

        /// <summary>
        /// The current dialog's actors.
        /// </summary>
        public T[] Actors
        {
            get => m_Actors ?? (m_Actors = new T[0]);
            set => m_Actors = value ?? new T[0];
        }

        /// <summary>
        /// The current dialog's actor.
        /// </summary>
        public int ActorIndex
        {
            get => m_ActorIndex;
            protected set => m_ActorIndex = value;
        }

        protected bool IsUpdatingText => _dialogIndex < _dialog.Count;
        protected string Text => _textBuilder.ToString();
        protected T Actor => ActorIndex >= 0 && ActorIndex < Actors.Length ? Actors[ActorIndex] : null;

        public sealed override DialogBranchBase CheckBranch(DialogSystem system)
        {
            if (system == null)
            {
                Debug.LogException(new System.ArgumentNullException(nameof(system)), gameObject);

                return null;
            }

            if (_system == null || _system != system)
            {
                _system = system;
            }

            if (ActorIndex < 0)
            {
                ActorIndex = 0;

                if (Actor != null || TryNextActor())
                {
                    OnBranchStart();

                    return this;
                }
            }
            else if (ActorIndex < Actors.Length)
            {
                if (IsUpdatingText)
                {
                    if (Actor.AllowSkip)
                    {
                        SkipText();
                    }

                    return this;
                }

                if (TryNextActor())
                {
                    OnBranchContinue();

                    return this;
                }
            }

            for (int i = 0; i < transitions.Length; i++)
            {
                if (transitions[i].Check(system.Parameters))
                {
                    OnBranchFinish(false);

                    return transitions[i].Branch.CheckBranch(system);
                }
            }

            OnBranchFinish(true);

            system.IsLocked = system.LockOnFinish;

            return null;
        }

        public sealed override void ResetBranch()
        {
            ActorIndex = -1;
            _dialogIndex = 0;
            _pauseCoroutine = null;
            _textCoroutine = null;
            _textBuilder.Remove(0, _textBuilder.Length);
            _dialog.Clear();
            _dictCustomTagValue.Clear();
            StopAllCoroutines();

            OnBranchReset();
        }

        protected virtual void OnBranchContinue()
        {
            ShowText(Actor.Text, Actor.DefaultSpeed, Actor.DialogEvents);
        }

        protected virtual void OnBranchFinish(bool isLast)
        {
            ResetBranch();
        }

        protected virtual void OnBranchReset() { }

        protected virtual void OnBranchStart()
        {
            ShowText(Actor.Text, Actor.DefaultSpeed, Actor.DialogEvents);
        }

        protected virtual void OnTextFinish() { }

        protected virtual void OnTextSkip() { }

        protected virtual void OnTextStart() { }

        protected virtual void OnTextUpdate() { }

        protected void PauseText(float seconds)
        {
            if (_pauseCoroutine != null)
            {
                StopCoroutine(_pauseCoroutine);

                _pauseCoroutine = null;
            }

            if (seconds > 0 && !_isSkipping)
            {
                _pauseCoroutine = StartCoroutine(PauseTextCoroutine(seconds));
            }
            else
            {
                _isPaused = false;
            }
        }

        protected void ShowText(string text, float speed, DialogEvent[] events)
        {
            if (_textCoroutine != null)
            {
                StopCoroutine(_textCoroutine);

                _textCoroutine = null;
            }

            _dialog.Clear();
            _dictCustomTagValue.Clear();

            if (string.IsNullOrEmpty(text) == false)
            {
                _textBuilder.Remove(0, _textBuilder.Length);
                _textBuilder.Append(text);

                SplitImmediate(_textBuilder, _prefixBuilder, _sufixBuilder, _dialog, _dictCustomTagValue);

                _prefixBuilder.Remove(0, _prefixBuilder.Length);
                _sufixBuilder.Remove(0, _sufixBuilder.Length);
            }

            _textBuilder.Remove(0, _textBuilder.Length);
            _textCoroutine = StartCoroutine(ShowTextCoroutine(speed, events));
        }

        protected void SkipText()
        {
            _isSkipping = true;
            _speedChanged = false;
            PauseText(0);

            OnTextSkip();
        }

        protected bool TryNextActor()
        {
            do
            {
                ActorIndex++;

                if (ActorIndex >= Actors.Length)
                {
                    return false;
                }

            }
            while (Actor == null);

            return true;
        }

        private static void SplitImmediate(StringBuilder textBuilder, StringBuilder prefix, StringBuilder sufix, List<string> parts, Dictionary<int, string> dictCustomTagValue)
        {
            while (textBuilder.Length > 0)
            {
                if (textBuilder[0] == '<')
                {
                    string text = textBuilder.ToString();

                    if (textBuilder[1] == '/')
                    {
                        if (RemoveTag(text, prefix, sufix, UnityTag.Bold)
                         || RemoveTag(text, prefix, sufix, UnityTag.Italic)
                         || RemoveTag(text, prefix, sufix, UnityTag.Color)
                         || RemoveTag(text, prefix, sufix, UnityTag.Size))
                        {
                            textBuilder.Remove(0, text.IndexOf('>') + 1);

                            continue;
                        }
                    }
                    else
                    {
                        if (CheckCustomTagAt(text, CustomTag.Pause, parts, dictCustomTagValue)
                         || CheckCustomTagAt(text, CustomTag.Speed, parts, dictCustomTagValue)
                         || CheckCustomTagAt(text, CustomTag.Event, parts, dictCustomTagValue)
                         || CheckCustomTagAt(text, CustomTag.Skip, parts, dictCustomTagValue)
                         || CheckCustomTagAt(text, CustomTag.Group, parts, dictCustomTagValue))
                        {
                            textBuilder.Remove(0, text.IndexOf('>') + 1);

                            continue;
                        }

                        if (AppendTag(text, prefix, sufix, UnityTag.Bold, false)
                         || AppendTag(text, prefix, sufix, UnityTag.Italic, false)
                         || AppendTag(text, prefix, sufix, UnityTag.Color, true)
                         || AppendTag(text, prefix, sufix, UnityTag.Size, true))
                        {
                            textBuilder.Remove(0, text.IndexOf('>') + 1);

                            continue;
                        }
                    }
                }

                parts.Add($"{prefix}{textBuilder[0]}{sufix}");
                textBuilder.Remove(0, 1);
            }
        }

        private static bool CheckCustomTagAt(string text, CustomTag tag, List<string> parts, Dictionary<int, string> dictCustomTagValue)
        {
            if (text.Length > DictCustomTags[tag].Length && text.StartsWith(DictCustomTags[tag]))
            {
                char c = text[DictCustomTags[tag].Length];

                switch (c)
                {
                    case '>':
                    {
                        dictCustomTagValue.Add(parts.Count, null);
                        parts.Add($"{DictCustomTags[tag]}>");

                        return true;
                    }

                    case '=':
                    {
                        int parameterEndIndex = text.IndexOf('>');

                        if (parameterEndIndex > 0)
                        {
                            dictCustomTagValue.Add(parts.Count, text.Substring(DictCustomTags[tag].Length + 1, parameterEndIndex - (DictCustomTags[tag].Length + 1)));
                            parts.Add($"{DictCustomTags[tag]}>");

                            return true;
                        }

                        break;
                    }
                }
            }

            return false;
        }

        private static bool AppendTag(string text, StringBuilder prefix, StringBuilder sufix, UnityTag tag, bool hasParameter)
        {
            if (text.Length > (DictUnityTagsPrefix[tag].Length + DictUnityTagsSufix[tag].Length + sufix.Length) && text.StartsWith(DictUnityTagsPrefix[tag]))
            {
                char c = text[DictUnityTagsPrefix[tag].Length];

                if (hasParameter == false)
                {
                    if (c == '>' || c == '=')
                    {
                        int tagSufixIndex = text.IndexOf(DictUnityTagsSufix[tag]);

                        if (tagSufixIndex > 0)
                        {
                            if (sufix.Length == 0)
                            {
                                prefix.Append($"{DictUnityTagsPrefix[tag]}{c}");
                                sufix.Append(DictUnityTagsSufix[tag]);

                                return true;
                            }

                            string sufixString = sufix.ToString();
                            int lastSufixIndex = text.IndexOf(sufixString.Substring(0, sufixString.IndexOf('>') + 1));

                            if (tagSufixIndex < lastSufixIndex)
                            {
                                prefix.Append($"{DictUnityTagsPrefix[tag]}{c}");
                                sufix.Insert(0, DictUnityTagsSufix[tag]);

                                return true;
                            }
                        }
                    }
                }
                else
                {
                    switch (c)
                    {
                        case '>':
                        {
                            int tagSufixIndex = text.IndexOf(DictUnityTagsSufix[tag]);

                            if (tagSufixIndex > 0)
                            {
                                if (sufix.Length == 0)
                                {
                                    prefix.Append($"{DictUnityTagsPrefix[tag]}>");
                                    sufix.Append(DictUnityTagsSufix[tag]);

                                    return true;
                                }

                                string sufixString = sufix.ToString();
                                int lastSufixIndex = text.IndexOf(sufixString.Substring(0, sufixString.IndexOf('>') + 1));

                                if (tagSufixIndex < lastSufixIndex)
                                {
                                    prefix.Append($"{DictUnityTagsPrefix[tag]}>");
                                    sufix.Insert(0, DictUnityTagsSufix[tag]);

                                    return true;
                                }
                            }

                            break;
                        }

                        case '=':
                        {
                            int parameterEndIndex = text.IndexOf('>');
                            int tagSufixIndex = text.IndexOf(DictUnityTagsSufix[tag]);

                            if (parameterEndIndex > 0 && parameterEndIndex < tagSufixIndex)
                            {
                                if (sufix.Length == 0)
                                {
                                    string prefixString = text.Substring(DictUnityTagsPrefix[tag].Length + 1, parameterEndIndex - (DictUnityTagsPrefix[tag].Length + 1));

                                    prefix.Append($"{DictUnityTagsPrefix[tag]}={prefixString}>");
                                    sufix.Append(DictUnityTagsSufix[tag]);

                                    return true;
                                }

                                string sufixString = sufix.ToString();
                                int lastSufixIndex = text.IndexOf(sufixString.Substring(0, sufixString.IndexOf('>') + 1));

                                if (tagSufixIndex < lastSufixIndex)
                                {
                                    string prefixString = text.Substring(DictUnityTagsPrefix[tag].Length + 1, parameterEndIndex - (DictUnityTagsPrefix[tag].Length + 1));

                                    prefix.Append($"{DictUnityTagsPrefix[tag]}={prefixString}>");
                                    sufix.Insert(0, DictUnityTagsSufix[tag]);

                                    return true;
                                }
                            }

                            break;
                        }
                    }
                }
            }

            return false;
        }

        private static bool RemoveTag(string text, StringBuilder prefix, StringBuilder sufix, UnityTag tag)
        {
            if (text.Length >= DictUnityTagsSufix[tag].Length && sufix.Length >= DictUnityTagsSufix[tag].Length && text.StartsWith(DictUnityTagsSufix[tag]) && sufix.ToString().StartsWith(DictUnityTagsSufix[tag]))
            {
                string prefixString = prefix.ToString();
                int prefixStartIndex = prefixString.LastIndexOf('<');

                prefix.Remove(prefixStartIndex, prefixString.Length - prefixStartIndex);
                sufix.Remove(0, DictUnityTagsSufix[tag].Length);

                return true;
            }

            return false;
        }

        private IEnumerator PauseTextCoroutine(float seconds)
        {
            _isPaused = true;

            yield return new WaitForSeconds(seconds);

            _isPaused = false;
        }

        private IEnumerator ShowTextCoroutine(float speed, DialogEvent[] events)
        {
            _groupStateChanged = false;
            _isGrouped = false;
            _isSkipping = false;
            _speedChanged = false;
            PauseText(0);
            OnTextStart();

            for (_dialogIndex = 0; _dialogIndex < _dialog.Count; _dialogIndex++)
            {
                if (_isPaused)
                {
                    yield return new WaitWhile(() => _isPaused);
                }

                if (_dictCustomTagValue.ContainsKey(_dialogIndex))
                {
                    switch (_dialog[_dialogIndex])
                    {
                        case GroupTag:
                        {
                            bool value = _isGrouped;

                            SetGroupValue(_dictCustomTagValue[_dialogIndex], ref value);

                            if (value != _isGrouped)
                            {
                                _isGrouped = value;
                                _groupStateChanged = true;
                            }

                            continue;
                        }

                        case SpeedTag:
                        {
                            float value = speed;

                            SetSpeedValue(_dictCustomTagValue[_dialogIndex], ref value);

                            if (Mathf.Abs(value - speed) > 0.001f)
                            {
                                speed = value;
                                _speedChanged = true;
                            }

                            continue;
                        }

                        case SkipTag:
                        {
                            _isSkipping = false;

                            continue;
                        }

                        case EventTag:
                        {
                            int value = GetEventValue(_dictCustomTagValue[_dialogIndex]);

                            if (value < events.Length && events[value] != null)
                            {
                                if (_isSkipping == false || events[value].IgnoreOnSkip == false)
                                {
                                    events[value].Invoke();
                                }
                            }

                            continue;
                        }

                        case PauseTag:
                        {
                            if (_isSkipping)
                            {
                                continue;
                            }

                            PauseText(GetPauseValue(_dictCustomTagValue[_dialogIndex]));

                            continue;
                        }

                        default:
                        {
                            Debug.LogError($"Case {_dialog[_dialogIndex]} not implemented!");

                            continue;
                        }
                    }
                }

                if (_dialog[_dialogIndex] != " "
                 && speed > 0
                 && _isSkipping == false
                 && _speedChanged == false
                 && (_isGrouped == false || _groupStateChanged))
                {
                    yield return new WaitForSeconds(1 / speed);
                }

                _groupStateChanged = false;
                _speedChanged = false;
                _textBuilder.Append(_dialog[_dialogIndex]);
                OnTextUpdate();
            }

            OnTextFinish();
        }
    }
}
