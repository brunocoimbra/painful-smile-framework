﻿namespace PainfulSmile.Experimental
{
    public enum StringCondition
    {
        Equals,
        Differs,
        Contains,
        DontContains,
        StartsWith,
        DontStartsWith,
        EndsWith,
        DontEndsWith
    }
}
