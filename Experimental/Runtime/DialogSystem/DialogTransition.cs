﻿using UnityEngine;

namespace PainfulSmile.Experimental
{
    [System.Serializable]
    public struct DialogTransition
    {
        [SerializeField] private DialogBranchBase m_Branch;
        [SerializeField] private DialogCondition[] m_Conditions;

        public DialogBranchBase Branch => m_Branch;
        public DialogCondition[] Conditions => m_Conditions;

        public DialogTransition(DialogBranchBase branch, DialogCondition[] conditions)
        {
            m_Branch = branch;
            m_Conditions = conditions;
        }

        public bool Check(DialogParameter[] parameters)
        {
            if (Branch == null)
            {
                return false;
            }

            for (int i = 0; i < Conditions.Length; i++)
            {
                if (CheckCondition(Conditions[i], parameters) == false)
                {
                    return false;
                }
            }

            return true;
        }

        private bool CheckCondition(DialogCondition condition, DialogParameter[] parameters)
        {
            for (int i = 0; i < parameters.Length; i++)
            {
                if (condition.Type != parameters[i].Type || condition.Name != parameters[i].Name)
                {
                    continue;
                }

                switch (condition.Type)
                {
                    case DialogParameterType.Bool:

                        return condition.BoolValue == parameters[i].BoolValue;

                    case DialogParameterType.Int:

                        return CheckParameter(condition, parameters[i].IntValue);

                    case DialogParameterType.Float:

                        return CheckParameter(condition, parameters[i].FloatValue);

                    case DialogParameterType.String:

                        return CheckParameter(condition, parameters[i].StringValue);

                    default:
                    {
                        Debug.LogError($"Case '{condition.Type}' not implemented.");

                        return false;
                    }
                }
            }

            return false;
        }

        private bool CheckParameter(DialogCondition condition, int value)
        {
            switch (condition.NumberCondition)
            {
                case NumberCondition.Equals:

                    return condition.IntValue == value;

                case NumberCondition.Differs:

                    return condition.IntValue != value;

                case NumberCondition.Greater:

                    return condition.IntValue > value;

                case NumberCondition.GreaterOrEquals:

                    return condition.IntValue >= value;

                case NumberCondition.Less:

                    return condition.IntValue < value;

                case NumberCondition.LessOrEquals:

                    return condition.IntValue <= value;

                default:
                {
                    Debug.LogError($"Case '{condition.NumberCondition}' not implemented.");

                    return false;
                }
            }
        }

        private bool CheckParameter(DialogCondition condition, float value)
        {
            switch (condition.NumberCondition)
            {
                case NumberCondition.Equals:

                    return Mathf.Abs(condition.FloatValue - value) < 0.001f;

                case NumberCondition.Differs:

                    return Mathf.Abs(condition.FloatValue - value) > 0.001f;

                case NumberCondition.Greater:

                    return condition.FloatValue > value;

                case NumberCondition.GreaterOrEquals:

                    return condition.FloatValue >= value;

                case NumberCondition.Less:

                    return condition.FloatValue < value;

                case NumberCondition.LessOrEquals:

                    return condition.FloatValue <= value;

                default:
                {
                    Debug.LogError($"Case '{condition.NumberCondition}' not implemented.");

                    return false;
                }
            }
        }

        private bool CheckParameter(DialogCondition condition, string value)
        {
            switch (condition.StringCondition)
            {
                case StringCondition.Equals:

                    return condition.StringValue == value;

                case StringCondition.Differs:

                    return condition.StringValue != value;

                case StringCondition.Contains:

                    return condition.StringValue.Contains(value);

                case StringCondition.DontContains:

                    return condition.StringValue.Contains(value) == false;

                case StringCondition.StartsWith:

                    return condition.StringValue.StartsWith(value);

                case StringCondition.DontStartsWith:

                    return condition.StringValue.StartsWith(value) == false;

                case StringCondition.EndsWith:

                    return condition.StringValue.EndsWith(value);

                case StringCondition.DontEndsWith:

                    return condition.StringValue.EndsWith(value) == false;

                default:
                {
                    Debug.LogError($"Case '{condition.StringCondition}' not implemented.");

                    return false;
                }
            }
        }
    }
}
