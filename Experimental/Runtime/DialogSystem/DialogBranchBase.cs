﻿using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

namespace PainfulSmile.Experimental
{
    public abstract class DialogBranchBase : MonoBehaviour
    {
        protected enum CustomTag
        {
            Event,
            Group,
            Pause,
            Skip,
            Speed
        }

        protected const string EventTag = "<event>";
        protected const string GroupTag = "<group>";
        protected const string PauseTag = "<pause>";
        protected const string SkipTag = "<skip>";
        protected const string SpeedTag = "<speed>";

        protected static readonly Dictionary<CustomTag, string> DictCustomTags = new Dictionary<CustomTag, string>()
        {
            { CustomTag.Event, "<event" },
            { CustomTag.Group, "<group" },
            { CustomTag.Pause, "<pause" },
            { CustomTag.Skip, "<skip" },
            { CustomTag.Speed, "<speed" },
        };

        [Tooltip("All transitions available on interact.")]
        [SerializeField, HideInInspector] private DialogTransition[] m_Transitions = new DialogTransition[0];

        /// <summary>
        /// All transitions available on interact.
        /// </summary>
        public DialogTransition[] transitions => m_Transitions ?? (m_Transitions = new DialogTransition[0]);

        public abstract void ResetBranch();

        public abstract DialogBranchBase CheckBranch(DialogSystem system);

        /// <summary>
        /// Set the group state from <see cref="string"/> s to <see cref="bool"/> value (inverts the value by default).
        /// </summary>
        protected static void SetGroupValue(string s, ref bool value)
        {
            if (bool.TryParse(s, out bool result))
            {
                value = result;
            }
            else
            {
                value = !value;
            }
        }

        /// <summary>
        /// Set the speed from <see cref="string"/> s to <see cref="float"/> value (don't change the value by default).
        /// </summary>
        protected static void SetSpeedValue(string s, ref float value)
        {
            if (float.TryParse(s, NumberStyles.Float, CultureInfo.InvariantCulture, out float result))
            {
                value = Mathf.Abs(result);
            }
        }

        /// <summary>
        /// Get the event id from <see cref="string"/> s (0 by default).
        /// </summary>
        protected static int GetEventValue(string s)
        {
            int.TryParse(s, out int value);

            return Mathf.Abs(value);
        }

        /// <summary>
        /// Get the pause duration from <see cref="string"/> s (0 by default).
        /// </summary>
        protected static float GetPauseValue(string s)
        {
            float.TryParse(s, NumberStyles.Float, CultureInfo.InvariantCulture, out float value);

            return Mathf.Abs(value);
        }
    }
}
