﻿using UnityEngine;
using UnityEngine.Events;

namespace PainfulSmile.Experimental
{
    [System.Serializable]
    public sealed class DialogEvent : UnityEvent
    {
        [SerializeField] private bool m_IgnoreOnSkip;

        public bool IgnoreOnSkip => m_IgnoreOnSkip;

        public DialogEvent()
        {
            m_IgnoreOnSkip = false;
        }

        public DialogEvent(bool ignoreOnSkip)
        {
            m_IgnoreOnSkip = ignoreOnSkip;
        }
    }
}
