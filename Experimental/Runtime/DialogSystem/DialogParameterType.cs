﻿namespace PainfulSmile.Experimental
{
    public enum DialogParameterType
    {
        Bool = 1,
        Int,
        Float,
        String
    }
}
