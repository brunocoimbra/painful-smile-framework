﻿using UnityEngine;

namespace PainfulSmile.Experimental
{
    [System.Serializable]
    public struct DialogParameter
    {
        [SerializeField] private DialogParameterType m_Type;
        [SerializeField] private string m_Name;
        [SerializeField] private bool m_BoolValue;
        [SerializeField] private int m_IntValue;
        [SerializeField] private float m_FloatValue;
        [SerializeField] private string m_StringValue;

        public DialogParameterType Type => m_Type;

        public bool BoolValue
        {
            get { return m_BoolValue; }
            set
            {
                if (ValidateType(DialogParameterType.Bool))
                {
                    m_BoolValue = value;
                }
            }
        }
        public int IntValue
        {
            get => m_IntValue;
            set
            {
                if (ValidateType(DialogParameterType.Int))
                {
                    m_IntValue = value;
                }
            }
        }
        public float FloatValue
        {
            get => m_FloatValue;
            set
            {
                if (ValidateType(DialogParameterType.Float))
                {
                    m_FloatValue = value;
                }
            }
        }
        public string Name
        {
            get => m_Name;
            set => m_Name = value;
        }
        public string StringValue
        {
            get => m_StringValue;
            set
            {
                if (ValidateType(DialogParameterType.String))
                {
                    m_StringValue = value;
                }
            }
        }

        public DialogParameter(string name, bool value)
        {
            m_Type = DialogParameterType.Bool;
            m_Name = name;
            m_BoolValue = value;
            m_IntValue = 0;
            m_FloatValue = 0;
            m_StringValue = null;
        }

        public DialogParameter(string name, int value)
        {
            m_Type = DialogParameterType.Int;
            m_Name = name;
            m_BoolValue = false;
            m_IntValue = value;
            m_FloatValue = 0;
            m_StringValue = null;
        }

        public DialogParameter(string name, float value)
        {
            m_Type = DialogParameterType.Float;
            m_Name = name;
            m_BoolValue = false;
            m_IntValue = 0;
            m_FloatValue = value;
            m_StringValue = null;
        }

        public DialogParameter(string name, string value)
        {
            m_Type = DialogParameterType.String;
            m_Name = name;
            m_BoolValue = false;
            m_IntValue = 0;
            m_FloatValue = 0;
            m_StringValue = value;
        }

        private bool ValidateType(DialogParameterType type)
        {
            if (Type == type)
            {
                return true;
            }

            Debug.LogError($"Trying to set parameter of type {Type} with value of type {type}.");

            return false;
        }
    }
}
