﻿using UnityEngine;

namespace PainfulSmile.Experimental
{
    [System.Serializable]
    public abstract class DialogActorBase
    {
        [Tooltip("The default actor's text speed.")]
        [SerializeField, Positive] private float m_DefaultSpeed = 15;
        [Tooltip("Should allow skip?")]
        [SerializeField] private bool m_AllowSkip = true;
        [Tooltip("Events fired while speaking. Use <event=index> to invoke, where index is the event's index in the array.")]
        [SerializeField, UnityEvent] private DialogEvent[] m_DialogEvents = new DialogEvent[0];

        /// <summary>
        /// The actor's name.
        /// </summary>
        public abstract string Name { get; }
        /// <summary>
        /// The actor's text.
        /// </summary>
        public abstract string Text { get; }

        /// <summary>
        /// Should allow skip?
        /// </summary>
        public bool AllowSkip
        {
            get => m_AllowSkip;
            set => m_AllowSkip = value;
        }
        /// <summary>
        /// The default actor's text speed.
        /// </summary>
        public float DefaultSpeed
        {
            get => m_DefaultSpeed;
            set => m_DefaultSpeed = Mathf.Abs(value);
        }
        /// <summary>
        /// Events fired while speaking.
        /// </summary>
        public DialogEvent[] DialogEvents
        {
            get => m_DialogEvents ?? (m_DialogEvents = new DialogEvent[0]);
            set => m_DialogEvents = value ?? new DialogEvent[0];
        }
    }
}
