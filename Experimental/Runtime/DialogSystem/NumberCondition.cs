﻿namespace PainfulSmile.Experimental
{
    public enum NumberCondition
    {
        Equals,
        Differs,
        Greater,
        GreaterOrEquals,
        Less,
        LessOrEquals
    }
}
