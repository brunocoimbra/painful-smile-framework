﻿using UnityEngine;

namespace PainfulSmile.Experimental
{
    public sealed class DialogSystem : MonoBehaviour
    {
        [SerializeField, HideInInspector] private DialogBranchBase m_CurrentBranch;
        [Tooltip("When locked, it becomes impossible to start or continue the dialog, but is still possible to force finish it.")]
        [SerializeField] private bool m_IsLocked;
        [Tooltip("Should start the dialog as soon as possible?")]
        [SerializeField] private bool m_StartOnAwake;
        [Tooltip("Should lock it when finished?")]
        [SerializeField] private bool m_LockOnFinish;
        [Tooltip("All parameters being used for this system.")]
        [SerializeField, HideInInspector] private DialogParameter[] m_Parameters;
        [Tooltip("All transitions available on interact.")]
        [SerializeField, HideInInspector] private DialogTransition[] m_Transitions;

        /// <summary>
        /// Is the dialog currently active?
        /// </summary>
        public bool IsActive => CurrentBranch != null;

        /// <summary>
        /// When locked, it becomes impossible to start or continue the dialog, but is still possible to force finish it.
        /// </summary>
        public bool IsLocked
        {
            get => m_IsLocked;
            set => m_IsLocked = value;
        }
        /// <summary>
        /// Should lock it when finished?
        /// </summary>
        public bool LockOnFinish
        {
            get => m_LockOnFinish;
            set => m_LockOnFinish = value;
        }
        /// <summary>
        /// Should start the dialog as soon as possible?
        /// </summary>
        public bool StartOnAwake
        {
            get => m_StartOnAwake;
            set => m_StartOnAwake = value;
        }

        /// <summary>
        /// The current dialog system's branch.
        /// </summary>
        public DialogBranchBase CurrentBranch
        {
            get => m_CurrentBranch;
            private set => m_CurrentBranch = value;
        }
        /// <summary>
        /// All parameters being used for this system.
        /// </summary>
        public DialogParameter[] Parameters
        {
            get => m_Parameters;
            private set => m_Parameters = value;
        }
        /// <summary>
        /// All transitions available on interact.
        /// </summary>
        public DialogTransition[] Transitions
        {
            get => m_Transitions;
            private set => m_Transitions = value;
        }

        private void Awake()
        {
            if (StartOnAwake)
            {
                Interact();
            }
        }

        /// <summary>
        /// Interact with the dialog. Use it to both start and continue it.
        /// </summary>
        public void Interact()
        {
            if (IsLocked)
            {
                return;
            }

            if (IsActive == false)
            {
                for (int i = 0; i < Transitions.Length; i++)
                {
                    if (Transitions[i].Check(Parameters))
                    {
                        CurrentBranch = Transitions[i].Branch;

                        break;
                    }
                }
            }

            if (CurrentBranch != null)
            {
                CurrentBranch = CurrentBranch.CheckBranch(this);
            }
        }

        /// <summary>
        /// Force quit the dialog.
        /// </summary>
        public void Quit()
        {
            IsLocked = LockOnFinish;

            if (CurrentBranch != null)
            {
                CurrentBranch.ResetBranch();
                CurrentBranch = null;
            }
        }

        /// <summary>
        /// Set a bool parameter value.
        /// </summary>
        public void SetBool(string parameter, bool value)
        {
            for (int i = 0; i < Parameters.Length; i++)
            {
                if (Parameters[i].Type == DialogParameterType.Bool && Parameters[i].Name == parameter)
                {
                    Parameters[i].BoolValue = value;

                    return;
                }
            }

            Debug.LogWarning($"Could not find parameter {parameter} (Bool)", gameObject);
        }

        /// <summary>
        /// Set a int parameter value.
        /// </summary>
        public void SetInt(string parameter, int value)
        {
            for (int i = 0; i < Parameters.Length; i++)
            {
                if (Parameters[i].Type == DialogParameterType.Int && Parameters[i].Name == parameter)
                {
                    Parameters[i].IntValue = value;

                    return;
                }
            }

            Debug.LogWarning($"Could not find parameter {parameter} (Int)", gameObject);
        }

        /// <summary>
        /// Set a float parameter value.
        /// </summary>
        public void SetFloat(string parameter, float value)
        {
            for (int i = 0; i < Parameters.Length; i++)
            {
                if (Parameters[i].Type == DialogParameterType.Float && Parameters[i].Name == parameter)
                {
                    Parameters[i].FloatValue = value;

                    return;
                }
            }

            Debug.LogWarning($"Could not find parameter {parameter} (Float)", gameObject);
        }

        /// <summary>
        /// Set a string parameter value.
        /// </summary>
        public void SetString(string parameter, string value)
        {
            for (int i = 0; i < Parameters.Length; i++)
            {
                if (Parameters[i].Type == DialogParameterType.String && Parameters[i].Name == parameter)
                {
                    Parameters[i].StringValue = value;

                    return;
                }
            }

            Debug.LogWarning($"Could not find parameter {parameter} (String)", gameObject);
        }

        /// <summary>
        /// Get a bool parameter value.
        /// </summary>
        public bool GetBool(string parameter, bool defaultValue = false)
        {
            for (int i = 0; i < Parameters.Length; i++)
            {
                if (Parameters[i].Type == DialogParameterType.Bool && Parameters[i].Name == parameter)
                {
                    return Parameters[i].BoolValue;
                }
            }

            Debug.LogWarning($"Could not find parameter {parameter} (Bool)", gameObject);

            return defaultValue;
        }

        /// <summary>
        /// Get a int parameter value.
        /// </summary>
        public int GetInt(string parameter, int defaultValue = 0)
        {
            for (int i = 0; i < Parameters.Length; i++)
            {
                if (Parameters[i].Type == DialogParameterType.Int && Parameters[i].Name == parameter)
                {
                    return Parameters[i].IntValue;
                }
            }

            Debug.LogWarning($"Could not find parameter {parameter} (Int)", gameObject);

            return defaultValue;
        }

        /// <summary>
        /// Get a float parameter value.
        /// </summary>
        public float GetFloat(string parameter, float defaultValue = 0)
        {
            for (int i = 0; i < Parameters.Length; i++)
            {
                if (Parameters[i].Type == DialogParameterType.Float && Parameters[i].Name == parameter)
                {
                    return Parameters[i].FloatValue;
                }
            }

            Debug.LogWarning($"Could not find parameter {parameter} (Float)", gameObject);

            return defaultValue;
        }

        /// <summary>
        /// Get a string parameter value.
        /// </summary>
        public string GetString(string parameter, string defaultValue = null)
        {
            for (int i = 0; i < Parameters.Length; i++)
            {
                if (Parameters[i].Type == DialogParameterType.String && Parameters[i].Name == parameter)
                {
                    return Parameters[i].StringValue;
                }
            }

            Debug.LogWarning($"Could not find parameter {parameter} (String)", gameObject);

            return defaultValue;
        }
    }
}
