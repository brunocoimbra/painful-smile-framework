﻿ using UnityEngine;

namespace PainfulSmile.Experimental
{
    [System.Serializable]
    public struct DialogCondition
    {
        [SerializeField] private DialogParameterType m_Type;
        [SerializeField] private string m_Name;
        [SerializeField] private bool m_BoolValue;
        [SerializeField] private int m_IntValue;
        [SerializeField] private float m_FloatValue;
        [SerializeField] private string m_StringValue;
        [SerializeField] private NumberCondition m_NumberOption;
        [SerializeField] private StringCondition m_StringOption;

        public DialogParameterType Type { get { return m_Type; } }

        public bool BoolValue
        {
            get => m_BoolValue;
            set
            {
                if (ValidateType(DialogParameterType.Bool))
                {
                    m_BoolValue = value;
                }
            }
        }
        public int IntValue
        {
            get => m_IntValue;
            set
            {
                if (ValidateType(DialogParameterType.Int))
                {
                    m_IntValue = value;
                }
            }
        }
        public float FloatValue
        {
            get => m_FloatValue;
            set
            {
                if (ValidateType(DialogParameterType.Float))
                {
                    m_FloatValue = value;
                }
            }
        }
        public string Name
        {
            get => m_Name;
            set => m_Name = value;
        }
        public string StringValue
        {
            get => m_StringValue;
            set
            {
                if (ValidateType(DialogParameterType.String))
                {
                    m_StringValue = value;
                }
            }
        }
        public NumberCondition NumberCondition
        {
            get => m_NumberOption;
            set
            {
                if (ValidateType(DialogParameterType.Int) || ValidateType(DialogParameterType.Float))
                {
                    m_NumberOption = value;
                }
            }
        }
        public StringCondition StringCondition
        {
            get => m_StringOption;
            set
            {
                if (ValidateType(DialogParameterType.String))
                {
                    m_StringOption = value;
                }
            }
        }

        public DialogCondition(string name, bool value)
        {
            m_Type = DialogParameterType.Bool;
            m_Name = name;
            m_BoolValue = value;
            m_IntValue = 0;
            m_FloatValue = 0;
            m_StringValue = null;
            m_NumberOption = 0;
            m_StringOption = 0;
        }

        public DialogCondition(string name, int value, NumberCondition condition)
        {
            m_Type = DialogParameterType.Int;
            m_Name = name;
            m_BoolValue = false;
            m_IntValue = value;
            m_FloatValue = 0;
            m_StringValue = null;
            m_NumberOption = condition;
            m_StringOption = 0;
        }

        public DialogCondition(string name, float value, NumberCondition condition)
        {
            m_Type = DialogParameterType.Float;
            m_Name = name;
            m_BoolValue = false;
            m_IntValue = 0;
            m_FloatValue = value;
            m_StringValue = null;
            m_NumberOption = condition;
            m_StringOption = 0;
        }

        public DialogCondition(string name, string value, StringCondition condition)
        {
            m_Type = DialogParameterType.String;
            m_Name = name;
            m_BoolValue = false;
            m_IntValue = 0;
            m_FloatValue = 0;
            m_StringValue = value;
            m_NumberOption = 0;
            m_StringOption = condition;
        }

        private bool ValidateType(DialogParameterType type)
        {
            if (Type == type)
            {
                return true;
            }

            Debug.LogError($"Trying to set parameter of type {Type} with value of type {type}.");

            return false;
        }
    }
}
