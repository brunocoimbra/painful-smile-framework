﻿using System.Collections;
using UnityEngine;

#if UNITY_ADS
using UnityEngine.Advertisements;
#endif

namespace PainfulSmile.Experimental
{
    /// <summary>
    /// Class used to manage Unity Ads.
    /// </summary>
    [AddComponentMenu("")]
    public sealed class UnityAds : MonoBehaviour, ISerializationCallbackReceiver
    {
        /// <summary>
        /// Various states that Unity Ads placements can be in.
        /// </summary>
        public enum PlacementState
        {
            /// <summary>
            /// Ads was not implemented.
            /// </summary>
            NotImplemented,
            /// <summary>
            /// Placement was not created.
            /// </summary>
            NoFill,
            /// <summary>
            /// Placement is disabled.
            /// </summary>
            Disabled,
            /// <summary>
            /// Placement not available
            /// </summary>
            NotAvailable,
            /// <summary>
            /// Placement is loading.
            /// </summary>
            Waiting,
            /// <summary>
            /// Placement is ready.
            /// </summary>
            Ready
        }

        /// <summary>
        /// ShowResult is passed to onFinish callback after the advertisement has completed or failed to start.
        /// </summary>
        public enum ShowResult
        {
            /// <summary>
            /// Ads was not implemented.
            /// </summary>
            NotImplemented,
            /// <summary>
            /// Ads is not supported in the current platform.
            /// </summary>
            NotSupported,
            /// <summary>
            /// Ads was not initialized or ready to show.
            /// </summary>
            TimeOut,
            /// <summary>
            /// Ads failed to show.
            /// </summary>
            Failed,
            /// <summary>
            /// Ads was skipped.
            /// </summary>
            Skipped,
            /// <summary>
            /// Ads was watched until the end.
            /// </summary>
            Finished
        }

        public delegate void ShowAdsHandler(ShowResult showResult);

        [SerializeField, HideInInspector] private bool m_Mute;
        [SerializeField, HideInInspector] private float m_ShowTimeLimit;
        [SerializeField, HideInInspector] private UnityAdsSettings m_Settings;
        [SerializeField, HideInInspector] private bool _isCurrent = false;
        [System.NonSerialized] private bool _isInitialized = false;

        /// <summary>
        /// Is currently showing any ads?
        /// </summary>
        public static bool isShowing
        {
            get
            {
#if UNITY_ADS
                return Advertisement.isShowing;
#else
                return false;
#endif
            }
        }
        /// <summary>
        /// The current ads version.
        /// </summary>
        public static string version
        {
            get
            {
#if UNITY_ADS
                return Advertisement.version;
#else
                return "";
#endif
            }
        }
        /// <summary>
        /// The current platform's game ID.
        /// </summary>
        public static string gameId
        {
            get
            {
#if UNITY_ADS
                return Advertisement.gameId;
#else
                return "";
#endif
            }
        }
        /// <summary>
        /// Ads-related settings.
        /// </summary>
        public static UnityAdsSettings settings
        {
            get
            {
                if (current.m_Settings == null)
                {
                    if (UnityAdsSettings.LoadOrCreateInstance(out current.m_Settings) == false)
                    {
                        current.m_Settings.hideFlags = HideFlags.HideAndDontSave;
                    }
                }

                return current.m_Settings;
            }
        }

        private static UnityAds _current;

        /// <summary>
        /// Should the Ads be muted? Needs to set mute variants in Edit -> Project Setting -> Unity Ads.
        /// </summary>
        public static bool mute { get { return current.m_Mute; } set { current.m_Mute = value; } }
        /// <summary>
        /// Time limit until time out. Set -1 to revert to default.
        /// </summary>
        public static float showTimeLimit { get { return current.m_ShowTimeLimit; } set { current.m_ShowTimeLimit = (value >= 0 ? value : settings.showTimeLimit); } }

        private static UnityAds current { get { if (_current == null) { CreateInstance(); } return _current; } }

#if UNITY_ADS
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        private static void RuntimeInitializeOnLoad()
        {
            CreateInstance();
        }
#endif

        private void Awake()
        {
            if (_isInitialized)
            {
                return;
            }

            _isInitialized = true;

#if !UNITY_EDITOR
            Initialize();
#else
            if (Application.isPlaying)
            {
                Initialize();
            }
            else
            {
                DestroyImmediate(gameObject);
            }
#endif
        }

        private void OnDestroy()
        {
            if (_current == this)
            {
                _current = null;
            }

            if (settings.hideFlags == HideFlags.HideAndDontSave)
            {
                DestroyImmediate(settings);
            }
        }

        /// <summary>
        /// Shows a rewarded video.
        /// </summary>
        public static void ShowRewardedVideo(ShowAdsHandler onFinish)
        {
            Show(current.GetRewardedVideoID(mute), onFinish);
        }

        /// <summary>
        /// Shows a skippable video.
        /// </summary>
        public static void ShowSkippableVideo(ShowAdsHandler onFinish)
        {
            Show(current.GetSkippableVideoID(mute), onFinish);
        }

        /// <summary>
        /// Shows a custom video.
        /// </summary>
        public static void Show(string placementId, ShowAdsHandler onFinish)
        {
            current.StartCoroutine(current.PlayVideoCoroutine(placementId, onFinish));
        }

        /// <summary>
        /// Get rewarded video's current state.
        /// </summary>
        public static PlacementState GetRewardedVideoState(bool muteVariant = false)
        {
            return GetState((settings.hasMuteVariant && muteVariant) ? settings.rewardedVideoIDMute : settings.rewardedVideoID);
        }

        /// <summary>
        /// Get skippable video's current state.
        /// </summary>
        public static PlacementState GetSkippableVideoState(bool muteVariant = false)
        {
            return GetState((settings.hasMuteVariant && muteVariant) ? settings.skippableVideoIDMute : settings.skippableVideoID);
        }

        /// <summary>
        /// Get custom video's current state.
        /// </summary>
        public static PlacementState GetState(string placementId)
        {
#if UNITY_ADS
            switch (Advertisement.GetPlacementState(placementId))
            {
                case UnityEngine.Advertisements.PlacementState.Ready:
                    return PlacementState.Ready;

                case UnityEngine.Advertisements.PlacementState.NotAvailable:
                    return PlacementState.NotAvailable;

                case UnityEngine.Advertisements.PlacementState.Disabled:
                    return PlacementState.Disabled;

                case UnityEngine.Advertisements.PlacementState.Waiting:
                    return PlacementState.Waiting;

                case UnityEngine.Advertisements.PlacementState.NoFill:
                    return PlacementState.NoFill;
            }
#endif

            return PlacementState.NotImplemented;
        }

        private static void CreateInstance()
        {
            UnityAds[] __instances = FindObjectsOfType<UnityAds>();

            if (__instances != null && __instances.Length > 0)
            {
                for (int i = 0; i < __instances.Length; i++)
                {
                    DestroyImmediate(__instances[i]);
                }
            }

            new GameObject(typeof(UnityAds).Name).AddComponent<UnityAds>();
        }

        private void Initialize()
        {
            if (_current == null)
            {
                _current = this;
                mute = false;
                showTimeLimit = settings.showTimeLimit;

                DontDestroyOnLoad(gameObject);
                StartCoroutine(InitializeCoroutine());
            }
            else if (_current != this)
            {
                Destroy(gameObject);
            }
        }

        private string GetRewardedVideoID(bool muteVariant = false)
        {
            return ((settings.hasMuteVariant && muteVariant) ? settings.rewardedVideoIDMute : settings.rewardedVideoID);
        }

        private string GetSkippableVideoID(bool muteVariant = false)
        {
            return ((settings.hasMuteVariant && muteVariant) ? settings.skippableVideoIDMute : settings.skippableVideoID);
        }

        private IEnumerator InitializeCoroutine()
        {
#if UNITY_ADS
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();

            if (Advertisement.isInitialized == false && string.IsNullOrEmpty(gameId) == false)
            {
                Advertisement.Initialize(gameId, Advertisement.testMode);
            }
#else
            yield break;
#endif
        }

        private IEnumerator PlayVideoCoroutine(string placementID, ShowAdsHandler onFinish)
        {
#if !UNITY_ADS
            if (onFinish != null)
            {
                onFinish.Invoke(ShowResult.NotImplemented);
            }

            yield break;
#else
            if (Advertisement.isSupported == false || string.IsNullOrEmpty(gameId))
            {
                if (onFinish != null)
                {
                    onFinish.Invoke(ShowResult.NotSupported);
                }

                yield break;
            }

            float __timeOut = Time.time + showTimeLimit;

            while (Advertisement.isInitialized == false || Advertisement.IsReady(placementID) == false)
            {
                if (Time.time > __timeOut)
                {
                    if (onFinish != null)
                    {
                        onFinish.Invoke(ShowResult.TimeOut);
                    }

                    yield break;
                }
                else
                {
                    yield return null;
                }
            }

            Advertisement.Show(placementID, new ShowOptions()
            {
                resultCallback = delegate (UnityEngine.Advertisements.ShowResult showResult)
                {
                    switch (showResult)
                    {
                        case UnityEngine.Advertisements.ShowResult.Failed:
                            if (onFinish != null)
                            {
                                onFinish.Invoke(ShowResult.Failed);
                            }
                            break;

                        case UnityEngine.Advertisements.ShowResult.Skipped:
                            if (onFinish != null)
                            {
                                onFinish.Invoke(ShowResult.Skipped);
                            }
                            break;

                        case UnityEngine.Advertisements.ShowResult.Finished:
                            if (onFinish != null)
                            {
                                onFinish.Invoke(ShowResult.Finished);
                            }
                            break;
                    }
                }
            });
#endif
        }

        void ISerializationCallbackReceiver.OnAfterDeserialize()
        {
            if (_isCurrent)
            {
                _current = this;
            }
        }

        void ISerializationCallbackReceiver.OnBeforeSerialize()
        {
            _isCurrent = (_current == this);
        }
    }
}
