﻿using UnityEngine;

namespace PainfulSmile.Experimental
{
    public sealed class UnityAdsMuteToggle : MonoBehaviour
    {
        public void Toggle()
        {
            UnityAds.mute = !UnityAds.mute;
        }
    }
}
