﻿using UnityEngine;
using UnityEngine.Events;

namespace PainfulSmile.Experimental
{
    public sealed class UnityAdsListener : MonoBehaviour
    {
        public enum AdsType
        {
            RewardedVideo,
            SkippableVideo,
            Custom
        }

        [SerializeField] private AdsType _adsType;
        [SerializeField] private string _placementID;
        [SerializeField] private UnityEvent _onNotImplemented = new UnityEvent();
        [SerializeField] private UnityEvent _onNotSupported = new UnityEvent();
        [SerializeField] private UnityEvent _onTimeOut = new UnityEvent();
        [SerializeField] private UnityEvent _onFailed = new UnityEvent();
        [SerializeField] private UnityEvent _onSkipped = new UnityEvent();
        [SerializeField] private UnityEvent _onFinished = new UnityEvent();

        /// <summary>
        /// The placement ID to be used when adsType is set to Custom.
        /// </summary>
        public string placementID { get { return _placementID; } set { _placementID = value; } }
        public AdsType adsType { get { return _adsType; } set { _adsType = value; } }
        public UnityEvent onNotImplemented { get { return _onNotImplemented ?? (_onNotImplemented = new UnityEvent()); } }
        public UnityEvent onNotSupported { get { return _onNotSupported ?? (_onNotSupported = new UnityEvent()); } }
        public UnityEvent onTimeOut { get { return _onTimeOut ?? (_onTimeOut = new UnityEvent()); } }
        public UnityEvent onFailed { get { return _onFailed ?? (_onFailed = new UnityEvent()); } }
        public UnityEvent onSkipped { get { return _onSkipped ?? (_onSkipped = new UnityEvent()); } }
        public UnityEvent onFinished { get { return _onFinished ?? (_onFinished = new UnityEvent()); } }

        public void Trigger()
        {
            switch (_adsType)
            {
                case AdsType.RewardedVideo:
                    UnityAds.ShowRewardedVideo(HandleVideoFinish);
                    break;

                case AdsType.SkippableVideo:
                    UnityAds.ShowSkippableVideo(HandleVideoFinish);
                    break;

                case AdsType.Custom:
                    UnityAds.Show(_placementID, HandleVideoFinish);
                    break;
            }
        }

        private void HandleVideoFinish(UnityAds.ShowResult result)
        {
            switch (result)
            {
                case UnityAds.ShowResult.NotImplemented:
                    onNotImplemented.Invoke();
                    break;

                case UnityAds.ShowResult.NotSupported:
                    onNotSupported.Invoke();
                    break;

                case UnityAds.ShowResult.TimeOut:
                    onTimeOut.Invoke();
                    break;

                case UnityAds.ShowResult.Failed:
                    onFailed.Invoke();
                    break;

                case UnityAds.ShowResult.Skipped:
                    onSkipped.Invoke();
                    break;

                case UnityAds.ShowResult.Finished:
                    onFinished.Invoke();
                    break;
            }
        }
    }
}
