﻿using UnityEngine;

namespace PainfulSmile.Experimental
{
    /// <summary>
    /// Class used to store Unity Ads settings.
    /// </summary>
    public sealed class UnityAdsSettings : ScriptableObject
    {
        public static class Defaults
        {
            public const string RewardedVideoID = "rewardedVideo";
            public const string RewardedVideoIDMute = "rewardedVideoMute";
            public const string SkippableVideoID = "video";
            public const string SkippableVideoIDMute = "videoMute";
        }

        [Tooltip("Time limit until time out.")]
        [SerializeField] private float _showTimeLimit = 5;
        [Tooltip("The rewarded video ID.")]
        [SerializeField] private string _rewardedVideoID = Defaults.RewardedVideoID;
        [Tooltip("The skippable video ID.")]
        [SerializeField] private string _skippableVideoID = Defaults.SkippableVideoID;
        [Tooltip("Can ads be muted?")]
        [SerializeField] private bool _hasMuteVariant = false;
        [Tooltip("The rewarded video ID for muted ads.")]
        [SerializeField] private string _rewardedVideoIDMute = Defaults.RewardedVideoIDMute;
        [Tooltip("The skippable video ID for muted ads.")]
        [SerializeField] private string _skippableVideoIDMute = Defaults.SkippableVideoIDMute;

        /// <summary>
        /// Can ads be muted?
        /// </summary>
        public bool hasMuteVariant { get { return _hasMuteVariant; } }
        /// <summary>
        /// Time limit until time out.
        /// </summary>
        public float showTimeLimit { get { return _showTimeLimit; } }
        /// <summary>
        /// The rewarded video ID.
        /// </summary>
        public string rewardedVideoID { get { return _rewardedVideoID; } }
        /// <summary>
        /// The rewarded video ID for muted ads.
        /// </summary>
        public string rewardedVideoIDMute { get { return _rewardedVideoIDMute; } }
        /// <summary>
        /// The skippable video ID.
        /// </summary>
        public string skippableVideoID { get { return _skippableVideoID; } }
        /// <summary>
        /// The skippable video ID for muted ads.
        /// </summary>
        public string skippableVideoIDMute { get { return _skippableVideoIDMute; } }

        /// <summary>
        /// Returns true if the instance was loaded, false if created.
        /// </summary>
        public static bool LoadOrCreateInstance(out UnityAdsSettings result)
        {
            result = Resources.Load<UnityAdsSettings>(typeof(UnityAdsSettings).Name);

            if (result == null)
            {
                result = CreateInstance<UnityAdsSettings>();

                return false;
            }

            return true;
        }
    }
}
