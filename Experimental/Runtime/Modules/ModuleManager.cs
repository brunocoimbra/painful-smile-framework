using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PainfulSmile.Experimental
{
    public static class ModuleManagerUtility
    {
        public static ModuleManager.ModuleListDictionary moduleListDictionary { get { return ModuleManager.mooduleListDictionary; } }
    }

    /// <summary>
    /// Class responsible for <seealso cref="Module"/>.
    /// </summary>
    [AddComponentMenu("")]
    public sealed class ModuleManager : Singleton<ModuleManager>
    {
		protected override void OnDispose(bool isInstance) { }
        #region Nested Classes

        [System.Serializable]
        public sealed class ModuleList : SerializableList<Module> { }

        [System.Serializable]
        public sealed class ModuleListDictionary : Dictionary<UpdateMode, ModuleList> { }

        #endregion

        #region Constant Fields

        private static readonly YieldInstruction EndOfFrameInstruction = new WaitForEndOfFrame();
        private static readonly YieldInstruction FixedUpdateInstruction = new WaitForFixedUpdate();

        #endregion

        #region Serialized Fields

        [SerializeField, HideInInspector] private ModuleListDictionary _moduleListDictionary;

        #endregion

        #region Internal Properties

        public static ModuleManager Instance { get { return GetInstance(true); } }
        internal static ModuleListDictionary mooduleListDictionary { get { return GetInstance(true)._moduleListDictionary; } }

        #endregion

        #region Public Methods

        /// <summary>
        /// Loop through all modules.
        /// </summary>
        public static void ForEachModule(System.Action<Module> callback)
        {
            if (callback != null)
            {
                foreach (var pair in mooduleListDictionary)
                {
                    ForEachModule(pair.Value, callback);
                }
            }
        }

        /// <summary>
        /// Loop through all modules with the selected tag.
        /// </summary>
        public static void ForEachModule(System.Action<Module> callback, string tag)
        {
            if (callback != null)
            {
                foreach (var pair in mooduleListDictionary)
                {
                    ForEachModule(pair.Value, callback, tag);
                }
            }
        }

        /// <summary>
        /// Loop through all modules with the referenced context.
        /// </summary>
        public static void ForEachModule(System.Action<Module> callback, GameObject context)
        {
            if (callback != null)
            {
                foreach (var pair in mooduleListDictionary)
                {
                    ForEachModule(pair.Value, callback, context);
                }
            }
        }

        /// <summary>
        /// Loop through all modules with the selected tag with the referenced context.
        /// </summary>
        public static void ForEachModule(System.Action<Module> callback, string tag, GameObject context)
        {
            if (callback != null)
            {
                foreach (var pair in mooduleListDictionary)
                {
                    ForEachModule(pair.Value, callback, tag, context);
                }
            }
        }

        /// <summary>
        /// Loop through all modules within the selected update.
        /// </summary>
        public static void ForEachModule(System.Action<Module> callback, UpdateMode updateMode)
        {
            if (callback != null)
            {
                ForEachModule(mooduleListDictionary[updateMode], callback);
            }
        }

        /// <summary>
        /// Loop through all modules within the selected update with the selected tag.
        /// </summary>
        public static void ForEachModule(System.Action<Module> callback, UpdateMode updateMode, string tag)
        {
            if (callback != null)
            {
                ForEachModule(mooduleListDictionary[updateMode], callback, tag);
            }
        }

        /// <summary>
        /// Loop through all modules within the selected update with the referenced context.
        /// </summary>
        public static void ForEachModule(System.Action<Module> callback, UpdateMode updateMode, GameObject context)
        {
            if (callback != null)
            {
                ForEachModule(mooduleListDictionary[updateMode], callback, context);
            }
        }

        /// <summary>
        /// Loop through all modules within the selected update with the selected tag with the referenced context.
        /// </summary>
        public static void ForEachModule(System.Action<Module> callback, UpdateMode updateMode, string tag, GameObject context)
        {
            if (callback != null)
            {
                ForEachModule(mooduleListDictionary[updateMode], callback, tag, context);
            }
        }

        /// <summary>
        /// Play a module.
        /// </summary>
        public static void Play(Module module)
        {
            module.Play();
        }

        /// <summary>
        /// Play a module with the selected update mode.
        /// </summary>
        public static void Play(Module module, UpdateMode updateMode)
        {
            module.Play(updateMode);
        }

        /// <summary>
        /// Play a new module using an existing reference.
        /// </summary>
        public static void Play(ref Module source, Module value)
        {
            source.Stop();
            source = value;
            source.Play();
        }

        /// <summary>
        /// Play a new module using an existing reference with the selected update mode.
        /// </summary>
        public static void Play(ref Module source, Module value, UpdateMode updateMode)
        {
            source.Stop();
            source = value;
            source.Play(updateMode);
        }

        #endregion

        #region Internal Methods

        internal static void Add(Module module)
        {
            Add(mooduleListDictionary[module.updateMode], module);
        }

        internal static void Remove(Module module)
        {
            Remove(mooduleListDictionary[module.updateMode], module);
        }

        #endregion

        #region Protected Methods

        protected override void OnInitialize()
        {
            _moduleListDictionary = new ModuleListDictionary()
            {
                { UpdateMode.Update, new ModuleList() },
                { UpdateMode.FixedUpdate, new ModuleList() },
                { UpdateMode.EndOfFrame, new ModuleList() },
                { UpdateMode.Manual, new ModuleList() }
            };

            DontDestroyOnLoad(gameObject);
            StartCoroutine(IUpdate());
            StartCoroutine(IFixedUpdate());
            StartCoroutine(IEndOfFrame());
        }

        #endregion

        #region Private Methods

        private static void Add(ModuleList list, Module module)
        {
            if (list.Contains(module) == false)
            {
                list.Add(module);
            }
        }

        private static void ForEachModule(ModuleList list, System.Action<Module> callback)
        {
            for (int i = list.Count - 1; i >= 0; i--)
            {
                if (list[i] != null)
                {
                    callback.Invoke(list[i]);
                }
                else
                {
                    list.RemoveAt(i);
                }
            }
        }

        private static void ForEachModule(ModuleList list, System.Action<Module> callback, string tag)
        {
            if (string.IsNullOrEmpty(tag))
            {
                for (int i = list.Count - 1; i >= 0; i--)
                {
                    if (list[i] != null)
                    {
                        if (string.IsNullOrEmpty(list[i].tag))
                        {
                            callback.Invoke(list[i]);
                        }
                    }
                    else
                    {
                        list.RemoveAt(i);
                    }
                }
            }
            else
            {
                for (int i = list.Count - 1; i >= 0; i--)
                {
                    if (list[i] != null)
                    {
                        if (list[i].tag == tag)
                        {
                            callback.Invoke(list[i]);
                        }
                    }
                    else
                    {
                        list.RemoveAt(i);
                    }
                }
            }
        }

        private static void ForEachModule(ModuleList list, System.Action<Module> callback, GameObject context)
        {
            for (int i = list.Count - 1; i >= 0; i--)
            {
                if (list[i] != null)
                {
                    if (list[i].context == context)
                    {
                        callback.Invoke(list[i]);
                    }
                }
                else
                {
                    list.RemoveAt(i);
                }
            }
        }

        private static void ForEachModule(ModuleList list, System.Action<Module> callback, string tag, GameObject context)
        {
            if (string.IsNullOrEmpty(tag))
            {
                for (int i = list.Count - 1; i >= 0; i--)
                {
                    if (list[i] != null)
                    {
                        if (string.IsNullOrEmpty(list[i].tag) && list[i].context == context)
                        {
                            callback.Invoke(list[i]);
                        }
                    }
                    else
                    {
                        list.RemoveAt(i);
                    }
                }
            }
            else
            {
                for (int i = list.Count - 1; i >= 0; i--)
                {
                    if (list[i] != null)
                    {
                        if (list[i].tag == tag && list[i].context == context)
                        {
                            callback.Invoke(list[i]);
                        }
                    }
                    else
                    {
                        list.RemoveAt(i);
                    }
                }
            }
        }

        private static void Remove(ModuleList list, Module module)
        {
            list.Remove(module);
        }

        private static void UpdateModule(Module module)
        {
            module.Update();
        }

        private IEnumerator IEndOfFrame()
        {
            while (true)
            {
                yield return EndOfFrameInstruction;

                ForEachModule(mooduleListDictionary[UpdateMode.EndOfFrame], UpdateModule);
            }
        }

        private IEnumerator IFixedUpdate()
        {
            while (true)
            {
                yield return FixedUpdateInstruction;

                ForEachModule(mooduleListDictionary[UpdateMode.FixedUpdate], UpdateModule);
            }
        }

        private IEnumerator IUpdate()
        {
            while (true)
            {
                yield return null;

                ForEachModule(mooduleListDictionary[UpdateMode.Update], UpdateModule);
            }
        }

        #endregion
    }
}
