﻿using UnityEngine;
using UnityEngine.Events;

namespace PainfulSmile.Experimental
{
    /// <summary>
    /// Base class for all <seealso cref="Tween"/>'s behaviours.
    /// </summary>
    public abstract class Tweener<T> : TimeModuleBehaviour<T> where T : Tween
    {
        #region Serialized Fields

        [Tooltip("The easing mode for the tween.")]
        [SerializeField] private EaseMode _easeMode = EaseMode.NormalizedCurve;
        [Tooltip("The easing curve for the tween.")]
        [SerializeField] private AnimationCurve _easeCurve = AnimationCurve.Linear(0, 0, 1, 1);
        [Tooltip("Event fired each frame the tweeen updates.")]
        [SerializeField] private Tween.UpdateTimeEvent _onUpdateTime = new Tween.UpdateTimeEvent();
        [Tooltip("Event fired when the tween starts going back on ping pong.")]
        [SerializeField] private UnityEvent _onPingPong = new UnityEvent();

        #endregion

        #region Public Events

        /// <summary>
        /// Event fired each frame the tweeen updates.
        /// </summary>
        public event Tween.UpdateTimeHandler onUpdateTime { add { settings.onUpdateTime += value; } remove { settings.onUpdateTime -= value; } }
        /// <summary>
        /// Event fired when the tween starts going back on ping pong.
        /// </summary>
        public event System.Action onPingPong { add { settings.onPingPong += value; } remove { settings.onPingPong -= value; } }

        #endregion

        #region Public Properties

        /// <summary>
        /// Is the tween current going back?
        /// </summary>
        public bool isGoingBack { get { return settings.isGoingBack; } set { settings.isGoingBack = value; } }
        /// <summary>
        /// Should the tween be ping pong like?
        /// </summary>
        public bool isPingPong { get { return settings.isPingPong; } set { settings.isPingPong = value; } }
        /// <summary>
        /// The easing mode for the tween.
        /// </summary>
        public EaseMode easeMode
        {
            get { return _easeMode; }
            set
            {
                if (_easeMode != value)
                {
                    _easeMode = value;

                    UpdateEaseMode();
                }
            }
        }
        /// <summary>
        /// The easing curve for the tween.
        /// </summary>
        public AnimationCurve easeCurve { get { return _easeCurve; } set { UpdateEaseCurve(value); } }

        #endregion

        #region Public Methods

        /// <summary>
        /// Reset all events.
        /// </summary>
        public override void ResetEvents()
        {
            base.ResetEvents();

            settings.onPingPong += HandlePingPong;
            settings.onPingPong += _onPingPong.Invoke;

            settings.onUpdateTime += HandleUpdateTime;
            settings.onUpdateTime += _onUpdateTime.Invoke;
        }

        #endregion

        #region Protected Methods

        protected override void OnUnityAwake()
        {
            UpdateEaseMode();

            base.OnUnityAwake();
        }

        protected virtual void HandlePingPong() { }

        protected virtual void HandleUpdateTime(float unclampedTime) { }

        protected void UpdateEaseCurve(AnimationCurve easeCurve)
        {
            this.easeCurve = (easeCurve ?? AnimationCurve.Linear(0, 0, 1, 1));

            if (_easeMode == EaseMode.NormalizedCurve)
            {
                settings.onEvaluateTime = this.easeCurve.Evaluate;
            }
        }

        protected void UpdateEaseMode()
        {
            switch (_easeMode)
            {
                case EaseMode.NormalizedCurve:
                    settings.onEvaluateTime = easeCurve.Evaluate;
                    break;

                case EaseMode.Linear:
                    settings.onEvaluateTime = Ease.Linear;
                    break;

                case EaseMode.Sine:
                    settings.onEvaluateTime = Ease.Sine;
                    break;

                case EaseMode.SineIn:
                    settings.onEvaluateTime = Ease.SineIn;
                    break;

                case EaseMode.SineOut:
                    settings.onEvaluateTime = Ease.SineOut;
                    break;

                case EaseMode.Quadratic:
                    settings.onEvaluateTime = Ease.Quadratic;
                    break;

                case EaseMode.QuadraticIn:
                    settings.onEvaluateTime = Ease.QuadraticIn;
                    break;

                case EaseMode.QuadraticOut:
                    settings.onEvaluateTime = Ease.QuadraticOut;
                    break;

                case EaseMode.Cubic:
                    settings.onEvaluateTime = Ease.Cubic;
                    break;

                case EaseMode.CubicIn:
                    settings.onEvaluateTime = Ease.CubicIn;
                    break;

                case EaseMode.CubicOut:
                    settings.onEvaluateTime = Ease.CubicOut;
                    break;

                case EaseMode.Quartic:
                    settings.onEvaluateTime = Ease.Quartic;
                    break;

                case EaseMode.QuarticIn:
                    settings.onEvaluateTime = Ease.QuarticIn;
                    break;

                case EaseMode.QuarticOut:
                    settings.onEvaluateTime = Ease.QuarticOut;
                    break;

                case EaseMode.Quintic:
                    settings.onEvaluateTime = Ease.Quintic;
                    break;

                case EaseMode.QuinticIn:
                    settings.onEvaluateTime = Ease.QuinticIn;
                    break;

                case EaseMode.QuinticOut:
                    settings.onEvaluateTime = Ease.QuinticOut;
                    break;

                case EaseMode.Exponential:
                    settings.onEvaluateTime = Ease.Exponential;
                    break;

                case EaseMode.ExponentialIn:
                    settings.onEvaluateTime = Ease.ExponentialIn;
                    break;

                case EaseMode.ExponentialOut:
                    settings.onEvaluateTime = Ease.ExponentialOut;
                    break;

                case EaseMode.Circular:
                    settings.onEvaluateTime = Ease.Circular;
                    break;

                case EaseMode.CircularIn:
                    settings.onEvaluateTime = Ease.CircularIn;
                    break;

                case EaseMode.CircularOut:
                    settings.onEvaluateTime = Ease.CircularOut;
                    break;

                case EaseMode.Bounce:
                    settings.onEvaluateTime = Ease.Bounce;
                    break;

                case EaseMode.BounceIn:
                    settings.onEvaluateTime = Ease.BounceIn;
                    break;

                case EaseMode.BounceOut:
                    settings.onEvaluateTime = Ease.BounceOut;
                    break;

                case EaseMode.Back:
                    settings.onEvaluateTime = Ease.Back;
                    break;

                case EaseMode.BackIn:
                    settings.onEvaluateTime = Ease.BackIn;
                    break;

                case EaseMode.BackOut:
                    settings.onEvaluateTime = Ease.BackOut;
                    break;

                case EaseMode.Elastic:
                    settings.onEvaluateTime = Ease.Elastic;
                    break;

                case EaseMode.ElasticIn:
                    settings.onEvaluateTime = Ease.ElasticIn;
                    break;

                case EaseMode.ElasticOut:
                    settings.onEvaluateTime = Ease.ElasticOut;
                    break;

                default:
                    Debug.LogErrorFormat("{0} not implemented.", easeMode);
                    break;
            }
        }

        #endregion
    }

    /// <summary>
    /// <seealso cref="Tween"/>'s behaviour. Use <seealso cref="Tweener{T}"/> to create your own.
    /// </summary>
    public sealed class Tweener : Tweener<Tween> { }
}
