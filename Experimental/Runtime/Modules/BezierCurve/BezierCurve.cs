﻿using System.Collections.Generic;
using UnityEngine;

namespace PainfulSmile.Experimental
{
    public sealed class BezierCurve : MonoBehaviour
    {
        #region Nested Enums

        public enum GizmosMode
        {
            None,
            Selected,
            Always
        }

        [System.Flags]
        public enum GizmosLines
        {
            Binormal = 1 << 0,
            Normal = 1 << 1,
            Tangent = 1 << 2
        }

        #endregion

        #region Serialized Fields

        [Tooltip("The gizmos visibility mode.")]
        [SerializeField] private GizmosMode _gizmosMode = GizmosMode.Always;
        [Tooltip("Debug point visible lines.")]
        [SerializeField, EnumFlags] private GizmosLines _gizmosLines = GizmosLines.Tangent;
        [SerializeField] private Color _curveColor = Color.green;
        [SerializeField] private Color _startColor = Color.red;
        [SerializeField] private Color _endColor = Color.blue;
        [Tooltip("Debug point normalized time.")]
        [SerializeField, Range(0f, 1f)] float _gizmosTime = 0.5f;
        [Tooltip("Force the curve to only have 2 dimensions.")]
        [SerializeField] private bool _force2D = true;
        [Tooltip("Should the curve end in the same point it starts?")]
        [SerializeField] private bool _isClosed;
        [Tooltip("The amount of points per segments.")]
        [SerializeField, NotLessThan(2)] private int _pointsPerSegment = 16;
        [Tooltip("The total length of the curve.")]
        [SerializeField, ReadOnly] private float _totalLength;
        [Tooltip("HThe total amount of points in the curve .")]
        [SerializeField, ReadOnly] private int _totalPoints;
        [Tooltip("List of key points in the curve.")]
        [SerializeField] private List<BezierPoint> _keyPoints = new List<BezierPoint>();
        [SerializeField] private bool _isDirty;
        [SerializeField] private float[] _lengths;
        [SerializeField] private Vector3[] _points;

        #endregion

        #region Public Events

        public event System.Action onUpdated;

        #endregion

        #region Public Properties

        /// <summary>
        /// Get a key point of the curve.
        /// </summary>
        public BezierPoint this[int index] { get { CheckDirty(); return _keyPoints[index]; } }

        /// <summary>
        /// The count of key points in the curve.
        /// </summary>
        public int count { get { CheckDirty(); return _keyPoints.Count; } }
        /// <summary>
        /// The total amount of points in the curve .
        /// </summary>
        public int totalPoints { get { CheckDirty(); return _totalPoints; } }
        /// <summary>
        /// The total length of the curve.
        /// </summary>
        public float totalLength { get { CheckDirty(); return _totalLength; } }
        /// <summary>
        /// Was the curve modified since the last time it was updated?
        /// </summary>
        public bool isDirty
        {
            get
            {
                for (int i = 0; i < _keyPoints.Count; i++)
                {
                    if (_keyPoints[i].isDirty)
                    {
                        return true;
                    }
                }

                return _isDirty || transform.hasChanged;
            }
        }

        /// <summary>
        /// Force the curve to only have 2 dimensions.
        /// </summary>
        public bool force2D { get { return _force2D; } set { if (_force2D != value) { _force2D = value; SetDirty(); } } }
        /// <summary>
        /// Should the curve end in the same point it starts?
        /// </summary>
        public bool isClosed { get { return _isClosed; } set { if (_isClosed != value) { _isClosed = value; SetDirty(); } } }
        /// <summary>
        /// The amount of points per segments.
        /// </summary>
        public int pointsPerSegment { get { return _pointsPerSegment; } set { if (_pointsPerSegment != value) { _pointsPerSegment = value; SetDirty(); } } }
        /// <summary>
        /// Debug point visible lines.
        /// </summary>
        public GizmosLines gizmosLines { get { return _gizmosLines; } set { _gizmosLines = value; } }
        /// <summary>
        /// The gizmos visibility mode.
        /// </summary>
        public GizmosMode gizmosMode { get { return _gizmosMode; } set { _gizmosMode = value; } }

        #endregion

        #region Unity Callbacks

#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            UnityDrawGizmos();
        }

        private void OnDrawGizmosSelected()
        {
            UnityDrawGizmosSelected();
        }
#endif

        #endregion

        #region Public Methods

        /// <summary>
        /// Get the length of a curve.
        /// </summary>
        public static float Length(BezierPoint startPoint, BezierPoint endPoint, int resolution)
        {
            return Length(startPoint.position, endPoint.position, startPoint.tangentBWorldPosition, endPoint.tangentAWorldPosition, resolution);
        }

        /// <summary>
        /// Get the length of a curve.
        /// </summary>
        public static float Length(Vector3 startPosition, Vector3 endPosition, Vector3 startTangent, Vector3 endTangent, int resolution)
        {
            float length = 0f;
            Vector3 fromPoint = Position(0f, startPosition, endPosition, startTangent, endTangent);

            for (int i = 0; i < resolution; i++)
            {
                float time = (i + 1) / (float)resolution;
                Vector3 toPoint = Position(time, startPosition, endPosition, startTangent, endTangent);
                length += Vector3.Distance(fromPoint, toPoint);
                fromPoint = toPoint;
            }

            return length;
        }

        /// <summary>
        /// Get the rotation in the specified time.
        /// </summary>
        public static Quaternion Rotation(float time, Vector3 up, BezierPoint startPoint, BezierPoint endPoint)
        {
            return Rotation(time, up, startPoint.position, endPoint.position, startPoint.tangentBWorldPosition, endPoint.tangentAWorldPosition);
        }

        /// <summary>
        /// Get the rotation in the specified time.
        /// </summary>
        public static Quaternion Rotation(float time, Vector3 up, Vector3 startPosition, Vector3 endPosition, Vector3 startTangent, Vector3 endTangent)
        {
            Vector3 tangent = Tangent(time, startPosition, endPosition, startTangent, endTangent);
            Vector3 binormal = Vector3.Cross(up, tangent).normalized;
            Vector3 normal = Vector3.Cross(tangent, binormal).normalized;

            return Quaternion.LookRotation(tangent, normal);
        }

        /// <summary>
        /// Get the binormal in the specified time.
        /// </summary>
        public static Vector3 Binormal(float time, Vector3 up, BezierPoint startPoint, BezierPoint endPoint)
        {
            return Binormal(time, up, startPoint.position, endPoint.position, startPoint.tangentBWorldPosition, endPoint.tangentAWorldPosition);
        }

        /// <summary>
        /// Get the binormal in the specified time.
        /// </summary>
        public static Vector3 Binormal(float time, Vector3 up, Vector3 startPosition, Vector3 endPosition, Vector3 startTangent, Vector3 endTangent)
        {
            Vector3 tangent = Tangent(time, startPosition, endPosition, startTangent, endTangent);

            return Vector3.Cross(up, tangent).normalized;
        }

        /// <summary>
        /// Get the normal in the specified time.
        /// </summary>
        public static Vector3 Normal(float time, Vector3 up, BezierPoint startPoint, BezierPoint endPoint)
        {
            return Normal(time, up, startPoint.position, endPoint.position, startPoint.tangentBWorldPosition, endPoint.tangentAWorldPosition);
        }

        /// <summary>
        /// Get the normal in the specified time.
        /// </summary>
        public static Vector3 Normal(float time, Vector3 up, Vector3 startPosition, Vector3 endPosition, Vector3 startTangent, Vector3 endTangent)
        {
            Vector3 tangent = Tangent(time, startPosition, endPosition, startTangent, endTangent);
            Vector3 binormal = Vector3.Cross(up, tangent).normalized;

            return Vector3.Cross(tangent, binormal).normalized;
        }

        /// <summary>
        /// Get the position in the specified time.
        /// </summary>
        public static Vector3 Position(float time, BezierPoint startPoint, BezierPoint endPoint)
        {
            return Position(time, startPoint.position, endPoint.position, startPoint.tangentBWorldPosition, endPoint.tangentAWorldPosition);
        }

        /// <summary>
        /// Get the position in the specified time.
        /// </summary>
        public static Vector3 Position(float time, Vector3 startPosition, Vector3 endPosition, Vector3 startTangent, Vector3 endTangent)
        {
            float timeQuad = time * time;
            float timeCubic = timeQuad * time;
            float inverse = 1f - time;
            float inverseQuad = inverse * inverse;
            float inverseCubic = inverseQuad * inverse;

            return (inverseCubic) * startPosition + (3f * inverseQuad * time) * startTangent + (3f * inverse * timeQuad) * endTangent + (timeCubic) * endPosition;
        }

        /// <summary>
        /// Get the tangent (forward) in the specified time.
        /// </summary>
        public static Vector3 Tangent(float time, BezierPoint startPoint, BezierPoint endPoint)
        {
            return Tangent(time, startPoint.position, endPoint.position, startPoint.tangentBWorldPosition, endPoint.tangentAWorldPosition);
        }

        /// <summary>
        /// Get the tangent (forward) in the specified time.
        /// </summary>
        public static Vector3 Tangent(float time, Vector3 startPosition, Vector3 endPosition, Vector3 startTangent, Vector3 endTangent)
        {
            float t = time;
            float u = 1f - t;
            float u2 = u * u;
            float t2 = t * t;
            Vector3 tangent = (-u2) * startPosition +
                              (u * (u - 2f * t)) * startTangent -
                              (t * (t - 2f * u)) * endTangent +
                              (t2) * endPosition;

            return tangent.normalized;
        }

        /// <summary>
        /// Get all segments in curve.
        /// </summary>
        public static Vector3[] Segment(BezierPoint startPoint, BezierPoint endPoint, int resolution)
        {
            return Segment(startPoint.position, endPoint.position, startPoint.tangentBWorldPosition, endPoint.tangentAWorldPosition, resolution);
        }

        /// <summary>
        /// Get all segments in curve.
        /// </summary>
        public static Vector3[] Segment(Vector3 startPosition, Vector3 endPosition, Vector3 startTangent, Vector3 endTangent, int resolution)
        {
            Vector3[] positions = new Vector3[resolution];

            for (int i = 0; i < resolution; i++)
            {
                positions[i] = Position(((float)i) / (resolution - 1), startPosition, endPosition, startTangent, endTangent);
            }

            return positions;
        }

        /// <summary>
        /// Add a key point at the end of the curve
        /// </summary>
        public void AddKeyPoint(BezierPoint point)
        {
            AddKeyPointAt(point, _keyPoints.Count);
        }

        /// <summary>
        /// Add a key point at a specified index
        /// </summary>
        public void AddKeyPointAt(BezierPoint point, int index)
        {
            _keyPoints.Insert(Mathf.Min(index, _keyPoints.Count), point);
            SetDirty();
        }

        /// <summary>
        /// Force the curve to be updated.
        /// </summary>
        public void ForceUpdate()
        {
            _isDirty = false;
            transform.hasChanged = false;

            UpdatePoints();

            if (_keyPoints.Count < 2)
            {
                ResetLength();

                if (onUpdated != null)
                {
                    onUpdated.Invoke();
                }

                return;
            }

            UpdateLength();

            if (onUpdated != null)
            {
                onUpdated.Invoke();
            }
        }

        /// <summary>
        /// Get the segment in the specified time.
        /// </summary>
        /// <param name="relativeTime">The segment-relative time.</param>
        public void GetCubicSegment(float time, out BezierPoint startPoint, out BezierPoint endPoint, out float relativeTime)
        {
            CheckDirty();

            startPoint = null;
            endPoint = null;
            relativeTime = 0f;

            if (_keyPoints.Count == 0)
            {
                return;
            }
            else if (_keyPoints.Count == 1)
            {
                endPoint = _keyPoints[_keyPoints.Count - 1];
                startPoint = endPoint;

                return;
            }

            float sergmentPercent = 0f;
            float totalPercent = 0f;

            for (int i = 0; i < _keyPoints.Count - 1; i++)
            {
                sergmentPercent = Length(_keyPoints[i], _keyPoints[i + 1], pointsPerSegment) / totalLength;

                if (sergmentPercent + totalPercent > time)
                {
                    startPoint = _keyPoints[i];
                    endPoint = _keyPoints[i + 1];

                    break;
                }

                totalPercent += sergmentPercent;
            }

            if (endPoint == null)
            {
                if (_isClosed)
                {
                    sergmentPercent = Length(_keyPoints[_keyPoints.Count - 1], _keyPoints[0], pointsPerSegment) / totalLength;
                    startPoint = _keyPoints[_keyPoints.Count - 1];
                    endPoint = _keyPoints[0];
                }
                else
                {
                    totalPercent -= sergmentPercent;
                    startPoint = _keyPoints[_keyPoints.Count - 2];
                    endPoint = _keyPoints[_keyPoints.Count - 1];
                }
            }

            relativeTime = (time - totalPercent) / sergmentPercent;
        }

        /// <summary>
        /// Remove a key point at a specified index
        /// </summary>
        public void RemoveKeyPoint(BezierPoint point, bool destroy = true)
        {
            if (_keyPoints.Remove(point))
            {
                SetDirty();
            }

            if (destroy && point != null)
            {
                Destroy(point.gameObject);
            }
        }

        /// <summary>
        /// Remove a key point at a specified index
        /// </summary>
        public void RemoveKeyPointAt(int index, bool destroy = true)
        {
            if (index >= _keyPoints.Count)
            {
                return;
            }

            BezierPoint point = _keyPoints[index];
            _keyPoints.RemoveAt(index);
            SetDirty();

            if (destroy && point != null)
            {
                Destroy(point.gameObject);
            }
        }

        /// <summary>
        /// Mark the curve as modified.
        /// </summary>
        public void SetDirty()
        {
            _isDirty = true;
        }

        /// <summary>
        /// Does the curve have the key point?
        /// </summary>
        public bool ContainsKeyPoint(BezierPoint point)
        {
            return _keyPoints.Contains(point);
        }

        /// <summary>
        /// Get the index of key point.
        /// </summary>
        public int GetKeyPointIndex(BezierPoint point)
        {
            return _keyPoints.IndexOf(point);
        }

        /// <summary>
        /// Get the key point displacement in curve.
        /// </summary>
        /// <param name="index">The key point's index.</param>
        public float GetKeyPointDisplacement(int index)
        {
            CheckDirty();

            if (index == 0 || _keyPoints.Count == 0 || _keyPoints.Count == 1)
            {
                return 0;
            }

            float displacement = 0;

            for (int i = 1; i < _keyPoints.Count; i++)
            {
                displacement += Length(_keyPoints[i - 1], _keyPoints[i], pointsPerSegment);

                if (i == index)
                {
                    return displacement;
                }
            }

            return displacement;
        }

        /// <summary>
        /// Get the key point normalized time in curve.
        /// </summary>
        /// <param name="index">The key point's index.</param>
        public float GetKeyPointTime(int index)
        {
            CheckDirty();

            if (index == 0 || _keyPoints.Count == 0 || _keyPoints.Count == 1)
            {
                return 0;
            }

            float displacement = 0;

            for (int i = 1; i < _keyPoints.Count; i++)
            {
                displacement += Length(_keyPoints[i - 1], _keyPoints[i], pointsPerSegment);

                if (i == index)
                {
                    return Mathf.Clamp01(displacement / totalLength);
                }
            }

            return Mathf.Clamp01(displacement / totalLength);
        }

        /// <summary>
        /// Get the rotation in the specified time.
        /// </summary>
        public Quaternion GetRotation(float time, Vector3 up)
        {
            if (_keyPoints.Count == 0)
            {
                return transform.rotation;
            }

            BezierPoint startPoint;
            BezierPoint endPoint;
            float relativeTime;

            GetCubicSegment(time, out startPoint, out endPoint, out relativeTime);

            return Rotation(relativeTime, up, startPoint, endPoint);
        }

        /// <summary>
        /// Get the binormal in the specified time.
        /// </summary>
        public Vector3 GetBinormal(float time, Vector3 up)
        {
            if (_keyPoints.Count == 0)
            {
                return transform.right;
            }

            BezierPoint startPoint;
            BezierPoint endPoint;
            float relativeTime;

            GetCubicSegment(time, out startPoint, out endPoint, out relativeTime);

            return Binormal(relativeTime, up, startPoint, endPoint);
        }

        /// <summary>
        /// Get the normal in the specified time.
        /// </summary>
        public Vector3 GetNormal(float time, Vector3 up)
        {
            if (_keyPoints.Count == 0)
            {
                return transform.up;
            }

            BezierPoint startPoint;
            BezierPoint endPoint;
            float relativeTime;

            GetCubicSegment(time, out startPoint, out endPoint, out relativeTime);

            return Normal(relativeTime, up, startPoint, endPoint);
        }

        /// <summary>
        /// Get the position in the specified time.
        /// </summary>
        public Vector3 GetPosition(float time)
        {
            return GetPositionAt(Mathf.Lerp(0, totalLength, time));
        }

        /// <summary>
        /// Get the position in the specified time.
        /// </summary>
        /// <param name="positionIndex">Use it to only check positions after the specified index.</param>
        public Vector3 GetPosition(float time, ref int positionIndex)
        {
            return GetPositionAt(Mathf.Lerp(0, totalLength, time), ref positionIndex);
        }

        /// <summary>
        /// Get the position with the displacement from the start point.
        /// </summary>
        public Vector3 GetPositionAt(float displacement)
        {
            CheckDirty();

            if (_points.Length < 2 || displacement <= 0)
            {
                return _points[0];
            }

            if (displacement >= totalLength)
            {
                return _points[_points.Length - 1];
            }

            for (int i = 0; i < _lengths.Length - 1; i++)
            {
                if (_lengths[i + 1] > displacement)
                {
                    return Vector3.Lerp(_points[i], _points[i + 1], Mathf.InverseLerp(_lengths[i], _lengths[i + 1], displacement));
                }
            }

            return _points[_points.Length - 1];
        }

        /// <summary>
        /// Get the position with the displacement from the start point.
        /// </summary>
        /// <param name="positionIndex">Use it to only check positions after the specified index.</param>
        public Vector3 GetPositionAt(float displacement, ref int positionIndex)
        {
            CheckDirty();

            if (_points.Length < 2 || displacement <= 0)
            {
                positionIndex = 0;

                return _points[positionIndex];
            }

            if (displacement >= totalLength || positionIndex >= _points.Length)
            {
                positionIndex = _points.Length - 1;

                return _points[positionIndex];
            }

            positionIndex = Mathf.Max(0, positionIndex);

            for (int i = positionIndex; i < _lengths.Length - 1; i++)
            {
                if (_lengths[i + 1] > displacement)
                {
                    positionIndex = i;
                    break;
                }
            }

            return Vector3.Lerp(_points[positionIndex], _points[positionIndex + 1], Mathf.InverseLerp(_lengths[positionIndex], _lengths[positionIndex + 1], displacement));
        }

        /// <summary>
        /// Get the tangent (forward) in the specified time.
        /// </summary>
        public Vector3 GetTangent(float time)
        {
            if (_keyPoints.Count == 0)
            {
                return transform.forward;
            }

            BezierPoint startPoint;
            BezierPoint endPoint;
            float relativeTime;

            GetCubicSegment(time, out startPoint, out endPoint, out relativeTime);

            return Tangent(relativeTime, startPoint, endPoint);
        }

        /// <summary>
        /// Add a key point at the end of the curve
        /// </summary>
        public BezierPoint AddKeyPoint()
        {
            return AddKeyPointAt(_keyPoints.Count);
        }

        /// <summary>
        /// Add a key point at a specified index
        /// </summary>
        public BezierPoint AddKeyPointAt(int index)
        {
            index = Mathf.Min(index, _keyPoints.Count);

            BezierPoint point = BezierPoint.CreateInstance(this);

            if (_keyPoints.Count == 0)
            {
                point.localPosition = Vector3.zero;
            }
            else if (_keyPoints.Count == 1)
            {
                point.localPosition = _keyPoints[0].localPosition + Vector3.right;
            }
            else
            {
                if (index == 0)
                {
                    point.position = (_keyPoints[0].position - _keyPoints[1].position).normalized + _keyPoints[0].position;
                }
                else if (index == _keyPoints.Count)
                {
                    point.position = (_keyPoints[index - 1].position - _keyPoints[index - 2].position).normalized + _keyPoints[index - 1].position;
                }
                else
                {
                    point.position = Position(0.5f, _keyPoints[index - 1], _keyPoints[index]);
                }
            }

            _keyPoints.Insert(index, point);
            SetDirty();

            return point;
        }

        #endregion

        #region Private Methods

        private void DrawCurve()
        {
            CheckDirty();

            for (int i = 0; i < _keyPoints.Count; i++)
            {
                if (_keyPoints[i] == null)
                {
                    ForceUpdate();
                    break;
                }
            }

            Color color = Gizmos.color;
            {
                if (_keyPoints.Count > 1)
                {
                    Gizmos.color = _curveColor;

                    for (int i = 0; i < _points.Length - 1; i++)
                    {
                        Gizmos.DrawLine(_points[i], _points[i + 1]);
                    }

                    Gizmos.color = _startColor;
                    Gizmos.DrawSphere(_keyPoints[0].position, 0.2f);

                    Gizmos.color = _endColor;
                    Gizmos.DrawSphere(_keyPoints[_keyPoints.Count - 1].position, 0.2f);
                }
            }
            Gizmos.color = color;
        }

        private void ResetLength()
        {
            _totalLength = 0;
            _totalPoints = 1;
            _lengths = new float[1];
            _points = new Vector3[1];

            if (_keyPoints.Count > 0)
            {
                _points[0] = _keyPoints[0].position;
            }
            else
            {
                _points[0] = transform.position;
            }
        }

        private void UnityDrawGizmos()
        {
            switch (_gizmosMode)
            {
                case GizmosMode.None:
                    return;

                case GizmosMode.Selected:
                    return;
            }

            DrawCurve();
        }

        private void UnityDrawGizmosSelected()
        {
            switch (_gizmosMode)
            {
                case GizmosMode.None:
                    return;

                case GizmosMode.Selected:
                    DrawCurve();
                    break;

                default:
                    CheckDirty();
                    break;
            }

            for (int i = 0; i < _keyPoints.Count; i++)
            {
                if (_keyPoints[i] == null)
                {
                    ForceUpdate();
                    break;
                }
            }

            Color color = Gizmos.color;
            {
                Vector3 point = GetPosition(_gizmosTime);
                Gizmos.color = Color.yellow;
                Gizmos.DrawSphere(point, 0.2f);

                if ((_gizmosLines & GizmosLines.Binormal) == GizmosLines.Binormal)
                {
                    Vector3 binormal = GetBinormal(_gizmosTime, Vector3.up);
                    Gizmos.color = Color.red;
                    Gizmos.DrawLine(point, point + binormal);
                }

                if ((_gizmosLines & GizmosLines.Normal) == GizmosLines.Normal)
                {
                    Vector3 normal = GetNormal(_gizmosTime, Vector3.up);
                    Gizmos.color = Color.green;
                    Gizmos.DrawLine(point, point + normal);
                }

                if ((_gizmosLines & GizmosLines.Tangent) == GizmosLines.Tangent)
                {
                    Vector3 tangent = GetTangent(_gizmosTime);
                    Gizmos.color = Color.blue;
                    Gizmos.DrawLine(point, point + tangent);
                }
            }
            Gizmos.color = color;
        }

        private void UpdateLength()
        {
            int count;

            if (_isClosed)
            {
                count = _keyPoints.Count;
            }
            else
            {
                count = _keyPoints.Count - 1;
            }

            _points = new Vector3[pointsPerSegment * count - count + 1];

            int index = 0;

            for (int i = 0; i < _keyPoints.Count - 1; i++)
            {
                Vector3[] results = Segment(_keyPoints[i], _keyPoints[i + 1], pointsPerSegment);

                for (int j = (i == 0 ? 0 : 1); j < pointsPerSegment; j++)
                {
                    _points[index++] = results[j];
                }
            }

            if (_isClosed)
            {
                Vector3[] results = Segment(_keyPoints[_keyPoints.Count - 1], _keyPoints[0], pointsPerSegment);

                for (int j = 1; j < pointsPerSegment; j++)
                {
                    _points[index++] = results[j];
                }
            }

            _lengths = new float[_points.Length];

            for (int i = 1; i < _lengths.Length; i++)
            {
                _lengths[i] = _lengths[i - 1] + Vector3.Distance(_points[i - 1], _points[i]);
            }

            _totalLength = _lengths[_lengths.Length - 1];
            _totalPoints = _points.Length;
        }

        private void UpdatePoints()
        {
            for (int i = 0; i < _keyPoints.Count; i++)
            {
                if (_keyPoints[i] == null)
                {
                    _keyPoints.RemoveAt(i);
                    i--;
                    continue;
                }

                _keyPoints[i].name = "Point " + i;
                _keyPoints[i].transform.SetSiblingIndex(i);

                if (_keyPoints[i].isDirty)
                {
                    _keyPoints[i].transform.position = _keyPoints[i].positionTransform.position;
                }

                if (_force2D)
                {
                    _keyPoints[i].transform.localPosition = (Vector2)_keyPoints[i].transform.localPosition;
                    _keyPoints[i].transform.localRotation = Quaternion.Euler(0, 0, _keyPoints[i].transform.localEulerAngles.z);
                }
            }
        }

        private void CheckDirty()
        {
            if (isDirty)
            {
                ForceUpdate();
            }
        }

        #endregion
    }
}
