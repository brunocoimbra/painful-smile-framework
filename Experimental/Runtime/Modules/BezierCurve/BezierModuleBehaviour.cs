﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace PainfulSmile.Experimental
{
    public sealed class BezierModuleBehaviour : ModuleBehaviour<BezierModule>
    {
        #region Nested Enums

        public enum EditModeOptions
        {
            /// <summary>
            /// Freely edit the object.
            /// </summary>
            None,
            /// <summary>
            /// Clamp the position based upon the normalized time along the curve.
            /// </summary>
            ClampPosition,
            /// <summary>
            /// Clamp both the position and the rotation based upon the normalized time along the curve.
            /// </summary>
            ClampPositionAndRotation
        }

        public enum RotationMode
        {
            /// <summary>
            /// Should not apply any rotation.
            /// </summary>
            None,
            /// <summary>
            /// Apply rotation based upon the Z-axis.
            /// </summary>
            ZForward,
            /// <summary>
            /// Apply rotation based upon the X-axis.
            /// </summary>
            XForward
        }

        #endregion

        #region Serialized Fields

        [Tooltip("The rotation mode when following the curve.")]
        [SerializeField] private RotationMode _rotationMode;
#if UNITY_EDITOR
        [Tooltip("How the object should react when being edited?")]
        [SerializeField, ReadWrite(ReadWriteMode.Write, ReadWriteMode.Read)] private EditModeOptions _editModeOption = EditModeOptions.ClampPositionAndRotation;
#endif
        [Tooltip("Events to fire when the module updates. It returns the curve being used and the normalized time in the curve.")]
        [SerializeField] private BezierModule.UpdateEvent _onUpdate = new BezierModule.UpdateEvent();
        [Tooltip("Events to fire when the module starts moving backwards along the curve.")]
        [SerializeField] private UnityEvent _onPingPong = new UnityEvent();
        [Tooltip("Events for when the object reaches at determined key point.")]
        [SerializeField] private List<UnityEvent> _onReachKeyPoints = new List<UnityEvent>();

        #endregion

        #region Public Fields

        /// <summary>
        /// Events to fire when the module updates. It returns the curve being used and the normalized time in the curve.
        /// </summary>
        public event BezierModule.UpdateHandler onUpdate { add { settings.onUpdate += value; } remove { settings.onUpdate -= value; } }
        /// <summary>
        /// Events to fire when the module starts moving backwards along the curve.
        /// </summary>
        public event System.Action onPingPong { add { settings.onPingPong += value; } remove { settings.onPingPong -= value; } }

        #endregion

        #region Private Fields

        private int _currentPoint;

        #endregion

        #region Public Properties

        /// <summary>
        /// The last control point passed by.
        /// </summary>
        public int currentPoint { get { return _currentPoint; } }
        /// <summary>
        /// The current displacement along the curve.
        /// </summary>
        public float displacement { get { return settings.displacement; } }
        /// <summary>
        /// The total lenght of the curve.
        /// </summary>
        public float totalLenght { get { return settings.totalLenght; } }
        /// <summary>
        /// Events for when the object reaches at determined key point.
        /// </summary>
        public UnityEvent[] onReachKeyPoints { get { return _onReachKeyPoints.ToArray(); } }

        /// <summary>
        /// Is moving backward along the curve?
        /// </summary>
        public bool isGoingBack { get { return settings.isGoingBack; } set { settings.isGoingBack = value; } }
        /// <summary>
        /// Should the movement be ping pong-like?
        /// </summary>
        public bool isPingPong { get { return settings.isPingPong; } set { settings.isPingPong = value; } }
        /// <summary>
        /// Should the movement be based upon the delta time?
        /// </summary>
        public bool useDeltaTime { get { return settings.useDeltaTime; } set { settings.useDeltaTime = value; } }
        /// <summary>
        /// Speed in units per second.
        /// </summary>
        public float speed { get { return settings.speed; } set { settings.speed = value; } }
        /// <summary>
        /// The local time scale.
        /// </summary>
        public float timeScale { get { return settings.timeScale; } set { settings.timeScale = value; } }
        /// <summary>
        /// The rotation mode when following the curve.
        /// </summary>
        public RotationMode rotationMode { get { return _rotationMode; } set { _rotationMode = value; } }
        /// <summary>
        /// The curve being used.
        /// </summary>
        public BezierCurve curve
        {
            get { return settings.curve; }
            set
            {
                if (settings.curve != value)
                {
                    if (settings.curve != null)
                    {
                        settings.curve.onUpdated -= ValidateReachKeyPoints;
                    }

                    settings.curve = value;

                    if (settings.curve != null)
                    {
                        settings.curve.onUpdated += ValidateReachKeyPoints;
                    }

                    ValidateReachKeyPoints();
                }
            }
        }
        
        #endregion

        #region Public Methods

        /// <summary>
        /// Reset all events.
        /// </summary>
        public override void ResetEvents()
        {
            base.ResetEvents();

            settings.onPingPong += HandlePingPong;
            settings.onPingPong += _onPingPong.Invoke;

            settings.onUpdate += HandleUpdate;
            settings.onUpdate += _onUpdate.Invoke;

            for (int i = 0; i < _onReachKeyPoints.Count; i++)
            {
                if (_onReachKeyPoints[i] != null)
                {
                    _onReachKeyPoints[i].RemoveAllListeners();
                }
                else
                {
                    _onReachKeyPoints[i] = new UnityEvent();
                }
            }
        }

        /// <summary>
        /// Rewind the amount of distance.
        /// </summary>
        public void Rewind(float distance)
        {
            settings.Rewind(distance);
        }

        /// <summary>
        /// Skip the amount of distance.
        /// </summary>
        public void Skip(float distance)
        {
            settings.Skip(distance);
        }

        #endregion

        #region Protected Methods

        protected override void HandleModuleLooped()
        {
            _currentPoint = 0;

            if (_onReachKeyPoints != null && _onReachKeyPoints.Count > _currentPoint)
            {
                if (_onReachKeyPoints[_currentPoint] != null)
                {
                    _onReachKeyPoints[_currentPoint].Invoke();
                }
            }
        }

        protected override void HandleModuleReseted()
        {
            _currentPoint = 0;
        }

        protected override void OnUnityAwake()
        {
            if (curve != null)
            {
                curve.onUpdated -= ValidateReachKeyPoints;
                curve.onUpdated += ValidateReachKeyPoints;
            }

            base.OnUnityAwake();
        }

        protected override void OnUnityValidate()
        {
            base.OnUnityValidate();

#if UNITY_EDITOR
            if (Application.isPlaying || UnityEditor.EditorApplication.isPlayingOrWillChangePlaymode || settings.curve == null)
            {
                return;
            }

            switch (_editModeOption)
            {
                case EditModeOptions.ClampPosition:
                    transform.position = settings.curve.GetPosition(normalized);
                    break;

                case EditModeOptions.ClampPositionAndRotation:
                    transform.position = settings.curve.GetPosition(normalized);

                    Vector3 tangent = settings.curve.GetTangent(normalized);
                    Vector3 binormal = Vector3.Cross(transform.up, tangent).normalized;
                    Vector3 normal = Vector3.Cross(tangent, binormal).normalized;

                    switch (_rotationMode)
                    {
                        case RotationMode.ZForward:
                            transform.rotation = Quaternion.LookRotation(tangent, normal);
                            break;

                        case RotationMode.XForward:
                            transform.rotation = Quaternion.LookRotation(binormal, normal);
                            break;
                    }
                    break;
            }
#endif
        }

        #endregion

        #region Private Methods

        private void HandlePingPong()
        {
            _currentPoint = 0;

            if (_onReachKeyPoints != null && _onReachKeyPoints.Count > _currentPoint)
            {
                if (_onReachKeyPoints[_currentPoint] != null)
                {
                    _onReachKeyPoints[_currentPoint].Invoke();
                }
            }
        }

        private void HandleUpdate(BezierCurve curve, float time)
        {
            if (curve != null)
            {
                if (curve.count > 0)
                {
                    transform.position = curve.GetPosition(time);

                    BezierPoint startPoint;
                    BezierPoint endPoint;
                    float relativeTime;

                    curve.GetCubicSegment(time, out startPoint, out endPoint, out relativeTime);

                    if (_rotationMode != RotationMode.None)
                    {
                        Vector3 tangent = BezierCurve.Tangent(relativeTime, startPoint, endPoint);
                        Vector3 binormal = Vector3.Cross(transform.up, tangent).normalized;
                        Vector3 normal = Vector3.Cross(tangent, binormal).normalized;

                        switch (_rotationMode)
                        {
                            case RotationMode.ZForward:
                                transform.rotation = Quaternion.LookRotation(tangent, normal);
                                break;

                            case RotationMode.XForward:
                                transform.rotation = Quaternion.LookRotation(binormal, normal);
                                break;
                        }
                    }

                    if (settings.isGoingBack)
                    {
                        int test = curve.GetKeyPointIndex(endPoint);

                        if (test != 0 || test < _currentPoint)
                        {
                            _currentPoint = test;

                            if (_onReachKeyPoints != null && _onReachKeyPoints.Count > _currentPoint)
                            {
                                if (_onReachKeyPoints[_currentPoint] != null)
                                {
                                    _onReachKeyPoints[_currentPoint].Invoke();
                                }
                            }
                        }
                    }
                    else
                    {
                        int test = curve.GetKeyPointIndex(startPoint);

                        if (test > _currentPoint)
                        {
                            _currentPoint = test;

                            if (_onReachKeyPoints != null && _onReachKeyPoints.Count > _currentPoint)
                            {
                                if (_onReachKeyPoints[_currentPoint] != null)
                                {
                                    _onReachKeyPoints[_currentPoint].Invoke();
                                }
                            }
                        }
                    }
                }
                else
                {
                    transform.position = curve.transform.position;
                    transform.rotation = curve.transform.rotation;
                }
            }
        }

        private void ValidateReachKeyPoints()
        {
            if (settings.curve == null)
            {
                _onReachKeyPoints = new List<UnityEvent>();
                return;
            }

            if (_onReachKeyPoints == null)
            {
                _onReachKeyPoints = new List<UnityEvent>();
            }

            for (int i = 0, length = Mathf.Min(_onReachKeyPoints.Count, settings.curve.count); i < length; i++)
            {
                if (_onReachKeyPoints[i] == null)
                {
                    _onReachKeyPoints[i] = new UnityEvent();
                }
            }

            while (_onReachKeyPoints.Count > settings.curve.count)
            {
                _onReachKeyPoints.RemoveAt(_onReachKeyPoints.Count - 1);
            }

            while (_onReachKeyPoints.Count < settings.curve.count)
            {
                _onReachKeyPoints.Add(new UnityEvent());
            }
        }

        #endregion
    }
}
