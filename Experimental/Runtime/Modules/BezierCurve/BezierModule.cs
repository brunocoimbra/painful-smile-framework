﻿using UnityEngine;
using UnityEngine.Events;

namespace PainfulSmile.Experimental
{
    [System.Serializable]
    public sealed class BezierModule : Module
    {
        #region Nested Delegates

        public delegate void UpdateHandler(BezierCurve curve, float normalizedCurveTime);

        #endregion

        #region Nested Classes

        [System.Serializable]
        public sealed class UpdateEvent : UnityEvent<BezierCurve, float> { }

        #endregion

        #region Serialized Fields

        [Tooltip("The curve being used.")]
        [SerializeField] private BezierCurve _curve = null;
        [Tooltip("Speed in units per second.")]
        [SerializeField] private float _speed = 1;
        [Tooltip("Displacement relative to the start of the curve.")]
        [SerializeField] private float _displacement = 0;
        [Tooltip("The local time scale.")]
        [SerializeField] private float _timeScale = 1;
        [Tooltip("Should use the delta time?")]
        [SerializeField] private bool _useDeltaTime = true;
        [Tooltip("Should the tween be ping pong like?")]
        [SerializeField] private bool _isPingPong = false;
        [Tooltip("Is the tween current going back?")]
        [SerializeField] private bool _isGoingBack = false;

        #endregion

        #region Public Fields

        /// <summary>
        /// Event fired each frame the module updates.
        /// </summary>
        public UpdateHandler onUpdate;
        /// <summary>
        /// Events to fire when the module starts moving backwards along the curve.
        /// </summary>
        public System.Action onPingPong;

        #endregion

        #region Public Properties

        /// <summary>
        /// The module's normalized state.
        /// </summary>
        public override float normalized { get { return totalLenght > 0 ? Mathf.Clamp01(_displacement / totalLenght) : 1; } }
        /// <summary>
        /// The current displacement along the curve.
        /// </summary>
        public float displacement { get { return _displacement; } private set { _displacement = value; } }
        /// <summary>
        /// The total lenght of the curve.
        /// </summary>
        public float totalLenght { get { return _curve != null ? _curve.totalLength : 0; } }

        /// <summary>
        /// Is moving backward along the curve?
        /// </summary>
        public bool isGoingBack { get { return _isGoingBack; } set { _isGoingBack = value; } }
        /// <summary>
        /// Should the movement be ping pong-like?
        /// </summary>
        public bool isPingPong { get { return _isPingPong; } set { _isPingPong = value; } }
        /// <summary>
        /// Should the movement be based upon the delta time?
        /// </summary>
        public bool useDeltaTime { get { return _useDeltaTime; } set { _useDeltaTime = value; } }
        /// <summary>
        /// Speed in units per second.
        /// </summary>
        public float speed { get { return _speed; } set { _speed = value; } }
        /// <summary>
        /// The local time scale.
        /// </summary>
        public float timeScale { get { return _timeScale; } set { _timeScale = value; } }
        /// <summary>
        /// The curve being used.
        /// </summary>
        public BezierCurve curve { get { return _curve; } set { _curve = value; } }

        #endregion

        #region Private Properties

        private float deltaMovement { get { return (useDeltaTime ? Time.deltaTime : Time.unscaledDeltaTime) * speed * timeScale; } }

        #endregion

        #region Constructors

        public BezierModule() { }

        public BezierModule(BezierCurve curve, float speed, UpdateHandler onUpdate, System.Action onFinished = null) : base(onFinished)
        {
            this.curve = curve;
            this.speed = speed;
        }

        /// <param name="callback">If inifinityLoop is true, represents the onLooped event. Else, represents the onFinish event.</param>
        public BezierModule(BezierCurve curve, float speed, bool infinityLoop, UpdateHandler onUpdate, System.Action callback = null) : base(infinityLoop, callback)
        {
            this.curve = curve;
            this.speed = speed;
        }

        public BezierModule(BezierCurve curve, float speed, int loops, UpdateHandler onUpdate, System.Action onLooped = null, System.Action onFinished = null) : base(loops, onLooped, onFinished)
        {
            this.curve = curve;
            this.speed = speed;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Finish the module.
        /// </summary>
        public override void Anticipate()
        {
            UpdateTime(isPingPong ? 0 : 1);

            base.Anticipate();
        }

        /// <summary>
        /// Clear all events.
        /// </summary>
        public override void ResetEvents()
        {
            onPingPong = null;
            onUpdate = null;

            base.ResetEvents();
        }

        /// <summary>
        /// Rewind the amount of distance.
        /// </summary>
        public void Rewind(float distance)
        {
            distance = Mathf.Abs(distance);

            if (isPingPong)
            {
                while (distance > 0)
                {
                    if (isGoingBack)
                    {
                        if (displacement + distance > totalLenght)
                        {
                            distance -= (totalLenght - displacement);
                            displacement = totalLenght;
                            isGoingBack = false;
                        }
                        else
                        {
                            displacement += distance;
                            break;
                        }
                    }
                    else
                    {
                        if (distance - distance < 0)
                        {
                            distance -= displacement;
                            displacement = 0;
                            completedLoops--;
                            isGoingBack = true;
                        }
                        else
                        {
                            displacement -= distance;
                            break;
                        }
                    }
                }

                if (completedLoops < 0)
                {
                    Reset();
                }
                else
                {
                    UpdateTime(normalized);
                }
            }
            else
            {
                displacement -= distance;

                while (displacement < 0)
                {
                    displacement += totalLenght;
                    completedLoops--;
                }

                if (completedLoops < 0)
                {
                    Reset();
                }
                else
                {
                    UpdateTime(normalized);
                }
            }
        }

        /// <summary>
        /// Skip the amount of distance.
        /// </summary>
        public void Skip(float time)
        {
            time = Mathf.Abs(time);

            if (isPingPong)
            {
                bool looped = false;

                while (time > 0)
                {
                    if (isGoingBack)
                    {
                        if (displacement - time < 0)
                        {
                            time -= displacement;
                            displacement = 0;
                            completedLoops++;
                            isGoingBack = false;
                            looped = true;
                        }
                        else
                        {
                            displacement -= time;
                            break;
                        }
                    }
                    else
                    {
                        if (displacement + time >= totalLenght)
                        {
                            time -= (totalLenght - displacement);
                            displacement = totalLenght;
                            isGoingBack = true;
                        }
                        else
                        {
                            displacement += time;
                            break;
                        }
                    }
                }

                UpdateTime(normalized);

                if (looped)
                {
                    if (infinityLoop || completedLoops < loops)
                    {
                        if (onLooped != null)
                        {
                            onLooped.Invoke();
                        }
                    }
                    else
                    {
                        OnFinish();
                    }
                }
            }
            else
            {
                time = Mathf.Abs(time);
                displacement += time;

                if (displacement >= totalLenght)
                {
                    while (displacement >= totalLenght)
                    {
                        displacement -= totalLenght;
                        completedLoops++;
                    }

                    UpdateTime(normalized);

                    if (infinityLoop || completedLoops < loops)
                    {
                        if (onLooped != null)
                        {
                            onLooped.Invoke();
                        }
                    }
                    else
                    {
                        OnFinish();
                    }
                }
                else
                {
                    UpdateTime(normalized);
                }
            }
        }

        #endregion

        #region Protected Methods

        protected override void OnFinish()
        {
            isGoingBack = false;
            displacement = totalLenght;

            base.OnFinish();
        }

        protected override void OnLoop()
        {
            displacement = 0;

            base.OnLoop();
        }

        protected override void OnReset()
        {
            UpdateTime(0);

            isGoingBack = false;
            displacement = 0;

            base.OnReset();
        }

        protected sealed override void OnUpdate()
        {
            if (isPingPong)
            {
                if (isGoingBack)
                {
                    displacement -= deltaMovement;

                    UpdateTime(normalized);

                    if (displacement <= 0)
                    {
                        displacement = 0;
                        isGoingBack = false;
                        completedLoops++;

                        if (infinityLoop || completedLoops < loops)
                        {
                            OnLoop();
                        }
                        else
                        {
                            OnFinish();
                        }
                    }
                }
                else
                {
                    displacement += deltaMovement;

                    UpdateTime(normalized);

                    if (displacement >= totalLenght)
                    {
                        displacement = totalLenght;
                        isGoingBack = true;

                        if (onPingPong != null)
                        {
                            onPingPong.Invoke();
                        }
                    }
                }
            }
            else
            {
                displacement += deltaMovement;

                UpdateTime(normalized);

                if (displacement >= totalLenght)
                {
                    completedLoops++;

                    if (infinityLoop || completedLoops < loops)
                    {
                        OnLoop();
                    }
                    else
                    {
                        OnFinish();
                    }
                }
            }
        }

        #endregion

        #region Private Methods

        private void UpdateTime(float normalizedTime)
        {
            if (onUpdate != null)
            {
                onUpdate.Invoke(_curve, normalizedTime);
            }
        }

        #endregion
    }
}
