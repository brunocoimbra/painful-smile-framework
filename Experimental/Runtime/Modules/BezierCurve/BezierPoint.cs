﻿using UnityEngine;

namespace PainfulSmile.Experimental
{
    [AddComponentMenu("")]
    public sealed class BezierPoint : MonoBehaviour
    {
        #region Nested Enums

        public enum TangentType
        {
            None,
            Flat,
            Broken
        }

        #endregion

        #region Serialized Fields

        [SerializeField] private BezierCurve _curve = null;
        [SerializeField] private Transform _positionTransform;
        [SerializeField] private TangentType _tangentType = TangentType.Flat;
        [SerializeField] private Vector3 _tangentA = new Vector3(-0.5f, 0f, 0f);
        [SerializeField] private Vector3 _tangentB = new Vector3(0.5f, 0f, 0f);

        #endregion

        #region Public Properties

        /// <summary>
        /// The curve this point belongs.
        /// </summary>
        public BezierCurve curve { get { return _curve; } }
        /// <summary>
        /// The index of this point in the curve.
        /// </summary>
        public int index { get { return _curve != null ? _curve.GetKeyPointIndex(this) : -1; } }
        /// <summary>
        /// Was the point modified but not yet updated by it's curve?
        /// </summary>
        public bool isDirty { get { return _positionTransform != null && _positionTransform.position != position; } }

        /// <summary>
        /// Tangent type being used.
        /// </summary>
        public TangentType tangentType
        {
            get { return _tangentType; }
            set
            {
                if (_tangentType != value)
                {
                    _tangentType = value;

                    if (_tangentType == TangentType.Flat)
                    {
                        if (_tangentB != -_tangentA)
                        {
                            _tangentB = -_tangentA;

                            if (curve != null)
                            {
                                curve.SetDirty();
                            }
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Euler angles in world space.
        /// </summary>
        public Vector3 eulerAngle
        {
            get { return transform.eulerAngles; }
            set
            {
                if (transform.eulerAngles != value)
                {
                    transform.eulerAngles = value;

                    if (curve != null)
                    {
                        curve.SetDirty();
                    }
                }
            }
        }
        /// <summary>
        /// Euler angles in local space.
        /// </summary>
        public Vector3 localEulerAngles
        {
            get { return transform.localEulerAngles; }
            set
            {
                if (transform.localEulerAngles != value)
                {
                    transform.localEulerAngles = value;

                    if (curve != null)
                    {
                        curve.SetDirty();
                    }
                }
            }
        }
        /// <summary>
        /// Position in world space.
        /// </summary>
        public Vector3 position
        {
            get { return transform.position; }
            set
            {
                if (transform.position != value)
                {
                    transform.position = value;

                    if (curve != null)
                    {
                        curve.SetDirty();
                    }
                }
            }
        }
        /// <summary>
        /// Position in local space.
        /// </summary>
        public Vector3 localPosition
        {
            get { return transform.localPosition; }
            set
            {
                if (transform.localPosition != value)
                {
                    transform.localPosition = value;

                    if (curve != null)
                    {
                        curve.SetDirty();
                    }
                }
            }
        }
        /// <summary>
        /// Scale in local space.
        /// </summary>
        public Vector3 localScale
        {
            get { return transform.localScale; }
            set
            {
                if (transform.localScale != value)
                {
                    transform.localScale = value;

                    if (curve != null)
                    {
                        curve.SetDirty();
                    }
                }
            }
        }
        /// <summary>
        /// Tangent A position in local space.
        /// </summary>
        public Vector3 tangentAPosition
        {
            get { return _tangentType == TangentType.None ? Vector3.zero : _tangentA; }
            set
            {
                if (_tangentA != value)
                {
                    _tangentA = value;

                    if (_tangentType == TangentType.Flat)
                    {
                        _tangentB = -_tangentA;
                    }

                    if (curve != null)
                    {
                        curve.SetDirty();
                    }
                }
            }
        }
        /// <summary>
        /// Tangent B position in local space.
        /// </summary>
        public Vector3 tangentBPosition
        {
            get { return _tangentType == TangentType.None ? Vector3.zero : _tangentB; }
            set
            {
                if (_tangentB != value)
                {
                    _tangentB = value;

                    if (_tangentType == TangentType.Flat)
                    {
                        _tangentA = -_tangentB;
                    }

                    if (curve != null)
                    {
                        curve.SetDirty();
                    }
                }
            }
        }
        /// <summary>
        /// Tangent A position in world space.
        /// </summary>
        public Vector3 tangentAWorldPosition { get { return transform.TransformPoint(tangentAPosition); } set { tangentAPosition = transform.InverseTransformPoint(value); } }
        /// <summary>
        /// Tangent B position in world space.
        /// </summary>
        public Vector3 tangentBWorldPosition { get { return transform.TransformPoint(tangentBPosition); } set { tangentBPosition = transform.InverseTransformPoint(value); } }
        /// <summary>
        /// Rotation in world space.
        /// </summary>
        public Quaternion rotation
        {
            get { return transform.rotation; }
            set
            {
                if (transform.rotation != value)
                {
                    transform.rotation = value;

                    if (curve != null)
                    {
                        curve.SetDirty();
                    }
                }
            }
        }
        /// <summary>
        /// Rotation in local space.
        /// </summary>
        public Quaternion localRotation
        {
            get { return transform.localRotation; }
            set
            {
                if (transform.localRotation != value)
                {
                    transform.localRotation = value;

                    if (curve != null)
                    {
                        curve.SetDirty();
                    }
                }
            }
        }
        /// <summary>
        /// Transform used to set it's point position.
        /// </summary>
        public Transform positionTransform { get { return _positionTransform; } set { _positionTransform = value; } }

        #endregion

        #region Unity Callbacks

        private void OnDestroy()
        {
            UnityDestroy();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Create a new point for a curve.
        /// </summary>
        public static BezierPoint CreateInstance(BezierCurve curve)
        {
            BezierPoint point = new GameObject(string.Format("Point {0}", curve.count), typeof(BezierPoint)).GetComponent<BezierPoint>();
            point._curve = curve;
            point.transform.parent = curve.transform;
            point.transform.localRotation = Quaternion.identity;

            return point;
        }

        #endregion

        #region Private Methods

        private void UnityDestroy()
        {
            if (curve != null)
            {
                curve.SetDirty();
            }
        }

        #endregion
    }
}
