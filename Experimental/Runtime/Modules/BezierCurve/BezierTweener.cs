﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace PainfulSmile.Experimental
{
    public sealed class BezierTweener : Tweener<Tween>
    {
        #region Nested Enums

        public enum EditModeOptions
        {
            /// <summary>
            /// Freely edit the object.
            /// </summary>
            None,
            /// <summary>
            /// Clamp the position based upon the normalized time along the curve.
            /// </summary>
            ClampPosition,
            /// <summary>
            /// Clamp both the position and the rotation based upon the normalized time along the curve.
            /// </summary>
            ClampPositionAndRotation
        }

        public enum RotationMode
        {
            /// <summary>
            /// Should not apply any rotation.
            /// </summary>
            None,
            /// <summary>
            /// Apply rotation based upon the Z-axis.
            /// </summary>
            ZForward,
            /// <summary>
            /// Apply rotation based upon the X-axis.
            /// </summary>
            XForward
        }

        #endregion

        #region Serialized Fields

        [Tooltip("The curve it should follow.")]
        [SerializeField] private BezierCurve _bezierCurve;
        [Tooltip("The rotation mode when following the curve.")]
        [SerializeField] private RotationMode _rotationMode;
#if UNITY_EDITOR
        [Tooltip("How the object should react when being edited?")]
        [SerializeField, ReadWrite(ReadWriteMode.Write, ReadWriteMode.Read)] private EditModeOptions _editModeOption = EditModeOptions.ClampPositionAndRotation;
#endif
        [SerializeField] private List<UnityEvent> _onReachKeyPoints = new List<UnityEvent>();

        #endregion

        #region Private Fields

        private int _currentPoint;

        #endregion

        #region Public Properties

        /// <summary>
        /// The last control point passed by.
        /// </summary>
        public int currentPoint { get { return _currentPoint; } }
        /// <summary>
        /// Events for when the object reaches at determined key point.
        /// </summary>
        public UnityEvent[] onReachKeyPoints { get { return _onReachKeyPoints.ToArray(); } }

        /// <summary>
        /// The rotation mode when following the curve.
        /// </summary>
        public RotationMode rotationMode { get { return _rotationMode; } set { _rotationMode = value; } }
        /// <summary>
        /// The curve it should follow.
        /// </summary>
        public BezierCurve bezierCurve
        {
            get { return _bezierCurve; }
            set
            {
                if (_bezierCurve != value)
                {
                    if (_bezierCurve != null)
                    {
                        _bezierCurve.onUpdated -= ValidateReachKeyPoints;
                    }

                    _bezierCurve = value;

                    if (_bezierCurve != null)
                    {
                        _bezierCurve.onUpdated += ValidateReachKeyPoints;
                    }

                    ValidateReachKeyPoints();
                }
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Reset all events.
        /// </summary>
        public override void ResetEvents()
        {
            for (int i = 0; i < _onReachKeyPoints.Count; i++)
            {
                if (_onReachKeyPoints[i] != null)
                {
                    _onReachKeyPoints[i].RemoveAllListeners();
                }
                else
                {
                    _onReachKeyPoints[i] = new UnityEvent();
                }
            }

            base.ResetEvents();
        }

        #endregion

        #region Protected Methods

        protected override void HandleModuleLooped()
        {
            _currentPoint = 0;

            if (_onReachKeyPoints != null && _onReachKeyPoints.Count > _currentPoint)
            {
                if (_onReachKeyPoints[_currentPoint] != null)
                {
                    _onReachKeyPoints[_currentPoint].Invoke();
                }
            }
        }

        protected override void HandlePingPong()
        {
            _currentPoint = 0;

            if (_onReachKeyPoints != null && _onReachKeyPoints.Count > _currentPoint)
            {
                if (_onReachKeyPoints[_currentPoint] != null)
                {
                    _onReachKeyPoints[_currentPoint].Invoke();
                }
            }
        }

        protected override void HandleModuleReseted()
        {
            _currentPoint = 0;
        }

        protected override void HandleUpdateTime(float unclampedTime)
        {
            if (_bezierCurve != null)
            {
                if (_bezierCurve.count > 0)
                {
                    float curveTime = Mathf.Repeat(unclampedTime, 1);

                    transform.position = _bezierCurve.GetPosition(curveTime);

                    BezierPoint startPoint;
                    BezierPoint endPoint;
                    float relativeTime;

                    _bezierCurve.GetCubicSegment(curveTime, out startPoint, out endPoint, out relativeTime);

                    if (_rotationMode != RotationMode.None)
                    {
                        Vector3 tangent = BezierCurve.Tangent(relativeTime, startPoint, endPoint);
                        Vector3 binormal = Vector3.Cross(transform.up, tangent).normalized;
                        Vector3 normal = Vector3.Cross(tangent, binormal).normalized;

                        switch (_rotationMode)
                        {
                            case RotationMode.ZForward:
                                transform.rotation = Quaternion.LookRotation(tangent, normal);
                                break;

                            case RotationMode.XForward:
                                transform.rotation = Quaternion.LookRotation(binormal, normal);
                                break;
                        }
                    }

                    if (isGoingBack)
                    {
                        int test = _bezierCurve.GetKeyPointIndex(endPoint);

                        if (test != 0 || test < _currentPoint)
                        {
                            _currentPoint = test;

                            if (_onReachKeyPoints != null && _onReachKeyPoints.Count > _currentPoint)
                            {
                                if (_onReachKeyPoints[_currentPoint] != null)
                                {
                                    _onReachKeyPoints[_currentPoint].Invoke();
                                }
                            }
                        }
                    }
                    else
                    {
                        int test = _bezierCurve.GetKeyPointIndex(startPoint);

                        if (test > _currentPoint)
                        {
                            _currentPoint = test;

                            if (_onReachKeyPoints != null && _onReachKeyPoints.Count > _currentPoint)
                            {
                                if (_onReachKeyPoints[_currentPoint] != null)
                                {
                                    _onReachKeyPoints[_currentPoint].Invoke();
                                }
                            }
                        }
                    }
                }
                else
                {
                    transform.position = _bezierCurve.transform.position;
                    transform.rotation = _bezierCurve.transform.rotation;
                }
            }
        }

        protected override void OnUnityAwake()
        {
            if (_bezierCurve != null)
            {
                _bezierCurve.onUpdated -= ValidateReachKeyPoints;
                _bezierCurve.onUpdated += ValidateReachKeyPoints;
            }

            base.OnUnityAwake();
        }

        protected override void OnUnityValidate()
        {
            base.OnUnityValidate();

#if UNITY_EDITOR
            if (Application.isPlaying || UnityEditor.EditorApplication.isPlayingOrWillChangePlaymode || _bezierCurve == null)
            {
                return;
            }

            switch (_editModeOption)
            {
                case EditModeOptions.ClampPosition:
                    transform.position = _bezierCurve.GetPosition(normalized);
                    break;

                case EditModeOptions.ClampPositionAndRotation:
                    transform.position = _bezierCurve.GetPosition(normalized);

                    Vector3 tangent = _bezierCurve.GetTangent(normalized);
                    Vector3 binormal = Vector3.Cross(transform.up, tangent).normalized;
                    Vector3 normal = Vector3.Cross(tangent, binormal).normalized;

                    switch (_rotationMode)
                    {
                        case RotationMode.ZForward:
                            transform.rotation = Quaternion.LookRotation(tangent, normal);
                            break;

                        case RotationMode.XForward:
                            transform.rotation = Quaternion.LookRotation(binormal, normal);
                            break;
                    }
                    break;
            }
#endif
        }

        #endregion

        #region Private Methods

        private void ValidateReachKeyPoints()
        {
            if (_bezierCurve == null)
            {
                _onReachKeyPoints = new List<UnityEvent>();
                return;
            }

            if (_onReachKeyPoints == null)
            {
                _onReachKeyPoints = new List<UnityEvent>();
            }

            for (int i = 0, length = Mathf.Min(_onReachKeyPoints.Count, _bezierCurve.count); i < length; i++)
            {
                if (_onReachKeyPoints[i] == null)
                {
                    _onReachKeyPoints[i] = new UnityEvent();
                }
            }

            while (_onReachKeyPoints.Count > _bezierCurve.count)
            {
                _onReachKeyPoints.RemoveAt(_onReachKeyPoints.Count - 1);
            }

            while (_onReachKeyPoints.Count < _bezierCurve.count)
            {
                _onReachKeyPoints.Add(new UnityEvent());
            }
        }

        #endregion
    }
}
