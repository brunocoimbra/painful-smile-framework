﻿using UnityEngine;

namespace PainfulSmile.Experimental
{
    /// <summary>
    /// Used to call an action after some time.
    /// </summary>
    [System.Serializable]
    public class TimeModule : Module
    {
        #region Serialized Fields
        
        [Tooltip("The total duration in seconds.")]
        [SerializeField, NotLessThan(0)] private float _duration = 1;
        [Tooltip("The current time in seconds.")]
        [SerializeField, NotLessThan(0)] private float _currentTime = 0;
        [Tooltip("The local time scale.")]
        [SerializeField] private float _timeScale = 1;
        [Tooltip("Should use the delta time?")]
        [SerializeField] private bool _useDeltaTime = true;

        #endregion

        #region Public Properties

        /// <summary>
        /// The module's normalized state.
        /// </summary>
        public sealed override float normalized { get { return duration > 0 ? Mathf.Clamp01(currentTime / duration) : 1; } }
        /// <summary>
        /// The current time in seconds.
        /// </summary>
        public float currentTime { get { return _currentTime; } protected set { _currentTime = value; } }

        /// <summary>
        /// Should use the delta time?
        /// </summary>
        public bool useDeltaTime { get { return _useDeltaTime; } set { _useDeltaTime = value; } }
        /// <summary>
        /// The total duration in seconds.
        /// </summary>
        public float duration { get { return _duration; } set { _duration = Mathf.Max(0, value); } }
        /// <summary>
        /// The local time scale.
        /// </summary>
        public float timeScale { get { return _timeScale; } set { _timeScale = value; } }

        #endregion

        #region Protected Properties

        protected float deltaTime { get { return (useDeltaTime ? Time.deltaTime : Time.unscaledDeltaTime) * timeScale; } }

        #endregion

        #region Constructors

        public TimeModule() { }

        public TimeModule(float duration, System.Action onFinished) : base(onFinished)
        {
            this.duration = duration;
        }

        /// <param name="callback">If inifinityLoop is true, represents the onLooped event. Else, represents the onFinish event.</param>
        public TimeModule(float duration, bool infinityLoop, System.Action callback) : base(infinityLoop, callback)
        {
            this.duration = duration;
        }

        public TimeModule(float duration, int loops, System.Action onLooped, System.Action onFinished) : base(loops, onLooped, onFinished)
        {
            this.duration = duration;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Rewind the amount of time.
        /// </summary>
        public virtual void Rewind(float time)
        {
            time = Mathf.Abs(time);
            currentTime -= time;

            while (currentTime < 0)
            {
                currentTime += duration;
                completedLoops--;
            }

            if (completedLoops < 0)
            {
                Reset();
            }
        }

        /// <summary>
        /// Skip the amount of time.
        /// </summary>
        public virtual void Skip(float time)
        {
            time = Mathf.Abs(time);
            currentTime += time;

            if (currentTime >= duration)
            {
                while (currentTime >= duration)
                {
                    currentTime -= duration;
                    completedLoops++;
                }

                if (infinityLoop || completedLoops < loops)
                {
                    if (onLooped != null)
                    {
                        onLooped.Invoke();
                    }
                }
                else
                {
                    OnFinish();
                }
            }
        }

        #endregion

        #region Protected Methods

        protected override void OnFinish()
        {
            currentTime = duration;

            base.OnFinish();
        }

        protected override void OnLoop()
        {
            currentTime = 0;

            base.OnLoop();
        }

        protected override void OnReset()
        {
            currentTime = 0;

            base.OnReset();
        }

        protected override void OnUpdate()
        {
            currentTime += deltaTime;

            if (currentTime >= duration)
            {
                completedLoops++;

                if (infinityLoop || completedLoops < loops)
                {
                    OnLoop();
                }
                else
                {
                    OnFinish();
                }
            }
        }

        #endregion
    }
}
