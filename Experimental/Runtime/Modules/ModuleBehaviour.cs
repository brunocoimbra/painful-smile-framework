﻿using UnityEngine;
using UnityEngine.Events;

namespace PainfulSmile.Experimental
{
    /// <summary>
    /// Base class for all <seealso cref="Module"/>'s behaviours. Extend <seealso cref="ModuleBehaviour{T}"/> if creating a new behaviour instead.
    /// </summary>
    public abstract class ModuleBehaviour : MonoBehaviour
    {
        #region Nested Enums

        public enum EaseMode
        {
            NormalizedCurve,
            Linear,
            Sine,
            SineIn,
            SineOut,
            Quadratic,
            QuadraticIn,
            QuadraticOut,
            Cubic,
            CubicIn,
            CubicOut,
            Quartic,
            QuarticIn,
            QuarticOut,
            Quintic,
            QuinticIn,
            QuinticOut,
            Exponential,
            ExponentialIn,
            ExponentialOut,
            Circular,
            CircularIn,
            CircularOut,
            Bounce,
            BounceIn,
            BounceOut,
            Back,
            BackIn,
            BackOut,
            Elastic,
            ElasticIn,
            ElasticOut
        }

        #endregion

        #region Public Events

        /// <summary>
        /// Event fired when the module is stopped.
        /// </summary>
        public abstract event System.Action onStopped;
        /// <summary>
        /// Event fired when the module finishes. 
        /// </summary>
        public abstract event System.Action onFinished;
        /// <summary>
        /// Event fired when the module complete a loop.
        /// </summary>
        public abstract event System.Action onLooped;
        /// <summary>
        /// Event fired when the module is paused.
        /// </summary>
        public abstract event System.Action onPaused;
        /// <summary>
        /// Event fired when the module is played.
        /// </summary>
        public abstract event System.Action onPlayed;
        /// <summary>
        /// Event fired when the module is reseted.
        /// </summary>
        public abstract event System.Action onReseted;

        #endregion

        #region Public Properties

        /// <summary>
        /// The module's normalized state.
        /// </summary>
        public abstract float normalized { get; }
        /// <summary>
        /// The update cycle being used.
        /// </summary>
        public abstract UpdateMode updateMode { get; }
        /// <summary>
        /// The current state.
        /// </summary>
        public abstract Module.State state { get; }

        /// <summary>
        /// Should the module keep looping?
        /// </summary>
        public abstract bool infinityLoop { get; set; }
        /// <summary>
        /// Should it start playing on Awake?
        /// </summary>
        public abstract bool playOnAwake { get; set; }
        /// <summary>
        /// The amount of loops completed.
        /// </summary>
        public abstract int completedLoops { get; set; }
        /// <summary>
        /// The total amount of loops.
        /// </summary>
        public abstract int loops { get; set; }
        /// <summary>
        /// Used to control several modules together.
        /// </summary>
        public abstract string moduleTag { get; set; }

        #endregion

        #region Public Methods

        /// <summary>
        /// Finish the module;
        /// </summary>
        public abstract void Anticipate();

        /// <summary>
        /// Reset all events.
        /// </summary>
        public abstract void ResetEvents();

        /// <summary>
        /// Pause the module. It does not remove it from <seealso cref="ModuleManager"/>.
        /// </summary>
        public abstract void Pause();

        /// <summary>
        /// Play the module, adding it to <seealso cref="ModuleManager"/>.
        /// </summary>
        public abstract void Play();

        /// <summary>
        /// Play the module in the selected update mode, adding it to <seealso cref="ModuleManager"/>.
        /// </summary>
        public abstract void Play(UpdateMode updateMode);

        /// <summary>
        /// Reset all values, except for events. If finished or stopped it returns to default state.
        /// </summary>
        public abstract void ResetModule();

        /// <summary>
        /// Stop the module, removing it from <seealso cref="ModuleManager"/>.
        /// </summary>
        public abstract void Stop();

        /// <summary>
        /// Manually update the module.
        /// </summary>
        public abstract void ForceUpdate();

        #endregion
    }

    /// <summary>
    /// Extend this class to create behaviours for your own <seealso cref="Module"/>. Use <seealso cref="ModuleBehaviour"/> if you just want to keep a reference.
    /// </summary>
    public abstract class ModuleBehaviour<T> : ModuleBehaviour where T : Module
    {
        #region Serialized Fields

        [Tooltip("The module to be used.")]
        [SerializeField] private T _settings = default(T);
        [Tooltip("Should it start playing on Awake?")]
        [SerializeField] private bool _playOnAwake;
        [Tooltip("Event fired when the module complete a loop.")]
        [SerializeField] private UnityEvent _onLooped = new UnityEvent();
        [Tooltip("Event fired when the module finishes. ")]
        [SerializeField] private UnityEvent _onFinished = new UnityEvent();
        [Tooltip("Event fired when the module is played.")]
        [SerializeField] private UnityEvent _onPlayed = new UnityEvent();
        [Tooltip("Event fired when the module is paused.")]
        [SerializeField] private UnityEvent _onPaused = new UnityEvent();
        [Tooltip("Event fired when the module is reseted.")]
        [SerializeField] private UnityEvent _onReseted = new UnityEvent();
        [Tooltip("Event fired when the module is stopped.")]
        [SerializeField] private UnityEvent _onStopped = new UnityEvent();

        #endregion

        #region Public Events

        /// <summary>
        /// Event fired when the module finishes. 
        /// </summary>
        public sealed override event System.Action onFinished { add { _settings.onFinished += value; } remove { _settings.onFinished -= value; } }
        /// <summary>
        /// Event fired when the module complete a loop.
        /// </summary>
        public sealed override event System.Action onLooped { add { _settings.onLooped += value; } remove { _settings.onLooped -= value; } }
        /// <summary>
        /// Event fired when the module is paused.
        /// </summary>
        public sealed override event System.Action onPaused { add { _settings.onPaused += value; } remove { _settings.onPaused -= value; } }
        /// <summary>
        /// Event fired when the module is played.
        /// </summary>
        public sealed override event System.Action onPlayed { add { _settings.onPlayed += value; } remove { _settings.onPlayed -= value; } }
        /// <summary>
        /// Event fired when the module is rewinded.
        /// </summary>
        public sealed override event System.Action onReseted { add { _settings.onReseted += value; } remove { _settings.onReseted -= value; } }
        /// <summary>
        /// Event fired when the module is canceled.
        /// </summary>
        public sealed override event System.Action onStopped { add { _settings.onStopped += value; } remove { _settings.onStopped -= value; } }

        #endregion

        #region Public Properties

        /// <summary>
        /// The module's normalized state.
        /// </summary>
        public sealed override float normalized { get { return _settings.normalized; } }
        /// <summary>
        /// The current state.
        /// </summary>
        public sealed override UpdateMode updateMode { get { return _settings.updateMode; } }
        /// <summary>
        /// The update cycle being used.
        /// </summary>
        public sealed override Module.State state { get { return _settings.state; } }

        /// <summary>
        /// Should the module keep looping?
        /// </summary>
        public sealed override bool infinityLoop { get { return _settings.infinityLoop; } set { _settings.infinityLoop = value; } }
        /// <summary>
        /// Should it start playing on Awake?
        /// </summary>
        public sealed override bool playOnAwake { get { return _playOnAwake; } set { _playOnAwake = value; } }
        /// <summary>
        /// The amount of loops completed.
        /// </summary>
        public sealed override int completedLoops { get { return _settings.completedLoops; } set { _settings.completedLoops = value; } }
        /// <summary>
        /// The total amount of loops.
        /// </summary>
        public sealed override int loops { get { return _settings.loops; } set { _settings.loops = value; } }
        /// <summary>
        /// Used to control several modules together.
        /// </summary>
        public sealed override string moduleTag { get { return _settings.tag; } set { _settings.tag = value; } }

        #endregion

        #region Protected Properties

        protected T settings { get { return _settings; } }

        #endregion

        #region Unity Callbacks

        private void Awake()
        {
            UnityAwake();
        }

        private void OnDestroy()
        {
            UnityDestroy();
        }

        private void OnValidate()
        {
            UnityValidate();
        }

        private void Reset()
        {
            UnityReset();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Finish the module.
        /// </summary>
        public override void Anticipate()
        {
            _settings.Anticipate();
        }

        /// <summary>
        /// Reset all events.
        /// </summary>
        public override void ResetEvents()
        {
            _settings.ResetEvents();

            _settings.onStopped += HandleModuleStopped;
            _settings.onStopped += _onStopped.Invoke;

            _settings.onFinished += HandleModuleFinished;
            _settings.onFinished += _onFinished.Invoke;

            _settings.onLooped += HandleModuleLooped;
            _settings.onLooped += _onLooped.Invoke;

            _settings.onPaused += HandleModulePaused;
            _settings.onPaused += _onPaused.Invoke;

            _settings.onPlayed += HandleModulePlayed;
            _settings.onPlayed += _onPlayed.Invoke;

            _settings.onReseted += HandleModuleReseted;
            _settings.onReseted += _onReseted.Invoke;
        }

        /// <summary>
        /// Reset all values, except for events. If finished or stopped it returns to default state.
        /// </summary>
        public override void ResetModule()
        {
            _settings.Reset();
        }

        /// <summary>
        /// Manually update the module.
        /// </summary>
        public sealed override void ForceUpdate()
        {
            _settings.ForceUpdate();
        }

        /// <summary>
        /// Pause the module.
        /// </summary>
        public sealed override void Pause()
        {
            _settings.Pause();
        }

        /// <summary>
        /// Play the module.
        /// </summary>
        public sealed override void Play()
        {
            _settings.Play();
        }

        /// <summary>
        /// Play the module in the selected update loop.
        /// </summary>
        public sealed override void Play(UpdateMode updateMode)
        {
            _settings.Play(updateMode);
        }

        /// <summary>
        /// Stop the module, removing it from <seealso cref="ModuleManager"/>.
        /// </summary>
        public sealed override void Stop()
        {
            _settings.Stop();
        }

        #endregion

        #region Protected Methods

        protected virtual void HandleModuleFinished() { }

        protected virtual void HandleModuleLooped() { }

        protected virtual void HandleModulePaused() { }

        protected virtual void HandleModulePlayed() { }

        protected virtual void HandleModuleReseted() { }

        protected virtual void HandleModuleStopped() { }

        protected virtual void OnUnityAwake() { }

        protected virtual void OnUnityDestroy() { }

        protected virtual void OnUnityReset() { }

        protected virtual void OnUnityValidate() { }

        #endregion

        #region Private Methods

        private void UnityAwake()
        {
            ResetEvents();
            OnUnityAwake();

            _settings.context = gameObject;

            if (_playOnAwake)
            {
                _settings.Play();
            }
        }

        private void UnityDestroy()
        {
            OnUnityDestroy();

            if (ModuleManager.Exists)
            {
                if (_settings != null)
                {
                    _settings.Stop();
                }
            }
        }

        private void UnityReset()
        {
            OnUnityReset();

            if (_settings != null)
            {
                _settings.context = gameObject;
            }
        }

        private void UnityValidate()
        {
            OnUnityValidate();

            _settings.context = gameObject;
        }

        #endregion
    }
}
