﻿using UnityEngine;

namespace PainfulSmile.Experimental
{
    /// <summary>
    /// Base class for all <seealso cref="FrameModule"/>'s behaviours.
    /// </summary>
    public abstract class FrameModuleBehaviour<T> : ModuleBehaviour<T> where T : FrameModule
    {
        #region Public Properties

        /// <summary>
        /// The current frame.
        /// </summary>
        public int currentFrame { get { return settings.currentFrame; } }
        /// <summary>
        /// The total duration in frames.
        /// </summary>
        public int duration { get { return settings.duration; } set { settings.duration = value; } }

        #endregion

        #region Public Methods

        /// <summary>
        /// Rewind the amount of frames.
        /// </summary>
        public void Rewind(int frames)
        {
            settings.Rewind(frames);
        }

        /// <summary>
        /// Skip the amount of frames.
        /// </summary>
        public void Skip(int frames)
        {
            settings.Skip(frames);
        }

        #endregion
    }

    /// <summary>
    /// <seealso cref="FrameModule"/>'s behaviour. Use <seealso cref="FrameModuleBehaviour{T}"/> to create your own.
    /// </summary>
    public sealed class FrameModuleBehaviour : FrameModuleBehaviour<FrameModule> { }
}
