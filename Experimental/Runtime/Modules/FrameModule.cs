﻿using UnityEngine;

namespace PainfulSmile.Experimental
{
    /// <summary>
    /// Used to call an action after some frames.
    /// </summary>
    [System.Serializable]
    public class FrameModule : Module
    {
        #region Serialized Fields

        [Tooltip("The total duration in frames.")]
        [SerializeField, NotLessThan(0)] private int _duration = 0;
        [Tooltip("The current frame.")]
        [SerializeField, NotLessThan(0)] private int _currentFrame = 0;

        #endregion

        #region Public Properties

        /// <summary>
        /// The module's normalized state.
        /// </summary>
        public sealed override float normalized { get { return Mathf.InverseLerp(0, _duration, currentFrame); } }
        /// <summary>
        /// The current frame.
        /// </summary>
        public int currentFrame { get { return _currentFrame; } protected set { _currentFrame = value; } }

        /// <summary>
        /// The total duration in frames.
        /// </summary>
        public int duration { get { return _duration; } set { _duration = Mathf.Max(0, value); } }

        #endregion

        #region Constructors

        public FrameModule() { }

        public FrameModule(int duration, System.Action onFinished) : base(onFinished)
        {
            this.duration = duration;
        }

        /// <param name="callback">If inifinityLoop is true, represents the onLooped event. Else, represents the onFinish event.</param>
        public FrameModule(int duration, bool infinityLoop, System.Action callback) : base(infinityLoop, callback)
        {
            this.duration = duration;
        }

        public FrameModule(int duration, int loops, System.Action onLooped, System.Action onFinished) : base(loops, onLooped, onFinished)
        {
            this.duration = duration;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Rewind the amount of frames.
        /// </summary>
        public virtual void Rewind(int frames)
        {
            frames = Mathf.Abs(frames);
            currentFrame -= frames;

            while (currentFrame < 0)
            {
                currentFrame += duration;
                completedLoops--;
            }

            if (completedLoops < 0)
            {
                Reset();
            }
        }

        /// <summary>
        /// Skip the amount of frames.
        /// </summary>
        public virtual void Skip(int frames)
        {
            frames = Mathf.Abs(frames);
            currentFrame += frames;

            if (currentFrame >= duration)
            {
                while (currentFrame >= duration)
                {
                    currentFrame -= duration;
                    completedLoops++;
                }

                if (infinityLoop || completedLoops < loops)
                {
                    if (onLooped != null)
                    {
                        onLooped.Invoke();
                    }
                }
                else
                {
                    OnFinish();
                }
            }
        }

        #endregion

        #region Protected Methods

        protected override void OnFinish()
        {
            currentFrame = duration;

            base.OnFinish();
        }

        protected override void OnLoop()
        {
            currentFrame = 0;

            base.OnLoop();
        }

        protected override void OnReset()
        {
            currentFrame = 0;

            base.OnReset();
        }

        protected override void OnUpdate()
        {
            currentFrame++;

            if (currentFrame >= duration)
            {
                completedLoops++;

                if (infinityLoop || completedLoops < loops)
                {
                    OnLoop();
                }
                else
                {
                    OnFinish();
                }
            }
        }

        #endregion
    }
}
