﻿using UnityEngine;

namespace PainfulSmile.Experimental
{
    /// <summary>
    /// Base class for all <seealso cref="TimeModule"/>'s behaviours.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class TimeModuleBehaviour<T> : ModuleBehaviour<T> where T : TimeModule
    {
        #region Public Properties

        /// <summary>
        /// The current time in seconds.
        /// </summary>
        public float currentTime { get { return settings.currentTime; } }
        /// <summary>
        /// Should use the delta time?
        /// </summary>
        public bool useDeltaTime { get { return settings.useDeltaTime; } set { settings.useDeltaTime = value; } }
        /// <summary>
        /// The total duration in seconds.
        /// </summary>
        public float duration { get { return settings.duration; } set { settings.duration = value; } }
        /// <summary>
        /// The local time scale.
        /// </summary>
        public float timeScale { get { return settings.timeScale; } set { settings.timeScale = value; } }

        #endregion

        #region Public Methods

        /// <summary>
        /// Rewind the amount of time.
        /// </summary>
        public void Rewind(float time)
        {
            settings.Rewind(time);
        }

        /// <summary>
        /// Skip the amount of time.
        /// </summary>
        public void Skip(float time)
        {
            settings.Skip(time);
        }

        #endregion
    }

    /// <summary>
    /// <seealso cref="TimeModule"/>'s behaviour. Use <seealso cref="TimeModuleBehaviour{T}"/> to create your own.
    /// </summary>
    public sealed class TimeModuleBehaviour : TimeModuleBehaviour<TimeModule> { }
}
