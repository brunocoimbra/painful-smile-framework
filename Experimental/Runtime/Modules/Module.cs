﻿using UnityEngine;

namespace PainfulSmile.Experimental
{
    public enum UpdateMode
    {
        /// <summary>
        /// Standard update.
        /// </summary>
        Update,
        /// <summary>
        /// Physics update.
        /// </summary>
        FixedUpdate,
        /// <summary>
        /// At the very end of frame.
        /// </summary>
        EndOfFrame,
        /// <summary>
        /// Manually updated.
        /// </summary>
        Manual
    }

    /// <summary>
    /// Base class for all modules.
    /// </summary>
    [System.Serializable]
    public class Module
    {
        #region Nested Enums

        public enum State
        {
            Default,
            Playing,
            Paused,
            Stopped,
            Finished
        }

        #endregion

        #region Serialized Fields

        [Tooltip("The current state.")]
        [SerializeField, ReadOnly] private State _state = State.Default;
        [Tooltip("The update cycle being used.")]
        [SerializeField, ReadWrite(ReadWriteMode.Write, ReadWriteMode.Read)] private UpdateMode _updateMode = UpdateMode.Update;
        [Tooltip("Used to control several modules together.")]
        [SerializeField] private string _tag = null;
        [Tooltip("Module's owner.")]
        [SerializeField] private GameObject _context = null;
        [Tooltip("The total amount of loops.")]
        [SerializeField, NotLessThan(1)] private int _loops = 1;
        [Tooltip("Should the module keep looping?")]
        [SerializeField] private bool _infinityLoop = false;
        [Tooltip("The amount of loops completed.")]
        [SerializeField, NotLessThan(0)] private int _completedLoops = 0;

        #endregion

        #region Public Events

        /// <summary>
        /// Event fired when the module finishes. 
        /// </summary>
        public System.Action onFinished;
        /// <summary>
        /// Event fired when the module complete a loop.
        /// </summary>
        public System.Action onLooped;
        /// <summary>
        /// Event fired when the module is paused.
        /// </summary>
        public System.Action onPaused;
        /// <summary>
        /// Event fired when the module is played.
        /// </summary>
        public System.Action onPlayed;
        /// <summary>
        /// Event fired when the module is reseted.
        /// </summary>
        public System.Action onReseted;
        /// <summary>
        /// Event fired when the module is stopped.
        /// </summary>
        public System.Action onStopped;

        #endregion

        #region Public Properties

        /// <summary>
        /// The module's normalized state.
        /// </summary>
        public virtual float normalized
        {
            get
            {
                Debug.LogException(new System.NotImplementedException(), context);

                return 0;
            }
        }
        /// <summary>
        /// The current state.
        /// </summary>
        public State state { get { return _state; } }
        /// <summary>
        /// The update cycle being used.
        /// </summary>
        public UpdateMode updateMode { get { return _updateMode; } }

        /// <summary>
        /// Should the module keep looping?
        /// </summary>
        public bool infinityLoop { get { return _infinityLoop; } set { _infinityLoop = value; } }
        /// <summary>
        /// The amount of loops completed.
        /// </summary>
        public int completedLoops { get { return _completedLoops; } set { _completedLoops = _infinityLoop ? Mathf.Max(0, value) : Mathf.Clamp(value, 0, loops); } }
        /// <summary>
        /// The total amount of loops.
        /// </summary>
        public int loops { get { return _loops; } set { _loops = Mathf.Max(1, value); } }
        /// <summary>
        /// Used to control several modules together.
        /// </summary>
        public string tag { get { return _tag; } set { _tag = value; } }
        /// <summary>
        /// Module's owner.
        /// </summary>
        public GameObject context { get { return _context; } set { _context = value; } }

        #endregion

        #region Constructors

        protected Module() { }

        protected Module(System.Action onFinished)
        {
            this.onFinished = onFinished;
        }

        protected Module(bool infinityLoop, System.Action callback)
        {
            this.infinityLoop = infinityLoop;

            if (infinityLoop)
            {
                onLooped = callback;
            }
            else
            {
                onFinished = callback;
            }
        }

        protected Module(int loops, System.Action onLooped, System.Action onFinished)
        {
            this.loops = loops;
            this.onLooped = onLooped;
            this.onFinished = onFinished;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Finish the module.
        /// </summary>
        public virtual void Anticipate()
        {
            OnFinish();
        }

        /// <summary>
        /// Reset all events.
        /// </summary>
        public virtual void ResetEvents()
        {
            onFinished = null;
            onLooped = null;
            onPaused = null;
            onPlayed = null;
            onReseted = null;
            onStopped = null;
        }

        /// <summary>
        /// Manually update the module.
        /// </summary>
        public void ForceUpdate()
        {
            OnUpdate();
        }

        /// <summary>
        /// Pause the module. It does not remove it from <seealso cref="ModuleManager"/>.
        /// </summary>
        public void Pause()
        {
            if (_state != State.Paused)
            {
                _state = State.Paused;

                if (onPaused != null)
                {
                    onPaused.Invoke();
                }
            }
        }

        /// <summary>
        /// Play the module, adding it to <seealso cref="ModuleManager"/>.
        /// </summary>
        public void Play()
        {
            OnPlay();
        }

        /// <summary>
        /// Play the module in the selected update mode, adding it to <seealso cref="ModuleManager"/>.
        /// </summary>
        public void Play(UpdateMode updateMode)
        {
            ModuleManager.Remove(this);

            _updateMode = updateMode;

            OnPlay();
        }

        /// <summary>
        /// Reset all values, except for events. If finished or stopped it returns to default state.
        /// </summary>
        public void Reset()
        {
            OnReset();

            if (_state == State.Finished || _state == State.Stopped)
            {
                _state = State.Default;
            }
        }

        /// <summary>
        /// Stop the module, removing it from <seealso cref="ModuleManager"/>.
        /// </summary>
        public void Stop()
        {
            ModuleManager.Remove(this);

            if (_state != State.Stopped)
            {
                _state = State.Stopped;

                if (onStopped != null)
                {
                    onStopped.Invoke();
                }
            }
        }

        #endregion

        #region Internal Methods

        internal void Update()
        {
            if (_state == State.Playing)
            {
                OnUpdate();
            }
        }

        #endregion

        #region Protected Methods

        protected virtual void OnUpdate()
        {
            Debug.LogException(new System.NotImplementedException(), context);
        }

        protected virtual void OnFinish()
        {
            completedLoops = loops;

            ModuleManager.Remove(this);

            if (_state != State.Finished)
            {
                _state = State.Finished;

                if (onFinished != null)
                {
                    onFinished.Invoke();
                }
            }
        }

        protected virtual void OnLoop()
        {
            if (onLooped != null)
            {
                onLooped.Invoke();
            }
        }

        protected virtual void OnReset()
        {
            completedLoops = 0;

            if (onReseted != null)
            {
                onReseted.Invoke();
            }
        }

        #endregion

        #region Private Methods

        private void OnPlay()
        {
            ModuleManager.Add(this);

            if (_state != State.Playing)
            {
                _state = State.Playing;

                if (onPlayed != null)
                {
                    onPlayed.Invoke();
                }
            }
        }

        #endregion
    }
}
