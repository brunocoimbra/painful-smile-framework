using UnityEngine;
using UnityEngine.Events;

namespace PainfulSmile.Experimental
{
    /// <summary>
    /// Used to call an action over time.
    /// </summary>
    [System.Serializable]
    public class Tween : TimeModule
    {
        #region Nested Delegates

        public delegate void UpdateTimeHandler(float unclampedTime);
        public delegate float EvaluateTimeHandler(float normalizedTime);

        #endregion

        #region Nested Classes

        [System.Serializable]
        public sealed class UpdateTimeEvent : UnityEvent<float> { }

        #endregion

        #region Serialized Fields

        [Tooltip("Should the tween be ping pong like?")]
        [SerializeField] private bool _isPingPong = false;
        [Tooltip("Is the tween current going back?")]
        [SerializeField] private bool _isGoingBack = false;

        #endregion

        #region Public Fields

        /// <summary>
        /// Event to control how the time should be evaluated.
        /// </summary>
        public EvaluateTimeHandler onEvaluateTime;
        /// <summary>
        /// Event fired each frame the tweeen updates.
        /// </summary>
        public UpdateTimeHandler onUpdateTime;
        /// <summary>
        /// Event fired when the tween starts going back on ping pong.
        /// </summary>
        public System.Action onPingPong;

        #endregion

        #region Public Properties

        /// <summary>
        /// Is the tween current going back?
        /// </summary>
        public bool isGoingBack { get { return _isGoingBack; } set { _isGoingBack = value; } }
        /// <summary>
        /// Should the tween be ping pong like?
        /// </summary>
        public bool isPingPong { get { return _isPingPong; } set { _isPingPong = value; } }

        #endregion

        #region Constructors

        public Tween() { }

        public Tween(float duration, EvaluateTimeHandler onEvaluateTime, UpdateTimeHandler onUpdateTime, System.Action onFinished = null) 
            : base(duration, onFinished)
        {
            this.onEvaluateTime = onEvaluateTime;
            this.onUpdateTime = onUpdateTime;
        }

        /// <param name="callback">If inifinityLoop is true, represents the onLooped event. Else, represents the onFinish event.</param>
        public Tween(float duration, bool infinityLoop, EvaluateTimeHandler onEvaluateTime, UpdateTimeHandler onUpdateTime, System.Action callback = null) 
            : base(duration, infinityLoop, callback)
        {
            this.onEvaluateTime = onEvaluateTime;
            this.onUpdateTime = onUpdateTime;
        }

        public Tween(float duration, int loops, EvaluateTimeHandler onEvaluateTime, UpdateTimeHandler onUpdateTime, System.Action onLooped = null, System.Action onFinished = null) 
            : base(duration, loops, onLooped, onFinished)
        {
            this.onEvaluateTime = onEvaluateTime;
            this.onUpdateTime = onUpdateTime;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Finish the module.
        /// </summary>
        public override void Anticipate()
        {
            UpdateTime((isPingPong ? 0 : 1));

            base.Anticipate();
        }

        /// <summary>
        /// Clear all events.
        /// </summary>
        public override void ResetEvents()
        {
            onEvaluateTime = null;
            onPingPong = null;
            onUpdateTime = null;

            base.ResetEvents();
        }

        /// <summary>
        /// Rewind the amount of time.
        /// </summary>
        public override void Rewind(float time)
        {
            time = Mathf.Abs(time);

            if (isPingPong)
            {
                while (time > 0)
                {
                    if (isGoingBack)
                    {
                        if (currentTime + time > duration)
                        {
                            time -= (duration - currentTime);
                            currentTime = duration;
                            isGoingBack = false;
                        }
                        else
                        {
                            currentTime += time;
                            break;
                        }
                    }
                    else
                    {
                        if (currentTime - time < 0)
                        {
                            time -= currentTime;
                            currentTime = 0;
                            completedLoops--;
                            isGoingBack = true;
                        }
                        else
                        {
                            currentTime -= time;
                            break;
                        }
                    }
                }

                if (completedLoops < 0)
                {
                    Reset();
                }
                else
                {
                    UpdateTime(normalized);
                }
            }
            else
            {
                currentTime -= time;

                while (currentTime < 0)
                {
                    currentTime += duration;
                    completedLoops--;
                }

                if (completedLoops < 0)
                {
                    Reset();
                }
                else
                {
                    UpdateTime(normalized);
                }
            }
        }

        /// <summary>
        /// Skip the amount of time.
        /// </summary>
        public override void Skip(float time)
        {
            time = Mathf.Abs(time);            

            if (isPingPong)
            {
                bool looped = false;

                while (time > 0)
                {
                    if (isGoingBack)
                    {
                        if (currentTime - time < 0)
                        {
                            time -= currentTime;
                            currentTime = 0;
                            completedLoops++;
                            isGoingBack = false;
                            looped = true;
                        }
                        else
                        {
                            currentTime -= time;
                            break;
                        }
                    }
                    else
                    {
                        if (currentTime + time >= duration)
                        {
                            time -= (duration - currentTime);
                            currentTime = duration;
                            isGoingBack = true;
                        }
                        else
                        {
                            currentTime += time;
                            break;
                        }
                    }
                }

                UpdateTime(normalized);

                if (looped)
                {
                    if (infinityLoop || completedLoops < loops)
                    {
                        if (onLooped != null)
                        {
                            onLooped.Invoke();
                        }
                    }
                    else
                    {
                        OnFinish();
                    }
                }
            }
            else
            {
                time = Mathf.Abs(time);
                currentTime += time;

                if (currentTime >= duration)
                {
                    while (currentTime >= duration)
                    {
                        currentTime -= duration;
                        completedLoops++;
                    }

                    UpdateTime(normalized);

                    if (infinityLoop || completedLoops < loops)
                    {
                        if (onLooped != null)
                        {
                            onLooped.Invoke();
                        }
                    }
                    else
                    {
                        OnFinish();
                    }
                }
                else
                {
                    UpdateTime(normalized);
                }
            }
        }

        #endregion

        #region Protected Methods

        protected virtual void OnUpdateTime(float unclampedTime)
        {
            if (onUpdateTime != null)
            {
                onUpdateTime.Invoke(unclampedTime);
            }
        }

        protected override void OnFinish()
        {
            isGoingBack = false;

            base.OnFinish();
        }

        protected override void OnReset()
        {
            UpdateTime(0);

            isGoingBack = false;

            base.OnReset();
        }

        protected override void OnUpdate()
        {
            if (isPingPong)
            {
                if (isGoingBack)
                {
                    currentTime -= deltaTime;

                    UpdateTime(normalized);

                    if (currentTime <= 0)
                    {
                        currentTime = 0;
                        isGoingBack = false;
                        completedLoops++;

                        if (infinityLoop || completedLoops < loops)
                        {
                            OnLoop();
                        }
                        else
                        {
                            OnFinish();
                        }
                    }
                }
                else
                {
                    currentTime += deltaTime;

                    UpdateTime(normalized);

                    if (currentTime >= duration)
                    {
                        currentTime = duration;
                        isGoingBack = true;

                        if (onPingPong != null)
                        {
                            onPingPong.Invoke();
                        }
                    }
                }
            }
            else
            {
                currentTime += deltaTime;

                UpdateTime(normalized);

                if (currentTime >= duration)
                {
                    completedLoops++;

                    if (infinityLoop || completedLoops < loops)
                    {
                        OnLoop();
                    }
                    else
                    {
                        OnFinish();
                    }
                }
            }
        }

        protected void UpdateTime(float normalizedTime)
        {
            if (onEvaluateTime != null)
            {
                OnUpdateTime(onEvaluateTime.Invoke(normalizedTime));
            }
            else
            {
                OnUpdateTime(normalizedTime);
            }
        }

        #endregion
    }
}
