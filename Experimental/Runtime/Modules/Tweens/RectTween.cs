using UnityEngine;

namespace PainfulSmile.Experimental
{
    /// <summary>
    /// Used to update a <seealso cref="Rect"/> over time.
    /// </summary>
    [System.Serializable]
    public sealed class RectTween : Tween
    {
        #region Nested Delegates

        public delegate void UpdateValueHandler(Rect value);

        #endregion

        #region Serialized Fields

        [Tooltip("The starting value.")]
        [SerializeField] private Rect _from = new Rect(Vector2.zero, Vector2.zero);
        [Tooltip("The target value.")]
        [SerializeField] private Rect _to = new Rect(Vector2.zero, Vector2.one);

        #endregion

        #region Public Fields

        /// <summary>
        /// Event fired each frame the tweeen updates. It returns the current value.
        /// </summary>
        public UpdateValueHandler onUpdateValue;

        #endregion

        #region Public Properties

        /// <summary>
        /// The starting value.
        /// </summary>
        public Rect from { get { return _from; } set { _from = value; } }
        /// <summary>
        /// The target value.
        /// </summary>
        public Rect to { get { return _to; } set { _to = value; } }

        #endregion

        #region Constructors

        public RectTween(Rect from, Rect to, float duration, EvaluateTimeHandler onEvaluateTime, UpdateValueHandler onUpdateValue, System.Action onFinished = null) 
            : base(duration, onEvaluateTime, null, onFinished)
        {
            this.from = from;
            this.to = to;
            this.onUpdateValue = onUpdateValue;
        }

        /// <param name="callback">If inifinityLoop is true, represents the onLooped event. Else, represents the onFinish event.</param>
        public RectTween(Rect from, Rect to, float duration, bool infinityLoop, EvaluateTimeHandler onEvaluateTime, UpdateValueHandler onUpdateValue, System.Action callback = null)
            : base(duration, infinityLoop, onEvaluateTime, null, callback)
        {
            this.from = from;
            this.to = to;
            this.onUpdateValue = onUpdateValue;
        }

        public RectTween(Rect from, Rect to, float duration, int loops, EvaluateTimeHandler onEvaluateTime, UpdateValueHandler onUpdateValue, System.Action onLooped = null, System.Action onFinished = null) 
            : base(duration, loops, onEvaluateTime, null, onLooped, onFinished)
        {
            this.from = from;
            this.to = to;
            this.onUpdateValue = onUpdateValue;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Clear all events.
        /// </summary>
        public override void ResetEvents()
        {
            onUpdateValue = null;

            base.ResetEvents();
        }

        #endregion

        #region Protected Methods

        protected override void OnUpdateTime(float unclampedTime)
        {
            if (onUpdateValue != null)
            {
                onUpdateValue.Invoke(new Rect(Mathf.LerpUnclamped(from.x, to.x, unclampedTime),
                                              Mathf.LerpUnclamped(from.y, to.y, unclampedTime),
                                              Mathf.LerpUnclamped(from.width, to.width, unclampedTime),
                                              Mathf.LerpUnclamped(from.height, to.height, unclampedTime)));
            }

            base.OnUpdateTime(unclampedTime);
        }

        #endregion
    }
}
