using UnityEngine;

namespace PainfulSmile.Experimental
{
    /// <summary>
    /// Used to update a <seealso cref="Vector3"/> over time.
    /// </summary>
    [System.Serializable]
    public sealed class Vector3Tween : Tween
    {
        #region Nested Delegates

        public delegate void UpdateValueHandler(Vector3 value);

        #endregion

        #region Serialized Fields

        [Tooltip("The starting value.")]
        [SerializeField] private Vector3 _from = Vector3.zero;
        [Tooltip("The target value.")]
        [SerializeField] private Vector3 _to = Vector3.one;

        #endregion

        #region Public Fields

        /// <summary>
        /// Event fired each frame the tweeen updates. It returns the current value.
        /// </summary>
        public UpdateValueHandler onUpdateValue;

        #endregion

        #region Public Properties

        /// <summary>
        /// The starting value.
        /// </summary>
        public Vector3 from { get { return _from; } set { _from = value; } }
        /// <summary>
        /// The target value.
        /// </summary>
        public Vector3 to { get { return _to; } set { _to = value; } }

        #endregion

        #region Constructors

        public Vector3Tween(Vector3 from, Vector3 to, float duration, EvaluateTimeHandler onEvaluateTime, UpdateValueHandler onUpdateValue, System.Action onFinished = null) 
            : base(duration, onEvaluateTime, null, onFinished)
        {
            this.from = from;
            this.to = to;
            this.onUpdateValue = onUpdateValue;
        }

        /// <param name="callback">If inifinityLoop is true, represents the onLooped event. Else, represents the onFinish event.</param>
        public Vector3Tween(Vector3 from, Vector3 to, float duration, bool infinityLoop, EvaluateTimeHandler onEvaluateTime, UpdateValueHandler onUpdateValue, System.Action callback = null) 
            : base(duration, infinityLoop, onEvaluateTime, null, callback)
        {
            this.from = from;
            this.to = to;
            this.onUpdateValue = onUpdateValue;
        }

        public Vector3Tween(Vector3 from, Vector3 to, float duration, int loops, EvaluateTimeHandler onEvaluateTime, UpdateValueHandler onUpdateValue, System.Action onLooped = null, System.Action onFinished = null) 
            : base(duration, loops, onEvaluateTime, null, onLooped, onFinished)
        {
            this.from = from;
            this.to = to;
            this.onUpdateValue = onUpdateValue;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Clear all events.
        /// </summary>
        public override void ResetEvents()
        {
            onUpdateValue = null;

            base.ResetEvents();
        }

        #endregion

        #region Protected Methods

        protected override void OnUpdateTime(float unclampedTime)
        {
            if (onUpdateValue != null)
            {
                onUpdateValue.Invoke(Vector3.LerpUnclamped(from, to, unclampedTime));
            }

            base.OnUpdateTime(unclampedTime);
        }

        #endregion
    }
}
