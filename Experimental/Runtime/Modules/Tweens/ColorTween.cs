using UnityEngine;

namespace PainfulSmile.Experimental
{
    /// <summary>
    /// Used to update a <seealso cref="Color"/> over time.
    /// </summary>
    [System.Serializable]
    public sealed class ColorTween : Tween
    {
        #region Nested Delegates

        public delegate void UpdateValueHandler(Color value);

        #endregion

        #region Serialized Fields

        [Tooltip("The starting value.")]
        [SerializeField] private Color _from = Color.clear;
        [Tooltip("The target value.")]
        [SerializeField] private Color _to = Color.white;

        #endregion

        #region Public Fields

        /// <summary>
        /// Event fired each frame the tweeen updates. It returns the current value.
        /// </summary>
        public UpdateValueHandler onUpdateValue;

        #endregion

        #region Public Properties

        /// <summary>
        /// The starting value.
        /// </summary>
        public Color from { get { return _from; } set { _from = value; } }
        /// <summary>
        /// The target value.
        /// </summary>
        public Color to { get { return _to; } set { _to = value; } }

        #endregion

        #region Constructors

        public ColorTween(Color from, Color to, float duration, EvaluateTimeHandler onEvaluateTime, UpdateValueHandler onUpdateValue, System.Action onFinished = null) 
            : base(duration, onEvaluateTime, null, onFinished)
        {
            this.from = from;
            this.to = to;
            this.onUpdateValue = onUpdateValue;
        }

        /// <param name="callback">If inifinityLoop is true, represents the onLooped event. Else, represents the onFinish event.</param>
        public ColorTween(Color from, Color to, float duration, bool infinityLoop, EvaluateTimeHandler onEvaluateTime, UpdateValueHandler onUpdateValue, System.Action callback = null) 
            : base(duration, infinityLoop, onEvaluateTime, null, callback)
        {
            this.from = from;
            this.to = to;
            this.onUpdateValue = onUpdateValue;
        }

        public ColorTween(Color from, Color to, float duration, int loops, EvaluateTimeHandler onEvaluateTime, UpdateValueHandler onUpdateValue, System.Action onLooped = null, System.Action onFinished = null) 
            : base(duration, loops, onEvaluateTime, null, onLooped, onFinished)
        {
            this.from = from;
            this.to = to;
            this.onUpdateValue = onUpdateValue;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Clear all events.
        /// </summary>
        public override void ResetEvents()
        {
            onUpdateValue = null;

            base.ResetEvents();
        }

        #endregion

        #region Protected Methods

        protected override void OnUpdateTime(float unclampedTime)
        {
            if (onUpdateValue != null)
            {
                onUpdateValue.Invoke(Color.LerpUnclamped(from, to, unclampedTime));
            }

            base.OnUpdateTime(unclampedTime);
        }

        #endregion
    }
}
