using UnityEngine;

namespace PainfulSmile.Experimental
{
    /// <summary>
    /// Used to update a <seealso cref="Quaternion"/> over time.
    /// </summary>
    [System.Serializable]
    public sealed class QuaternionTween : Tween
    {
        #region Nested Delegates

        public delegate void UpdateValueHandler(Quaternion value);

        #endregion

        #region Serialized Fields

        [Tooltip("The starting value.")]
        [SerializeField] private Quaternion _from = Quaternion.identity;
        [Tooltip("The target value.")]
        [SerializeField] private Quaternion _to = Quaternion.Euler(0, 0, 180);

        #endregion

        #region Public Fields

        /// <summary>
        /// Event fired each frame the tweeen updates. It returns the current value.
        /// </summary>
        public UpdateValueHandler onUpdateValue;

        #endregion

        #region Public Properties

        /// <summary>
        /// The starting value.
        /// </summary>
        public Quaternion from { get { return _from; } set { _from = value; } }
        /// <summary>
        /// The target value.
        /// </summary>
        public Quaternion to { get { return _to; } set { _to = value; } }

        #endregion

        #region Constructors

        public QuaternionTween(Quaternion from, Quaternion to, float duration, EvaluateTimeHandler onEvaluateTime, UpdateValueHandler onUpdateValue, System.Action onFinished = null) 
            : base(duration, onEvaluateTime, null, onFinished)
        {
            this.from = from;
            this.to = to;
            this.onUpdateValue = onUpdateValue;
        }

        /// <param name="callback">If inifinityLoop is true, represents the onLooped event. Else, represents the onFinish event.</param>
        public QuaternionTween(Quaternion from, Quaternion to, float duration, bool infinityLoop, EvaluateTimeHandler onEvaluateTime, UpdateValueHandler onUpdateValue, System.Action callback = null) 
            : base(duration, infinityLoop, onEvaluateTime, null, callback)
        {
            this.from = from;
            this.to = to;
            this.onUpdateValue = onUpdateValue;
        }

        public QuaternionTween(Quaternion from, Quaternion to, float duration, int loops, EvaluateTimeHandler onEvaluateTime, UpdateValueHandler onUpdateValue, System.Action onLooped = null, System.Action onFinished = null) 
            : base(duration, loops, onEvaluateTime, null, onLooped, onFinished)
        {
            this.from = from;
            this.to = to;
            this.onUpdateValue = onUpdateValue;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Clear all events.
        /// </summary>
        public override void ResetEvents()
        {
            onUpdateValue = null;

            base.ResetEvents();
        }

        #endregion

        #region Protected Methods

        protected override void OnUpdateTime(float unclampedTime)
        {
            if (onUpdateValue != null)
            {
                onUpdateValue.Invoke(Quaternion.LerpUnclamped(from, to, unclampedTime));
            }

            base.OnUpdateTime(unclampedTime);
        }

        #endregion
    }
}
