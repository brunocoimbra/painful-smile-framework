using UnityEngine;

namespace PainfulSmile.Experimental
{
    /// <summary>
    /// Used to update a <seealso cref="Bounds"/> over time.
    /// </summary>
    [System.Serializable]
    public sealed class BoundsTween : Tween
    {
        #region Nested Delegates

        public delegate void UpdateValueHandler(Bounds value);

        #endregion

        #region Serialized Fields

        [Tooltip("The starting value.")]
        [SerializeField] private Bounds _from = new Bounds(Vector3.zero, Vector3.zero);
        [Tooltip("The target value.")]
        [SerializeField] private Bounds _to = new Bounds(Vector3.zero, Vector3.one);

        #endregion

        #region Public Fields

        /// <summary>
        /// Event fired each frame the tweeen updates. It returns the current value.
        /// </summary>
        public UpdateValueHandler onUpdateValue;

        #endregion

        #region Public Properties

        /// <summary>
        /// The starting value.
        /// </summary>
        public Bounds from { get { return _from; } set { _from = value; } }
        /// <summary>
        /// The target value.
        /// </summary>
        public Bounds to { get { return _to; } set { _to = value; } }

        #endregion

        #region Constructors

        public BoundsTween(Bounds from, Bounds to, float duration, EvaluateTimeHandler onEvaluateTime, UpdateValueHandler onUpdateValue, System.Action onFinished = null) 
            : base(duration, onEvaluateTime, null, onFinished)
        {
            this.from = from;
            this.to = to;
            this.onUpdateValue = onUpdateValue;
        }

        /// <param name="callback">If inifinityLoop is true, represents the onLooped event. Else, represents the onFinish event.</param>
        public BoundsTween(Bounds from, Bounds to, float duration, bool infinityLoop, EvaluateTimeHandler onEvaluateTime, UpdateValueHandler onUpdateValue, System.Action callback = null) 
            : base(duration, infinityLoop, onEvaluateTime, null, callback)
        {
            this.from = from;
            this.to = to;
            this.onUpdateValue = onUpdateValue;
        }

        public BoundsTween(Bounds from, Bounds to, float duration, int loops, EvaluateTimeHandler onEvaluateTime, UpdateValueHandler onUpdateValue, System.Action onLooped = null, System.Action onFinished = null)
            : base(duration, loops, onEvaluateTime, null, onLooped, onFinished)
        {
            this.from = from;
            this.to = to;
            this.onUpdateValue = onUpdateValue;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Clear all events.
        /// </summary>
        public override void ResetEvents()
        {
            onUpdateValue = null;

            base.ResetEvents();
        }

        #endregion

        #region Protected Methods

        protected override void OnUpdateTime(float unclampedTime)
        {
            if (onUpdateValue != null)
            {
                onUpdateValue.Invoke(new Bounds(new Vector3(Mathf.LerpUnclamped(from.center.x, to.center.x, unclampedTime),
                                                            Mathf.LerpUnclamped(from.center.y, to.center.y, unclampedTime),
                                                            Mathf.LerpUnclamped(from.center.z, to.center.z, unclampedTime)),
                                                new Vector3(Mathf.LerpUnclamped(from.size.x, to.size.x, unclampedTime),
                                                            Mathf.LerpUnclamped(from.size.y, to.size.y, unclampedTime),
                                                            Mathf.LerpUnclamped(from.size.z, to.size.z, unclampedTime))));
            }

            base.OnUpdateTime(unclampedTime);
        }

        #endregion
    }
}
