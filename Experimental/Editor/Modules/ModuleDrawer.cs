﻿using UnityEditor;
using UnityEngine;

namespace PainfulSmile.Experimental
{
    [CustomPropertyDrawer(typeof(Module), true)]
    public class ModuleDrawer : PropertyDrawer
    {
        #region Constant Fields

        protected const float Space = 6f;

        #endregion

        #region Public Methods
        
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            DrawHeaderGUI(position, property, label);

            if (property.isExpanded)
            {
                EditorGUI.indentLevel++;
                {
                    position.y += GetHeaderGUIHeight(property, label) + EditorGUIUtility.standardVerticalSpacing;
                    DrawMainGUI(position, property);

                    position.y += GetMainGUIHeight(property) + EditorGUIUtility.standardVerticalSpacing;
                    DrawLoopGUI(position, property);
                }
                EditorGUI.indentLevel--;
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            float height = GetHeaderGUIHeight(property, label);

            if (property.isExpanded)
            {
                height += GetMainGUIHeight(property) + EditorGUIUtility.standardVerticalSpacing;
                height += GetLoopGUIHeight(property) + EditorGUIUtility.standardVerticalSpacing;
            }

            return height;
        }

        #endregion

        #region Protected Methods

        protected static void DrawHeaderGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            position.width = EditorGUIUtility.labelWidth;
            position.height = EditorGUIUtility.singleLineHeight;

            SerializedProperty _tag = property.FindPropertyRelative("_tag");

            if (string.IsNullOrEmpty(_tag.stringValue) == false)
            {
                label.text = string.Format("{0} ({1})", label.text, _tag.stringValue);
            }

            EditorGUI.PropertyField(position, property, label, false);

            float foldoutWidth = position.x + 4;

            int indentLevel = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;
            {
                position.x += position.width;
                position.width = (EditorGUIUtility.currentViewWidth - EditorGUIUtility.labelWidth - foldoutWidth) / 2;
                EditorGUI.PropertyField(position, property.FindPropertyRelative("_state"), GUIContent.none);

                position.x += position.width;
                EditorGUI.PropertyField(position, property.FindPropertyRelative("_updateMode"), GUIContent.none);
            }
            EditorGUI.indentLevel = indentLevel;
        }

        protected static void DrawLoopGUI(Rect position, SerializedProperty property)
        {
            SerializedProperty _loops = property.FindPropertyRelative("_loops");
            SerializedProperty _infinityLoop = property.FindPropertyRelative("_infinityLoop");

            position.height = EditorGUIUtility.singleLineHeight;
            Rect fieldPosition = EditorGUI.PrefixLabel(position, new GUIContent(_loops.displayName, "The total amount of loops."));

            int indentLevel = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;
            {
                float boolLabelWidth = EditorStyles.label.CalcSize(new GUIContent("Infinity?")).x;
                float boolFieldWidth = EditorStyles.toggle.CalcSize(GUIContent.none).x;
                fieldPosition.width -= boolLabelWidth + boolFieldWidth;

                if (_infinityLoop.boolValue)
                {
                    using (new EditorGUI.DisabledScope(true))
                    {
                        EditorGUI.TextField(fieldPosition, GUIContent.none, "Infinity");
                    }
                }
                else
                {
                    EditorGUI.PropertyField(fieldPosition, _loops, GUIContent.none);
                }

                fieldPosition.x += fieldPosition.width;
                fieldPosition.width = boolLabelWidth;
                EditorGUI.LabelField(fieldPosition, "Infinity?");

                fieldPosition.x += fieldPosition.width;
                fieldPosition.width = boolFieldWidth;
                EditorGUI.PropertyField(fieldPosition, _infinityLoop, GUIContent.none);
            }
            EditorGUI.indentLevel = indentLevel;

            if (_infinityLoop.boolValue == false)
            {
                SerializedProperty _completedLoops = property.FindPropertyRelative("_completedLoops");

                position.y += position.height + EditorGUIUtility.standardVerticalSpacing;
                EditorGUI.IntSlider(position, _completedLoops, 0, _loops.intValue, new GUIContent("Completed Loops", _completedLoops.tooltip));
            }
        }

        protected static void DrawMainGUI(Rect position, SerializedProperty property)
        {
            position.height = EditorGUIUtility.singleLineHeight;
            EditorGUI.PropertyField(position, property.FindPropertyRelative("_tag"));

            position.y += position.height + EditorGUIUtility.standardVerticalSpacing;
            EditorGUI.PropertyField(position, property.FindPropertyRelative("_context"));
        }

        protected static float GetHeaderGUIHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUIUtility.singleLineHeight;
        }

        protected static float GetLoopGUIHeight(SerializedProperty property)
        {
            if (property.FindPropertyRelative("_infinityLoop").boolValue)
            {
                return EditorGUIUtility.singleLineHeight;
            }
            else
            {
                return EditorGUIUtility.singleLineHeight * 2 + EditorGUIUtility.standardVerticalSpacing;
            }
        }

        protected static float GetMainGUIHeight(SerializedProperty property)
        {
            return EditorGUIUtility.singleLineHeight * 2 + EditorGUIUtility.standardVerticalSpacing;
        }

        #endregion
    }
}
