﻿using UnityEngine;
using UnityEditor;
using UnityEditor.AnimatedValues;

namespace PainfulSmile.Experimental
{
    [CustomEditor(typeof(BezierPoint)), CanEditMultipleObjects]
    internal sealed class BezierPointEditor : Editor
    {
        #region Constant Fields

        public const float CircleCapSize = 0.075f;
        public const float PointCapSize = RectangeCapSize;
        public const float RectangeCapSize = 0.1f;
        public const float SphereCapSize = 0.15f;

        #endregion

        #region Public Fields

        public static float handleCapSize = CircleCapSize;

        #endregion

        #region Private Fields

        private AnimFloat _animFloat;
        private SerializedProperty _tangentType;
        private SerializedProperty _tangentA;
        private SerializedProperty _tangentB;

        #endregion

        #region Private Properties

        private float animFloatTarget
        {
            get
            {
                BezierPoint.TangentType value = (BezierPoint.TangentType)_tangentType.intValue;

                switch (value)
                {
                    case BezierPoint.TangentType.None:
                        return 0;

                    default:
                        return 1;
                }
            }
        }

        #endregion

        #region Unity Callbacks

        private void OnEnable()
        {
            UnityEnable();
        }

        private void OnSceneGUI()
        {
            UnitySceneGUI();
        }

        #endregion

        #region Public Methods

        public static void DrawPointSceneGUI(BezierPoint point)
        {
            DrawPointSceneGUI(point, Handles.RectangleHandleCap, Handles.CircleHandleCap);
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            using (EditorGUI.ChangeCheckScope objectChangeCheck = new EditorGUI.ChangeCheckScope())
            {
                EditorGUILayout.PropertyField(serializedObject.FindProperty("_positionTransform"));

                using (EditorGUI.ChangeCheckScope tangentChangeCheck = new EditorGUI.ChangeCheckScope())
                {
                    EditorGUILayout.PropertyField(_tangentType);

                    if (tangentChangeCheck.changed)
                    {
                        _animFloat.target = animFloatTarget;

                        if (((BezierPoint.TangentType)_tangentType.intValue) == BezierPoint.TangentType.Flat)
                        {
                            _tangentB.vector3Value = -_tangentA.vector3Value;
                        }
                    }
                }

                using (EditorGUILayout.FadeGroupScope tangentFadeGroup = new EditorGUILayout.FadeGroupScope(_animFloat.value))
                {
                    if (tangentFadeGroup.visible && (_tangentType.hasMultipleDifferentValues == false || serializedObject.isEditingMultipleObjects == false))
                    {
                        if (((BezierPoint.TangentType)_tangentType.intValue) == BezierPoint.TangentType.Broken)
                        {
                            EditorGUILayout.PropertyField(_tangentA);
                            EditorGUILayout.PropertyField(_tangentB);
                        }
                        else
                        {
                            using (EditorGUI.ChangeCheckScope tangentChangeCheck = new EditorGUI.ChangeCheckScope())
                            {
                                EditorGUILayout.PropertyField(_tangentA);

                                if (tangentChangeCheck.changed)
                                {
                                    _tangentB.vector3Value = -_tangentA.vector3Value;
                                }
                            }

                            using (EditorGUI.ChangeCheckScope tangentChangeCheck = new EditorGUI.ChangeCheckScope())
                            {
                                EditorGUILayout.PropertyField(_tangentB);

                                if (tangentChangeCheck.changed)
                                {
                                    _tangentA.vector3Value = -_tangentB.vector3Value;
                                }
                            }
                        }
                    }
                }

                if (objectChangeCheck.changed)
                {
                    serializedObject.ApplyModifiedProperties();

                    for (int i = 0; i < serializedObject.targetObjects.Length; i++)
                    {
                        BezierPoint point = serializedObject.targetObjects[i] as BezierPoint;

                        if (point.curve != null)
                        {
                            point.curve.ForceUpdate();
                        }
                    }
                }
            }
        }

        #endregion

        #region Private Methods

        private static void DrawPointSceneGUI(BezierPoint point, Handles.CapFunction onDrawPoint, Handles.CapFunction onDrawTangent)
        {
            Color color = Handles.color;
            {
                Handles.color = Color.black;
                Handles.Label(point.position + new Vector3(0f, HandleUtility.GetHandleSize(point.position) * 0.4f, 0f), point.gameObject.name);

                Handles.color = Color.yellow;
                Vector3 pointPosition = Handles.FreeMoveHandle(point.position, point.transform.rotation, HandleUtility.GetHandleSize(point.position) * PointCapSize, Vector3.one * 0.5f, onDrawPoint);

                if (point.position != pointPosition)
                {
                    Undo.RegisterCompleteObjectUndo(point.transform, "Move Point");

                    point.position = pointPosition;
                }

                switch (point.tangentType)
                {
                    case BezierPoint.TangentType.Flat:
                    case BezierPoint.TangentType.Broken:
                        Handles.color = Color.white;
                        Handles.DrawLine(point.position, point.tangentAWorldPosition);
                        Handles.DrawLine(point.position, point.tangentBWorldPosition);

                        Handles.color = Color.cyan;
                        Vector3 tangentAWorldPosition = Handles.FreeMoveHandle(point.tangentAWorldPosition, point.transform.rotation, HandleUtility.GetHandleSize(point.tangentAWorldPosition) * handleCapSize, Vector3.zero, onDrawTangent);

                        if (point.tangentAWorldPosition != tangentAWorldPosition)
                        {
                            Undo.RegisterCompleteObjectUndo(point, "Move Left Tangent");

                            point.tangentAWorldPosition = tangentAWorldPosition;
                        }

                        Vector3 tangentBWorldPosition = Handles.FreeMoveHandle(point.tangentBWorldPosition, point.transform.rotation, HandleUtility.GetHandleSize(point.tangentBWorldPosition) * handleCapSize, Vector3.zero, onDrawTangent);

                        if (point.tangentBWorldPosition != tangentBWorldPosition)
                        {
                            Undo.RegisterCompleteObjectUndo(point, "Move Right Tangent");

                            point.tangentBWorldPosition = tangentBWorldPosition;
                        }
                        break;
                }
            }
            Handles.color = color;

            if (point.transform.hasChanged)
            {
                point.transform.hasChanged = false;

                if (point.curve != null)
                {
                    point.curve.ForceUpdate();
                }
            }
        }

        private static bool MouseButtonDown(int button)
        {
            return Event.current.type == EventType.MouseDown && Event.current.button == button;
        }

        private static bool MouseButtonUp(int button)
        {
            return Event.current.type == EventType.MouseUp && Event.current.button == button;
        }

        private void UnityEnable()
        {
            _tangentType = serializedObject.FindProperty("_tangentType");
            _tangentA = serializedObject.FindProperty("_tangentA");
            _tangentB = serializedObject.FindProperty("_tangentB");
            _animFloat = new AnimFloat(animFloatTarget, Repaint);

            for (int i = 0; i < targets.Length; i++)
            {
                BezierPoint point = targets[i] as BezierPoint;

                if (point.curve.ContainsKeyPoint(point) == false)
                {
                    point.curve.AddKeyPoint(point);
                }
            }
        }

        private void UnitySceneGUI()
        {
            BezierPoint __point = target as BezierPoint;

            handleCapSize = CircleCapSize;
            BezierCurveEditor.DrawPointsSceneGUI(__point.curve, __point);

            handleCapSize = SphereCapSize;
            DrawPointSceneGUI(__point, Handles.DotHandleCap, Handles.SphereHandleCap);
        }

        #endregion
    }
}
