﻿using UnityEditor;
using UnityEngine;

namespace PainfulSmile.Experimental
{
    [CustomPropertyDrawer(typeof(BezierModule), true)]
    internal sealed class BezierModuleDrawer : ModuleDrawer
    {
        #region Public Methods

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            DrawHeaderGUI(position, property, label);

            if (property.isExpanded)
            {
                EditorGUI.indentLevel++;
                {
                    position.y += GetHeaderGUIHeight(property, label) + EditorGUIUtility.standardVerticalSpacing;
                    DrawMainGUI(position, property);

                    position.y += GetMainGUIHeight(property) + EditorGUIUtility.standardVerticalSpacing + Space;
                    DrawMovementGUI(position, property);

                    position.y += GetMovementGUIHeight(property) + EditorGUIUtility.standardVerticalSpacing;
                    DrawTimeGUI(position, property);

                    position.y += GetTimeGUIHeight(property) + EditorGUIUtility.standardVerticalSpacing;
                    DrawPingPongGUI(position, property);

                    position.y += GetPingPongGUIHeight(property) + EditorGUIUtility.standardVerticalSpacing;
                    DrawLoopGUI(position, property);
                }
                EditorGUI.indentLevel--;
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            float height = GetHeaderGUIHeight(property, label);

            if (property.isExpanded)
            {
                height += GetMainGUIHeight(property) + EditorGUIUtility.standardVerticalSpacing + Space;
                height += GetMovementGUIHeight(property) + EditorGUIUtility.standardVerticalSpacing;
                height += GetTimeGUIHeight(property) + EditorGUIUtility.standardVerticalSpacing;
                height += GetLoopGUIHeight(property) + EditorGUIUtility.standardVerticalSpacing;
                height += GetPingPongGUIHeight(property) + EditorGUIUtility.standardVerticalSpacing;
            }

            return height;
        }

        #endregion

        #region Private Methods

        private static void DrawMovementGUI(Rect position, SerializedProperty property)
        {
            position.height = EditorGUIUtility.singleLineHeight;

            SerializedProperty _curve = property.FindPropertyRelative("_curve");
            EditorGUI.PropertyField(position, _curve);

            position.y += position.height + EditorGUIUtility.standardVerticalSpacing;
            EditorGUI.PropertyField(position, property.FindPropertyRelative("_speed"));

            if (_curve.hasMultipleDifferentValues == false)
            {
                BezierCurve curve = _curve.objectReferenceValue as BezierCurve;

                position.y += position.height + EditorGUIUtility.standardVerticalSpacing;
                EditorGUI.Slider(position, property.FindPropertyRelative("_displacement"), 0, curve != null ? curve.totalLength : 0);
            }
        }

        private static void DrawPingPongGUI(Rect position, SerializedProperty property)
        {
            position.height = EditorGUIUtility.singleLineHeight;
            position = EditorGUI.PrefixLabel(position, new GUIContent("Ping Pong", "Should the tween be ping pong like?."));

            int indentLevel = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;
            {
                float boolLabelWidth = EditorStyles.label.CalcSize(new GUIContent("Is Going Back?")).x;
                float boolFieldWidth = EditorStyles.toggle.CalcSize(GUIContent.none).x;
                Rect sidePosition = position;

                position.width = boolFieldWidth;
                EditorGUI.PropertyField(position, property.FindPropertyRelative("_isPingPong"), GUIContent.none);

                sidePosition.x += boolFieldWidth;
                sidePosition.width = boolLabelWidth;
                EditorGUI.LabelField(sidePosition, "Is Going Back?");

                sidePosition.x += sidePosition.width;
                sidePosition.width = boolFieldWidth;
                EditorGUI.PropertyField(sidePosition, property.FindPropertyRelative("_isGoingBack"), GUIContent.none);
            }
            EditorGUI.indentLevel = indentLevel;
        }

        private static void DrawTimeGUI(Rect position, SerializedProperty property)
        {
            position.height = EditorGUIUtility.singleLineHeight;
            position = EditorGUI.PrefixLabel(position, new GUIContent("Time Scale", "The local time scale."));

            int indentLevel = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;
            {
                float boolLabelWidth = EditorStyles.label.CalcSize(new GUIContent("Use Delta Time?")).x;
                float boolFieldWidth = EditorStyles.toggle.CalcSize(GUIContent.none).x;

                position.width -= boolLabelWidth + boolFieldWidth;
                EditorGUI.PropertyField(position, property.FindPropertyRelative("_timeScale"), GUIContent.none);

                position.x += position.width;
                position.width = boolLabelWidth;
                EditorGUI.LabelField(position, "Use Delta Time?");

                position.x += position.width;
                position.width = boolFieldWidth;
                EditorGUI.PropertyField(position, property.FindPropertyRelative("_useDeltaTime"), GUIContent.none);
            }
            EditorGUI.indentLevel = indentLevel;
        }

        private static float GetMovementGUIHeight(SerializedProperty property)
        {
            if (property.FindPropertyRelative("_curve").hasMultipleDifferentValues)
            {
                return EditorGUIUtility.singleLineHeight * 2 + EditorGUIUtility.standardVerticalSpacing;
            }
            else
            {
                return EditorGUIUtility.singleLineHeight * 3 + EditorGUIUtility.standardVerticalSpacing * 2;
            }
        }

        private static float GetPingPongGUIHeight(SerializedProperty property)
        {
            return EditorGUIUtility.singleLineHeight;
        }

        private static float GetTimeGUIHeight(SerializedProperty property)
        {
            return EditorGUIUtility.singleLineHeight;
        }

        #endregion
    }
}
