﻿using UnityEngine;
using UnityEditor;

namespace PainfulSmile.Experimental
{
    [CustomEditor(typeof(BezierCurve)), CanEditMultipleObjects]
    internal sealed class BezierCurveEditor : Editor
    {
        #region Constant Fields

        private static readonly GUIContent GizmosColorContent = new GUIContent("Gizmos Colors", "Gizmos colors for debug:\n\nLeft: curve segments.\nMiddle: start point.\nRight: end point.");
        private static readonly GUIContent AddAfterContent = new GUIContent("Add After");
        private static readonly GUIContent AddBeforeContent = new GUIContent("Add Before");
        private static readonly GUIContent RemoveContent = new GUIContent("X");

        #endregion

        #region Private Fields

        private static bool UndoRedoFlag;

        private ReorderableList _points;

        #endregion

        #region Unity Callbacks

        private static void CreateBezierCurve()
        {
            BezierCurve curve = new GameObject("Bezier Curve", typeof(BezierCurve)).GetComponent<BezierCurve>();

            if (Camera.current != null)
            {
                curve.transform.position = Camera.current.transform.position + Camera.current.transform.forward * 10f;
            }

            BezierPoint start = curve.AddKeyPoint();
            start.localPosition = new Vector3(-1f, 0f, 0f);
            start.tangentAPosition = new Vector3(-0.35f, -0.35f, 0f);

            BezierPoint end = curve.AddKeyPoint();
            end.localPosition = new Vector3(1f, 0f, 0f);
            end.tangentAPosition = new Vector3(-0.35f, 0.35f, 0f);

            Undo.RegisterCreatedObjectUndo(curve.gameObject, "Create Bezier Curve");

            Selection.activeGameObject = curve.gameObject;
        }

        private void OnEnable()
        {
            UnityEnable();
        }

        private void OnSceneGUI()
        {
            UnitySceneGUI();
        }

        #endregion

        #region Public Methods

        public static void DrawPointsSceneGUI(BezierCurve curve, BezierPoint exception = null)
        {
            if (curve == null || curve.gizmosMode == BezierCurve.GizmosMode.None || (curve.gizmosMode == BezierCurve.GizmosMode.Selected && exception != null))
            {
                return;
            }

            if (UndoRedoFlag)
            {
                UndoRedoFlag = false;

                curve.ForceUpdate();
            }

            for (int i = 0; i < curve.count; i++)
            {
                if (curve[i] == exception)
                {
                    continue;
                }

                BezierPointEditor.handleCapSize = BezierPointEditor.CircleCapSize;
                BezierPointEditor.DrawPointSceneGUI(curve[i]);
            }
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            using (EditorGUI.ChangeCheckScope changeCheck = new EditorGUI.ChangeCheckScope())
            {
                float labelWidth = EditorGUIUtility.labelWidth;
                EditorGUIUtility.labelWidth = EditorGUIUtility.currentViewWidth - 212;
                {
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("_gizmosMode"));
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("_gizmosLines"));

                    using (EditorGUILayout.HorizontalScope horizontal = new EditorGUILayout.HorizontalScope())
                    {
                        Rect position = EditorGUILayout.GetControlRect();
                        position = EditorGUI.PrefixLabel(position, GizmosColorContent);

                        position.width /= 3;
                        EditorGUI.PropertyField(position, serializedObject.FindProperty("_curveColor"), GUIContent.none);

                        position.x += position.width;
                        EditorGUI.PropertyField(position, serializedObject.FindProperty("_startColor"), GUIContent.none);

                        position.x += position.width;
                        EditorGUI.PropertyField(position, serializedObject.FindProperty("_endColor"), GUIContent.none);
                    }

                    EditorGUILayout.PropertyField(serializedObject.FindProperty("_gizmosTime"));
                    EditorGUILayout.Separator();
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("_force2D"));
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("_isClosed"));
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("_pointsPerSegment"));
                    EditorGUILayout.Separator();
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("_totalLength"));
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("_totalPoints"));
                }
                EditorGUIUtility.labelWidth = labelWidth;

                if (changeCheck.changed)
                {
                    serializedObject.ApplyModifiedProperties();

                    for (int i = 0; i < targets.Length; i++)
                    {
                        (targets[i] as BezierCurve).ForceUpdate();
                    }
                }
            }

            if (serializedObject.isEditingMultipleObjects == false)
            {
                using (new EditorGUILayout.HorizontalScope())
                {
                    if (GUILayout.Button("Add Key Point"))
                    {
                        BezierCurve curve = target as BezierCurve;
                        AddPointAt(curve, curve.count);
                    }

                    if (GUILayout.Button("Add Key Point and Select"))
                    {
                        BezierCurve curve = target as BezierCurve;
                        Selection.activeGameObject = AddPointAt(curve, curve.count).gameObject;
                    }
                }

                _points.DrawGUILayout();
            }
        }

        public override bool RequiresConstantRepaint()
        {
            return true;
        }

        #endregion

        #region Private Methods

        private static void RemovePointAt(BezierCurve curve, int index)
        {
            BezierPoint point = curve[index];

            Undo.IncrementCurrentGroup();
            Undo.RegisterCompleteObjectUndo(curve, "Save Curve");
            curve.RemoveKeyPointAt(index, false);
            Undo.DestroyObjectImmediate(point.gameObject);
        }

        private static void HandleUndoRedoPerformed()
        {
            UndoRedoFlag = true;
        }

        private static BezierPoint AddPointAt(BezierCurve curve, int index)
        {
            BezierPoint point = BezierPoint.CreateInstance(curve);

            if (curve.count == 0)
            {
                point.localPosition = Vector3.zero;
            }
            else if (curve.count == 1)
            {
                point.localPosition = curve[0].localPosition + Vector3.right;
            }
            else
            {
                if (index == 0)
                {
                    point.position = (curve[0].position - curve[1].position).normalized + curve[0].position;
                }
                else if (index == curve.count)
                {
                    point.position = (curve[index - 1].position - curve[index - 2].position).normalized + curve[index - 1].position;
                }
                else
                {
                    point.position = BezierCurve.Position(0.5f, curve[index - 1], curve[index]);
                }
            }

            Undo.IncrementCurrentGroup();
            Undo.RegisterCreatedObjectUndo(point.gameObject, "Create Key Point");
            Undo.RegisterCompleteObjectUndo(curve, "Save Curve");
            curve.AddKeyPointAt(point, index);

            return point;
        }

        private void HandleDrawElement(ReorderableList list, Rect rect, SerializedProperty element, GUIContent label, bool selected, bool focused)
        {
            BezierCurve curve = target as BezierCurve;
            int index = _points.IndexOf(element);

            float addAfterWidth = GUI.skin.button.CalcSize(AddAfterContent).x;
            float addBeforeWidth = GUI.skin.button.CalcSize(AddBeforeContent).x;
            float removeWidth = GUI.skin.button.CalcSize(RemoveContent).x;

            rect.y += 1;
            rect.height = EditorGUIUtility.singleLineHeight;
            rect.width -= addAfterWidth + addBeforeWidth + removeWidth + 4 + 2 + 2;
            EditorGUI.PropertyField(rect, element, GUIContent.none);

            rect.x += rect.width + 4;
            rect.width = addBeforeWidth;

            if (GUI.Button(rect, AddBeforeContent))
            {
                AddPointAt(curve, index);
            }

            rect.x += rect.width + 2;
            rect.width = addAfterWidth;

            if (GUI.Button(rect, AddAfterContent))
            {
                AddPointAt(curve, index + 1);
            }

            rect.x += rect.width + 2;
            rect.width = removeWidth;

            using (new EditorGUI.DisabledScope(curve.count <= 2))
            {
                if (GUI.Button(rect, RemoveContent))
                {
                    RemovePointAt(curve, index);
                }
            }
        }

        private void UnityEnable()
        {
            Undo.undoRedoPerformed -= HandleUndoRedoPerformed;
            Undo.undoRedoPerformed += HandleUndoRedoPerformed;

            _points = new ReorderableList(serializedObject.FindProperty("_keyPoints"), ReorderablePermissions.Drag | ReorderablePermissions.Collapse | ReorderablePermissions.EditElements);
            _points.OnDrawElement += HandleDrawElement;
        }

        private void UnitySceneGUI()
        {
            DrawPointsSceneGUI(target as BezierCurve);
        }

        #endregion
    }
}
