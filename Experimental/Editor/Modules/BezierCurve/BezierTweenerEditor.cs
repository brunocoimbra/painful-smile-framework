﻿using UnityEditor;
using UnityEngine;

namespace PainfulSmile.Experimental
{
    [CustomEditor(typeof(BezierTweener)), CanEditMultipleObjects]
    internal sealed class BezierTweenerEditor : TweenerEditor
    {
        #region Private Fields

        private SerializedProperty _onReachKeyPoints;
        private UnityEventDrawer _eventDrawer;

        #endregion

        #region Protected Methods

        protected override void DrawCallbackButtons()
        {
            base.DrawCallbackButtons();

            int i = 0;
            int split = 2;
            int left = _onReachKeyPoints.arraySize % split;
            Rect rect;

            for (; i < _onReachKeyPoints.arraySize - left; i += split)
            {
                rect = GetCallbackControlRect();
                rect.width /= split;

                for (int j = 0; j < split; j++)
                {
                    CallbackButton(rect, _onReachKeyPoints.GetArrayElementAtIndex(i + j), string.Format("On Reach Key Point [{0}]", i + j));
                    rect.x += rect.width;
                }
            }

            if (left != 0)
            {
                rect = GetCallbackControlRect();
                rect.width /= split;

                for (; i < _onReachKeyPoints.arraySize; i++)
                {
                    CallbackButton(rect, _onReachKeyPoints.GetArrayElementAtIndex(i), string.Format("On Reach Key Point [{0}]", i));
                    rect.x += rect.width;
                }
            }
        }

        protected override void DrawCallbackEvents()
        {
            base.DrawCallbackEvents();

            for (int i = 0; i < _onReachKeyPoints.arraySize; i++)
            {
                if (_onReachKeyPoints.GetArrayElementAtIndex(i).isExpanded)
                {
                    SerializedProperty property = _onReachKeyPoints.GetArrayElementAtIndex(i);
                    GUIContent label = new GUIContent(string.Format("On Reach Key Point [{0}]", i));
                    Rect position = EditorGUILayout.GetControlRect(true, _eventDrawer.GetPropertyHeight(property, label));

                    _eventDrawer.OnGUI(position, property, label);
                }
            }
        }

        protected override void DrawGUI()
        {
            for (int i = 0; i < serializedObject.targetObjects.Length; i++)
            {
                (serializedObject.targetObjects[i] as BezierTweener).Invoke("ValidateReachKeyPoints", 0);
            }

            EditorGUILayout.PropertyField(serializedObject.FindProperty("_playOnAwake"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("_editModeOption"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("_rotationMode"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("_bezierCurve"));
            EditorGUILayout.PropertyField(_settings);

            using (EditorGUI.ChangeCheckScope changeCheck = new EditorGUI.ChangeCheckScope())
            {
                EditorGUILayout.PropertyField(_easeMode);

                if (changeCheck.changed)
                {
                    for (int i = 0; i < serializedObject.targetObjects.Length; i++)
                    {
                        (serializedObject.targetObjects[i] as BezierTweener).Invoke("UpdateEaseMode", 0);
                    }
                }
            }

            if (_easeMode.hasMultipleDifferentValues == false && _easeMode.intValue == 0)
            {
                EditorGUI.indentLevel++;
                {
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("_easeCurve"));
                }
                EditorGUI.indentLevel--;
            }

            EditorGUILayout.Separator();

            DrawCallbackButtons();

            EditorGUILayout.Separator();

            DrawCallbackEvents();
        }

        protected override void UnityEnable()
        {
            base.UnityEnable();

            _onReachKeyPoints = serializedObject.FindProperty("_onReachKeyPoints");
            _eventDrawer = new UnityEventDrawer();
        }

        #endregion
    }
}
