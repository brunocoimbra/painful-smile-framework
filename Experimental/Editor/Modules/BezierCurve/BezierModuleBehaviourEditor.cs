﻿using UnityEditor;
using UnityEngine;

namespace PainfulSmile.Experimental
{
    [CustomEditor(typeof(BezierModuleBehaviour)), CanEditMultipleObjects]
    internal sealed class BezierModuleBehaviourEditor : ModuleBehaviourEditor
    {
        #region Private Fields

        private SerializedProperty _onUpdate;
        private SerializedProperty _onPingPong;
        private SerializedProperty _onReachKeyPoints;
        private UnityEventDrawer _eventDrawer;

        #endregion

        #region Protected Methods

        protected override void DrawCallbackButtons()
        {
            CallbackButtonGroup(_onUpdate);
            CallbackButtonGroup(_onPingPong, _onLooped, _onFinished);
            CallbackButtonGroup(_onPlayed, _onPaused, _onReseted, _onStopped);

            int i = 0;
            int split = 2;
            int left = _onReachKeyPoints.arraySize % split;
            Rect rect;

            for (; i < _onReachKeyPoints.arraySize - left; i += split)
            {
                rect = GetCallbackControlRect();
                rect.width /= split;

                for (int j = 0; j < split; j++)
                {
                    CallbackButton(rect, _onReachKeyPoints.GetArrayElementAtIndex(i + j), string.Format("On Reach Key Point [{0}]", i + j));
                    rect.x += rect.width;
                }
            }

            if (left != 0)
            {
                rect = GetCallbackControlRect();
                rect.width /= split;

                for (; i < _onReachKeyPoints.arraySize; i++)
                {
                    CallbackButton(rect, _onReachKeyPoints.GetArrayElementAtIndex(i), string.Format("On Reach Key Point [{0}]", i));
                    rect.x += rect.width;
                }
            }
        }

        protected override void DrawCallbackEvents()
        {
            if (_onUpdate.isExpanded)
            {
                EditorGUILayout.PropertyField(_onUpdate);
            }

            if (_onPingPong.isExpanded)
            {
                EditorGUILayout.PropertyField(_onPingPong);
            }

            base.DrawCallbackEvents();

            for (int i = 0; i < _onReachKeyPoints.arraySize; i++)
            {
                if (_onReachKeyPoints.GetArrayElementAtIndex(i).isExpanded)
                {
                    SerializedProperty property = _onReachKeyPoints.GetArrayElementAtIndex(i);
                    GUIContent label = new GUIContent(string.Format("On Reach Key Point [{0}]", i));
                    Rect position = EditorGUILayout.GetControlRect(true, _eventDrawer.GetPropertyHeight(property, label));

                    _eventDrawer.OnGUI(position, property, label);
                }
            }
        }

        protected override void DrawGUI()
        {
            for (int i = 0; i < serializedObject.targetObjects.Length; i++)
            {
                (serializedObject.targetObjects[i] as BezierModuleBehaviour).Invoke("ValidateReachKeyPoints", 0);
            }

            EditorGUILayout.PropertyField(serializedObject.FindProperty("_playOnAwake"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("_editModeOption"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("_rotationMode"));
            EditorGUILayout.PropertyField(_settings);
            EditorGUILayout.Separator();

            DrawCallbackButtons();

            EditorGUILayout.Separator();

            DrawCallbackEvents();
        }

        protected override void UnityEnable()
        {
            base.UnityEnable();

            _onReachKeyPoints = serializedObject.FindProperty("_onReachKeyPoints");
            _eventDrawer = new UnityEventDrawer();
            _onUpdate = serializedObject.FindProperty("_onUpdate");
            _onPingPong = serializedObject.FindProperty("_onPingPong");
        }

        #endregion
    }
}
