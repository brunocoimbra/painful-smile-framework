﻿using UnityEditor;

namespace PainfulSmile.Experimental
{
    [CustomEditor(typeof(Tweener), true), CanEditMultipleObjects]
    public class TweenerEditor : ModuleBehaviourEditor
    {
        #region Protected Fields

        protected SerializedProperty _easeMode;
        protected SerializedProperty _onUpdateTime;
        protected SerializedProperty _onPingPong;

        #endregion

        #region Protected Methods

        protected override void DrawGUI()
        {
            EditorGUILayout.PropertyField(serializedObject.FindProperty("_playOnAwake"));
            EditorGUILayout.PropertyField(_settings);

            using (EditorGUI.ChangeCheckScope changeCheck = new EditorGUI.ChangeCheckScope())
            {
                EditorGUILayout.PropertyField(_easeMode);

                if (changeCheck.changed)
                {
                    for (int i = 0; i < serializedObject.targetObjects.Length; i++)
                    {
                        (serializedObject.targetObjects[i] as BezierTweener).Invoke("UpdateEaseMode", 0);
                    }
                }
            }

            if (_easeMode.hasMultipleDifferentValues == false && _easeMode.intValue == 0)
            {
                EditorGUI.indentLevel++;
                {
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("_easeCurve"));
                }
                EditorGUI.indentLevel--;
            }

            EditorGUILayout.Separator();

            DrawCallbackButtons();

            EditorGUILayout.Separator();

            DrawCallbackEvents();
        }

        protected override void DrawCallbackButtons()
        {
            CallbackButtonGroup(_onUpdateTime);
            CallbackButtonGroup(_onPingPong, _onLooped, _onFinished);
            CallbackButtonGroup(_onPlayed, _onPaused, _onReseted, _onStopped);
        }

        protected override void DrawCallbackEvents()
        {
            if (_onUpdateTime.isExpanded)
            {
                EditorGUILayout.PropertyField(_onUpdateTime);
            }

            if (_onPingPong.isExpanded)
            {
                EditorGUILayout.PropertyField(_onPingPong);
            }

            base.DrawCallbackEvents();
        }

        protected override void UnityEnable()
        {
            base.UnityEnable();

            _easeMode = serializedObject.FindProperty("_easeMode");
            _onUpdateTime = serializedObject.FindProperty("_onUpdateTime");
            _onPingPong = serializedObject.FindProperty("_onPingPong");
        }

        #endregion
    }
}
