﻿using UnityEditor;
using UnityEngine;

namespace PainfulSmile.Experimental
{
    [CustomPropertyDrawer(typeof(TimeModule), true)]
    public class TimeModuleDrawer : ModuleDrawer
    {
        #region Public Methods

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            DrawHeaderGUI(position, property, label);

            if (property.isExpanded)
            {
                EditorGUI.indentLevel++;
                {
                    position.y += GetHeaderGUIHeight(property, label) + EditorGUIUtility.standardVerticalSpacing;
                    DrawMainGUI(position, property);

                    position.y += GetMainGUIHeight(property) + EditorGUIUtility.standardVerticalSpacing + Space;
                    DrawTimeGUI(position, property);

                    position.y += GetTimeGUIHeight(property) + EditorGUIUtility.standardVerticalSpacing;
                    DrawLoopGUI(position, property);
                }
                EditorGUI.indentLevel--;
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            float height = GetHeaderGUIHeight(property, label);

            if (property.isExpanded)
            {
                height += GetMainGUIHeight(property) + EditorGUIUtility.standardVerticalSpacing + Space;
                height += GetTimeGUIHeight(property) + EditorGUIUtility.standardVerticalSpacing;
                height += GetLoopGUIHeight(property) + EditorGUIUtility.standardVerticalSpacing;
            }

            return height;
        }

        #endregion

        #region Protected Methods

        protected static void DrawTimeGUI(Rect position, SerializedProperty property)
        {
            position.height = EditorGUIUtility.singleLineHeight;

            SerializedProperty _duration = property.FindPropertyRelative("_duration");
            EditorGUI.PropertyField(position, _duration);

            position.y += position.height + EditorGUIUtility.standardVerticalSpacing;
            EditorGUI.Slider(position, property.FindPropertyRelative("_currentTime"), 0, _duration.floatValue);

            position.y += position.height + EditorGUIUtility.standardVerticalSpacing;
            position = EditorGUI.PrefixLabel(position, new GUIContent("Time Scale", "The local time scale."));

            int indentLevel = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;
            {
                float boolLabelWidth = EditorStyles.label.CalcSize(new GUIContent("Use Delta Time?")).x;
                float boolFieldWidth = EditorStyles.toggle.CalcSize(GUIContent.none).x;

                position.width -= boolLabelWidth + boolFieldWidth;
                EditorGUI.PropertyField(position, property.FindPropertyRelative("_timeScale"), GUIContent.none);

                position.x += position.width;
                position.width = boolLabelWidth;
                EditorGUI.LabelField(position, "Use Delta Time?");

                position.x += position.width;
                position.width = boolFieldWidth;
                EditorGUI.PropertyField(position, property.FindPropertyRelative("_useDeltaTime"), GUIContent.none);
            }
            EditorGUI.indentLevel = indentLevel;
        }

        protected static float GetTimeGUIHeight(SerializedProperty property)
        {
            return EditorGUIUtility.singleLineHeight * 3 + EditorGUIUtility.standardVerticalSpacing * 2;
        }

        #endregion
    }
}
