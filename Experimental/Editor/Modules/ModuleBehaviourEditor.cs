﻿using UnityEditor;
using UnityEngine;

namespace PainfulSmile.Experimental
{
    [CustomEditor(typeof(ModuleBehaviour), true), CanEditMultipleObjects]
    public class ModuleBehaviourEditor : Editor
    {
        #region Protected Fields

        protected SerializedProperty _settings;
        protected SerializedProperty _onFinished;
        protected SerializedProperty _onLooped;
        protected SerializedProperty _onPlayed;
        protected SerializedProperty _onPaused;
        protected SerializedProperty _onReseted;
        protected SerializedProperty _onStopped;

        #endregion

        #region Unity Callbacks

        private void OnEnable()
        {
            UnityEnable();
        }

        #endregion

        #region Public Methods

        public sealed override void OnInspectorGUI()
        {
            serializedObject.Update();

            using (EditorGUI.ChangeCheckScope changeCheck = new EditorGUI.ChangeCheckScope())
            {
                DrawGUI();

                if (changeCheck.changed)
                {
                    serializedObject.ApplyModifiedProperties();
                }
            }
        }

        #endregion

        #region Protected Methods

        protected virtual void DrawCallbackButtons()
        {
            CallbackButtonGroup(_onLooped, _onFinished);
            CallbackButtonGroup(_onPlayed, _onPaused, _onReseted, _onStopped);
        }

        protected virtual void DrawCallbackEvents()
        {
            if (_onLooped.isExpanded)
            {
                EditorGUILayout.PropertyField(_onLooped);
            }

            if (_onFinished.isExpanded)
            {
                EditorGUILayout.PropertyField(_onFinished);
            }

            if (_onPlayed.isExpanded)
            {
                EditorGUILayout.PropertyField(_onPlayed);
            }

            if (_onPaused.isExpanded)
            {
                EditorGUILayout.PropertyField(_onPaused);
            }

            if (_onReseted.isExpanded)
            {
                EditorGUILayout.PropertyField(_onReseted);
            }

            if (_onStopped.isExpanded)
            {
                EditorGUILayout.PropertyField(_onStopped);
            }
        }

        protected virtual void DrawGUI()
        {
            EditorGUILayout.PropertyField(serializedObject.FindProperty("_playOnAwake"));
            EditorGUILayout.PropertyField(_settings);
            EditorGUILayout.Separator();

            DrawCallbackButtons();

            EditorGUILayout.Separator();

            DrawCallbackEvents();
        }

        protected virtual void UnityEnable()
        {
            _settings = serializedObject.FindProperty("_settings");
            _onLooped = serializedObject.FindProperty("_onLooped");
            _onFinished = serializedObject.FindProperty("_onFinished");
            _onPlayed = serializedObject.FindProperty("_onPlayed");
            _onPaused = serializedObject.FindProperty("_onPaused");
            _onReseted = serializedObject.FindProperty("_onReseted");
            _onStopped = serializedObject.FindProperty("_onStopped");
        }

        protected void CallbackButton(Rect position, SerializedProperty property, string label = null)
        {
            using (EditorGUI.ChangeCheckScope changeCheck = new EditorGUI.ChangeCheckScope())
            {
                Color backgroundColor = GUI.backgroundColor;

                if (property.hasMultipleDifferentValues)
                {
                    GUI.backgroundColor = Color.yellow;
                }
                else if (property.FindPropertyRelative("m_PersistentCalls").FindPropertyRelative("m_Calls").arraySize > 0)
                {
                    GUI.backgroundColor = Color.green;
                }
                else
                {
                    GUI.backgroundColor = Color.red;
                }

                bool isExpanded = EditorGUI.Toggle(position, property.isExpanded, EditorStyles.toolbarButton);

                if (changeCheck.changed)
                {
                    property.isExpanded = isExpanded;
                }

                GUIStyle style;

                if (property.isExpanded)
                {
                    style = EditorStyles.miniLabel;
                    style.alignment = TextAnchor.MiddleCenter;
                }
                else
                {
                    style = EditorStyles.centeredGreyMiniLabel;
                }

                EditorGUI.LabelField(position, (label ?? property.displayName), style);

                GUI.backgroundColor = backgroundColor;
            }
        }

        protected void CallbackButtonGroup(params SerializedProperty[] properties)
        {
            Rect rect = GetCallbackControlRect();
            rect.width /= properties.Length;

            for (int i = 0; i < properties.Length; i++)
            {
                CallbackButton(rect, properties[i]);
                rect.x += rect.width;
            }
        }

        protected Rect GetCallbackControlRect()
        {
            Rect rect = EditorGUILayout.GetControlRect(false, 18, EditorStyles.toolbarButton);

            rect.y += 1;
            rect.height -= 2;

            return rect;
        }

        #endregion
    }
}
