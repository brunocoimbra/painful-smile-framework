﻿using UnityEditor;
using UnityEngine;

namespace PainfulSmile.Experimental
{
    [CustomPropertyDrawer(typeof(Tween), true)]
    public class TweenDrawer : TimeModuleDrawer
    {
        #region Public Methods

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            DrawHeaderGUI(position, property, label);

            if (property.isExpanded)
            {
                EditorGUI.indentLevel++;
                {
                    position.y += GetHeaderGUIHeight(property, label) + EditorGUIUtility.standardVerticalSpacing;
                    DrawMainGUI(position, property);

                    position.y += GetMainGUIHeight(property) + EditorGUIUtility.standardVerticalSpacing + Space;
                    DrawTimeGUI(position, property);

                    position.y += GetTimeGUIHeight(property) + EditorGUIUtility.standardVerticalSpacing;
                    DrawPingPongGUI(position, property);

                    position.y += GetPingPongGUIHeight(property) + EditorGUIUtility.standardVerticalSpacing;
                    DrawLoopGUI(position, property);
                }
                EditorGUI.indentLevel--;
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            float height = GetHeaderGUIHeight(property, label);

            if (property.isExpanded)
            {
                height += GetMainGUIHeight(property) + EditorGUIUtility.standardVerticalSpacing + Space;
                height += GetTimeGUIHeight(property) + EditorGUIUtility.standardVerticalSpacing;
                height += GetLoopGUIHeight(property) + EditorGUIUtility.standardVerticalSpacing;
                height += GetPingPongGUIHeight(property) + EditorGUIUtility.standardVerticalSpacing;
            }

            return height;
        }

        #endregion

        #region Protected Methods

        protected static void DrawPingPongGUI(Rect position, SerializedProperty property)
        {
            position.height = EditorGUIUtility.singleLineHeight;
            position = EditorGUI.PrefixLabel(position, new GUIContent("Ping Pong", "Should the tween be ping pong like?."));

            int indentLevel = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;
            {
                float boolLabelWidth = EditorStyles.label.CalcSize(new GUIContent("Is Going Back?")).x;
                float boolFieldWidth = EditorStyles.toggle.CalcSize(GUIContent.none).x;
                Rect sidePosition = position;

                position.width = boolFieldWidth;
                EditorGUI.PropertyField(position, property.FindPropertyRelative("_isPingPong"), GUIContent.none);

                sidePosition.x += boolFieldWidth;
                sidePosition.width = boolLabelWidth;
                EditorGUI.LabelField(sidePosition, "Is Going Back?");

                sidePosition.x += sidePosition.width;
                sidePosition.width = boolFieldWidth;
                EditorGUI.PropertyField(sidePosition, property.FindPropertyRelative("_isGoingBack"), GUIContent.none);
            }
            EditorGUI.indentLevel = indentLevel;
        }

        protected static float GetPingPongGUIHeight(SerializedProperty property)
        {
            return EditorGUIUtility.singleLineHeight;
        }

        #endregion
    }
}
