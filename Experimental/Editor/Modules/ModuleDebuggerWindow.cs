﻿using UnityEditor;
using UnityEngine;

namespace PainfulSmile.Experimental
{
    internal sealed class ModuleDebuggerWindow : EditorWindow
    {
        #region Constant Fields

        private static readonly GUIContent SlashLabel = new GUIContent("/");
        private static readonly GUIContent CompletedLoopsLabel = new GUIContent("Completed Loops", "Completed loops and the target amount.");
        private static readonly GUIContent NormalizedLabel = new GUIContent("Normalized", "The module in it's normalized state.");
        private static readonly GUIContent ManualLabel = new GUIContent("Manual", "Modules manually updated.");
        private static readonly GUIContent EndOfFrameLabel = new GUIContent("End Of Frame", "Modules update at the very end of frame.");
        private static readonly GUIContent FixedUpdateLabel = new GUIContent("Fixed Update", "Modules update in the physics cycle.");
        private static readonly GUIContent UpdateLabel = new GUIContent("Update", "Modules update in the basic update loop.");

        #endregion

        #region Private Fields

        private Vector2 _scrollPosition;
        private ModuleManager _instance;
        private ReorderableList _listManual;
        private ReorderableList _listEndOfFrame;
        private ReorderableList _listFixedUpdate;
        private ReorderableList _listUpdate;
        private SerializedObject _serializedObject;

        #endregion

        #region Unity Callbacks

        private static void ShowWindow()
        {
            GetWindow<ModuleDebuggerWindow>(false, "Module Debug", true);
        }

        private void OnEnable()
        {
            UnityEnable();
        }

        private void OnGUI()
        {
            UnityGUI();
        }

        private void Update()
        {
            UnityUpdate();
        }

        #endregion

        #region Private Methods

        private void HandleDrawHeader(Rect position, ReorderableList list, GUIContent label)
        {
            using (EditorGUI.ChangeCheckScope changeCheck = new EditorGUI.ChangeCheckScope())
            {
                bool value = EditorGUI.Foldout(position, list.Property.isExpanded, label);

                if (changeCheck.changed)
                {
                    list.Property.isExpanded = value;
                }
            }
        }

        private void HandleDrawElement(Rect position, SerializedProperty property, int index, Module module)
        {
            position.x -= 10;
            position.height = EditorGUIUtility.singleLineHeight;

            Rect header = position;
            header.width = EditorGUIUtility.labelWidth;

            using (EditorGUI.ChangeCheckScope changeCheck = new EditorGUI.ChangeCheckScope())
            {
                bool isExpanded = EditorGUI.Foldout(header, property.isExpanded, $"[{index}] {module.GetType().Name}{(string.IsNullOrEmpty(module.tag) ? "" : $" ({module.tag})")}");

                if (changeCheck.changed)
                {
                    property.isExpanded = isExpanded;
                }
            }

            using (new EditorGUI.DisabledScope(true))
            {
                float foldoutWidth = header.x + 4;
                float stateWidth = 60;

                header.x += header.width;
                header.width = stateWidth;
                EditorGUI.PropertyField(header, property.FindPropertyRelative("_state"), GUIContent.none);

                header.x += stateWidth;
                header.width = EditorGUIUtility.currentViewWidth - EditorGUIUtility.labelWidth - foldoutWidth - 8 - stateWidth;
                EditorGUI.PropertyField(header, property.FindPropertyRelative("_context"), GUIContent.none);

                if (property.isExpanded)
                {
                    EditorGUI.indentLevel++;
                    {
                        position.y += position.height + EditorGUIUtility.standardVerticalSpacing;
                        EditorGUI.PropertyField(position, property.FindPropertyRelative("_tag"));

                        position.y += position.height + EditorGUIUtility.standardVerticalSpacing;
                        EditorGUI.Slider(position, NormalizedLabel, module.normalized, 0, 1);

                        position.y += position.height + EditorGUIUtility.standardVerticalSpacing;
                        Rect loop = EditorGUI.PrefixLabel(position, CompletedLoopsLabel);

                        float slashWidth = EditorStyles.label.CalcSize(SlashLabel).x;
                        float loopWidth = (EditorGUIUtility.currentViewWidth - EditorGUIUtility.labelWidth - slashWidth - 40) / 2;

                        EditorGUI.indentLevel--;
                        {
                            loop.width = loopWidth;
                            EditorGUI.IntField(loop, module.completedLoops);

                            loop.x += loop.width;
                            loop.width = slashWidth;
                            EditorGUI.LabelField(loop, SlashLabel);

                            loop.x += loop.width;
                            loop.width = loopWidth;

                            if (module.infinityLoop)
                            {
                                EditorGUI.TextField(loop, "Infinity");
                            }
                            else
                            {
                                EditorGUI.IntField(loop, module.loops);
                            }
                        }
                        EditorGUI.indentLevel++;
                    }
                    EditorGUI.indentLevel--;
                }
            }
        }

        private void UnityEnable()
        {
            _instance = ModuleManager.Instance;

            if (_instance == null)
            {
                return;
            }

            _serializedObject = new SerializedObject(_instance);

            SerializedProperty _moduleListDictionary = _serializedObject.FindProperty("_moduleListDictionary");

            _listUpdate = new ReorderableList(_moduleListDictionary.FindPropertyRelative("_values").GetArrayElementAtIndex(0), ReorderablePermissions.Collapse);
            _listUpdate.OnDrawHeader += (list, position, label) => { HandleDrawHeader(position, _listUpdate, UpdateLabel); };
            _listUpdate.OnDrawElement += (list, position, property, label, selected, focused) =>
            {
                int index = _listUpdate.IndexOf(property);
                HandleDrawElement(position, property, index, ModuleManagerUtility.moduleListDictionary[UpdateMode.Update][index]);
            };
            _listUpdate.OnGetElementHeight += HandleGetElementHeight;

            _listFixedUpdate = new ReorderableList(_moduleListDictionary.FindPropertyRelative("_values").GetArrayElementAtIndex(1), ReorderablePermissions.Collapse);
            _listFixedUpdate.OnDrawHeader += (list, position, label) => { HandleDrawHeader(position, _listFixedUpdate, FixedUpdateLabel); };
            _listFixedUpdate.OnDrawElement += (list, position, property, label, selected, focused) => 
            {
                int index = _listFixedUpdate.IndexOf(property);
                HandleDrawElement(position, property, index, ModuleManagerUtility.moduleListDictionary[UpdateMode.FixedUpdate][index]);
            };
            _listFixedUpdate.OnGetElementHeight += HandleGetElementHeight;

            _listEndOfFrame = new ReorderableList(_moduleListDictionary.FindPropertyRelative("_values").GetArrayElementAtIndex(2), ReorderablePermissions.Collapse);
            _listEndOfFrame.OnDrawHeader += (list, position, label) => { HandleDrawHeader(position, _listEndOfFrame, EndOfFrameLabel); };
            _listEndOfFrame.OnDrawElement += (list, position, property, label, selected, focused) => 
            {
                int index = _listEndOfFrame.IndexOf(property);
                HandleDrawElement(position, property, index, ModuleManagerUtility.moduleListDictionary[UpdateMode.EndOfFrame][index]);
            };
            _listEndOfFrame.OnGetElementHeight += HandleGetElementHeight;

            _listManual = new ReorderableList(_moduleListDictionary.FindPropertyRelative("_values").GetArrayElementAtIndex(3), ReorderablePermissions.Collapse);
            _listManual.OnDrawHeader += (list, position, label) => { HandleDrawHeader(position, _listManual, ManualLabel); };
            _listManual.OnDrawElement += (list, position, property, label, selected, focused) => 
            {
                int index = _listManual.IndexOf(property);
                HandleDrawElement(position, property, index, ModuleManagerUtility.moduleListDictionary[UpdateMode.Manual][index]);
            };
            _listManual.OnGetElementHeight += HandleGetElementHeight;
        }

        private void UnityGUI()
        {
            if (_instance == null)
            {
                UnityEnable();

                if (_instance == null)
                {
                    return;
                }
            }

            _serializedObject.Update();

            using (EditorGUILayout.ScrollViewScope scrollViewScope = new EditorGUILayout.ScrollViewScope(_scrollPosition))
            {
                _scrollPosition = scrollViewScope.scrollPosition;

                using (EditorGUI.ChangeCheckScope changeCheckScope = new EditorGUI.ChangeCheckScope())
                {
                    _listUpdate.DrawGUILayout();
                    _listFixedUpdate.DrawGUILayout();
                    _listEndOfFrame.DrawGUILayout();
                    _listManual.DrawGUILayout();

                    if (changeCheckScope.changed)
                    {
                        _serializedObject.ApplyModifiedProperties();
                    }
                }
            }
        }

        private void UnityUpdate()
        {
            Repaint();
        }

        private float HandleGetElementHeight(ReorderableList list, SerializedProperty property, int index)
        {
            if (property.isExpanded)
            {
                return EditorGUIUtility.singleLineHeight * 4 + EditorGUIUtility.standardVerticalSpacing * 3;
            }
            else
            {
                return EditorGUIUtility.singleLineHeight;
            }
        }

        #endregion
    }
}
