﻿using UnityEditor;
using UnityEngine;

namespace PainfulSmile.Experimental
{
    [CustomPropertyDrawer(typeof(BoundsTween))]
    [CustomPropertyDrawer(typeof(ColorTween))]
    [CustomPropertyDrawer(typeof(FloatTween))]
    [CustomPropertyDrawer(typeof(QuaternionTween))]
    [CustomPropertyDrawer(typeof(RectTween))]
    [CustomPropertyDrawer(typeof(Vector2Tween))]
    [CustomPropertyDrawer(typeof(Vector3Tween))]
    [CustomPropertyDrawer(typeof(Vector4Tween))]
    internal sealed class ValueTweenDrawer : TweenDrawer
    {
        #region Public Methods

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            DrawHeaderGUI(position, property, label);

            if (property.isExpanded)
            {
                EditorGUI.indentLevel++;
                {
                    position.y += GetHeaderGUIHeight(property, label) + EditorGUIUtility.standardVerticalSpacing;
                    DrawMainGUI(position, property);

                    position.y += GetMainGUIHeight(property) + EditorGUIUtility.standardVerticalSpacing + Space;
                    DrawValueGUI(position, property);

                    position.y += GetValueGUIHeight(property) + EditorGUIUtility.standardVerticalSpacing + Space;
                    DrawTimeGUI(position, property);

                    position.y += GetTimeGUIHeight(property) + EditorGUIUtility.standardVerticalSpacing;
                    DrawPingPongGUI(position, property);

                    position.y += GetPingPongGUIHeight(property) + EditorGUIUtility.standardVerticalSpacing;
                    DrawLoopGUI(position, property);
                }
                EditorGUI.indentLevel--;
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            float height = GetHeaderGUIHeight(property, label);

            if (property.isExpanded)
            {
                height += GetMainGUIHeight(property) + EditorGUIUtility.standardVerticalSpacing + Space;
                height += GetValueGUIHeight(property) + EditorGUIUtility.standardVerticalSpacing + Space;
                height += GetTimeGUIHeight(property) + EditorGUIUtility.standardVerticalSpacing;
                height += GetLoopGUIHeight(property) + EditorGUIUtility.standardVerticalSpacing;
                height += GetPingPongGUIHeight(property) + EditorGUIUtility.standardVerticalSpacing;
            }

            return height;
        }

        #endregion

        #region Protected Methods

        private static void DrawValueGUI(Rect position, SerializedProperty property)
        {
            SerializedProperty _from = property.FindPropertyRelative("_from");
            position.height = EditorGUI.GetPropertyHeight(_from);
            EditorGUI.PropertyField(position, _from, new GUIContent(_from.displayName, "The starting value."), true);

            position.y += position.height;
            SerializedProperty _to = property.FindPropertyRelative("_to");
            position.height = EditorGUI.GetPropertyHeight(_to);
            EditorGUI.PropertyField(position, _to, new GUIContent(_to.displayName, "The target value."), true);
        }

        private static float GetValueGUIHeight(SerializedProperty property)
        {
            float height = EditorGUIUtility.standardVerticalSpacing;

            height += EditorGUI.GetPropertyHeight(property.FindPropertyRelative("_from"));
            height += EditorGUI.GetPropertyHeight(property.FindPropertyRelative("_to"));

            return height;
        }

        #endregion
    }
}
