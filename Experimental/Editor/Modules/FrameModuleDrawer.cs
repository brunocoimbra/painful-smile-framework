﻿using UnityEditor;
using UnityEngine;

namespace PainfulSmile.Experimental
{
    [CustomPropertyDrawer(typeof(FrameModule), true)]
    public class FrameModuleDrawer : ModuleDrawer
    {
        #region Public Methods

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            DrawHeaderGUI(position, property, label);

            if (property.isExpanded)
            {
                EditorGUI.indentLevel++;
                {
                    position.y += GetHeaderGUIHeight(property, label) + EditorGUIUtility.standardVerticalSpacing;
                    DrawMainGUI(position, property);

                    position.y += GetMainGUIHeight(property) + EditorGUIUtility.standardVerticalSpacing + Space;
                    DrawFrameGUI(position, property);

                    position.y += GetFrameGUIHeight(property) + EditorGUIUtility.standardVerticalSpacing;
                    DrawLoopGUI(position, property);
                }
                EditorGUI.indentLevel--;
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            float height = GetHeaderGUIHeight(property, label);

            if (property.isExpanded)
            {
                height += GetMainGUIHeight(property) + EditorGUIUtility.standardVerticalSpacing + Space;
                height += GetFrameGUIHeight(property) + EditorGUIUtility.standardVerticalSpacing;
                height += GetLoopGUIHeight(property) + EditorGUIUtility.standardVerticalSpacing;
            }

            return height;
        }

        #endregion

        #region Private Methods

        private static void DrawFrameGUI(Rect position, SerializedProperty property)
        {
            SerializedProperty _duration = property.FindPropertyRelative("_duration");

            position.height = EditorGUIUtility.singleLineHeight;
            EditorGUI.PropertyField(position, _duration);

            position.y += position.height + EditorGUIUtility.standardVerticalSpacing;
            EditorGUI.IntSlider(position, property.FindPropertyRelative("_currentFrame"), 0, _duration.intValue);
        }

        private static float GetFrameGUIHeight(SerializedProperty property)
        {
            return EditorGUIUtility.singleLineHeight * 2 + EditorGUIUtility.standardVerticalSpacing;
        }

        #endregion
    }
}
