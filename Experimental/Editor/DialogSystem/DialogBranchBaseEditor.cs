﻿using UnityEditor;

namespace PainfulSmile.Experimental
{
    [CustomEditor(typeof(DialogBranchBase), true)]
    public class DialogBranchBaseEditor : Editor
    {
        protected ReorderableList Transitions { get; private set; }

        protected virtual void OnEnable()
        {
            Transitions = new ReorderableList(serializedObject.FindProperty("m_Transitions"), "@Transition");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            using (var changeCheck = new EditorGUI.ChangeCheckScope())
            {
                DrawPropertiesExcluding(serializedObject, "m_Script");

                Transitions.DrawGUILayout();

                if (changeCheck.changed)
                {
                    serializedObject.ApplyModifiedProperties();
                }
            }
        }
    }
}
