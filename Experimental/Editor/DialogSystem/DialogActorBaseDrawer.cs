﻿using UnityEditor;
using UnityEngine;

namespace PainfulSmile.Experimental
{
    [CustomPropertyDrawer(typeof(DialogActorBase), true)]
    public class DialogActorBaseDrawer : PropertyDrawer
    {
        private static readonly ReorderableAttribute Attribute = new ReorderableAttribute("@Event", ReorderableElementDisplay.NonExpandable);

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            DrawMainPropertyGUI(ref position, property, label);

            if (property.isExpanded)
            {
                using (new EditorGUI.IndentLevelScope())
                {
                    DrawTextOptionsGUI(ref position, property);
                    DrawDialogEventsGUI(ref position, property);
                }
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            float height = GetMainPropertyHeight();

            if (property.isExpanded)
            {
                height += GetTextOptionsHeight();
                height += GetDialogEventsHeight(property);
                height += EditorGUIUtility.standardVerticalSpacing * 3;
            }

            return height;
        }

        protected void DrawDialogEventsGUI(ref Rect position, SerializedProperty property)
        {
            ReorderableList dialogEvents = ReorderableList.GetReorderableList(property.FindPropertyRelative("m_DialogEvents"), Attribute);
            position.y += position.height + EditorGUIUtility.standardVerticalSpacing;
            position.height = dialogEvents.TotalHeight;
            dialogEvents.DrawGUI(EditorGUI.IndentedRect(position));
        }

        protected void DrawMainPropertyGUI(ref Rect position, SerializedProperty property, GUIContent label)
        {
            position.x += 10;
            position.width -= 10;
            position.height = EditorGUIUtility.singleLineHeight;

            var target = property.GetValue<DialogActorBase>();
            var content = new GUIContent(string.IsNullOrEmpty(target.Name) ? label.text : target.Name, property.tooltip);

            using (var propertyScope = new EditorGUI.PropertyScope(position, content, property))
            {
                using (var changeCheck = new EditorGUI.ChangeCheckScope())
                {
                    bool value = EditorGUI.Foldout(EditorGUI.IndentedRect(position), property.isExpanded, propertyScope.content);

                    if (changeCheck.changed)
                    {
                        property.isExpanded = value;
                    }
                }
            }
        }

        protected void DrawTextOptionsGUI(ref Rect position, SerializedProperty property)
        {
            position.y += position.height + EditorGUIUtility.standardVerticalSpacing;
            position.height = EditorGUIUtility.singleLineHeight;
            Rect fieldPosition = EditorGUI.PrefixLabel(position, new GUIContent("Default Speed", "The default actor's text speed."));

            using (new EditorGUI.IndentLevelScope(-EditorGUI.indentLevel))
            {
                var allowSkipLabel = new GUIContent("Allow Skip?", "Should allow skip?");
                float boolLabelWidth = EditorStyles.label.CalcSize(allowSkipLabel).x;
                float boolFieldWidth = EditorStyles.toggle.CalcSize(GUIContent.none).x;

                fieldPosition.width -= boolLabelWidth + boolFieldWidth + 5;
                EditorGUI.PropertyField(fieldPosition, property.FindPropertyRelative("m_DefaultSpeed"), GUIContent.none);

                fieldPosition.x += fieldPosition.width + 5;
                fieldPosition.width = boolLabelWidth;
                EditorGUI.LabelField(fieldPosition, allowSkipLabel);

                fieldPosition.x += fieldPosition.width;
                fieldPosition.width = boolFieldWidth;
                EditorGUI.PropertyField(fieldPosition, property.FindPropertyRelative("m_AllowSkip"), GUIContent.none);
            }
        }

        protected float GetDialogEventsHeight(SerializedProperty property)
        {
            return ReorderableList.GetReorderableList(property.FindPropertyRelative("m_DialogEvents"), Attribute).TotalHeight;
        }

        protected float GetMainPropertyHeight()
        {
            return EditorGUIUtility.singleLineHeight;
        }

        protected float GetTextOptionsHeight()
        {
            return EditorGUIUtility.singleLineHeight;
        }
    }
}
