﻿using UnityEditor;

namespace PainfulSmile.Experimental
{
    [CustomEditor(typeof(DialogBranch), true)]
    public class DialogBranchEditor : DialogBranchBaseEditor
    {
        protected ReorderableList Actors { get; private set; }

        protected override void OnEnable()
        {
            base.OnEnable();

            Actors = new ReorderableList(serializedObject.FindProperty("m_Actors"));
            Actors.OnAdd += HandleActorsAdd;
        }

        private void HandleActorsAdd(ReorderableList list)
        {
            if (list.Count == 0)
            {
                SerializedProperty item = list.AddItem();
                item.FindPropertyRelative("m_DefaultSpeed").floatValue = 15;
                item.FindPropertyRelative("m_AllowSkip").boolValue = true;
            }
            else
            {
                list.AddItem();
            }
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            using (var changeCheck = new EditorGUI.ChangeCheckScope())
            {
                DrawPropertiesExcluding(serializedObject, "m_Script");

                if (serializedObject.isEditingMultipleObjects == false)
                {
                    Actors.DrawGUILayout();
                }
                else
                {
                    EditorGUILayout.HelpBox("Unable to edit dialog actors on multiple objects.", MessageType.Warning);
                }

                Transitions.DrawGUILayout();

                if (changeCheck.changed)
                {
                    serializedObject.ApplyModifiedProperties();
                }
            }
        }
    }
}
