﻿using UnityEditor;
using UnityEngine;

namespace PainfulSmile.Experimental
{
    [CustomPropertyDrawer(typeof(DialogEvent), true)]
    internal sealed class DialogEventDrawer : UnityEventDrawer
    {
        private static readonly GUIContent IgnoreOnSkipContent = new GUIContent("Ignore On Skip", "Allow the event to be skipped?");

        protected override void DrawHeaderOverlay(Rect position, SerializedProperty property)
        {
            position.x += EditorGUIUtility.labelWidth;
            position.width -= EditorGUIUtility.labelWidth;
            SerializedProperty ignoreOnSkip = property.FindPropertyRelative("m_IgnoreOnSkip");

            using (var scope = new EditorGUI.PropertyScope(position, IgnoreOnSkipContent, ignoreOnSkip))
            {
                using (var changeCheck = new EditorGUI.ChangeCheckScope())
                {
                    bool value = EditorGUI.ToggleLeft(position, scope.content, ignoreOnSkip.boolValue);

                    if (changeCheck.changed)
                    {
                        ignoreOnSkip.boolValue = value;
                    }
                }
            }
        }
    }
}
