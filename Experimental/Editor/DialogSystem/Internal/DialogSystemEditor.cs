﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace PainfulSmile.Experimental
{
    [CustomEditor(typeof(DialogSystem), true)]
    internal sealed class DialogSystemEditor : Editor
    {
        private ReorderableList _parameters;
        private ReorderableList _transitions;
        private List<string> _parametersNames;

        private void OnEnable()
        {
            _parametersNames = new List<string>();
            _parameters = new ReorderableList(serializedObject.FindProperty("m_Parameters"), ReorderableElementDisplay.NonExpandable);
            _parameters.OnAddDropdown += HandleParametersAddDropdown;
            _parameters.OnDrawElementBackground += HandleParametersDrawElementBackground;
            _transitions = new ReorderableList(serializedObject.FindProperty("m_Transitions"), "@Transition");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            using (var changeCheck = new EditorGUI.ChangeCheckScope())
            {
                EditorGUILayout.Separator();
                DrawButtons();
                EditorGUILayout.Separator();
                DrawPropertiesExcluding(serializedObject, "m_Script");

                _parametersNames.Clear();
                _parameters.DrawGUILayout();
                _transitions.DrawGUILayout();

                if (changeCheck.changed)
                {
                    serializedObject.ApplyModifiedProperties();
                }
            }
        }

        private void DrawButtons()
        {
            using (new EditorGUI.DisabledScope(EditorApplication.isPlaying == false))
            {
                using (new EditorGUILayout.HorizontalScope())
                {
                    Rect position = EditorGUILayout.GetControlRect();
                    position.width = position.width / 2 - 2;

                    if (GUI.Button(position, "Interact", EditorStyles.toolbarButton))
                    {
                        (target as DialogSystem).Interact();
                    }

                    position.x += position.width + 2;

                    if (GUI.Button(position, "Quit", EditorStyles.toolbarButton))
                    {
                        (target as DialogSystem).Quit();
                    }
                }
            }
        }

        private void HandleParametersAddDropdown(ReorderableList list, Rect position)
        {
            var menu = new GenericMenu();

            void AddItem(object type)
            {
                SerializedProperty item = list.AddItem();
                item.FindPropertyRelative("m_Type").intValue = (int)type;
                item.FindPropertyRelative("m_Name").stringValue = null;
                item.FindPropertyRelative("m_BoolValue").boolValue = false;
                item.FindPropertyRelative("m_IntValue").intValue = 0;
                item.FindPropertyRelative("m_FloatValue").floatValue = 0;
                item.FindPropertyRelative("m_StringValue").stringValue = null;

                serializedObject.ApplyModifiedProperties();
                Repaint();
            }

            menu.AddItem(new GUIContent("Bool"), false, AddItem, DialogParameterType.Bool);
            menu.AddItem(new GUIContent("Int"), false, AddItem, DialogParameterType.Int);
            menu.AddItem(new GUIContent("Float"), false, AddItem, DialogParameterType.Float);
            menu.AddItem(new GUIContent("String"), false, AddItem, DialogParameterType.String);
            menu.DropDown(position);
        }

        private void HandleParametersDrawElementBackground(ReorderableList list, Rect position, SerializedProperty property, GUIContent label, bool selected, bool focused)
        {
            position.x += 19;
            position.width -= 23;
            position.height -= EditorGUIUtility.standardVerticalSpacing;

            string value = property.FindPropertyRelative("m_Name").stringValue;

            if (string.IsNullOrEmpty(value) || _parametersNames.Contains(value))
            {
                EditorGUI.DrawRect(position, Color.red);
            }
            else
            {
                _parametersNames.Add(value);
            }

            if (Event.current.type == EventType.Repaint)
            {
                position.x -= 18;
                position.width = 18;
                position.height += 2;
                new GUIStyle("RL Element").Draw(position, false, selected, selected, focused);
            }
        }
    }
}
