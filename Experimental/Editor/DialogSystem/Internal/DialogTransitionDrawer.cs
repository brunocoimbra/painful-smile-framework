﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace PainfulSmile.Experimental
{
    [CustomPropertyDrawer(typeof(DialogTransition))]
    internal sealed class DialogTransitionDrawer : PropertyDrawer
    {
        private static readonly GUIContent ConditionsLabel = new GUIContent("Conditions", "The conditions needed to go to the branch.");
        private static readonly ReorderableAttribute ConditionsAttribute = new ReorderableAttribute(ReorderableElementDisplay.NonExpandable);

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            position.x += 10;
            position.width -= 10;
            position.height = EditorGUIUtility.singleLineHeight;

            using (var changeCheck = new EditorGUI.ChangeCheckScope())
            {
                bool value = EditorGUI.Foldout(EditorGUI.IndentedRect(position), property.isExpanded, label);

                if (changeCheck.changed)
                {
                    property.isExpanded = value;
                }
            }

            if (property.isExpanded)
            {
                using (new EditorGUI.IndentLevelScope())
                {
                    position.y += position.height + EditorGUIUtility.standardVerticalSpacing;
                    position.height = EditorGUIUtility.singleLineHeight;
                    EditorGUI.PropertyField(position, property.FindPropertyRelative("m_Branch"));

                    position.y += position.height + EditorGUIUtility.standardVerticalSpacing;

                    if (property.serializedObject.isEditingMultipleObjects)
                    {
                        EditorGUI.HelpBox(EditorGUI.IndentedRect(position), "Unable to edit dialog conditions on multiple objects.", MessageType.Warning);
                    }
                    else
                    {
                        var target = property.serializedObject.targetObject as MonoBehaviour;
                        var system = target != null ? target.GetComponentInParent<DialogSystem>() : null;

                        if (system == null)
                        {
                            EditorGUI.HelpBox(EditorGUI.IndentedRect(position), "Unable to find any dialog system in parents.", MessageType.Error);
                        }
                        else
                        {
                            ReorderableList conditions = ReorderableList.GetReorderableList(property.FindPropertyRelative("m_Conditions"), ConditionsAttribute);

                            conditions.OnDrawElementBackground += delegate(ReorderableList list, Rect elementPosition, SerializedProperty elementProperty, GUIContent elementLabel, bool selected, bool focused)
                            {
                                elementPosition.x += 19;
                                elementPosition.width -= 23;
                                elementPosition.height -= EditorGUIUtility.standardVerticalSpacing;

                                string name = elementProperty.FindPropertyRelative("m_Name").stringValue;
                                var type = (DialogParameterType)elementProperty.FindPropertyRelative("m_Type").intValue;
                                DialogParameter[] parameters = system.Parameters;
                                bool flag = false;

                                for (int i = 0; i < parameters.Length; i++)
                                {
                                    if (parameters[i].Name == name && parameters[i].Type == type)
                                    {
                                        flag = true;

                                        break;
                                    }
                                }

                                if (flag == false)
                                {
                                    EditorGUI.DrawRect(elementPosition, Color.red);
                                }

                                if (Event.current.type == EventType.Repaint)
                                {
                                    elementPosition.x -= 18;
                                    elementPosition.width = 18;
                                    elementPosition.height += 2;
                                    new GUIStyle("RL Element").Draw(elementPosition, false, selected, selected, focused);
                                }
                            };

                            conditions.OnAddDropdown += delegate(ReorderableList list, Rect dropdownPosition)
                            {
                                var menu = new GenericMenu();
                                var names = new List<string>();
                                DialogParameter[] parameters = system.Parameters;

                                for (int i = 0; i < parameters.Length; i++)
                                {
                                    if (string.IsNullOrEmpty(parameters[i].Name) || names.Contains(parameters[i].Name))
                                    {
                                        continue;
                                    }

                                    int index = i;
                                    names.Add(parameters[i].Name);

                                    menu.AddItem(new GUIContent(string.Format("{0} ({1})", parameters[index].Name, parameters[index].Type)), false, delegate
                                    {
                                        SerializedProperty item = list.AddItem();
                                        item.FindPropertyRelative("m_Type").intValue = (int)parameters[index].Type;
                                        item.FindPropertyRelative("m_Name").stringValue = parameters[index].Name;
                                        item.serializedObject.ApplyModifiedProperties();
                                    });
                                }

                                if (menu.GetItemCount() == 0)
                                {
                                    menu.AddItem(new GUIContent("No parameter found."), false, null);
                                }

                                menu.DropDown(dropdownPosition);
                            };

                            position.height = conditions.TotalHeight;
                            conditions.DrawGUI(EditorGUI.IndentedRect(position), ConditionsLabel);
                        }
                    }
                }
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            float height = EditorGUIUtility.singleLineHeight;

            if (property.isExpanded)
            {
                height += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing * 2;
                height += ReorderableList.GetReorderableList(property.FindPropertyRelative("m_Conditions"), ConditionsAttribute).TotalHeight;
            }

            return height;
        }
    }
}
