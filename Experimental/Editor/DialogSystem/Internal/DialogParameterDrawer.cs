﻿using UnityEditor;
using UnityEngine;

namespace PainfulSmile.Experimental
{
    [CustomPropertyDrawer(typeof(DialogParameter))]
    internal sealed class DialogParameterDrawer : PropertyDrawer
    {
        private const float HorizontalSpace = 2;
        private const float ValueRectRelativeWidth = 0.3f;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            position.height = EditorGUIUtility.singleLineHeight;

            SerializedProperty type = property.FindPropertyRelative("m_Type");
            SerializedProperty name = property.FindPropertyRelative("m_Name");

            float valueTotalWidth = position.width * ValueRectRelativeWidth;
            Rect valueRect = position;
            valueRect.x += position.width - valueTotalWidth;

            Rect nameRect = position;
            nameRect.width -= HorizontalSpace + valueTotalWidth;

            switch ((DialogParameterType)type.intValue)
            {
                case DialogParameterType.Bool:
                {
                    valueRect.width = 16;

                    break;
                }

                case DialogParameterType.Int:
                case DialogParameterType.Float:
                case DialogParameterType.String:
                {
                    valueRect.width = valueTotalWidth;

                    break;
                }
            }

            using (var changeCheckScope = new EditorGUI.ChangeCheckScope())
            {
                string value = EditorGUI.TextField(nameRect, name.stringValue);

                if (changeCheckScope.changed)
                {
                    name.stringValue = value;
                }
            }

            using (var changeCheckScope = new EditorGUI.ChangeCheckScope())
            {
                switch ((DialogParameterType)type.intValue)
                {
                    case DialogParameterType.Bool:
                    {
                        SerializedProperty serializedProperty = property.FindPropertyRelative("m_BoolValue");
                        bool value = EditorGUI.Toggle(valueRect, serializedProperty.boolValue);

                        if (changeCheckScope.changed)
                        {
                            serializedProperty.boolValue = value;
                        }

                        break;
                    }

                    case DialogParameterType.Int:
                    {
                        SerializedProperty serializedProperty = property.FindPropertyRelative("m_IntValue");
                        int value = EditorGUI.IntField(valueRect, serializedProperty.intValue);

                        if (changeCheckScope.changed)
                        {
                            serializedProperty.intValue = value;
                        }

                        break;
                    }

                    case DialogParameterType.Float:
                    {
                        SerializedProperty serializedProperty = property.FindPropertyRelative("m_FloatValue");
                        float value = EditorGUI.FloatField(valueRect, serializedProperty.floatValue);

                        if (changeCheckScope.changed)
                        {
                            serializedProperty.floatValue = value;
                        }

                        break;
                    }

                    case DialogParameterType.String:
                    {
                        SerializedProperty serializedProperty = property.FindPropertyRelative("m_StringValue");
                        string value = EditorGUI.TextField(valueRect, serializedProperty.stringValue);

                        if (changeCheckScope.changed)
                        {
                            serializedProperty.stringValue = value;
                        }

                        break;
                    }
                }
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUIUtility.singleLineHeight;
        }
    }
}
