﻿using UnityEditor;
using UnityEngine;

namespace PainfulSmile.Experimental
{
    [CustomPropertyDrawer(typeof(DialogCondition))]
    internal sealed class DialogConditionDrawer : PropertyDrawer
    {
        private const float HorizontalSpace = 2;
        private const float ValueRectRelativeWidth = 0.5f;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            position.height = EditorGUIUtility.singleLineHeight;

            SerializedProperty type = property.FindPropertyRelative("m_Type");
            SerializedProperty name = property.FindPropertyRelative("m_Name");

            float valueTotalWidth = position.width * ValueRectRelativeWidth;
            Rect valueRect = position;
            valueRect.x += position.width - valueTotalWidth;

            Rect nameRect = position;
            nameRect.width -= HorizontalSpace + valueTotalWidth;

            switch ((DialogParameterType)type.intValue)
            {
                case DialogParameterType.Bool:
                {
                    valueRect.width = valueTotalWidth;

                    break;
                }

                case DialogParameterType.Int:
                case DialogParameterType.Float:
                case DialogParameterType.String:
                {
                    valueRect.width = (valueTotalWidth - HorizontalSpace) / 2;

                    break;
                }
            }

            using (new EditorGUI.DisabledScope(true))
            {
                EditorGUI.TextField(nameRect, name.stringValue);
            }

            switch ((DialogParameterType)type.intValue)
            {
                case DialogParameterType.Bool:
                {
                    using (EditorGUI.ChangeCheckScope changeCheckScope = new EditorGUI.ChangeCheckScope())
                    {
                        SerializedProperty boolValue = property.FindPropertyRelative("m_BoolValue");
                        bool value = EditorGUI.Popup(valueRect, boolValue.boolValue ? 1 : 0, new[] { "false", "true" }) != 0;

                        if (changeCheckScope.changed)
                        {
                            boolValue.boolValue = value;
                        }
                    }

                    break;
                }

                case DialogParameterType.Int:
                {
                    using (var changeCheckScope = new EditorGUI.ChangeCheckScope())
                    {
                        SerializedProperty numberOption = property.FindPropertyRelative("m_NumberOption");
                        var value = (int)(NumberCondition)EditorGUI.EnumPopup(valueRect, (NumberCondition)numberOption.intValue);

                        if (changeCheckScope.changed)
                        {
                            numberOption.intValue = value;
                        }
                    }

                    using (var changeCheckScope = new EditorGUI.ChangeCheckScope())
                    {
                        valueRect.x += valueRect.width + HorizontalSpace;
                        SerializedProperty intValue = property.FindPropertyRelative("m_IntValue");
                        int value = EditorGUI.IntField(valueRect, intValue.intValue);

                        if (changeCheckScope.changed)
                        {
                            intValue.intValue = value;
                        }
                    }

                    break;
                }

                case DialogParameterType.Float:
                {
                    using (var changeCheckScope = new EditorGUI.ChangeCheckScope())
                    {
                        SerializedProperty numberOption = property.FindPropertyRelative("m_NumberOption");
                        int value = (int)(NumberCondition)EditorGUI.EnumPopup(valueRect, (NumberCondition)numberOption.intValue);

                        if (changeCheckScope.changed)
                        {
                            numberOption.intValue = value;
                        }
                    }

                    using (var changeCheckScope = new EditorGUI.ChangeCheckScope())
                    {
                        valueRect.x += valueRect.width + HorizontalSpace;
                        SerializedProperty floatValue = property.FindPropertyRelative("m_FloatValue");
                        float value = EditorGUI.FloatField(valueRect, floatValue.floatValue);

                        if (changeCheckScope.changed)
                        {
                            floatValue.floatValue = value;
                        }
                    }

                    break;
                }

                case DialogParameterType.String:
                {
                    using (EditorGUI.ChangeCheckScope changeCheckScope = new EditorGUI.ChangeCheckScope())
                    {
                        SerializedProperty stringOption = property.FindPropertyRelative("m_StringOption");
                        var value = (int)(StringCondition)EditorGUI.EnumPopup(valueRect, (StringCondition)stringOption.intValue);

                        if (changeCheckScope.changed)
                        {
                            stringOption.intValue = value;
                        }
                    }

                    using (var changeCheckScope = new EditorGUI.ChangeCheckScope())
                    {
                        valueRect.x += valueRect.width + HorizontalSpace;
                        SerializedProperty stringValue = property.FindPropertyRelative("m_StringValue");
                        string value = EditorGUI.TextField(valueRect, stringValue.stringValue);

                        if (changeCheckScope.changed)
                        {
                            stringValue.stringValue = value;
                        }
                    }

                    break;
                }
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUIUtility.singleLineHeight;
        }
    }
}
