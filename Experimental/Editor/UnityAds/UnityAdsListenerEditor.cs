﻿using UnityEditor;
using UnityEditor.AnimatedValues;
using UnityEngine;

namespace PainfulSmile.Experimental
{
    [CustomEditor(typeof(UnityAdsListener), true), CanEditMultipleObjects]
    public sealed class UnityAdsListenerEditor : Editor
    {
        private UnityEventDrawer _eventDrawer;
        private SerializedProperty _adsType;
        private AnimFloat _animFloat;

        private float currentAnimFloatTarget { get { return _adsType.intValue == (int)UnityAdsListener.AdsType.Custom ? 1 : 0; } }

        private void OnEnable()
        {
            _eventDrawer = new UnityEventDrawer();
            _adsType = serializedObject.FindProperty("_adsType");
            _animFloat = new AnimFloat(currentAnimFloatTarget, Repaint);
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            UnityAdsEditorTools.DrawWarningBox();

            switch ((UnityAdsListener.AdsType)_adsType.intValue)
            {
                case UnityAdsListener.AdsType.RewardedVideo:
                    UnityAdsEditorTools.DrawVideoState(true, false);
                    break;

                case UnityAdsListener.AdsType.SkippableVideo:
                    UnityAdsEditorTools.DrawVideoState(false, true);
                    break;
            }

            using (EditorGUI.ChangeCheckScope scope = new EditorGUI.ChangeCheckScope())
            {
                EditorGUILayout.PropertyField(_adsType);

                _animFloat.target = currentAnimFloatTarget;

                using (EditorGUILayout.FadeGroupScope fadeGroupScope = new EditorGUILayout.FadeGroupScope(_animFloat.value))
                {
                    if (fadeGroupScope.visible)
                    {
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("_placementID"));
                    }
                }

                EditorGUILayout.Separator();

                DrawEvent(serializedObject.FindProperty("_onNotImplemented"));
                DrawEvent(serializedObject.FindProperty("_onNotSupported"));
                DrawEvent(serializedObject.FindProperty("_onTimeOut"));
                DrawEvent(serializedObject.FindProperty("_onFailed"));
                DrawEvent(serializedObject.FindProperty("_onSkipped"));
                DrawEvent(serializedObject.FindProperty("_onFinished"));

                if (scope.changed)
                {
                    serializedObject.ApplyModifiedProperties();
                }
            }
        }

        private void DrawEvent(SerializedProperty property)
        {
            GUIContent label = new GUIContent(property.displayName, property.tooltip);
            _eventDrawer.OnGUI(EditorGUILayout.GetControlRect(true, _eventDrawer.GetPropertyHeight(property, label)), property, label);
        }
    }
}
