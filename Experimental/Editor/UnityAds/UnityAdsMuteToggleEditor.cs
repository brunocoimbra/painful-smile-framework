﻿using UnityEditor;

namespace PainfulSmile.Experimental
{
    [CustomEditor(typeof(UnityAdsMuteToggle), true), CanEditMultipleObjects]
    public sealed class UnityAdsMuteToggleEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            UnityAdsEditorTools.DrawWarningBox();

#if UNITY_ADS && (UNITY_ANDROID || UNITY_IOS)
            if (UnityAdsSettingsEditor.settings.hasMuteVariant == false)
            {
                EditorGUILayout.HelpBox("Missing mute variants! Go to 'Edit -> Project Settings -> Unity Ads'.", MessageType.Warning, true);
            }
#endif

            using (new EditorGUI.DisabledScope(true))
            {
                EditorGUILayout.Toggle("Is Unity Ads Mute?", EditorApplication.isPlaying ? UnityAds.mute : false);
            }
        }
    }
}
