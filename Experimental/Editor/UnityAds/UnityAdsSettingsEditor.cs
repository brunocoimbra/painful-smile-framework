﻿using System.IO;
using UnityEditor;
using UnityEditor.AnimatedValues;
using UnityEngine;

namespace PainfulSmile.Experimental
{
    [CustomEditor(typeof(UnityAdsSettings))]
    public sealed class UnityAdsSettingsEditor : Editor
    {
        private const string AssetDirectory = "Assets/Resources/";

        private static UnityAdsSettings _settings;

        private AnimFloat _animFloat;
        private SerializedProperty _hasMuteVariant;
        private SerializedProperty _rewardedVideoID;
        private SerializedProperty _rewardedVideoIDMute;
        private SerializedProperty _skippableVideoID;
        private SerializedProperty _skippableVideoIDMute;

        public static UnityAdsSettings settings
        {
            get
            {
                if (_settings == null)
                {
                    if (UnityAdsSettings.LoadOrCreateInstance(out _settings) == false)
                    {
                        string __directory = string.Format("{0}{1}", Application.dataPath.Replace("Assets", ""), AssetDirectory).Replace("/", "\\");

                        if (Directory.Exists(__directory) == false)
                        {
                            Directory.CreateDirectory(__directory);
                        }

                        AssetDatabase.CreateAsset(_settings, AssetDirectory + typeof(UnityAdsSettings).Name + ".asset");
                        AssetDatabase.SaveAssets();
                        AssetDatabase.Refresh();
                    }
                }

                return _settings;
            }
        }

        private float targetAnimFloatValue { get { return (_hasMuteVariant.boolValue ? 1 : 0); } }

        private static void ShowSettings()
        {
            Selection.activeObject = settings;
        }

        private void OnEnable()
        {
            _hasMuteVariant = serializedObject.FindProperty("_hasMuteVariant");
            _rewardedVideoID = serializedObject.FindProperty("_rewardedVideoID");
            _rewardedVideoIDMute = serializedObject.FindProperty("_rewardedVideoIDMute");
            _skippableVideoID = serializedObject.FindProperty("_skippableVideoID");
            _skippableVideoIDMute = serializedObject.FindProperty("_skippableVideoIDMute");
            _animFloat = new AnimFloat(targetAnimFloatValue, Repaint);
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            UnityAdsEditorTools.DrawWarningBox();
            UnityAdsEditorTools.DrawVideoState(true, true);

            using (EditorGUI.ChangeCheckScope changeCheckScope = new EditorGUI.ChangeCheckScope())
            {
                EditorGUILayout.PropertyField(serializedObject.FindProperty("_showTimeLimit"));
                EditorGUILayout.PropertyField(_rewardedVideoID);

                if (string.IsNullOrEmpty(_rewardedVideoID.stringValue))
                {
                    _rewardedVideoID.stringValue = UnityAdsSettings.Defaults.RewardedVideoID;
                }

                EditorGUILayout.PropertyField(_skippableVideoID);

                if (string.IsNullOrEmpty(_skippableVideoID.stringValue))
                {
                    _skippableVideoID.stringValue = UnityAdsSettings.Defaults.SkippableVideoID;
                }

                EditorGUILayout.Separator();
                EditorGUILayout.PropertyField(_hasMuteVariant);

                _animFloat.target = targetAnimFloatValue;

                using (EditorGUILayout.FadeGroupScope fadeGroupScope = new EditorGUILayout.FadeGroupScope(_animFloat.value))
                {
                    if (fadeGroupScope.visible)
                    {
                        EditorGUILayout.PropertyField(_rewardedVideoIDMute);

                        if (string.IsNullOrEmpty(_rewardedVideoIDMute.stringValue))
                        {
                            _rewardedVideoIDMute.stringValue = UnityAdsSettings.Defaults.RewardedVideoIDMute;
                        }

                        EditorGUILayout.PropertyField(_skippableVideoIDMute);

                        if (string.IsNullOrEmpty(_skippableVideoIDMute.stringValue))
                        {
                            _skippableVideoIDMute.stringValue = UnityAdsSettings.Defaults.SkippableVideoIDMute;
                        }
                    }
                }

                if (changeCheckScope.changed)
                {
                    serializedObject.ApplyModifiedProperties();
                }
            }
        }
    }
}
