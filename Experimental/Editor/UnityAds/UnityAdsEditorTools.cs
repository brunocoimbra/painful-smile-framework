﻿using UnityEditor;

namespace PainfulSmile.Experimental
{
    internal static class UnityAdsEditorTools
    {
        public static void DrawWarningBox()
        {
#if !UNITY_ADS
            EditorGUILayout.HelpBox("Unity Ads was not implemented!", MessageType.Error, true);
#elif !UNITY_ANDROID && !UNITY_IOS
            EditorGUILayout.HelpBox("Not supported in the current platform.", MessageType.Warning, true);
#endif
        }

        public static void DrawVideoState(bool rewarded, bool skippable)
        {
#if UNITY_ADS && (UNITY_ANDROID || UNITY_IOS)
            if (EditorApplication.isPlaying)
            {
                if (rewarded && skippable)
                {
                    using (new EditorGUILayout.HorizontalScope())
                    {
                        SwitchVideoState(PainfulSmile.UnityAds.GetRewardedVideoState(), "Rewarded");
                        SwitchVideoState(PainfulSmile.UnityAds.GetSkippableVideoState(), "Skippable");
                    }
                }
                else if (rewarded)
                {
                    SwitchVideoState(PainfulSmile.UnityAds.GetRewardedVideoState(), "Rewarded");
                }
                else if (skippable)
                {
                    SwitchVideoState(PainfulSmile.UnityAds.GetSkippableVideoState(), "Skippable");
                }
            }
        }

        private static void SwitchVideoState(PainfulSmile.UnityAds.PlacementState state, string type)
        {
            switch (state)
            {
                case PainfulSmile.UnityAds.PlacementState.Ready:
                    EditorGUILayout.HelpBox(type + " video is ready.", MessageType.None, true);
                    break;

                case PainfulSmile.UnityAds.PlacementState.NotAvailable:
                    EditorGUILayout.HelpBox(type + " video is not available!", MessageType.Error, true);
                    break;

                case PainfulSmile.UnityAds.PlacementState.Disabled:
                    EditorGUILayout.HelpBox(type + " video is disabled!", MessageType.Error, true);
                    break;

                case PainfulSmile.UnityAds.PlacementState.Waiting:
                    EditorGUILayout.HelpBox(type + " video is waiting!", MessageType.Warning, true);
                    break;

                case PainfulSmile.UnityAds.PlacementState.NoFill:
                    EditorGUILayout.HelpBox(type + " video is empty.", MessageType.Error, true);
                    break;
            }
#endif
        }
    }
}
