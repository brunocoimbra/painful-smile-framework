## `ParameterSetter`

A `StateMachineBehaviour` that allows the target `Animator` to automatically set a parameter value when entering or exiting a state or a state machine.

To use it, just attach the behaviour in your animator state or your animator state machine and select which callbacks to listen and which parameters to set.
