# Threading

* [`ThreadCommand`](#threadcommand)
* [`ThreadCommand<TResult>`](#threadcommandtresult)
* [`ThreadAction`](#threadaction)
* [`ThreadAction<T>`](#threadactiont)
* [`ThreadFunc<TResult>`](#threadfunctresult)
* [`ThreadFunc<T, TResult>`](#threadfuncttresult)
* [`ThreadUtility`](#threadutility)
* [`WaitForThreadCommand`](#waitforthreadcommand)
* [`WaitForTask`](#waitfortask)

## `ThreadCommand`

Inherit from it to create your own commands that can be enqueued to be executed in another thread.

Check [`ThreadAction`](#threadaction) or [`ThreadAction<T>`](#threadactiont) for an example of how to implement your own.

## `ThreadCommand<TResult>`

Similar to [`ThreadCommand<TResult>`](#threadcommandtresult), but you can also get a result from the enqueued command.

Check [`ThreadFunc<TResult>`](#threadfunctresult) or [`ThreadFunc<T, TResult>`](#threadfuncttresult) for an example of how to implement your own.

## `ThreadAction`

A generic implementation for the [`ThreadCommand`](#threadcommand). Just use `SetCallback` to change what happens and then enqueue the command.

## `ThreadAction<T>`

Similar to [`ThreadAction`](#threadaction) but allows the caller to send an argument when going to enqueue the command.

## `ThreadFunc<TResult>`

A generic implementation for the [`ThreadCommand<TResult>`](#threadcommandtresult). Just use `SetCallback` to change what happens and then enqueue the command.

## `ThreadFunc<T, TResult>`

Similar to [`ThreadFunc<TResult>`](#threadfunctresult) but allows the caller to send an argument when going to enqueue the command.

## `ThreadUtility`

Threading-related utility stuff. Currently it only have a property that allows you to check whether or not the code is currently running in the Unity main thread, thus allowing you to check if you can make use of the Unity API.

## `WaitForThreadCommand`

Custom yield instruction to wait for a [`ThreadCommand`](#threadcommand) to finish.

## `WaitForTask`

Custom yield instruction to wait for a C# `Task` to finish.
