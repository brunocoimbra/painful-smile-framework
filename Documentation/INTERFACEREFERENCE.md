## `InterfaceReference<T>`

Class to allow to expose a `UnityEngine.Object` field that requires the object to implement a specific interface.

##### Example

```c#
[System.Serializable]
public class PointerClickHandlerReference : InterfaceReference<IPointerClickHandler> { }

[SerializeField] private PointerClickHandlerReference _pointerClickHandler;

public void OnPointerClick(PointerEventData pointerEventData)
{
    if (_pointerClickHandler.HasValue)
    {
        _pointerClickHandler.Value.OnPointerClick(pointerEventData);
    }
}
```
