## `ConstantsGenerator`

This is a tool to generate type safe constants to avoid errors when using Unity's special strings. To use it, go to `Tools/Painful Smile/Generate Constants`, which will generate the following files:

* `InputType.cs`: holds the input axes created in Unity's Input menu. You can access the real input name using `InputType.MyInput.GetValue()`.
* `LayerType.cs`: holds the layers created in Unity's Tag and Layers menu. You can access the layer index using `(int)LayerType.MyLayer` or the real layer name using `LayerType.MyLayer.GetValue()`.
* `ResourceType.cs`: holds the assets inside the specials `Resources` folders. You can access the actual `Resources` relative path by using `ResourceType.MyResource.GetValue()`.
* `SceneType.cs`: holds the scenes added to the Unity's Build Settings menu. You can access the scene index using `(int)SceneType.MyScene` or the scene path by using `SceneType.MyScene.GetValue()`.
* `SortingLayerType.cs`:  holds the sorting layers created in Unity's Tags and Layers menu. You can access the sorting layer index using `(int)SortingLayerType.MySortingLayer` or the real sorting layer name using `SortingLayerType.MySortingLayer.GetValue()`.
* `TagType.cs`: holds the tags created in Unity's Tags and Layers menu. You can access the real tag name using `TagType.MyTag.GetValue()`.

Additionally, you can customize the output of this tool by going to `Edit/Project Settings...` and selecting `Constants`, there you can change:

* The folder were the files will be saved, which by default is `Assets/PainfulSmile/Generated/GeneratedConstants/`
* The namespace for the types, which by default is none.
* The line endings, which by default will pick the system's default line endings.
* `Resources` folders to be ignored from the generation, which by default already ignores all `Resources` folders found inside any folder named `PainfulSmile`.

**WARNING**: the generated enums are not guarantee to be serialized always on the same order by Unity, so you should not use them to input data though the inspector. To input data through the inspector, see the [`Attributes`](ATTRIBUTES.md) section;
