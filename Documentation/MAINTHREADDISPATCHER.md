# MainThread Dispatcher

A system that allows to call the Unity API from another thread.

**WARNING**: the Unity API will not be called immediately. Instead, it will be scheduled to run the Unity's Update cycle. It is up to the caller to wait for the call or not.

* [`MainThreadDispatcher`](#mainthreaddispatcher)
* [`MainThreadCommand`](#mainthreadcommand)
* [`MainThreadCommand<TResult>`](#mainthreadcommandtresult)
* [`MainThreadAction`](#mainthreadaction)
* [`MainThreadFunc<TResult>`](#mainthreadfunctresult)
* [`WaitForMainThreadCommand`](#waitformainthreadcommand)

## `MainThreadDispatcher`

This is the core class of the system. Through that you can schedule an action to occur in the next available Update cycle, while also choosing if the action should persist between scene loads.

To configure it, just go to `Edit/Project Setting...` and select `MainThread Dispatcher`, there you can edit the max amount of time (in milliseconds) to be spent each frame to execute the scheduled actions. Do note that this is only verified before starting a new action on the same frame, so it is up to the caller to ensure that the call will not be too expensive.

## `MainThreadCommand`

Inherit from it to create your own commands that can be scheduled to be executed by the [`MainThreadDispatcher`](#mainthreaddispatcher).

Check [`MainThreadAction`](#mainthreadaction) for an example of how to implement your own.

## `MainThreadCommand<TResult>`

Similar to [`MainThreadCommand<TResult>`](#mainthreadcommandtresult), but you can also get a result from the scheduled command.

Check [`MainThreadFunc<TResult>`](#mainthreadfunctresult) for an example of how to implement your own.

## `MainThreadAction`

A generic implementation for the [`MainThreadCommand`](#mainthreadcommand). Just use `SetCallback` to change what happens and then schedule the command.

## `MainThreadFunc<TResult>`

A generic implementation for the [`MainThreadCommand<TResult>`](#mainthreadcommandtresult). Just use `SetCallback` to change what happens and then schedule the command.

## `WaitForMainThreadCommand`

Custom yield instruction to wait for a [`MainThreadCommand`](#mainthreadcommand) to finish.
