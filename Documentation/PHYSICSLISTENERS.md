# Physics Listeners

Collection of components to help when interacting with Unity's physics callbacks, as those callbacks usually requires the listener to be on the same GameObject than the Collider or Rigidbody being used.

* [`Collision2DListener`](#collision2dlistener)
* [`CollisionListener`](#collisionlistener)
* [`Trigger2DListener`](#collision2dlistener)
* [`TriggerListener`](#collisionlistener)

## `Collision2DListener`

Listen to `OnCollisionEnter2D`, `OnCollisionStay2D` and `OnCollisionExit2D` callbacks. It will show a warning in the inspector if there is no non-trigger Collider2D attached to the same GameObject.

##### Example

```c#
[SerializeField] private Collision2DListener _listener;

private void Start()
{
    _listener.OnEnter += HandleCollisionEnter;
    _listener.OnStay += HandleCollisionStay;
    _listener.OnExit += HandleCollisionExit;
}

private void HandleCollisionEnter(GameObject listener, Collision2D collision)
{
    Debug.Log($"{listener} collided with {collision.gameObject}");
}

private void HandleCollisionStay(GameObject listener, Collision2D collision)
{
    Debug.Log($"{listener} is colliding with {collision.gameObject}");
}

private void HandleCollisionExit(GameObject listener, Collision2D collision)
{
    Debug.Log($"{listener} stopped colliding with {collision.gameObject}");
}
```

## `CollisionListener`

Listen to `OnCollisionEnter`, `OnCollisionStay` and `OnCollisionExit` callbacks. It will show a warning in the inspector if there is no non-trigger Collider attached to the same GameObject or if there is a Rigidbody on any parent on the hierarchy, as the Rigidbody would be the callback receiver instead.

##### Example

```c#
[SerializeField] private CollisionListener _listener;

private void Start()
{
    _listener.OnEnter += HandleCollisionEnter;
    _listener.OnStay += HandleCollisionStay;
    _listener.OnExit += HandleCollisionExit;
}

private void HandleCollisionEnter(GameObject listener, Collision collision)
{
    Debug.Log($"{listener} collided with {collision.gameObject}");
}

private void HandleCollisionStay(GameObject listener, Collision collision)
{
    Debug.Log($"{listener} is colliding with {collision.gameObject}");
}

private void HandleCollisionExit(GameObject listener, Collision collision)
{
    Debug.Log($"{listener} stopped colliding with {collision.gameObject}");
}
```

## `Trigger2DListener`

Listen to `OnTriggerEnter2D`, `OnTriggerStay2D` and `OnTriggerExit2D` callbacks. It will show a warning in the inspector if there is no trigger Collider2D attached to the same GameObject.

##### Example

```c#
[SerializeField] private Trigger2DListener _listener;

private void Start()
{
    _listener.OnEnter += HandleTriggerEnter;
    _listener.OnStay += HandleTriggerStay;
    _listener.OnExit += HandleTriggerExit;
}

private void HandleTriggerEnter(GameObject listener, Collider2D collider)
{
    Debug.Log($"{listener} overlaped {collider.gameObject}");
}

private void HandleTriggerStay(GameObject listener, Collider2D collider)
{
    Debug.Log($"{listener} is overlaping {collider.gameObject}");
}

private void HandleTriggerExit(GameObject listener, Collider2D collider)
{
    Debug.Log($"{listener} stopped overlaping {collider.gameObject}");
}
```

## `TriggerListener`

Listen to `OnTriggerEnter`, `OnTriggerStay` and `OnTriggerExit` callbacks. It will show a warning in the inspector if there is no trigger Collider attached to the same GameObject.

##### Example

```c#
[SerializeField] private TriggerListener _listener;

private void Start()
{
    _listener.OnEnter += HandleTriggerEnter;
    _listener.OnStay += HandleTriggerStay;
    _listener.OnExit += HandleTriggerExit;
}

private void HandleTriggerEnter(GameObject listener, Collider collider)
{
    Debug.Log($"{listener} overlaped {collider.gameObject}");
}

private void HandleTriggerStay(GameObject listener, Collider collider)
{
    Debug.Log($"{listener} is overlaping {collider.gameObject}");
}

private void HandleTriggerExit(GameObject listener, Collider collider)
{
    Debug.Log($"{listener} stopped overlaping {collider.gameObject}");
}
```
