## `SerializableList<T>`

A simple wrapper for a `List<T>` so that you can serialize it properly in Unity.

Check the [`ReorderableAttribute`](ATTRIBUTES.md#reorderable) to see an example of its usage.
