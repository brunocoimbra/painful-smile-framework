# Singletons

Collection of classes to help you to use the Singleton pattern properly inside Unity.

* [`Singleton<T>`](#singletont)
* [`DynamicSingleton<T>`](#dynamicsingletont)
* [`SceneSingleton<T>`](#scenesingletont)
* [`DynamicSceneSingleton<T>`](#dynamicscenesingletont)
* [`ScriptableSingleton<T>`](#scriptablesingletont)
* [`EditorScriptableSingleton<T>`](#editorscriptablesingletont)

## `Singleton<T>`

Singleton pattern where the frist object created is the one kept until it gets destroyed.

## `DynamicSingleton<T>`

Singleton pattern where the last objected created is the one kept.

## `SceneSingleton<T>`

Similar to [`Singleton<T>`](#singletont), but it allows to have one object per scene.

## `DynamicSceneSingleton<T>`

Similar to [`DynamicSingleton<T>`](#dynamicsingletont), but it allows to have one object per scene.

## `ScriptableSingleton<T>`

Singleton pattern to be used with a `ScriptableObject`.

## `EditorScriptableSingleton<T>`

Singleton pattern to be used with a `ScriptableObject` for editor-only stuff.
