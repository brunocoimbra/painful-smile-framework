## `Reference<T>`

Use it to hold a reference to some struct value.

##### Example

```c#
private IEnumerator Start()
{
    var myIntReference = new Reference<int>(0);

    yield return ModifyMyReference(myIntReference);

    Debug.Log(myIntReference.Value);
}

private IEnumerator ModifyMyReference(Reference<int> someNumber)
{
    yield return null;

    someNumber++;
}
```
