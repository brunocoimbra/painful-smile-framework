## `ReorderableList`

An editor class to allow you to show a reorderable list in place of the usual Unity's array/list field.

##### Example

```c#
private ReorderableList _list;

private void OnEnable()
{
    _list = new ReorderableList(serializedObject.FindProperty("_myList"));
}

public override void OnInspectorGUI()
{
    _list.DrawGUILayout();
}
```
```c#
public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
{
    ReorderableList list = ReorderableList.GetReorderableList(property.FindPropertyRelative("_myList"));

    position.height = list.TotalHeight;

    list.DrawGUI(position);
}
```
