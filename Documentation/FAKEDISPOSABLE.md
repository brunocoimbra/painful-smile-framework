## `FakeDisposable<T>`

Use this to create - as the same suggests - a fake disposable type, allowing you to create explicit scope dependencies in your code.

## Example

```
private void Start()
{
    // create a temporary Transform to do some calculations
    using (var fakeTransform = new FakeDisposable<Transform>(new GameObject().transform, (ref Transform value) => Destroy(value)))
    {
        // use fakeTransform.Value to access the created transform
    }
}
```
