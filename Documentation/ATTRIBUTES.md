# Attributes

Collection of attributes to help the creation of better looking and more intuitive inspectors.

**WARNING**: you won't be able to combine those attributes with each other or with Unity's own attributes, with the only exceptions being `SerializeField` and decorators (like `Header` and `Space`).

* [`AnimatorParameter`](#animatorparameter)
* [`Button`](#button)
* [`Callback`](#callback)
* [`EnumFlags`](#enumflags)
* [`InputSelector`](#inputselector)
* [`LayerSelector`](#layerselector)
* [`Negative`](#negative)
* [`NotGreaterThan`](#notgreaterthan)
* [`NotLessThan`](#notlessthan)
* [`ObjectPicker`](#objectpicker)
* [`Positive`](#positive)
* [`RangeSlider`](#rangeslider)
* [`ReadOnly`](#readonly)
* [`ReadWrite`](#readwrite)
* [`Reorderable`](#reorderable)
* [`ScenePath`](#scenepath)
* [`SceneSelector`](#sceneselector)
* [`SortingLayerID`](#sortinglayerid)
* [`TagSelector`](#tagselector)
* [`UnityEvent`](#unityevent)

## `AnimatorParameter`

Create a dropdown to select a parameter of an Animator, which you can either pick the parameter id (`int`) or the parameter name (`string`).

##### Parameters

_`animatorField`_: the Animator field with a reference to the target Animator.

_`parameterType`_: the type of the Animator parameter.

_`editMode`_: customize how the dropdown should behave in edit mode.

_`playMode`_: customize how the dropdown should behave in play mode.

_`callbacks`_: array of methods to call when the value changes.

##### Example

```c#
[SerializeField] private Animator _animatorWithBools;
[SerializeField] private Animator _animatorWithFloats;

[AnimatorParameter(AnimatorControllerParameterType.Bool)]
[SerializeField] private string _boolParameterName;

[AnimatorParameter("_animatorWithFloats", AnimatorControllerParameterType.Float)]
[SerializeField] private int _floatParameterId;
```

## `Button`

Create a button on top of a `bool` field to call one or more methods.

##### Parameters

_`options`_: how the button should appear.

_`editMode`_: customize how the button should behave in edit mode.

_`playMode`_: customize how the button should behave in play mode.

_`callbacks`_: array of methods to call when clicked.

##### Example

```c#
[SerializeField, Button(nameof(ButtonClicked))] private bool _button;

private void ButtonClicked()
{
    Debug.Log("Button clicked!");
}
```

## `Callback`

Use it to receive a callback when changing the value of a field.

##### Parameters

_`delayed`_: if should wait the user press enter to execute the callbacks (for `string`, `int` and `float`).

_`callbacks`_: array of methods to call.

##### Example

```c#
[SerializeField, Callback(nameof(ValueChanged))] private bool _button;

private void ValueChanged()
{
    Debug.Log("Value changed!");
}
```

## `EnumFlags`

Create a flags selector popup for any `enum` with the attribute `System.Flags`.

##### Parameters

_`editMode`_: customize how the popup should behave in edit mode.

_`playMode`_: customize how the popup should behave in play mode.

_`callbacks`_: array of methods to call when the value changes.

##### Example

```c#
[System.Flags]
public enum MyFlags
{
    Nothing = 0,
    Everything = ~0,
    A = 1 << 0,
    B = 1 << 1,
    C = 1 << 2
}

[SerializeField, EnumFlags] private MyFlags _flags;
```

## `InputSelector`

Create a dropdown in top of a `string` field to select an input defined in the project settings.

##### Parameters

_`editMode`_: customize how the dropdown should behave in edit mode.

_`playMode`_: customize how the dropdown should behave in play mode.

_`callbacks`_: array of methods to call when the value changes.

##### Example

```c#
[SerializeField, InputSelector] private string _input;
```

## `LayerSelector`

Create a dropdown to select a layer defined in the project settings, which you can either pick the layer id (`int`) or the layer name (`string`).

##### Parameters

_`editMode`_: customize how the dropdown should behave in edit mode.

_`playMode`_: customize how the dropdown should behave in play mode.

_`callbacks`_: array of methods to call when the value changes.

##### Example

```c#
[SerializeField, LayerSelector] private int _layerId;
[SerializeField, LayerSelector] private string _layerName;
```

## `Negative`

Ensure that the number (`int` or `float`) will always be negative (`<= 0`).

##### Parameters

_`editMode`_: customize how the field should behave in edit mode.

_`playMode`_: customize how the field should behave in play mode.

_`delayed`_: if should wait the user press enter to execute the callbacks.

_`callbacks`_: array of methods to call when the value changes.

##### Example

```c#
[SerializeField, Negative] private int _negativeInteger;
[SerializeField, Negative] private float _negativeFloat;
```

## `NotGreaterThan`

Ensure that the number (`int` or `float`) will never be greater than the value (`<= value`).

##### Parameters

_`value`_: the which the number should never be greater than.

_`editMode`_: customize how the field should behave in edit mode.

_`playMode`_: customize how the field should behave in play mode.

_`delayed`_: if should wait the user press enter to execute the callbacks.

_`callbacks`_: array of methods to call when the value changes.

##### Example

```c#
[SerializeField, NotGreaterThan(10)] private int _notGreaterThanTen;
[SerializeField, NotGreaterThan(1.5f)] private float _notGreaterThanOneDotFive;
```

## `NotLessThan`

Ensure that the number (`int` or `float`) will never be less than the value (`>= value`).

##### Parameters

_`value`_: the which the number should never be less than.

_`editMode`_: customize how the field should behave in edit mode.

_`playMode`_: customize how the field should behave in play mode.

_`delayed`_: if should wait the user press enter to execute the callbacks.

_`callbacks`_: array of methods to call when the value changes.

##### Example

```c#
[SerializeField, NotLessThan(10)] private int _notLessThanTen;
[SerializeField, NotLessThan(1.5f)] private float _notLessThanOneDotFive;
```

## `ObjectPicker`

Customize a `UnityEngine.Object` field's picker.

##### Parameters

_`allowSceneObjects`_: if should be allowed to pick objects from the scene.

_`editMode`_: customize how the field should behave in edit mode.

_`playMode`_: customize how the field should behave in play mode.

_`callbacks`_: array of methods to call when the value changes.

##### Example

```c#
[SerializeField, ObjectPicker(false)] private GameObject _prefab;
```

## `Positive`

Ensure that the number (`int` or `float`) will always be positive (`>= 0`).

##### Parameters

_`editMode`_: customize how the field should behave in edit mode.

_`playMode`_: customize how the field should behave in play mode.

_`delayed`_: if should wait the user press enter to execute the callbacks.

_`callbacks`_: array of methods to call when the value changes.

##### Example

```c#
[SerializeField, Positive] private int _positiveInteger;
[SerializeField, Positive] private float _positiveFloat;
```

## `RangeSlider`

Create a slider with a min and a max value, which works with `Vector2`, `Vector2Int`, [`FloatRange`](UTILITIES.md#floatrange) or [`IntRange`](UTILITIES.md#intrange).

##### Parameters

_`a`_: the min or the max value.

_`b`_: the min or the max value.

_`editMode`_: customize how the slider should behave in edit mode.

_`playMode`_: customize how the slider should behave in play mode.

_`callbacks`_: array of methods to call when any of the value changes.

##### Example

```c#
[SerializeField, RangeSlider(0, 100)] private Vector2 _vector2Slider;
[SerializeField, RangeSlider(0, 100)] private Vector2Int _vector2IntSlider;
[SerializeField, RangeSlider(0, 100)] private FloatRange _floatRangeSlider;
[SerializeField, RangeSlider(0, 100)] private IntRange _intRangeSlider;
```

## `ReadOnly`

Use it to show the field as a read-only field.

##### Example

```c#
[SerializeField] private string _exposed = "This message has the default behavior";
[SerializeField, ReadOnly] private string _readOnly = "This message can't be edited!";
[SerializeField, HideInInspector] private string _hidden = "This message will not show up!";
```

## `ReadWrite`

Customize how the field should behave in edit mode and/or play mode.

##### Parameters

_`editMode`_: edit mode behavior.

_`playMode`_: play mode behavior.

_`delayed`_: if should wait the user press enter to execute the callbacks.

_`callbacks`_: array of methods to call when the value changes.

##### Example

```c#
[ReadWrite(ReadWriteMode.Hide, ReadWriteMode.Read)]
[SerializeField] private string _example1 = "Hidden in edit mode and read-only in play mode";

[ReadWrite(ReadWriteMode.Write, ReadWriteMode.Hide)]
[SerializeField] private string _example2 = "Editable in edit mode and hidden in play mode";
```

## `Reorderable`

Use it with a custom class inherited from [`SerializableList<T>`](UTILITIES.md#serializable-list) to show it as a reorderable list in the inspector.

##### Parameters

_`elementName`_: what will be displayed in each element's name, you can use some special syntax here:

* if the elements are `UnityEngine.Object`, you can use `"name"` to display the object's name;
* you can use a method name to define the element's name, where the method signature is `string MethodName(IList list, int element)`;
* you can use a Unity serialized field in the element, just use `"$fieldName"`;
* you can use a constant string followed by the element index by using `"@My Element Name"`.

_`permissions`_: inspector actions permissions when both in edit mode and in play mode:

_`editModePermissions`_: inspector actions permissions when in edit mode.

_`playModePermissions`_: inspector actions permissions when in play mode.

_`elementDisplay`_: how the elements should be displayed.

##### Example

```c#
[System.Serializable]
public class CustomData
{
    [SerializeField] private Transform _transform;
}

[System.Serializable]
public class CustomDataList : SerializableList<CustomData> { }

[System.Serializable]
public class GameObjectList : SerializableList<GameObject> { }

[System.Serializable]
public class StringList : SerializableList<string> { }

[SerializeField, Reorderable] private StringList _example1;
[SerializeField, Reorderable("name")] private GameObjectList _example2;
[SerializeField, Reorderable("GetElementName")] private StringList _example3;
[SerializeField, Reorderable("$_transform")] private CustomDataList _example4;
[SerializeField, Reorderable("@Item")] private StringList _example5;

private string GetElementName(StringList list, int element)
{
    return $"{list[element]} {element}";
}
```

## `ScenePath`

Create a dropdown on top of a `string` to select a scene path from the scenes set on the build settings.

##### Parameters

_`excludeDisabled`_: if the dropdown should hide scenes disabled on the build settings.

_`editMode`_: customize how the dropdown should behave in edit mode.

_`playMode`_: customize how the dropdown should behave in play mode.

_`callbacks`_: array of methods to call when the value changes.

##### Example

```c#
[SerializeField, ScenePath] private string _scenePath;
```

## `SceneSelector`

Create a dropdown to select a scene from the build settings, which you can either pick the scene index (`int`) or the scene name (`string`).

##### Parameters

_`excludeDisabled`_: if the dropdown should hide scenes disabled on the build settings.

_`editMode`_: customize how the dropdown should behave in edit mode.

_`playMode`_: customize how the dropdown should behave in play mode.

_`callbacks`_: array of methods to call when the value changes.

##### Example

```c#
[SerializeField, SceneSelector] private string _sceneName;
[SerializeField, SceneSelector] private int _sceneIndex;
```

## `SortingLayerID`

Create a dropdown to select a sorting layer defined in the project settings, which you can either pick the sorting layer id (`int`) or the sorting layer name (`string`).

##### Parameters

_`editMode`_: customize how the dropdown should behave in edit mode.

_`playMode`_: customize how the dropdown should behave in play mode.

_`callbacks`_: array of methods to call when the value changes.

##### Example

```c#
[SerializeField, SortingLayerID] private int _sortingLayerId;
[SerializeField, SortingLayerID] private string _sortingLayerName;
```

## `TagSelector`

Create a dropdown on top of a `string` to select a tag defined in the project settings.

##### Parameters

_`editMode`_: customize how the dropdown should behave in edit mode.

_`playMode`_: customize how the dropdown should behave in play mode.

_`callbacks`_: array of methods to call when the value changes.

##### Example

```c#
[SerializeField, TagSelector] private string _tag;
```

## `UnityEvent`

Use with with an `UnityEvent` to expand what you can do on the inspector.

##### Parameters

_`permissions`_: inspector actions permissions when both in edit mode and in play mode:

_`editModePermissions`_: inspector actions permissions when in edit mode.

_`playModePermissions`_: inspector actions permissions when in play mode.

_`elementDisplay`_: how the elements should be displayed.

##### Example

```c#
[SerializeField, UnityEvent] private UnityEvent _unityEvent;
```
