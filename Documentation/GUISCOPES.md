# GUI Scopes

Some custom `GUI.Scope` implementations to help on the creation of editor GUI scripts.

* [`BackgroundColorScope`](#backgroundcolorscope)
* [`HierarchyModeScope`](#hierarchymodescope)
* [`LabelWidthScope`](#labelwidthscope)
* [`ShowMixedValueScope`](#showmixedvaluescope)

## `BackgroundColorScope`

Backup the current `GUI.backgroundColor` and revert it back at the end of the scope.

##### Parameters

_`backgroundColor`_: the new temporary `GUI.backgroundColor` value.

##### Example

```c#
using (new BackgroundColorScope())
{
    // you can safely change the GUI.backgroundColor property here
}

using (new BackgroundColorScope(Color.red))
{
    // GUI.backgroundColor will be red until the end of the scope
}
```

## `HierarchyModeScope`

Backup the current `EditorGUIUtility.hierarchyMode` value and revert it back at the end of the scope.

##### Parameters

_`hierarchyMode`_: the new temporary `EditorGUIUtility.hierarchyMode` value.

##### Example

```c#
using (new HierarchyModeScope())
{
    // you can safely change the EditorGUIUtility.hierarchyMode property here
}

using (new HierarchyModeScope(true))
{
    // EditorGUIUtility.hierarchyMode will be true until the end of the scope
}
```

## `LabelWidthScope`

Temporarily modify the `EditorGUIUtility.labelWidth` property.

##### Parameters

_`increment`_: the temporary increment of `EditorGUIUtility.labelWidth`.

##### Example

```c#
using (new LabelWidthScope(10))
{
    // the label width will be 10 pixels larger here
}

using (new LabelWidthScope(-10))
{
    // the label width will be 10 pixels smaller here
}
```

## `ShowMixedValueScope`

Backup the current `EditorGUI.showMixedValue` value and revert it back at the end of the scope.

##### Parameters

_`showMixedValue`_: the new temporary `EditorGUI.showMixedValue` value.

##### Example

```c#
using (new ShowMixedValueScope())
{
    // you can safely change the EditorGUI.showMixedValue property here
}

using (new ShowMixedValueScope(true))
{
    // EditorGUI.showMixedValue will be true until the end of the scope
}
```
