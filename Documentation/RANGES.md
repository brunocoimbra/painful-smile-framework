# Ranges

Utility structures to make it easier to deal with a range of values.

* [`FloatRange`](#floatrange)
* [`IntRange`](#intrange)

## `FloatRange`

Holds a min and a max value for something. It has some utility methods, like to get a random number in between or to get the difference between the two numbers.

You can implicitly convert a `Vector2` to a `FloatRange` and vice-versa.

## `IntRange`

Holds a min and a max value for something. It has some utility methods, like to get a random number in between or to get the difference between the two numbers.

You can implicitly convert a `Vector2Int` to a `IntRange` and vice-versa.
