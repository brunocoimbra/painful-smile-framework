# Utilities

* [`CacheCleaner`](#cachecleaner)
* [`ScriptableObjectWizard`](#scriptableobjectwizard)
* [`Extensions`](#extensions)
* [`DescriptionUtility`](#descriptionutility)
* [`Ease`](#ease)
* [`Math`](#math)
* [`PSUtility`](#psutility)
* [`PSEditorUtility`](#pseditorutility)
* [`PSEditorGUIUtility`](#pseditorguiutility)

## `CacheCleaner`

Editor tool found in `Tools/Painful Smile/Clear Cache`. Use it to call `Caching.ClearCache()`, thus clearing all the AssetBundles and ProceduralMaterials that has been cached by Unity.

## `ScriptableObjectWizard`

Editor tool found in `Assets/Create/ScriptableObject`. Use it to quickly create any `ScritableObject` while inside the editor without needing the `CreateAssetMenuAttribute`.

## `Extensions`

Collections of extensions methods. They are all documented and separated per type, so you can easily check what is available to use.

## `DescriptionUtility`

Utility class to expand the `DescriptionAttribute` functionality.

## `Ease`

Collection of easing operations meant to be used with lerp methods. All the methods assume that the value is already clamped between 0 and 1.

##### Example

```c#
private IEnumerator MoveTo(Transform target, Vector to, float duration)
{
    Vector3 from = target.position;
    float elapsedTime = 0;

    while (elapsedTime < duration)
    {
        yield return null;

        elapsedTime += Time.deltaTime;

        float normalizedTime = Mathf.InverseLerp(0, duration, elapsedTime);
        normalizedTime = Mathf.Clamp01(normalizedTime);

        target.position = Vector3.Lerp(from, to, Ease.Sine(normalizedTime));
    }
}
```

## `Math`

Collection of methods with a wide range of use case related to mathematics.

## `PSUtility`

Undocumented stuff to be used in runtime. Feel free to take a look.

## `PSEditorUtility`

Undocumented editor-only stuff. Feel free to take a look.

## `PSEditorGUIUtility`

Undocumented `EditorGUI`-related stuff. Feel free to take a look.
