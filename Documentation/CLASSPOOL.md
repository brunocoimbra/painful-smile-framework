# Class Pool

Collection of classes to help in the creation of pools to re-use objects, avoiding unnecessary garbage in runtime. They are meant to be thread safe, but there may be race conditions.

* [`ClassPool<T>`](#classpoolt)
* [`ClassPoolBase<T>`](#classpoolbaset)
* [`DictionaryPool<TKey, TValue>`](#dictionarypooltkey-tvalue)
* [`HashSetPool<T>`](#hashsetpoolt)
* [`ListPool<T>`](#listpoolt)
* [`QueuePool<T>`](#queuepoolt)
* [`StackPool<T>`](#stackpoolt)

## `ClassPool<T>`

Create an instance from that class to quickly start pooling your objects.

##### Example

```c#
private void Awake()
{
    var texturePool = new ClassPool<Texture2D>(Create, Get, Release, Delete);
    
    Texture texture = texturePool.Get();

    // do some stuff with texture

    texturePool.Release(ref texture);

    using (Texture scopeTexture = texturePool.GetDisposable())
    {
        // do some stuff with scopeTexture
    }
}

private Texture2D Create()
{
    return new Texture2D(2, 2);
}

private void Get(Texture texture)
{
    Debug.Log($"{texture} is being picked from the pool!");
}

private void Release(Texture texture)
{
    Debug.Log($"{texture} is returning to the pool!");
}

private void Delete(Texture texture)
{
    Object.Destroy(texture);
}
```

## `ClassPoolBase<T>`

Inherit from this class to create custom pools.

##### Example

```c#
[System.Serializable]
public class Texture2DPool : ClassPoolBase<Texture2D>
{
    [SerializeField] private int _defaultWidth = 2;
    [SerializeField] private int _defaultHeight = 2;

    public Texture2DPool() { }

    public Texture2DPool(int preloadCount, int maxCapacity)
        : base(preloadCount, maxCapacity)
    {
        Preload();
    }

    protected override Texture2D Create()
    {
        return new Texture2D(_defaultWidth, _defaultHeight);
    }

    protected override void OnDelete(Texture2D item)
    {
        Object.Destroy(texture);
    }

    protected override void OnGet(Texture2D item)
    {
        Debug.Log($"{texture} is being picked from the pool!");
    }

    protected override void OnRelease(Texture2D item)
    {
        Debug.Log($"{texture} is returning to the pool!");
    }
}
```

## `DictionaryPool<TKey, TValue>`

A ready to use static pool for dictionaries.

##### Example

```c#
var stringToInt = DictionaryPool<string, int>.Get();

// do some stuff with stringToInt

DictionaryPool<string, int>.Release(ref stringToInt);

using (var stringToString = DictionaryPool<string, string>.GetDisposable())
{
    // do some stuff with stringToString
}
```

## `HashSetPool<T>`

A ready to use static pool for hash sets.

##### Example

```c#
var stringSet = HashSetPool<string>.Get();

// do some stuff with stringSet

HashSetPool<string>.Release(ref stringSet);

using (var intSet = HashSetPool<int>.GetDisposable())
{
    // do some stuff with intSet
}
```

## `ListPool<T>`

A ready to use static pool for lists.

##### Example

```c#
var stringList = ListPool<string>.Get();

// do some stuff with stringList

ListPool<string>.Release(ref stringList);

using (var intList = ListPool<int>.GetDisposable())
{
    // do some stuff with intList
}
```

## `QueuePool<T>`

A ready to use static pool for queues.

##### Example

```c#
var stringQueue = QueuePool<string>.Get();

// do some stuff with stringQueue

QueuePool<string>.Release(ref stringQueue);

using (var intQueue = QueuePool<int>.GetDisposable())
{
    // do some stuff with intQueue
}
```

## `StackPool<T>`

A ready to use static pool for stacks.

##### Example

```c#
var stringStack = StackPool<string>.Get();

// do some stuff with stringStack

StackPool<string>.Release(ref stringStack);

using (var intStack = StackPool<int>.GetDisposable())
{
    // do some stuff with intStack
}
```
