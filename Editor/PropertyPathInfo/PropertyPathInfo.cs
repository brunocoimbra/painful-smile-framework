﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace PainfulSmile
{
    public sealed class PropertyPathInfo
    {
        public delegate T SetValueHandler<T>(T value);

        private readonly int? Index;
        private readonly FieldInfo FieldInfo;
        private readonly PropertyPathInfo Next;

        public PropertyPathInfo(FieldInfo fieldInfo, PropertyPathInfo next, int? index = null)
        {
            FieldInfo = fieldInfo;
            Next = next;
            Index = index;
        }

        public object GetValue(Object target)
        {
            return GetValueImmediate(target);
        }

        public T GetValue<T>(Object target)
        {
            object value = GetValueImmediate(target);

            return value != null ? (T)value : default;
        }

        public void GetValues<T>(Object[] targets, List<T> append)
        {
            append.EnsureCapacity(append.Count + targets.Length);

            for (int i = 0, length = targets.Length; i < length; i++)
            {
                append.Add(GetValue<T>(targets[i]));
            }
        }

        public T[] GetValues<T>(Object[] targets)
        {
            var values = new T[targets.Length];

            for (int i = 0, length = targets.Length; i < length; i++)
            {
                values[i] = GetValue<T>(targets[i]);
            }

            return values;
        }

        public void SetValue<T>(Object target, T value)
        {
            SetValueImmediate(target, value);
        }

        public void SetValue<T>(Object target, SetValueHandler<T> onSetValue)
        {
            SetValueImmediate(target, onSetValue.Invoke(GetValue<T>(target)));
        }

        public void SetValues<T>(Object[] targets, T value)
        {
            for (int i = 0; i < targets.Length; i++)
            {
                SetValueImmediate(targets[i], value);
            }
        }

        public void SetValues<T>(Object[] targets, SetValueHandler<T> onSetValue)
        {
            for (int i = 0; i < targets.Length; i++)
            {
                SetValueImmediate(targets[i], onSetValue.Invoke(GetValue<T>(targets[i])));
            }
        }

        public object GetOwnerObject(Object target)
        {
            object value = target;

            for (PropertyPathInfo current = this; current.Next != null; current = current.Next)
            {
                if (current.Index.HasValue == false)
                {
                    value = current.FieldInfo.GetValue(value);
                }
                else
                {
                    IEnumerator enumerator = ((IEnumerable)current.FieldInfo.GetValue(value)).GetEnumerator();

                    for (int i = 0; enumerator.MoveNext(); i++)
                    {
                        if (current.Index == i)
                        {
                            value = enumerator.Current;

                            break;
                        }
                    }
                }
            }

            return value;
        }

        public T GetOwnerObject<T>(Object target)
        {
            object value = GetOwnerObject(target);

            return value != null ? (T)value : default;
        }

        public void GetOwnerObjects(Object[] targets, List<object> append)
        {
            append.EnsureCapacity(append.Count + targets.Length);

            for (int i = 0, length = targets.Length; i < length; i++)
            {
                append.Add(GetOwnerObject(targets[i]));
            }
        }

        public object[] GetOwnerObjects(Object[] targets)
        {
            var values = new object[targets.Length];

            for (int i = 0, length = targets.Length; i < length; i++)
            {
                values[i] = GetOwnerObject(targets[i]);
            }

            return values;
        }

        public void GetOwnerObjects<T>(Object[] targets, List<T> append)
        {
            append.EnsureCapacity(append.Count + targets.Length);

            for (int i = 0, length = targets.Length; i < length; i++)
            {
                append.Add(GetOwnerObject<T>(targets[i]));
            }
        }

        public T[] GetOwnerObjects<T>(Object[] targets)
        {
            var values = new T[targets.Length];

            for (int i = 0, length = targets.Length; i < length; i++)
            {
                values[i] = GetOwnerObject<T>(targets[i]);
            }

            return values;
        }

        private object GetValueImmediate(object target)
        {
            object value = null;

            if (Index.HasValue == false)
            {
                value = FieldInfo.GetValue(target);
            }
            else
            {
                IEnumerator enumerable = ((IEnumerable)FieldInfo.GetValue(target)).GetEnumerator();

                for (int i = 0; enumerable.MoveNext(); i++)
                {
                    if (Index == i)
                    {
                        value = enumerable.Current;

                        break;
                    }
                }
            }

            return Next != null ? Next.GetValueImmediate(value) : value;
        }

        private void SetValueImmediate<T>(object obj, T value)
        {
            object target = obj;
            PropertyPathInfo current = this;

            while (current.Next != null)
            {
                if (current.Index.HasValue == false)
                {
                    target = current.FieldInfo.GetValue(target);
                }
                else
                {
                    IEnumerator enumerator = ((IEnumerable)current.FieldInfo.GetValue(target)).GetEnumerator();

                    for (int i = 0; enumerator.MoveNext(); i++)
                    {
                        if (current.Index == i)
                        {
                            target = enumerator.Current;

                            break;
                        }
                    }
                }

                current = current.Next;
            }

            if (current.Index.HasValue == false)
            {
                current.FieldInfo.SetValue(target, value);
            }
            else
            {
                var array = current.FieldInfo.GetValue(target) as T[];

                if (array == null)
                {
                    return;
                }

                if (array.Length > current.Index)
                {
                    array[current.Index.Value] = value;
                }
                else
                {
                    var temp = new List<T>(array);
                    temp.Add(value);
                    array = temp.ToArray();
                }

                current.FieldInfo.SetValue(target, array);
            }
        }
    }
}
