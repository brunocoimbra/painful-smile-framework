﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;

namespace PainfulSmile
{
    /// <summary>
    /// Extensions to use with PropertyPathInfo.
    /// </summary>
    public static class PropertyPathInfoExtensions
    {
        private const BindingFlags PropertyPathInfoFlags = BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public;

        /// <summary>
        /// Set the property value.
        /// </summary>
        /// <param name="property"></param>
        /// <param name="value"></param>
        /// <param name="index"> Index of the target object. Used for multi-editing. </param>
        public static void SetValue<T>(this SerializedProperty property, T value, int index = 0)
        {
            property.GetPropertyPathInfo().SetValue(property.serializedObject.targetObjects[index], value);
        }

        /// <summary>
        /// Set the property value.
        /// </summary>
        /// <param name="property"></param>
        /// <param name="onSetValue"></param>
        /// <param name="index"> Index of the target object. Used for multi-editing. </param>
        public static void SetValue<T>(this SerializedProperty property, PropertyPathInfo.SetValueHandler<T> onSetValue, int index = 0)
        {
            if (onSetValue == null)
            {
                return;
            }

            property.GetPropertyPathInfo().SetValue(property.serializedObject.targetObjects[index], onSetValue);
        }

        /// <summary>
        /// Set the property value.
        /// </summary>
        /// <param name="property"></param>
        /// <param name="info"></param>
        /// <param name="value"></param>
        /// <param name="index"> Index of the target object. Used for multi-editing. </param>
        public static void SetValue<T>(this SerializedProperty property, PropertyPathInfo info, T value, int index = 0)
        {
            info.SetValue(property.serializedObject.targetObjects[index], value);
        }

        /// <summary>
        /// Set the property value.
        /// </summary>
        /// <param name="property"></param>
        /// <param name="info"></param>
        /// <param name="onSetValue"></param>
        /// <param name="index"> Index of the target object. Used for multi-editing. </param>
        public static void SetValue<T>(this SerializedProperty property, PropertyPathInfo info, PropertyPathInfo.SetValueHandler<T> onSetValue, int index = 0)
        {
            if (onSetValue == null)
            {
                return;
            }

            info.SetValue(property.serializedObject.targetObjects[index], onSetValue);
        }

        /// <summary>
        /// Set all properties values.
        /// </summary>
        public static void SetValues<T>(this SerializedProperty property, T value)
        {
            GetPropertyPathInfo(property).SetValues(property.serializedObject.targetObjects, value);
        }

        /// <summary>
        /// Set all properties values.
        /// </summary>
        public static void SetValues<T>(this SerializedProperty property, PropertyPathInfo.SetValueHandler<T> onSetValue)
        {
            if (onSetValue == null)
            {
                return;
            }

            GetPropertyPathInfo(property).SetValues(property.serializedObject.targetObjects, onSetValue);
        }

        /// <summary>
        /// Set all properties values.
        /// </summary>
        public static void SetValues<T>(this SerializedProperty property, PropertyPathInfo info, T value)
        {
            info.SetValues(property.serializedObject.targetObjects, value);
        }

        /// <summary>
        /// Set all properties values.
        /// </summary>
        public static void SetValues<T>(this SerializedProperty property, PropertyPathInfo info, PropertyPathInfo.SetValueHandler<T> onSetValue)
        {
            if (onSetValue == null)
            {
                return;
            }

            info.SetValues(property.serializedObject.targetObjects, onSetValue);
        }

        /// <summary>
        /// Creates a <see cref="PropertyPathInfo"/> for this property.
        /// </summary>
        public static PropertyPathInfo GetPropertyPathInfo(this SerializedProperty property)
        {
            return GetPropertyPathInfo(property, new List<string>(property.propertyPath.Split('.')));
        }

        /// <summary>
        /// Creates a <see cref="PropertyPathInfo"/> for this property.
        /// </summary>
        public static PropertyPathInfo GetPropertyPathInfo(this SerializedProperty property, List<string> propertyPath)
        {
            return GetPropertyPathInfoImmediate(property.serializedObject.targetObject.GetType(), propertyPath);
        }

        /// <summary>
        /// Get the object that contains the property.
        /// </summary>
        /// <param name="property"></param>
        /// <param name="index"> Index of the target object. Used for multi-editing. </param>
        public static object GetOwnerObject(this SerializedProperty property, int index = 0)
        {
            return property.GetPropertyPathInfo().GetOwnerObject(property.serializedObject.targetObjects[index]);
        }

        /// <summary>
        /// Get the object that contains the property.
        /// </summary>
        /// <param name="property"></param>
        /// <param name="info"></param>
        /// <param name="index"> Index of the target object. Used for multi-editing. </param>
        public static object GetOwnerObject(this SerializedProperty property, PropertyPathInfo info, int index = 0)
        {
            return info.GetOwnerObject(property.serializedObject.targetObjects[index]);
        }

        /// <summary>
        /// Get the object that contains the property.
        /// </summary>
        /// <param name="property"></param>
        /// <param name="index"> Index of the target object. Used for multi-editing. </param>
        public static T GetOwnerObject<T>(this SerializedProperty property, int index = 0)
        {
            return property.GetPropertyPathInfo().GetOwnerObject<T>(property.serializedObject.targetObjects[index]);
        }

        /// <summary>
        /// Get the object that contains the property.
        /// </summary>
        /// <param name="property"></param>
        /// <param name="info"></param>
        /// <param name="index"> Index of the target object. Used for multi-editing. </param>
        public static T GetOwnerObject<T>(this SerializedProperty property, PropertyPathInfo info, int index = 0)
        {
            return info.GetOwnerObject<T>(property.serializedObject.targetObjects[index]);
        }

        /// <summary>
        /// Get the property value.
        /// </summary>
        /// <param name="property"></param>
        /// <param name="index"> Index of the target object. Used for multi-editing. </param>
        public static object GetValue(this SerializedProperty property, int index = 0)
        {
            return property.GetPropertyPathInfo().GetValue(property.serializedObject.targetObjects[index]);
        }

        /// <summary>
        /// Get the property value.
        /// </summary>
        /// <param name="property"></param>
        /// <param name="index"> Index of the target object. Used for multi-editing. </param>
        public static T GetValue<T>(this SerializedProperty property, int index = 0)
        {
            return property.GetPropertyPathInfo().GetValue<T>(property.serializedObject.targetObjects[index]);
        }

        /// <summary>
        /// Get the property value.
        /// </summary>
        /// <param name="property"></param>
        /// <param name="info"></param>
        /// <param name="index"> Index of the target object. Used for multi-editing. </param>
        public static T GetValue<T>(this SerializedProperty property, PropertyPathInfo info, int index = 0)
        {
            return info.GetValue<T>(property.serializedObject.targetObjects[index]);
        }

        /// <summary>
        /// Get the objects that contains the property.
        /// </summary>
        public static void GetOwnerObjects(this SerializedProperty property, List<object> append)
        {
            property.GetPropertyPathInfo().GetOwnerObjects(property.serializedObject.targetObjects, append);
        }

        /// <summary>
        /// Get the objects that contains the property.
        /// </summary>
        public static void GetOwnerObjects(this SerializedProperty property, PropertyPathInfo info, List<object> append)
        {
            info.GetOwnerObjects(property.serializedObject.targetObjects, append);
        }

        /// <summary>
        /// Get the objects that contains the property.
        /// </summary>
        public static object[] GetOwnerObjects(this SerializedProperty property)
        {
            return property.GetPropertyPathInfo().GetOwnerObjects(property.serializedObject.targetObjects);
        }

        /// <summary>
        /// Get the objects that contains the property.
        /// </summary>
        public static object[] GetOwnerObjects(this SerializedProperty property, PropertyPathInfo info)
        {
            return info.GetOwnerObjects(property.serializedObject.targetObjects);
        }

        /// <summary>
        /// Get the objects that contains the property.
        /// </summary>
        public static void GetOwnerObjects<T>(this SerializedProperty property, List<T> append)
        {
            property.GetPropertyPathInfo().GetOwnerObjects(property.serializedObject.targetObjects, append);
        }

        /// <summary>
        /// Get the objects that contains the property.
        /// </summary>
        public static void GetOwnerObjects<T>(this SerializedProperty property, PropertyPathInfo info, List<T> append)
        {
            info.GetOwnerObjects(property.serializedObject.targetObjects, append);
        }

        /// <summary>
        /// Get the objects that contains the property.
        /// </summary>
        public static T[] GetOwnerObjects<T>(this SerializedProperty property)
        {
            return property.GetPropertyPathInfo().GetOwnerObjects<T>(property.serializedObject.targetObjects);
        }

        /// <summary>
        /// Get the objects that contains the property.
        /// </summary>
        public static T[] GetOwnerObjects<T>(this SerializedProperty property, PropertyPathInfo info)
        {
            return info.GetOwnerObjects<T>(property.serializedObject.targetObjects);
        }

        /// <summary>
        /// Get all properties values.
        /// </summary>
        public static void GetValues<T>(this SerializedProperty property, List<T> append)
        {
            property.GetPropertyPathInfo().GetValues(property.serializedObject.targetObjects, append);
        }

        /// <summary>
        /// Get all properties values.
        /// </summary>
        public static void GetValues<T>(this SerializedProperty property, PropertyPathInfo info, List<T> append)
        {
            info.GetValues(property.serializedObject.targetObjects, append);
        }

        /// <summary>
        /// Get all properties values.
        /// </summary>
        public static T[] GetValues<T>(this SerializedProperty property)
        {
            return property.GetPropertyPathInfo().GetValues<T>(property.serializedObject.targetObjects);
        }

        /// <summary>
        /// Get all properties values.
        /// </summary>
        public static T[] GetValues<T>(this SerializedProperty property, PropertyPathInfo info)
        {
            return info.GetValues<T>(property.serializedObject.targetObjects);
        }

        private static PropertyPathInfo GetPropertyPathInfoImmediate(System.Type rootTargetType, List<string> propertyPath)
        {
            if (propertyPath.Count == 0)
            {
                return null;
            }

            FieldInfo fieldInfo = rootTargetType.GetField(propertyPath[0], PropertyPathInfoFlags);

            while (fieldInfo == null && rootTargetType.BaseType != null)
            {
                rootTargetType = rootTargetType.BaseType;
                fieldInfo = rootTargetType.GetField(propertyPath[0], PropertyPathInfoFlags);
            }

            if (propertyPath.Count > 2 && propertyPath[1] == "Array")
            {
                var index = new string(propertyPath[2].Where(char.IsDigit).ToArray());

                if (propertyPath[2].Replace(index, "") == "data[]")
                {
                    propertyPath.RemoveRange(0, 3);

                    PropertyPathInfo nextInfo = GetPropertyPathInfoImmediate(fieldInfo.FieldType.GetCollectionType(), propertyPath);

                    return new PropertyPathInfo(fieldInfo, nextInfo, System.Convert.ToInt32(index));
                }
            }

            propertyPath.RemoveAt(0);

            return new PropertyPathInfo(fieldInfo, GetPropertyPathInfoImmediate(fieldInfo.FieldType, propertyPath));
        }

        private static System.Type GetCollectionType(this System.Type type)
        {
            System.Type value = type.GetElementType();

            if (value != null)
            {
                return value;
            }

            System.Type[] arguments = type.GetGenericArguments();

            return arguments.Length > 0 ? arguments[0] : type;
        }
    }
}
