﻿using System.Text.RegularExpressions;

namespace PainfulSmile
{
    public static class PSEditorUtility
    {
        public static string GetValidMethodName(string name, string defaultValue = "_")
        {
            name = Regex.Replace(name, "[^a-zA-Z0-9_]", "", RegexOptions.Compiled);

            while (string.IsNullOrEmpty(name) == false)
            {
                if (char.IsDigit(name[0]))
                {
                    name = name.Remove(0, 1);
                }
                else
                {
                    return name;
                }
            }

            return defaultValue;
        }

        public static string GetValidScriptName(string name, string defaultValue = "_")
        {
            if (string.IsNullOrEmpty(name))
            {
                return defaultValue;
            }

            name = name.Replace("-", "_");
            name = name.Replace("\\", "_");
            name = name.Replace("/", "_");
            name = Regex.Replace(name, "[^a-zA-Z0-9_]", "", RegexOptions.Compiled);

            if (string.IsNullOrEmpty(name))
            {
                return defaultValue;
            }

            return char.IsDigit(name[0]) ? $"_{name}" : name;
        }
    }
}
