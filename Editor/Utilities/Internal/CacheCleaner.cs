﻿using UnityEngine;
using UnityEditor;

namespace PainfulSmile
{
    internal static class CacheCleaner
    {
        [MenuItem(PSUtility.ToolsMenuItem + "Clear Cache", false, PSUtility.QuickUtilityMenuOrder + 3)]
        private static void ClearCache()
        {
            if (Caching.ClearCache())
            {
                Debug.Log("Deleted all AssetBundle and Procedural Material content that has been cached.");
            }
            else
            {
                Debug.LogWarning("Failed to delete all AssetBundle and Procedural Material content that has been cached.");
            }
        }
    }
}
