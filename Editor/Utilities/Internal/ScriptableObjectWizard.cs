﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace PainfulSmile
{
    internal sealed class ScriptableObjectWizard : ScriptableWizard
    {
        private const int MinWidth = 512;
        private const string CreateButtonText = "Create";
        private const string ErrorMessage = "No ScriptableObject class found.";
        private const string HelpMessage = "Select a ScriptableObject class to create the asset.";
        private const string ObjectLabelText = "ScriptableObject Class";
        private const string OtherButtonText = "Cancel";
        private const string WindowTitleText = "ScriptableObject Wizard";

        private static readonly string[] ExcludedNamespaces =
        {
            "AmplifyShaderEditor",
            "Cinemachine",
            "DG",
            "Facebook",
            "FlyingWormConsole3",
            "JetBrains",
            "PainfulSmile",
            "PainfulSmile.Experimental",
            "SyntaxTree",
            "TMPro",
            "TreeEditor",
            "Unity",
            "UnityEditor",
            "UnityEditorInternal",
            "UnityEngine",
            "UnityEngineInternal",
            "UnityScript",
            "Vuforia"
        };
        private static readonly HashSet<string> ExcludedAssemblies = new HashSet<string>
        {
            "Assembly-CSharp-firstpass",
            "Assembly-CSharp-Editor-firstpass",
        };

        private static bool _disablePopup;
        private static int _selectedIndex;
        private static string[] _displayedOptions;
        private static List<System.Type> _types;

        [MenuItem(PSUtility.CreateAssetMenuItem + "ScriptableObject %#w", false, 1000)]
        private static void CallMenuItem()
        {
            _types = new List<System.Type>();

            foreach (Assembly assembly in System.AppDomain.CurrentDomain.GetAssemblies())
            {
                if (ExcludedAssemblies.Contains(assembly.GetName().Name))
                {
                    continue;
                }

                foreach (var type in assembly.GetTypes())
                {
                    if (type.IsSubclassOf(typeof(ScriptableObject)) == false)
                    {
                        continue;
                    }

                    bool skip = false;

                    foreach (string excludedNamespace in ExcludedNamespaces)
                    {
                        if (type.FullName.StartsWith(excludedNamespace))
                        {
                            skip = true;

                            break;
                        }
                    }

                    if (skip)
                    {
                        continue;
                    }

                    _types.Add(type);
                }
            }

            ScriptableWizard wizard = DisplayWizard<ScriptableObjectWizard>(WindowTitleText, CreateButtonText, OtherButtonText);
            wizard.minSize = new Vector2(MinWidth, wizard.minSize.y);
            wizard.helpString = HelpMessage;

            _displayedOptions = _types.Select(type => type.FullName).ToArray();
            _disablePopup = _displayedOptions == null || _displayedOptions.Length == 0;

            if (_disablePopup)
            {
                wizard.errorString = ErrorMessage;
                _selectedIndex = 0;
                _displayedOptions = new[] { "" };
            }
        }

        private void OnWizardCreate()
        {
            ScriptableObject asset = CreateInstance(_types[_selectedIndex]);
            string path = AssetDatabase.GetAssetPath(Selection.activeObject);

            if (string.IsNullOrEmpty(path))
            {
                path = "Assets";
            }
            else if (Path.GetExtension(path) != "")
            {
                path = path.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");
            }

            path = AssetDatabase.GenerateUniqueAssetPath(string.Format("{0}/New{1}.asset", path, _displayedOptions[_selectedIndex]));

            AssetDatabase.CreateAsset(asset, path);
            EditorUtility.FocusProjectWindow();

            Selection.activeObject = asset;
        }

        private void OnWizardOtherButton()
        {
            Close();
        }

        protected override bool DrawWizardGUI()
        {
            using (var state = new EditorGUI.ChangeCheckScope())
            {
                using (new EditorGUI.DisabledScope(_disablePopup))
                {
                    _selectedIndex = EditorGUILayout.Popup(ObjectLabelText, _selectedIndex, _displayedOptions);
                }

                return state.changed;
            }
        }
    }
}
