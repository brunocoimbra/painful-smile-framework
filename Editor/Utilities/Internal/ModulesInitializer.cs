#if !UNITY_2019_1_OR_NEWER
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEditor;
using UnityEditor.Build;
using UnityEngine;

namespace PainfulSmile
{
    [InitializeOnLoad, System.Serializable]
    internal sealed class ModulesInitializer
    {
        public sealed class BuildTargetChangedListener : IActiveBuildTargetChanged
        {
            int IOrderedCallback.callbackOrder => 0;

            void IActiveBuildTargetChanged.OnActiveBuildTargetChanged(BuildTarget previousTarget, BuildTarget newTarget)
            {
                Refresh(BuildPipeline.GetBuildTargetGroup(newTarget));
            }
        }

        private const string UnityAnimationSymbol = "UNITY_ANIMATION";
        private const string UnityParticleSystemSymbol = "UNITY_PARTICLESYSTEM";
        private const string UnityPhysicsSymbol = "UNITY_PHYSICS";
        private const string UnityPhysics2DSymbol = "UNITY_PHYSICS2D";
        private const string UnityTerrainSymbol = "UNITY_TERRAIN";
        private const string UnityTerrainPhysicsSymbol = "UNITY_TERRAINPHYSICS";
        private const string UnityVehiclesSymbol = "UNITY_VEHICLES";
        private const string UnityTextMeshProSymbol = "UNITY_TEXTMESHPRO";

        private static readonly char[] DefineSymbolsSeparator = { ';' };
        private static readonly char[] DependenciesSeparator = { ':', ',' };
        private static readonly HashSet<string> AllSymbolsHash = new HashSet<string>
        {
            UnityAnimationSymbol,
            UnityParticleSystemSymbol,
            UnityPhysicsSymbol,
            UnityPhysics2DSymbol,
            UnityTerrainSymbol,
            UnityTerrainPhysicsSymbol,
            UnityVehiclesSymbol,
            UnityTextMeshProSymbol,
        };
        private static readonly Dictionary<string, string> UnitySymbols = new Dictionary<string, string>
        {
            ["com.unity.modules.animation"] = UnityAnimationSymbol,
            ["com.unity.modules.particlesystem"] = UnityParticleSystemSymbol,
            ["com.unity.modules.physics"] = UnityPhysicsSymbol,
            ["com.unity.modules.physics2d"] = UnityPhysics2DSymbol,
            ["com.unity.modules.terrain"] = UnityTerrainSymbol,
            ["com.unity.modules.terrainphysics"] = UnityTerrainPhysicsSymbol,
            ["com.unity.modules.vehicles"] = UnityVehiclesSymbol,
            ["com.unity.textmeshpro"] = UnityTextMeshProSymbol,
        };

#pragma warning disable 0649
        [SerializeField] private BuildTargetGroup m_LastBuildTargetGroup;
        [SerializeField] private string m_LastModificationTime;
        [SerializeField] private string m_LastDefineSymbols;
#pragma warning restore 0649

        private static readonly string FilePath = $"Library/{nameof(ModulesInitializer)}.json";

        private string LastDefineSymbols
        {
            get => m_LastDefineSymbols;
            set => m_LastDefineSymbols = value;
        }
        private string LastModificationTime
        {
            get => m_LastModificationTime;
            set => m_LastModificationTime = value;
        }
        private BuildTargetGroup LastBuildTargetGroup
        {
            get => m_LastBuildTargetGroup;
            set => m_LastBuildTargetGroup = value;
        }

        static ModulesInitializer()
        {
            EditorApplication.projectChanged -= Initialize;
            EditorApplication.projectChanged += Initialize;
        }

        [InitializeOnLoadMethod]
        private static void Initialize()
        {
            Refresh(EditorUserBuildSettings.selectedBuildTargetGroup);
        }

        private static void Refresh(BuildTargetGroup buildTargetGroup)
        {
            var json = new ModulesInitializer();
            string fullFilePath = Path.GetFullPath(FilePath);

            if (File.Exists(fullFilePath))
            {
                EditorJsonUtility.FromJsonOverwrite(File.ReadAllText(fullFilePath), json);
            }

            string defineSymbols = PlayerSettings.GetScriptingDefineSymbolsForGroup(buildTargetGroup);
            string manifestPath = Path.GetFullPath("Packages/manifest.json");
            string modificationTime = File.GetLastWriteTime(manifestPath).ToString();

            if (json.LastBuildTargetGroup == buildTargetGroup && json.LastModificationTime == modificationTime && json.LastDefineSymbols == defineSymbols)
            {
                return;
            }

            string manifestContent = File.ReadAllText(manifestPath).ToLowerInvariant();
            int startIndex = manifestContent.IndexOf("\"dependencies\"");
            startIndex = 1 + manifestContent.IndexOf("{", startIndex);
            int endIndex = manifestContent.IndexOf("}", startIndex);
            var builder = new StringBuilder(manifestContent.Substring(startIndex, endIndex - startIndex));
            builder.Replace("\r\n", string.Empty);
            builder.Replace("\n", string.Empty);
            builder.Replace("\"", string.Empty);
            builder.Replace(" ", string.Empty);
            string[] dependencies = builder.ToString().Split(DependenciesSeparator, System.StringSplitOptions.RemoveEmptyEntries);
            string[] defineSymbolsArray = defineSymbols.Split(DefineSymbolsSeparator, System.StringSplitOptions.RemoveEmptyEntries);
            builder.Clear();

            for (int i = 0; i < defineSymbolsArray.Length; i++)
            {
                if (AllSymbolsHash.Contains(defineSymbolsArray[i]))
                {
                    continue;
                }

                if (builder.Length > 0)
                {
                    builder.Append($";");
                }

                builder.Append($"{defineSymbolsArray[i]}");
            }

            for (int i = 0; i < dependencies.Length; i += 2)
            {
                if (UnitySymbols.TryGetValue(dependencies[i], out string symbol))
                {
                    if (builder.Length > 0)
                    {
                        builder.Append($";");
                    }

                    builder.Append($"{symbol}");
                }
            }

            string newDefineSymbols = builder.ToString();

            if (defineSymbols != newDefineSymbols)
            {
                json.LastDefineSymbols = newDefineSymbols;

                PlayerSettings.SetScriptingDefineSymbolsForGroup(buildTargetGroup, newDefineSymbols);
            }

            json.LastBuildTargetGroup = buildTargetGroup;
            json.LastModificationTime = modificationTime;

            File.WriteAllText(fullFilePath, EditorJsonUtility.ToJson(json, true));
        }
    }
}
#endif
