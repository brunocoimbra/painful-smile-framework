﻿using UnityEngine;
using UnityEditor;

namespace PainfulSmile
{
    /// <summary>
    /// Utilities to use with CustomEditors.
    /// </summary>
    public static class PSEditorGUIUtility
    {
        /// <summary>
        /// Margin to be used when using the <see cref="ListHeaderStyle"/>.
        /// </summary>
        public const float ListHeaderStyleMargin = 6;
        /// <summary>
        /// The default foldout width;
        /// </summary>
        public const float FoldoutWidth = 10;

        /// <summary>
        /// The standard list body style (RL Background).
        /// </summary>
        public static GUIStyle ListBodyStyle { get; } = new GUIStyle("RL Background") { border = new RectOffset(6, 3, 3, 6) };
        /// <summary>
        /// The standard list header style (RL Header).
        /// </summary>
        public static GUIStyle ListHeaderStyle { get; } = new GUIStyle("RL Header");

        /// <summary>
        /// Draw a property field with the desired property drawer.
        /// </summary>
        public static void PropertyField(SerializedProperty property, PropertyDrawer drawer)
        {
            var label = new GUIContent(property.displayName, property.tooltip);
            float height = drawer.GetPropertyHeight(property, label);
            Rect position = EditorGUILayout.GetControlRect(true, height);
            drawer.OnGUI(position, property, label);
        }
    }
}
