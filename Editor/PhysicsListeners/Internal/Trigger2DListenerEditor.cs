﻿#if UNITY_PHYSICS2D
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace PainfulSmile
{
    [CustomEditor(typeof(Trigger2DListener)), CanEditMultipleObjects]
    internal sealed class Trigger2DListenerEditor : PhysicsListenerEditor
    {
        private static readonly List<Collider2D> Colliders = new List<Collider2D>();

        private bool HasAnyTriggerColliderAttached
        {
            get
            {
                Colliders.Clear();

                (serializedObject.targetObject as MonoBehaviour).GetComponents(Colliders);

                for (int i = 0; i < Colliders.Count; i++)
                {
                    if (Colliders[i].isTrigger)
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        protected override void DrawWarningBox()
        {
            if (HasAnyTriggerColliderAttached == false)
            {
                EditorGUILayout.HelpBox("Could not find any trigger Collider2D attached to this object!", MessageType.Warning, true);
            }
        }
    }
}
#endif
