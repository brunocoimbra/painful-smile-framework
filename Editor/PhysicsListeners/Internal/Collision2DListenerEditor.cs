﻿#if UNITY_PHYSICS2D
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace PainfulSmile
{
    [CustomEditor(typeof(Collision2DListener)), CanEditMultipleObjects]
    internal sealed class Collision2DListenerEditor : PhysicsListenerEditor
    {
        private static readonly List<Collider2D> Colliders = new List<Collider2D>();

        private bool HasAnyNonTriggerColliderAttached
        {
            get
            {
                Colliders.Clear();

                (serializedObject.targetObject as MonoBehaviour).GetComponents(Colliders);

                for (int i = 0; i < Colliders.Count; i++)
                {
                    if (Colliders[i].isTrigger == false)
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        protected override void DrawWarningBox()
        {
            if (HasAnyNonTriggerColliderAttached == false)
            {
                EditorGUILayout.HelpBox("Could not find any non-trigger Collider2D attached to this object!", MessageType.Warning, true);
            }
        }
    }
}
#endif
