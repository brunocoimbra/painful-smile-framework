﻿using UnityEditor;

namespace PainfulSmile
{
    [CustomEditor(typeof(PhysicsCallbackBase), true)]
    internal sealed class PhysicsCallbackEditor : Editor
    {
        public override void OnInspectorGUI() { }
    }
}
