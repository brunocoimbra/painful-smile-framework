﻿using UnityEditor;
using UnityEditor.AnimatedValues;
using UnityEngine;

namespace PainfulSmile
{
    internal abstract class PhysicsListenerEditor : Editor
    {
        private static class SerializedFields
        {
            public const string GizmosMode = "_gizmosMode";
            public const string GizmosColor = "_gizmosColor";
            public const string Callbacks = "m_Callbacks";
            public const string OnEnter = "m_OnEnter";
            public const string OnExit = "m_OnExit";
            public const string OnStay = "m_OnStay";
        }

        private static readonly GUILayoutOption GizmosColorOption = GUILayout.MaxWidth(50);

        private AnimFloat _enterCallbackAnimFloat;
        private AnimFloat _exitCallbackAnimFloat;
        private AnimFloat _stayCallbackAnimFloat;
        private SerializedProperty _callbacks;
        private SerializedProperty _gizmosMode;
        private SerializedProperty _gizmosColor;
        private SerializedProperty _onEnter;
        private SerializedProperty _onExit;
        private SerializedProperty _onStay;

        private float TargetEnterCallbackAnimFloatValue => HasFlags(PhysicsCallbacks.Enter) ? 1 : 0;
        private float TargetExitCallbackAnimFloatValue => HasFlags(PhysicsCallbacks.Exit) ? 1 : 0;
        private float TargetStayCallbackAnimFloatValue => HasFlags(PhysicsCallbacks.Stay) ? 1 : 0;

        private void OnEnable()
        {
            _callbacks = serializedObject.FindProperty(SerializedFields.Callbacks);
            _gizmosMode = serializedObject.FindProperty(SerializedFields.GizmosMode);
            _gizmosColor = serializedObject.FindProperty(SerializedFields.GizmosColor);
            _onEnter = serializedObject.FindProperty(SerializedFields.OnEnter);
            _onExit = serializedObject.FindProperty(SerializedFields.OnExit);
            _onStay = serializedObject.FindProperty(SerializedFields.OnStay);
            _enterCallbackAnimFloat = new AnimFloat(TargetEnterCallbackAnimFloatValue, Repaint);
            _exitCallbackAnimFloat = new AnimFloat(TargetExitCallbackAnimFloatValue, Repaint);
            _stayCallbackAnimFloat = new AnimFloat(TargetStayCallbackAnimFloatValue, Repaint);
        }

        public sealed override void OnInspectorGUI()
        {
            serializedObject.Update();

            using (var changeCheckScope = new EditorGUI.ChangeCheckScope())
            {
                if (serializedObject.isEditingMultipleObjects == false)
                {
                    DrawWarningBox();
                }

                using (new EditorGUILayout.HorizontalScope())
                {
                    EditorGUILayout.PropertyField(_gizmosMode, new GUIContent("Gizmos"));
                    EditorGUILayout.PropertyField(_gizmosColor, GUIContent.none, GizmosColorOption);
                }

                using (new EditorGUI.DisabledScope(EditorApplication.isPlaying))
                {
                    EditorGUILayout.PropertyField(_callbacks);
                }

                if (_callbacks.hasMultipleDifferentValues == false)
                {
                    _enterCallbackAnimFloat.target = TargetEnterCallbackAnimFloatValue;
                    _exitCallbackAnimFloat.target = TargetExitCallbackAnimFloatValue;
                    _stayCallbackAnimFloat.target = TargetStayCallbackAnimFloatValue;

                    using (var fadeGroupScope = new EditorGUILayout.FadeGroupScope(_enterCallbackAnimFloat.value))
                    {
                        if (fadeGroupScope.visible)
                        {
                            EditorGUILayout.PropertyField(_onEnter);
                        }
                    }

                    using (var fadeGroupScope = new EditorGUILayout.FadeGroupScope(_exitCallbackAnimFloat.value))
                    {
                        if (fadeGroupScope.visible)
                        {
                            EditorGUILayout.PropertyField(_onExit);
                        }
                    }

                    using (var fadeGroupScope = new EditorGUILayout.FadeGroupScope(_stayCallbackAnimFloat.value))
                    {
                        if (fadeGroupScope.visible)
                        {
                            EditorGUILayout.PropertyField(_onStay);
                        }
                    }
                }

                if (changeCheckScope.changed)
                {
                    serializedObject.ApplyModifiedProperties();
                }
            }
        }

        protected abstract void DrawWarningBox();

        private bool HasFlags(PhysicsCallbacks callbacks)
        {
            return ((PhysicsCallbacks)_callbacks.intValue & callbacks) == callbacks;
        }
    }
}
