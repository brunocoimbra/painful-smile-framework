﻿#if UNITY_PHYSICS
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace PainfulSmile
{
    [CustomEditor(typeof(TriggerListener)), CanEditMultipleObjects]
    internal sealed class TriggerListenerEditor : PhysicsListenerEditor
    {
        private static readonly List<Collider> Colliders = new List<Collider>();

        private bool HasAnyTriggerColliderAttached
        {
            get
            {
                Colliders.Clear();

                (serializedObject.targetObject as MonoBehaviour).GetComponents(Colliders);

                for (int i = 0; i < Colliders.Count; i++)
                {
                    if (Colliders[i].isTrigger)
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        protected override void DrawWarningBox()
        {
            if (HasAnyTriggerColliderAttached == false)
            {
                EditorGUILayout.HelpBox("Could not find any trigger Collider attached to this object!", MessageType.Warning, true);
            }
        }
    }
}
#endif
