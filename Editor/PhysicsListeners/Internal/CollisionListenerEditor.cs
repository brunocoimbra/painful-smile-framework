﻿#if UNITY_PHYSICS
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace PainfulSmile
{
    [CustomEditor(typeof(CollisionListener)), CanEditMultipleObjects]
    internal sealed class CollisionListenerEditor : PhysicsListenerEditor
    {
        private static readonly List<Collider> Colliders = new List<Collider>();

        private bool HasAnyNonTriggerColliderAttached
        {
            get
            {
                Colliders.Clear();

                (serializedObject.targetObject as MonoBehaviour).GetComponents(Colliders);

                for (int i = 0; i < Colliders.Count; i++)
                {
                    if (Colliders[i].isTrigger == false)
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        private bool HasRigidbodyOnParent
        {
            get
            {
                var behaviour = serializedObject.targetObject as MonoBehaviour;
                var rigidbody = behaviour.GetComponentInParent<Rigidbody>();

                return rigidbody != null && rigidbody.gameObject != behaviour.gameObject;

            }
        }

        protected override void DrawWarningBox()
        {
            if (HasRigidbodyOnParent)
            {
                EditorGUILayout.HelpBox("This object is parented to a Rigidbody and will not receive any callback!", MessageType.Warning, true);
            }

            if (HasAnyNonTriggerColliderAttached == false)
            {
                EditorGUILayout.HelpBox("Could not find any non-trigger Collider attached to this object!", MessageType.Warning, true);
            }
        }
    }
}
#endif
