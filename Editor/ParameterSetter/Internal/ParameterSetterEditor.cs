﻿#if UNITY_ANIMATION
using UnityEditor;
using UnityEditor.AnimatedValues;

namespace PainfulSmile
{
    [CustomEditor(typeof(ParameterSetter))]
    internal sealed class ParameterSetterEditor : Editor
    {
        private static class SerializedFields
        {
            public const string Callbacks = "_callbacks";
            public const string ParametersOnStateEnter = "_parametersOnStateEnter";
            public const string ParametersOnStateExit = "_parametersOnStateExit";
            public const string ParametersOnStateMachineEnter = "_parametersOnStateMachineEnter";
            public const string ParametersOnStateMachineExit = "_parametersOnStateMachineExit";
        }

        private AnimFloat _stateEnterAnimFloat;
        private AnimFloat _stateExitAnimFloat;
        private AnimFloat _stateMachineEnterAnimFloat;
        private AnimFloat _stateMachineExitAnimFloat;
        private ReorderableList _parametersOnStateEnter;
        private ReorderableList _parametersOnStateExit;
        private ReorderableList _parametersOnStateMachineEnter;
        private ReorderableList _parametersOnStateMachineExit;
        private SerializedProperty _callbacks;

        private float TargetStateEnterAnimFloat => HasFlags(ParameterSetter.Callbacks.StateEnter) ? 1 : 0;
        private float TargetStateExitAnimFloat => HasFlags(ParameterSetter.Callbacks.StateExit) ? 1 : 0;
        private float TargetStateMachineEnterAnimFloat => HasFlags(ParameterSetter.Callbacks.StateMachineEnter) ? 1 : 0;
        private float TargetStateMachineExitAnimFloat => HasFlags(ParameterSetter.Callbacks.StateMachineExit) ? 1 : 0;

        private void OnEnable()
        {
            if (target == null)
            {
                return;
            }

            _callbacks = serializedObject.FindProperty(SerializedFields.Callbacks);

            _parametersOnStateEnter = new ReorderableList(serializedObject.FindProperty(SerializedFields.ParametersOnStateEnter), ReorderableElementDisplay.NonExpandable);
            _parametersOnStateExit = new ReorderableList(serializedObject.FindProperty(SerializedFields.ParametersOnStateExit), ReorderableElementDisplay.NonExpandable);
            _parametersOnStateMachineEnter = new ReorderableList(serializedObject.FindProperty(SerializedFields.ParametersOnStateMachineEnter), ReorderableElementDisplay.NonExpandable);
            _parametersOnStateMachineExit = new ReorderableList(serializedObject.FindProperty(SerializedFields.ParametersOnStateMachineExit), ReorderableElementDisplay.NonExpandable);

            _stateEnterAnimFloat = new AnimFloat(TargetStateEnterAnimFloat, Repaint);
            _stateExitAnimFloat = new AnimFloat(TargetStateExitAnimFloat, Repaint);
            _stateMachineEnterAnimFloat = new AnimFloat(TargetStateMachineEnterAnimFloat, Repaint);
            _stateMachineExitAnimFloat = new AnimFloat(TargetStateMachineExitAnimFloat, Repaint);
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            using (var changeCheckScope = new EditorGUI.ChangeCheckScope())
            {
                EditorGUILayout.PropertyField(_callbacks);

                _stateEnterAnimFloat.target = TargetStateEnterAnimFloat;
                _stateExitAnimFloat.target = TargetStateExitAnimFloat;
                _stateMachineEnterAnimFloat.target = TargetStateMachineEnterAnimFloat;
                _stateMachineExitAnimFloat.target = TargetStateMachineExitAnimFloat;

                using (var fadeGroupScope = new EditorGUILayout.FadeGroupScope(_stateEnterAnimFloat.value))
                {
                    if (fadeGroupScope.visible)
                    {
                        _parametersOnStateEnter.DrawGUILayout();
                    }
                }

                using (var fadeGroupScope = new EditorGUILayout.FadeGroupScope(_stateExitAnimFloat.value))
                {
                    if (fadeGroupScope.visible)
                    {
                        _parametersOnStateExit.DrawGUILayout();
                    }
                }

                using (var fadeGroupScope = new EditorGUILayout.FadeGroupScope(_stateMachineEnterAnimFloat.value))
                {
                    if (fadeGroupScope.visible)
                    {
                        _parametersOnStateMachineEnter.DrawGUILayout();
                    }
                }

                using (var fadeGroupScope = new EditorGUILayout.FadeGroupScope(_stateMachineExitAnimFloat.value))
                {
                    if (fadeGroupScope.visible)
                    {
                        _parametersOnStateMachineExit.DrawGUILayout();
                    }
                }

                if (changeCheckScope.changed)
                {
                    serializedObject.ApplyModifiedProperties();
                }
            }
        }

        private bool HasFlags(ParameterSetter.Callbacks callbacks)
        {
            return ((ParameterSetter.Callbacks)_callbacks.intValue & callbacks) == callbacks;
        }
    }
}
#endif
