﻿#if UNITY_ANIMATION
using UnityEditor;
using UnityEngine;

namespace PainfulSmile
{
    [CustomPropertyDrawer(typeof(ParameterSetter.AnimatorControllerParameter))]
    internal sealed class AnimatorControllerParameterDrawer : PropertyDrawer
    {
        private static class SerializedFields
        {
            public const string Type = "Type";
            public const string Name = "Name";
            public const string BoolValue = "BoolValue";
            public const string IntValue = "IntValue";
            public const string FloatValue = "FloatValue";
        }

        private const float HorizontalSpace = 5;
        private const float TypeRectWidth = 60;
        private const float ValueRectMaxWidth = 50;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            position.height = EditorGUIUtility.singleLineHeight;

            SerializedProperty type = property.FindPropertyRelative(SerializedFields.Type);
            SerializedProperty name = property.FindPropertyRelative(SerializedFields.Name);

            if (type.intValue == 0)
            {
                type.intValue = (int)AnimatorControllerParameterType.Bool;
            }

            Rect typeRect = position;
            typeRect.width = TypeRectWidth;

            Rect valueRect = position;
            valueRect.x += position.width - ValueRectMaxWidth;

            Rect nameRect = position;
            nameRect.x += TypeRectWidth + HorizontalSpace;
            nameRect.width -= TypeRectWidth + HorizontalSpace + HorizontalSpace + ValueRectMaxWidth;

            switch ((AnimatorControllerParameterType)type.intValue)
            {
                case AnimatorControllerParameterType.Float:
                case AnimatorControllerParameterType.Int:
                {
                    valueRect.width = 50;

                    break;
                }

                case AnimatorControllerParameterType.Bool:
                case AnimatorControllerParameterType.Trigger:
                {
                    valueRect.width = 16;

                    break;
                }
            }

            EditorGUI.PropertyField(typeRect, type, GUIContent.none);
            EditorGUI.PropertyField(nameRect, name, GUIContent.none);

            using (var changeCheckScope = new EditorGUI.ChangeCheckScope())
            {
                switch ((AnimatorControllerParameterType)type.intValue)
                {
                    case AnimatorControllerParameterType.Float:
                    {
                        EditorGUI.PropertyField(valueRect, property.FindPropertyRelative(SerializedFields.FloatValue), GUIContent.none);

                        break;
                    }

                    case AnimatorControllerParameterType.Int:
                    {
                        EditorGUI.PropertyField(valueRect, property.FindPropertyRelative(SerializedFields.IntValue), GUIContent.none);

                        break;
                    }

                    case AnimatorControllerParameterType.Bool:
                    {
                        EditorGUI.PropertyField(valueRect, property.FindPropertyRelative(SerializedFields.BoolValue), GUIContent.none);

                        break;
                    }

                    case AnimatorControllerParameterType.Trigger:
                    {
                        SerializedProperty field = property.FindPropertyRelative(SerializedFields.BoolValue);
                        bool value = EditorGUI.Toggle(valueRect, field.boolValue, EditorStyles.radioButton);

                        if (changeCheckScope.changed)
                        {
                            field.boolValue = value;
                        }

                        break;
                    }
                }
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUIUtility.singleLineHeight;
        }
    }
}
#endif
