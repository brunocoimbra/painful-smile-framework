﻿using UnityEngine;

namespace PainfulSmile
{
    internal sealed class ConstantsGenerator : EditorScriptableSingleton<ConstantsGenerator>
    {
        private enum LineEndings
        {
            System,
            Windows,
            Unix
        }

        [SerializeField] private string m_Folder = "";
        [SerializeField] private string m_Namespace = "";
        [SerializeField] private LineEndings m_LineEndings = LineEndings.System;
        [SerializeField] private string[] m_ResourcesIgnoredFolders = { "PainfulSmile" };

        public static ConstantsGenerator Instance => GetInstance(true);

        public string Folder => m_Folder;
        public string Namespace => m_Namespace;
        public string NewLine
        {
            get
            {
                switch (m_LineEndings)
                {
                    case LineEndings.Windows:
                        return "\r\n";

                    case LineEndings.Unix:
                        return "\n";

                    default:
                        return System.Environment.NewLine;
                }
            }
        }
        public string[] ResourcesIgnoredFolders => m_ResourcesIgnoredFolders;

        protected override string DefaultAssetPath => $"{PSUtility.DataPath}{GetType().Name}.asset";

        protected override void OnInitialize() { }
    }
}
