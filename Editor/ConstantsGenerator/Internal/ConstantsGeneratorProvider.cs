﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEditor;
using UnityEngine;
#if UNITY_2019_1_OR_NEWER
using UnityEngine.UIElements;
#else
using UnityEngine.Experimental.UIElements;
#endif

namespace PainfulSmile
{
    internal sealed class ConstantsGeneratorProvider : SettingsProvider
    {
        private struct SerializedFields
        {
            public const string Folder = "m_Folder";
            public const string Namespace = "m_Namespace";
            public const string LineEndings = "m_LineEndings";
            public const string ResourcesIgnoredFolders = "m_ResourcesIgnoredFolders";
        }

        private static class ProgressBar
        {
            public const string Title = "Generating Constants...";
            public const string Info = "It may take a few seconds.";
        }

        private static class Tags
        {
            public const string Switch = "#SWITCH#";
            public const string Enum = "#ENUM#";
            public const string Namespace = "#NAMESPACE#";
            public const string NamespaceTab = "#NAMESPACETAB#";
            public const string Newline = "#NEWLINE#";
            public const string Type = "#TYPE#";
        }

        private static class Templates
        {
            public const string EnumType = Tags.Type + "Type";
            public const string EnumTabs = Tags.NamespaceTab + Tab;
            public const string SwitchTabs = Tags.NamespaceTab + Tab + Tab + Tab;
            // @formatter:off
            public const string Default = "// This file is auto-generated!" + Tags.Newline + Tags.Newline
                                        + Tags.NamespaceTab + "/// <summary>" + Tags.Newline
                                        + Tags.NamespaceTab + "/// Use ToValue() to get the actual string value." + Tags.Newline
                                        + Tags.NamespaceTab + "/// </summary>" + Tags.Newline
                                        + Tags.NamespaceTab + "public enum " + EnumType + Tags.Newline
                                        + Tags.NamespaceTab + "{" + Tags.Enum + Tags.Newline
                                        + Tags.NamespaceTab + "}" + Tags.Newline + Tags.Newline
                                        + Tags.NamespaceTab + "public static class " + EnumType + "Extensions" + Tags.Newline
                                        + Tags.NamespaceTab + "{" + Tags.Newline
                                        + Tags.NamespaceTab + Tab + "/// <summary>" + Tags.Newline
                                        + Tags.NamespaceTab + Tab + "/// Get the actual string value for the " + Tags.Type + "." + Tags.Newline
                                        + Tags.NamespaceTab + Tab + "/// </summary>" + Tags.Newline
                                        + Tags.NamespaceTab + Tab + "public static string GetValue(this " + EnumType + " enumValue)" + Tags.Newline
                                        + Tags.NamespaceTab + Tab + "{" + Tags.Newline
                                        + Tags.NamespaceTab + Tab + Tab + "switch (enumValue)" + Tags.Newline
                                        + Tags.NamespaceTab + Tab + Tab + "{" + Tags.Switch + Tags.Newline
                                        + Tags.NamespaceTab + Tab + Tab + Tab + "default: throw new System.ArgumentOutOfRangeException();" + Tags.Newline
                                        + Tags.NamespaceTab + Tab + Tab + "}" + Tags.Newline
                                        + Tags.NamespaceTab + Tab + "}" + Tags.Newline
                                        + Tags.NamespaceTab + "}" + Tags.Newline;
            public const string WithNamespace = "namespace " + Tags.Namespace + Tags.Newline
                                              + "{" + Tags.Newline
                                              + Default
                                              + "}" + Tags.Newline;
            // @formatter:on
        }

        private static class Types
        {
            public const string Input = "Input";
            public const string Layer = "Layer";
            public const string Resource = "Resource";
            public const string Scene = "Scene";
            public const string SortingLayer = "SortingLayer";
            public const string Tag = "Tag";
        }

        private const string FileFormat = "{0}Type.cs";
        private const string Tab = "    ";

        private static readonly GUIContent ResetButtonLabel = new GUIContent("Reset");
        private static readonly GUIContent SelectButtonLabel = new GUIContent("Select");
        private static readonly StringBuilder EnumBuilder = new StringBuilder();
        private static readonly StringBuilder SwitchBuilder = new StringBuilder();
        private static readonly string[] GeneratedFiles =
        {
            string.Format(FileFormat, Types.Input),
            string.Format(FileFormat, Types.Layer),
            string.Format(FileFormat, Types.Resource),
            string.Format(FileFormat, Types.Scene),
            string.Format(FileFormat, Types.SortingLayer),
            string.Format(FileFormat, Types.Tag)
        };
        private static readonly string[] ResourcesSeparator = { "/Resources/" };
        private static readonly GUIContent[] FieldsNames =
        {
            new GUIContent(Types.Input + "s"),
            new GUIContent(Types.Layer + "s"),
            new GUIContent(Types.Resource + "s"),
            new GUIContent(Types.Scene + "s"),
            new GUIContent(Types.SortingLayer + "s"),
            new GUIContent(Types.Tag + "s")
        };
        private static readonly System.Action<bool>[] Actions =
        {
            GenerateInputsFile,
            GenerateLayersFile,
            GenerateResourcesFile,
            GenerateScenesFile,
            GenerateSortingLayersFile,
            GenerateTagsFile
        };

        private ReorderableList _resourcesIgnoredFolders;
        private SerializedObject _serializedObject;
        private SerializedProperty _folder;
        private SerializedProperty _namespace;
        private SerializedProperty _lineEndings;

        public ConstantsGeneratorProvider(string path, SettingsScope scope = SettingsScope.Project)
            : base(path, scope) { }

        [SettingsProvider]
        public static SettingsProvider CreateConstantsGeneratorProvider()
        {
            return new ConstantsGeneratorProvider("Project/Constants")
            {
                keywords = GetSearchKeywordsFromGUIContentProperties<SerializedFields>()
            };
        }

        [MenuItem(PSUtility.ToolsMenuItem + "Generate Constants %g", false, PSUtility.MappedUtilityMenuOrder + 3)]
        private static void GenerateAllFiles()
        {
            if (EditorApplication.isPlaying)
            {
                return;
            }

            try
            {
                string fileFolder = GetFileFolder(true);

                if (Directory.Exists(fileFolder) == false)
                {
                    Directory.CreateDirectory(fileFolder);
                }

                EditorUtility.DisplayCancelableProgressBar(ProgressBar.Title, ProgressBar.Info, 0f);

                for (int i = 0; i < Actions.Length; i++)
                {
                    if (EditorUtility.DisplayCancelableProgressBar(ProgressBar.Title, ProgressBar.Info, Mathf.InverseLerp(0, Actions.Length, i + 1)))
                    {
                        break;
                    }

                    Actions[i].Invoke(false);
                }

                AssetDatabase.Refresh();
                EditorUtility.ClearProgressBar();
            }
            catch (System.Exception exception)
            {
                Debug.LogException(exception);
                EditorUtility.ClearProgressBar();
            }
        }

        public override void OnActivate(string searchContext, VisualElement rootElement)
        {
            _serializedObject = new SerializedObject(ConstantsGenerator.Instance);
            _resourcesIgnoredFolders = new ReorderableList(_serializedObject.FindProperty(SerializedFields.ResourcesIgnoredFolders), "@Folder");
            _resourcesIgnoredFolders.OnDrawElement += HandleResourcesIgnoredFoldersDrawElement;
            _folder = _serializedObject.FindProperty(SerializedFields.Folder);
            _namespace = _serializedObject.FindProperty(SerializedFields.Namespace);
            _lineEndings = _serializedObject.FindProperty(SerializedFields.LineEndings);
        }

        public override void OnGUI(string searchContext)
        {
            _serializedObject.Update();

            using (new HierarchyModeScope(true))
            using (new EditorGUI.DisabledScope(EditorApplication.isPlaying))
            {
                using (var scope = new EditorGUI.ChangeCheckScope())
                {
                    Rect position = EditorGUILayout.GetControlRect(true, EditorGUIUtility.singleLineHeight);
                    float resetButtonWidth = EditorStyles.miniButtonLeft.CalcSize(ResetButtonLabel).x;
                    float selectButtonWidth = EditorStyles.miniButtonRight.CalcSize(SelectButtonLabel).x;
                    position.width -= resetButtonWidth + selectButtonWidth + 2;

                    string oldFileFolder = GetFileFolder(false);

                    using (new EditorGUI.DisabledScope(true))
                    {
                        EditorGUI.TextField(position, _folder.displayName, oldFileFolder);
                    }

                    position.y -= 2;
                    position.x += position.width + 2;
                    position.width = selectButtonWidth;

                    if (GUI.Button(position, "Select", EditorStyles.miniButtonLeft))
                    {
                        GUI.FocusControl("");

                        string newValue = EditorUtility.OpenFolderPanel("Scenes Folder", oldFileFolder, "");

                        if (string.IsNullOrEmpty(newValue) == false)
                        {
                            if (newValue.StartsWith(Application.dataPath))
                            {
                                newValue = newValue.Remove(0, Application.dataPath.Length - "Assets".Length);
                            }
                            else
                            {
                                newValue = "";
                            }

                            ChangeFileFolder(oldFileFolder, newValue);
                        }
                    }

                    position.x += position.width;
                    position.width = resetButtonWidth;

                    if (GUI.Button(position, "Reset", EditorStyles.miniButtonRight))
                    {
                        ChangeFileFolder(oldFileFolder, null);
                    }

                    using (var namespaceScope = new EditorGUI.ChangeCheckScope())
                    {
                        EditorGUILayout.PropertyField(_namespace);

                        if (namespaceScope.changed)
                        {
                            _namespace.stringValue = PSEditorUtility.GetValidScriptName(_namespace.stringValue, "");
                        }
                    }

                    EditorGUILayout.PropertyField(_lineEndings);
                    _resourcesIgnoredFolders.DrawGUILayout();

                    if (scope.changed)
                    {
                        _serializedObject.ApplyModifiedProperties();
                    }
                }

                EditorGUILayout.Separator();

                float buttonWidth = GUI.skin.button.CalcSize(new GUIContent("Generate")).x;
                string fileFolder = GetFileFolder(false);

                for (int i = 0; i < GeneratedFiles.Length; i++)
                {
                    Rect position = EditorGUILayout.GetControlRect();
                    position = EditorGUI.PrefixLabel(position, FieldsNames[i]);
                    position.width -= buttonWidth - 16;

                    using (new EditorGUI.DisabledScope(true))
                    {
                        string scriptPath = Path.Combine(fileFolder, GeneratedFiles[i]);
                        var script = AssetDatabase.LoadAssetAtPath<MonoScript>(scriptPath);

                        if (script != null)
                        {
                            EditorGUI.ObjectField(position, script, typeof(MonoScript), false);
                        }
                    }

                    position.x += position.width - 16;
                    position.width = buttonWidth;

                    if (GUI.Button(position, "Generate", EditorStyles.miniButton))
                    {
                        string fileFolderWithDataPath = GetFileFolder(true);

                        if (Directory.Exists(fileFolderWithDataPath) == false)
                        {
                            Directory.CreateDirectory(fileFolderWithDataPath);
                        }

                        Actions[i].Invoke(true);
                    }
                }

                if (GUILayout.Button("Generate All"))
                {
                    GenerateAllFiles();
                }
            }
        }

        private void ChangeFileFolder(string oldValue, string newValue)
        {
            _folder.stringValue = newValue;

            foreach (string file in GeneratedFiles)
            {
                string from = $"{oldValue}/{file}";

                if (AssetDatabase.LoadAssetAtPath<MonoScript>(from) == null)
                {
                    continue;
                }

                string to = $"{GetFileFolder(newValue, false)}/{file}";
                FileUtil.MoveFileOrDirectory(from, to);
                FileUtil.MoveFileOrDirectory($"{from}.meta", $"{to}.meta");
            }

            AssetDatabase.Refresh();
        }

        private static void Append(string name, string value)
        {
            EnumBuilder.Append(ConstantsGenerator.Instance.NewLine);
            EnumBuilder.Append($"{Templates.EnumTabs}{name},");
            SwitchBuilder.Append(ConstantsGenerator.Instance.NewLine);
            SwitchBuilder.Append($"{Templates.SwitchTabs}case {Templates.EnumType}.{name}: return \"{value}\";");
        }

        private static void Append(string name, int id, string value)
        {
            EnumBuilder.Append(ConstantsGenerator.Instance.NewLine);
            EnumBuilder.Append($"{Templates.EnumTabs}{name} = {id},");
            SwitchBuilder.Append(ConstantsGenerator.Instance.NewLine);
            SwitchBuilder.Append($"{Templates.SwitchTabs}case {Templates.EnumType}.{name}: return \"{value}\";");
        }

        private static void GenerateInputsFile(bool refresh)
        {
            EnumBuilder.Clear();
            SwitchBuilder.Clear();

            var namesSet = new HashSet<string>();
            var serializedObject = new SerializedObject(AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/InputManager.asset")[0]);
            SerializedProperty axesProperty = serializedObject.FindProperty("m_Axes");

            foreach (SerializedProperty property in axesProperty)
            {
                string input = property.FindPropertyRelative("m_Name").stringValue;
                string name = PSEditorUtility.GetValidScriptName(input);

                if (namesSet.Contains(name) == false)
                {
                    namesSet.Add(name);
                    Append(name, input);
                }
            }

            GenerateFile(Types.Input, EnumBuilder.ToString(), SwitchBuilder.ToString(), refresh);
        }

        private static void GenerateLayersFile(bool refresh)
        {
            EnumBuilder.Clear();
            SwitchBuilder.Clear();

            foreach (string layer in UnityEditorInternal.InternalEditorUtility.layers)
            {
                Append(PSEditorUtility.GetValidScriptName(layer), LayerMask.NameToLayer(layer), layer);
            }

            GenerateFile(Types.Layer, EnumBuilder.ToString(), SwitchBuilder.ToString(), refresh);
        }

        private static void GenerateResourcesFile(bool refresh)
        {
            EnumBuilder.Clear();
            SwitchBuilder.Clear();

            string[] directories = Directory.GetDirectories(Application.dataPath, "Resources", SearchOption.AllDirectories);

            if (directories.Length > 0)
            {
                var names = new List<string>();
                var paths = new List<string>();
                var pathsWithExtension = new List<string>();
                var duplicates = new List<int>();

                foreach (string directory in directories)
                {
                    string projectRelativeDirectory = directory.Replace(Application.dataPath, "Assets").Replace('\\', '/');

                    bool skip = false;

                    foreach (string folder in ConstantsGenerator.Instance.ResourcesIgnoredFolders)
                    {
                        if (string.IsNullOrEmpty(folder))
                        {
                            continue;
                        }

                        if (projectRelativeDirectory.Contains($"/{folder}/"))
                        {
                            skip = true;

                            break;
                        }
                    }

                    if (skip)
                    {
                        continue;
                    }

                    string[] files = Directory.GetFiles(directory, "*", SearchOption.AllDirectories);

                    foreach (string file in files)
                    {
                        string projectRelativeFile = file.Replace(Application.dataPath, "Assets").Replace('\\', '/');

                        if (AssetDatabase.LoadMainAssetAtPath(projectRelativeFile) == null)
                        {
                            continue;
                        }

                        string[] parts = projectRelativeFile.Split(ResourcesSeparator, System.StringSplitOptions.RemoveEmptyEntries);

                        if (parts.Length < 2)
                        {
                            continue;
                        }

                        string pathWithExtension = parts[Mathf.Min(parts.Length - 1, 1)];
                        string name = Path.GetFileNameWithoutExtension(pathWithExtension);
                        string path = pathWithExtension.Replace(Path.GetFileName(pathWithExtension), name);

                        if (names.Contains(name))
                        {
                            if (pathsWithExtension.Contains(pathWithExtension))
                            {
                                continue;
                            }

                            if (paths.Contains(path))
                            {
                                continue;
                            }

                            if (duplicates.Count == 0)
                            {
                                duplicates.Add(names.IndexOf(name));
                            }

                            duplicates.Add(names.Count);
                        }

                        names.Add(name);
                        paths.Add(path);
                        pathsWithExtension.Add(pathWithExtension);
                    }
                }

                for (int i = 0; i < duplicates.Count; i++)
                {
                    names[duplicates[i]] = paths[duplicates[i]];
                }

                int length = Mathf.Min(names.Count, paths.Count);

                for (int i = 0; i < length; i++)
                {
                    Append(PSEditorUtility.GetValidScriptName(names[i]), paths[i]);
                }
            }

            GenerateFile(Types.Resource, EnumBuilder.ToString(), SwitchBuilder.ToString(), refresh);
        }

        private static void GenerateScenesFile(bool refresh)
        {
            EnumBuilder.Clear();
            SwitchBuilder.Clear();

            EditorBuildSettingsScene[] scenes = EditorBuildSettings.scenes;

            if (scenes != null && scenes.Length > 0)
            {
                var names = new List<string>();
                var paths = new List<string>();
                var duplicates = new List<int>();

                for (int i = 0; i < scenes.Length; i++)
                {
                    if (scenes[i].enabled)
                    {
                        string path = scenes[i].path;

                        if (path.Length <= "Assets/".Length + ".unity".Length)
                        {
                            continue;
                        }

                        string name = Path.GetFileNameWithoutExtension(path);

                        path = path.Remove(0, "Assets/".Length);
                        path = path.Remove(path.Length - ".unity".Length);

                        if (names.Contains(name))
                        {
                            if (duplicates.Count == 0)
                            {
                                duplicates.Add(names.IndexOf(name));
                            }

                            duplicates.Add(names.Count);
                        }

                        names.Add(name);
                        paths.Add(path);
                    }
                }

                for (int i = 0; i < duplicates.Count; i++)
                {
                    names[duplicates[i]] = paths[duplicates[i]];
                }

                for (int i = 0; i < paths.Count; i++)
                {
                    string name = PSEditorUtility.GetValidScriptName(names[i]);
                    Append(name, i, paths[i]);
                }
            }

            GenerateFile(Types.Scene, EnumBuilder.ToString(), SwitchBuilder.ToString(), refresh);
        }

        private static void GenerateSortingLayersFile(bool refresh)
        {
            EnumBuilder.Clear();
            SwitchBuilder.Clear();

            foreach (SortingLayer layer in SortingLayer.layers)
            {
                Append(PSEditorUtility.GetValidScriptName(layer.name), layer.value, layer.name);
            }

            GenerateFile(Types.SortingLayer, EnumBuilder.ToString(), SwitchBuilder.ToString(), refresh);
        }

        private static void GenerateTagsFile(bool refresh)
        {
            EnumBuilder.Clear();
            SwitchBuilder.Clear();

            foreach (string tag in UnityEditorInternal.InternalEditorUtility.tags)
            {
                Append(PSEditorUtility.GetValidScriptName(tag), tag);
            }

            GenerateFile(Types.Tag, EnumBuilder.ToString(), SwitchBuilder.ToString(), refresh);
        }

        private static void GenerateFile(string typeContent, string enumContent, string switchContent, bool refresh)
        {
            bool hasNamespace = !string.IsNullOrEmpty(ConstantsGenerator.Instance.Namespace);
            string content = hasNamespace ? Templates.WithNamespace : Templates.Default;
            content = content.Replace(Tags.Switch, switchContent);
            content = content.Replace(Tags.Enum, enumContent);
            content = content.Replace(Tags.Namespace, ConstantsGenerator.Instance.Namespace);
            content = content.Replace(Tags.NamespaceTab, hasNamespace ? Tab : "");
            content = content.Replace(Tags.Type, typeContent);

            using (var writer = new StreamWriter(Path.Combine(GetFileFolder(true), string.Format(FileFormat, typeContent))))
            {
                writer.Write(content.Replace(Tags.Newline, ConstantsGenerator.Instance.NewLine));
            }

            if (refresh)
            {
                AssetDatabase.Refresh();
            }
        }

        private static string GetFileFolder(bool withDataPath)
        {
            return GetFileFolder(ConstantsGenerator.Instance.Folder, withDataPath);
        }

        private static string GetFileFolder(string fileFolder, bool withDataPath)
        {
            if (string.IsNullOrEmpty(fileFolder))
            {
                fileFolder = $"{PSUtility.GeneratedPath}ConstantsGenerator";
            }

            return withDataPath ? $"{Application.dataPath}{fileFolder.Remove(0, "Assets".Length)}" : fileFolder;
        }

        private static void HandleResourcesIgnoredFoldersDrawElement(ReorderableList list, Rect position, SerializedProperty element, GUIContent label, bool selected, bool focused)
        {
            using (var scope = new EditorGUI.ChangeCheckScope())
            {
                EditorGUI.PropertyField(position, element, label, true);

                if (scope.changed)
                {
                    element.stringValue = PSEditorUtility.GetValidScriptName(element.stringValue);
                }
            }
        }
    }
}
