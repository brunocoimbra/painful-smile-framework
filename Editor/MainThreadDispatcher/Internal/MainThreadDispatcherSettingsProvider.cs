﻿using UnityEditor;
using UnityEngine;
#if UNITY_2019_1_OR_NEWER
using UnityEngine.UIElements;
#else
using UnityEngine.Experimental.UIElements;
#endif

namespace PainfulSmile
{
    internal sealed class MainThreadDispatcherSettingsProvider : SettingsProvider
    {
        private struct Styles
        {
            public static readonly GUIContent MillisecondsPerFrame = new GUIContent("Milliseconds Per Frame", "Max milliseconds per frame that the MainThreadDispatcher will expend.");
        }

        private SerializedObject _serializedObject;
        private SerializedProperty _millisecondsPerFrame;

        public MainThreadDispatcherSettingsProvider(string path, SettingsScope scope = SettingsScope.Project)
            : base(path, scope) { }

        [SettingsProvider]
        public static SettingsProvider CreateMainThreadDispatcherSettingsProvider()
        {
            return new MainThreadDispatcherSettingsProvider("Project/MainThread Dispatcher")
            {
                keywords = GetSearchKeywordsFromGUIContentProperties<Styles>()
            };
        }

        public override void OnActivate(string searchContext, VisualElement rootElement)
        {
            _serializedObject = new SerializedObject(MainThreadDispatcherSettings.GetInstance());
            _millisecondsPerFrame = _serializedObject.FindProperty("m_MillisecondsPerFrame");
        }

        public override void OnGUI(string searchContext)
        {
            _serializedObject.Update();

            using (new HierarchyModeScope(true))
            using (var scope = new EditorGUI.ChangeCheckScope())
            {
                using (new EditorGUI.DisabledScope(EditorApplication.isPlaying))
                {
                    EditorGUILayout.PropertyField(_millisecondsPerFrame, Styles.MillisecondsPerFrame);
                }

                if (scope.changed)
                {
                    _serializedObject.ApplyModifiedProperties();
                }
            }
        }
    }
}
