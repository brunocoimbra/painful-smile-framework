﻿using UnityEditor;
using UnityEngine;

namespace PainfulSmile
{
    [CustomEditor(typeof(MainThreadDispatcher))]
    internal sealed class MainThreadDispatcherEditor : Editor
    {
        private SerializedProperty _commandsCount;

        private void OnEnable()
        {
            _commandsCount = serializedObject.FindProperty("m_CommandsCount");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            using (var scope = new EditorGUI.ChangeCheckScope())
            {
                EditorGUILayout.PropertyField(_commandsCount);

                if (scope.changed)
                {
                    serializedObject.ApplyModifiedProperties();
                }
            }
        }
    }
}
