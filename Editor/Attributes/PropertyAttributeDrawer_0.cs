﻿using UnityEditor;

namespace PainfulSmile
{
    [CustomPropertyDrawer(typeof(PropertyAttributeBase), true)]
    public sealed class PropertyAttributeDrawer : PropertyAttributeDrawer<PropertyAttributeBase> { }
}
